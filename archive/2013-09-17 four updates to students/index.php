<?php 

include('EDIT_ME_IN_NOTEPAD_TO_CUSTOMIZE_EMAIL.php');

// do not edit below this line -------------------------------------

# Install PSR-0-compatible class autoloader
spl_autoload_register(function($class){
	require 'libs' . DIRECTORY_SEPARATOR . preg_replace('{\\\\|_(?!.*\\\\)}', DIRECTORY_SEPARATOR, ltrim($class, '\\')).'.php';
});

	# Get Markdown class
	use \Michelf\Markdown;
	
	# Get CSS style inliner class
	use \TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;
	
	// TODO: add scss compiler http://leafo.net/scssphp/docs/
	
$css_filename = 'templates/' . $template_name . '.css';
$template_filename = 'templates/' . $template_name . '-template.php';
$template = (file_exists($template_filename)!==FALSE) ? file_get_contents($template_filename) : '';
$css = (file_exists($css_filename)!==FALSE) ? file_get_contents($css_filename) : '';

//$google_analytics_url_suffix = '?utm_source=blogpost-2013-08&utm_medium=email&utm_campaign=Forum+on+Healthcare+Innovation';

$ga = array();
$ga[] = isset($ga_source) && strlen($ga_source) ? 'utm_source=' . urlencode($ga_source) : '';
$ga[] = isset($ga_medium) && strlen($ga_medium) ? '&utm_medium=' . urlencode($ga_medium) : '';
$ga[] = isset($ga_campaign) && strlen($ga_campaign) ? '&utm_campaign=' . urlencode($ga_campaign) : '';
$ga[] = isset($ga_name) && strlen($ga_name) ? '&utm_name=' . urlencode($ga_name) : '';
$ga[] = isset($ga_content) && strlen($ga_content) ? '&utm_content=' . urlencode($ga_content) : '';

$google_analytics_url_suffix = implode($ga);
$google_analytics_url_suffix = $use_google_analytics ? $google_analytics_url_suffix : '';
$main_link_url = (stripos($main_link_url, $ga_replace)!==FALSE) ? str_replace($ga_replace, $google_analytics_url_suffix, $main_link_url) : $main_link_url ;

function get_color($color_name) {
	global $possible_colors;
	$color_name = ($color_name === null) ? "white" : $color_name ;
	if( isset($possible_colors[$color_name]) && strlen($possible_colors[$color_name]) > 0) {
		echo $possible_colors[$color_name];
	} // TODO: handle case where color name isn't found
}

function email_body_html() {
	global $body_text; 
	global $links;
	global $google_analytics_url_suffix;
	global $ga_replace;
	global $css;
	//$body_text = file_get_contents("body text.txt");

	# Read file and pass content through the Markdown praser
	$body_text = file_get_contents('body text.txt');
	
	$body_text = Markdown::defaultTransform($body_text);

	
			
	// todo - only replace first instance of text http://stackoverflow.com/questions/1252693/php-str-replace-that-only-acts-on-the-first-match
	// $suffix = (isset($google_analytics_url_suffix) && strlen($google_analytics_url_suffix) > 0 ) ? $google_analytics_url_suffix : '';
	
	foreach ($links as $linktext => $url) {
		if(isset($google_analytics_url_suffix) && strlen($google_analytics_url_suffix) > 0 && (strpos($url, $ga_replace)!== false)) { 
			$url = str_replace($ga_replace, $google_analytics_url_suffix, $url);
		}
		$body_text = str_replace($linktext, get_link($url , $linktext),$body_text);
	}
	$contains_paras = stripos($body_text, '<p>') === false;
	$body_text = (! $contains_paras ) ? $body_text :  str_replace("\n", "</p>\n<p>", '<p>'.$body_text.'</p>');
	$body_text = style($body_text);
	echo $body_text;
	//echo nl2br(style($body_text));
}

function get_link($href, $text) {
	return "<a href='$href'>$text</a>";
}

function style($bodytext){
	global $highlight_text_color;
	global $heading_text_color;
	global $possible_colors;
	global $default_text_color;
	$heading_text_color = strlen($heading_text_color) > 0 ? $heading_text_color : $highlight_text_color;
	$highlight_text_colorstr = $possible_colors[$highlight_text_color];
	$heading_text_colorstr = $possible_colors[$heading_text_color];
	$searchReplaceArray = array();
	$searchReplaceArray["<strong>"] 
		= "<strong style='color:" . $highlight_text_colorstr . "; font-size:18px; font-weight:bold;font-family:Arial;'>";
	$searchReplaceArray["<h1>"] 
		= "<h1 style='color:" . $heading_text_colorstr . "; font-size:25px; font-weight:bold;margin-bottom:0px;'>";
	$searchReplaceArray["<h2>"] 
		= "<h2 style='color:" . $heading_text_colorstr . "; font-size:1.8em; font-weight:bold;margin-bottom:0px;'>";
	$searchReplaceArray["<a href"] 
		= "<a style='color:" . $highlight_text_colorstr . "; font-weight:bold;' href";
	$searchReplaceArray['<p>']
	 	= '<p style="line-height: 18px; color: ' . $possible_colors[$default_text_color] . '; font-size: 13px"><span style="font-family:Arial">';
	$searchReplaceArray["<p class='small'>"]
	 	= '<p style="line-height: 10px; color: ' . $possible_colors[$default_text_color] . '; font-size: 8px"><span style="font-family:Arial">';
	$searchReplaceArray['</p>']
		= '</span></p>';
	return str_replace(array_keys($searchReplaceArray), array_values($searchReplaceArray), $bodytext);
}

function get_header_image($image_name = null) {
	global $possible_header_images;
	global $header_image_name;
	$image_name = ($image_name === null) ? $header_image_name : $image_name ;
	if( isset($possible_header_images[$image_name]) && strlen($possible_header_images[$image_name]) > 0) {
		echo $possible_header_images[$image_name];
	} // TODO: handle case where image name isn't found
}

// TODO: make single 'get image' function
function get_department_info($info) {
	global $possible_departments;
	global $department;
	global $google_analytics_url_suffix;
	global $ga_replace;
	$info = ($info === null) ? $department : $info ;
	if( isset($possible_departments[$department][$info]) && strlen($possible_departments[$department][$info]) > 0) {
			$url = $possible_departments[$department][$info];	
		if(($info === 'url') && isset($google_analytics_url_suffix) && (strpos($url, $ga_replace)!== false)) {
			 
			echo str_replace($ga_replace, $google_analytics_url_suffix, $url);
		} else {
			echo $possible_departments[$department][$info];
		}
	} // TODO: handle case where image name isn't found
}

function get_author_info($info) {
	global $possible_authors;
	global $author;
	global $google_analytics_url_suffix;
	global $ga_replace;
	$suffix = (isset($google_analytics_url_suffix) && strlen($google_analytics_url_suffix) > 0 ) ? $google_analytics_url_suffix : '';
	
	$info = ($info === null) ? $author : $info ;
	if( isset($possible_authors[$author][$info]) && strlen($possible_authors[$author][$info]) > 0) {
		$url = $possible_authors[$author][$info];	
		if(($info === 'url') && isset($google_analytics_url_suffix) && (strpos($url, $ga_replace)!== false)) {
			 
			echo str_replace($ga_replace, $google_analytics_url_suffix, $url);
		} else {
		echo $possible_authors[$author][$info];
			}
	} // TODO: handle case where image name isn't found
}

function get_linkedin_share() {
	global	$google_analytics_url_suffix;
	global $post_url;
	global $post_title;
	global $post_summary;
	$ptitle = urlencode($post_title);
	$psummary = urlencode($post_summary);
	$purl = urlencode($post_url . $google_analytics_url_suffix);
	$url = 'http://www.linkedin.com/shareArticle?mini=true&';
	$url .= 'url=' . $purl . '&';
	$url .= 'title=' . $ptitle . '&';
	$url .= 'summary=' . $psummary;

?>
<a 	target="_blank" xt="SPCLICK"
href="<?php echo $url; ?>"
name="linkedin_img"> <span style="font-family: Arial"><img
id="social_network_image" name="Cont_0" alt=""
style="border-bottom: medium none; border-left: medium none; padding-bottom: 3px; padding-left: 0px; padding-right: 0px; vertical-align: middle; border-top: medium none; border-right: medium none; padding-top: 0px"
src="http://projects.iq.harvard.edu/files/styles/os_files_small/public/forum-on-healthcare-innovation/files/sn_linkedin.gif" /></span></a>

&nbsp;

<a 	target="_blank" xt="SPCLICK"
style="font-size: 12px"
href="<?php echo $url; ?>"
name="linkedin_1"> <span style="font-size: 12px"><span style="font-family: Arial">LinkedIn</span></span> </a><!-- /linkedin text button --><?php
}

function get_twitter_share() {
global	$google_analytics_url_suffix;
global $post_url;
global $post_title;
global $post_tweet;
$psummary = urlencode($post_tweet . ' ' . $post_url . $google_analytics_url_suffix);
$url = 'https://twitter.com/intent/tweet?';
$url .= 'text=' . $psummary;
?>
<a 	target="_blank" xt="SPCLICK"
href="<?php echo $url ?>"
name="twitter_img"> <span style="font-family: Arial"><img 	id="social_network_image"
name="Cont_1"
alt="" style="border-bottom: medium none; border-left: medium none; padding-bottom: 3px; padding-left: 0px; padding-right: 0px; vertical-align: middle; border-top: medium none; border-right: medium none; padding-top: 0px"
src="http://projects.iq.harvard.edu/files/styles/os_files_small/public/forum-on-healthcare-innovation/files/sn_twittert.gif" /></span></a><!-- /twitter image button -->
&nbsp;
<!-- twitter text button -->
<a 	target="_blank" xt="SPCLICK" style="font-size: 12px"
href="<?php echo $url ?>"
name="twitter_1"> <span style="font-size: 12px"><span style="font-family: Arial">Twitter</span></span> </a><!-- /twitter text button --><?php
}

function get_comment_bar() { global $main_link_url;
?>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td style="padding: 0 0 0 0;" align="center">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#b1233b" style="padding: 12px 18px 12px 18px; -webkit-border-radius:3px; border-radius:3px"><a href="<?php echo $main_link_url . '#disqus_thread'; ?>" target="_blank" style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none;"><b>Comment</b></a></td>
</tr>
</table>
</td>
<td style="text-align: center;padding-top:.7em;">

<?php get_linkedin_share(); ?>

</td><!-- /linkedin buttons -->

<!-- twitter buttons -->
<td style="text-align: center;padding-top:.7em;">
<?php get_twitter_share(); ?>
</td><!-- /twitter buttons -->
</tr>
</table><?php
}


ob_start();
include($template_filename);
$html = ob_get_clean();
$cssToInlineTransformer = new CssToInlineStyles($html, $css);
$html = $cssToInlineTransformer->convert();

echo $html;


Dear HBS alumni working in the health care industry:

## Are you willing to help a student by sharing your health care industry expertise?

HBS is seeking alumni working in all sectors of the health care industry to mentor current MBA students. Survey results indicate that mentoring is an extremely rewarding experience for alumni and is highly valued by students.

**[To sign up to be a mentor, please complete this brief survey by Friday, September 27th.][]**

The mentor program requires a minimal time commitment on your part - just a few emails or phone conversations over the course of the year. Please consider volunteering.

Thank you,	
<small>
Richard Hamermesh, HBS Health Care Initiative Faculty Co-Chair  
Robert Huckman, HBS Health Care Initiative Faculty Co-Chair  
Cara Sterling, Director, HBS Health Care Initiative Director  
</small>

[To sign up to be a mentor, please complete this brief survey by Friday, September 27th.]: <?php echo $survey_link; ?>
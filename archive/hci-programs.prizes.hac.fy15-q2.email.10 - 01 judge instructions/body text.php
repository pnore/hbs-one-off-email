<?php 
// MAILING INFO
$title = "Instructions to Judges";
$campaignName = "hci-programs.prizes.hac.fy15-q2.email.10 - 01 judge instructions";
$campaignUrl = "https://na15.salesforce.com/701i0000000suGPAAY";
$from = $caraFrom;
$sig = $caraSig;
$subject = "Instructions for Judging the Health Acceleration Challenge";
$containsFirstName = true;
$baseLink = 'https://getfeedback.com/r/9jeRmlY6';
$baseLinkName = "getFeedback_hac_judging";
$relationalTableQuery = "/Shared/hac judging 6 query";

// NUMBERS
$number_of_applications = "187";
$number_participators = "940";



/**************  BEGIN EMAIL ////////////////

# <?php echo $title ?>  
**Campaign:** <?php echo $campaignName ?>       
**Campaign Link:** <?php echo $campaignUrl ?>  
**From:** <?php echo $from; ?>
**Subject:** <?php echo $subject; ?> 
*/
?>

## Thank you for judging the Health Acceleration Challenge!

Please review the applications assigned to you by **Monday, October 13.**

As you review the applications, please remember this is a &ldquo;scale up&rdquo; competition, not a business plan competition. We are looking for care delivery innovations that are proven, transferrable, and adaptable. Specifically, they must meet 3 criteria:

*   **Impact** -- significant positive value for the US health care system
*   **Evidence** -- proof that the innovation works
*   **Dissemination** -- a strong plan to overcome barriers to adoption

You are encouraged to consult external resources (e.g. the innovator's website) to help you evaluate the application. For more information on what to expect of the evaluation process, watch this short [video](http://youtu.be/92p_XgpSDFo).

You have been assigned to judge the following applications: 

<?php 
beginTable( "$relationalTableQuery", "10", "leftright", "%%Judge_App_Num%%");

left( "%%Judge_App_Num%%" );
// note, somehow this gets translated from & to &amp; and it messes the merge up. you have to manually fix it!
right( "<a xt='SPNOTRACK' name='hac_app_%%Judge_App_Num%%' href='%%App_URL%%' target='_blank'>%%App_Name_Short%%</a>; Evaluation link is <a xt='SPNOTRACK' name='getFeedback_hac_judging_%%Judge_App_Num%%' href='$baseLink?ContactID=%%SFID%%%26Application_URL=%%App_URL%%%26Last_Name=%%Last Name%%%26First_Name=%%First Name%%%26Application_Name=%%App_Name%%%26App_Name_Short=%%App_Name_Short%%%26App_ID=%%App_ID%%'>here</a> for this application.");

endTable();
?>

To find out more about the challenge, please visit [www.HealthAccelerationChallenge.com].

Thank you, 

<?php echo $sig; ?>



<?php // LINKS FILLED IN BY MARKDOWN ?>
[Boston Magazine]: http://www.bostonmagazine.com/health/blog/2014/08/25/harvard-health-acceleration-challenge/
[Forbes]: http://www.forbes.com/sites/hbsworkingknowledge/2014/08/18/harvard-wants-your-proven-ideas-to-improve-health-care/
[Health Acceleration Challenge]: http://forumonhealthcareinnovation.org/?utm_campaign=hac_launch&utm_source=hci-programs.prizes.hac.fhi15-q1.email.09_-_final_reminder_to_most_groups&utm_medium=social
[www.HealthAccelerationChallenge.com]: http://forumonhealthcareinnovation.org/?utm_campaign=hac_launch&utm_source=hci-programs.prizes.hac.fhi15-q1.email.09_-_final_reminder_to_most_groups&utm_medium=social
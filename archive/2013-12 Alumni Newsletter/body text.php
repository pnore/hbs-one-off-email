<?php section('Current News and Upcoming Events in Health Care'); ?>

## Alumni Reception at JP Morgan Conference 


<?php
beginTable();
left("Join more than 100 fellow HBS alumni in San Francisco on January 12, 2014, from 7:30 p.m.--9:30 p.m at the Bluestem Brasserie.  Kick off the start of the 32nd annual JP Morgan Health Care Conference with cocktails and networking.  Cost is $25.  
[RSVP](http://www.hbshealthalumni.org/article.html?aid=797) is required.  
" . button('RSVP', 'http://www.hbshealthalumni.org/article.html?aid=797'), "290px", "normal");
right('[![Bluestem Brasserie Image][]](http://www.hbshealthalumni.org/article.html?aid=797)', "282px");

endTable();
?>  

## Health Care Featured in [December Alumni Bulletin]  
The December issue of the *HBS Alumni Bulletin* focuses on health care. Read about current faculty research and what health care alumni "prescribe" for the future of health care.    

<?php 
beginTable();
left("
*   [Your Own Medicine][]  
Re: Gene Williams (MBA 1987)", "50%", "normal");

right("
*   [Prescription: Free the Data!][]  
Re: Jonathan Bush (MBA 1997)", "50%", "normal");

left("
*   [Prescription: Integrate Preventive Care and Payment][]  
Re: Seth Blackley (MBA 2007),  
David Bradley (MBA 1977)  
&amp; Frank Williams (MBA 1995)", "50%", "normal");
 
right("
*   [Prescription: Measure Health Care's Real Costs][]  
Re: Bob Kaplan, Michael E. Porter & Sachin Jain (MBA 2007) ", "50%", "normal");

left("
*   [Prescription: Make Medicine Personal][]  
Re: Gregory Stock (MBA 1987)", "50%", "normal");  

right("
*   [Prescription: Leverage Human Nature][] 
Re: Nava Ashraf", "50%", "normal");

left("
*   [Prescription: Build a Killer App][]  
Re: Nate Gross (MBA 2011)  
&amp; Halle Tecco (MBA 2011)", "50%", "normal");

right();
endTable();
?>

## Health Care Events  
<?php 
beginTable();
left("Thu Jan 09  
 <span class='time'>6:00 p.m.  -- 8:30 p.m.</span>   
 <span class='place'>Boston</span>");
right("[Innovation in Health Care Dinner: Investor &amp; Entrepreneur Perspectives]  
Join us for a discussion on both the investor and entrepreneur perspectives with our special guests: Bob Higgins, Founding Partner and Co-Founder, Highland Capital Partners; and Graham Gardner, MD, MBA, Co-Founder &amp; Chief Executive Officer, Kyruus. Mr. Higgins and Dr. Graham will talk about entrepreneurship in the health care sector, with topics ranging from current trends and emerging business models to opportunities for innovation and timing around starting a company. Registration is required. Hosted by  HBS Alumni Angels and HBS Healthcare Alumni Association (HAA). *Open to HBS HAA members*.");

left("Sun Jan 12  
 <span class='time'>7:00 p.m.  -- 9:30 p.m.</span>   
 <span class='place'>Bluestem Brasserie  
 1 Yerba Buena Lane  
 San Francisco</span>");
right("[JP Morgan Health Care Conference Reception]  
Meet HBS alumni who are in San Francisco for the 32nd annual JP Morgan Health Care Conference for cocktails and hors d'oeuvres at Bluestem Brasserie. Last year, this event sold out quickly with more than 150 alumni attending. Registration is required. Hosted by  the HBS Health Care Initiative and the HBS Healthcare Alumni Association. *Open to HBS alumni, faculty, and students*.");

left("Wed Jan 22  
 <span class='time'>6:30 p.m.  -- 8:00 p.m.</span>   
 <span class='place'>HBS Campus  
 Hawes 101
 Boston</span>");
right("[Crowdfunding Health Care: Secrets from Industry Insiders]  
Hear from industry practitioners on how crowdfunding benefits both health care companies and investors. Moderated by Daniel Gorfine, Director, Financial Markets Policy and Legal Counsel, Milken Institute. Panelists include Andrew Farquharson (MBA 1999), Managing Director, InCube Ventures; Scott Jordan, Founder, HealthiosXchange; Howard Leonhardt, Inventor and Serial Entrepreneur; Greg Simon, Chief Executive Officer, Poliwogg; and Sean Schantzen, JD, Co-Founder & COO, Healthfundr.  Registration is required. $30 for members; $60 for non-members. Hosted by  the HBS Healthcare Club. *Open to the public*.");

left("Wed Jan 29  
 <span class='time'>6:00 p.m.  -- 8:30 p.m.</span>   
 <span class='place'>Raven Bar  
 1151 Folsom St.  
 San Francisco</span>");
right("[San Francisco Networking Event for HBS Health Care Alumni]  
If you are in the Bay Area, please join members of the HBS Healthcare Alumni Association for an evening of networking and socializing. Please contact [Lauren Pflepsen] or [Elle Pishny], Co-Regional Directors for the Bay Area with any questions. There is no fee to attend, and a cash bar will be available. However, RSVP is appreciated. Sponsored by  the HBS Healthcare Club. *Open to HBS alumni*.");

left("Wed Feb 05  
 <span class='time'>6:30 p.m.  -- 9:30 p.m.</span>   
 <span class='place'>New York</span>");
right("[New York Networking Event: HBS--Wharton--Columbia]  
If you are in the New York City area, please join HBS, Wharton, and Columbia Health Care Alumni for a joint evening of networking and socializing. There is no fee for admission and a cash bar will be available. Registration is required. *Open to the HBS, Wharton, and Columbia networks*.");

left("Sat Feb 08  
 <span class='time'>8:00 a.m. -- 6:30 p.m.</span>   
 <span class='place'>HBS Campus  
 Boston</span>");
right("[HBS Health Care Club Conference 2014]  
The keynotes for this year's Health Care Conference have not been announced yet, but the event will feature panels on consumer health, devices, emerging markets, health IT, health care financing, payer/provider, the Affordable Care Act, and pharma/biotech. Hosted by the Harvard Business School Health Care Club. *Open to the public*.");

endTable();
?>

  
<?php 
beginTable();
left("## Seeking Alumni Applicants: Blavatnik Fellowship in Life Science Entrepreneurship", "390px", "normal");
right("[![blavatnik logo][]](http://www.biomedicalaccelerator.org/fellowship/)", "171px", "normal");
endTable();
?>
The Blavatnik Fellowship in Life Science Entrepreneurship is a one-year fellowship for HBS alumni who have earned their  MBA in the last seven years. Blavatnik Fellows are hired to foster entrepreneurship and commercialization of biomedical innovation from laboratories throughout Harvard University. Fellows work in the vibrant Harvard Innovation Lab community on the HBS campus and receive a $95,000 yearly stipend with extra resources for due diligence, market research, and licensing options. To [apply to the program](http://www.biomedicalaccelerator.org/fellowship/), submit a completed application to blavatnikfellowship@hbs.edu by February 14, 2014.  

Read about current Fellows and their projects below: 
<?php section('Health Care Features'); ?>

## Blavatnik Fellow Interviews

### Daniel Oliver, MBA 2013

<?php 
beginTable();
left('![Daniel Oliver][]');
right(
    "#### Now that you have been a fellow for three months, can you describe your experience?
The fellowship is a wonderful experience and has exceeded all my expectations. Professor Vicki Sato and her team do a magnificent job and have set us up to succeed. They provide unrivaled access to potentially transformative technologies, connect us with the brilliant minds behind them, and then mentor us on ways to bring them to market.  It's everything I came to business school for and more. ");
left();
right(button("Full Interview >>", "http://healthcareinitiative.mkt1960.com/alumni_newsletter_2013-12_blavatnik_see_more"));
endTable();
 /*
#### How did you determine which project you were going to select?  
I was looking for a project that would expose me to the rigors of commercializing breakthrough, disruptive technology.  I found just that working with Dr. Jennifer Lewis on commercializing her novel 3D printing projects. Jennifer's work has a multitude of applications and a high degree of commercial readiness. She is enabling the next generation of wearable technology and laying the foundation for human tissue replacement.  

#### Have the networks that the fellowship implemented into the program fulfilled your expectations?  
Absolutely.  From Vicki to Professor Joe Lassiter, to successful entrepreneur Jonathan Solomon (to name just a few),  they just couldn't be better. I knew that I would associate with people who achieved success commercializing projects in the life sciences, but I did not foresee the time and effort they put into working on my behalf and the favors they call in from their networks. I am eternally grateful for what they do, and I am trying my best to pay them back by making the fellowship a success.  

#### Can you sum up your experience of being a Blavatnik Fellow?
The Blavatnik Fellowship is about working with extraordinarily accomplished people who are passionate about taking groundbreaking technology from the lab to market. I get to seize each day with some of the most remarkable people I'll ever have the pleasure of meeting.

*Daniel received a BS in Mechanical Engineering from the  California
Institute of Technology (2007) and an MBA from  Harvard Business School
(2013).*
*/
?>

### Steve Porter, MBA 2011
<?php 
beginTable();
left('![Steve Porter][]');
right("####  Now that you have been a fellow for three months, can you describe your experience?  
It's been an incredible ride!   During the search and selection process, I had the opportunity to meet with a great number of investigators---upwards of 20 researchers in various schools and departments across the University---each of whom is engaged in interesting and meaningful work.  The hardest part has been deciding where to focus!  Everyone---from our program administrator, Laura Kitson, to our Faculty Lead, Professor Vicki Sato, to the staff of Harvard's Office of Technology Development, to our incredible network of mentors---has been unfalteringly supportive and committed to helping us realize our individual goals. And my co-fellows are a fantastic group---bright, collegial, and, of course, super fun.  We each came into the experience with different backgrounds and interests, and I think we've all learned a great deal from one another.
    ");
    left();
right(button("Full Interview >>", "http://healthcareinitiative.mkt1960.com/alumni_newsletter_2013-12_blavatnik_see_more"));
endTable();

/*
####  How did you determine which project you were going to select?  
I framed my initial search around some basic filtering criteria.  I was hoping to find something exciting, in an attractive market, whose science I could engage with and that had the potential to achieve a significant health impact.  I am a graduate of the MD/MBA program and, as such, was interested in a project that would leverage my clinical interests in infectious diseases and reproductive health.  Lastly, having spent a significant portion of my career to date in South Africa, I was drawn to technologies that would have applications in global health -- either by addressing the burdens of disease found in low-resource settings or whose cost structure would enable sustainable delivery in those settings.

#### Have the networks in the fellowship implemented into the program fulfilled your expectations?  
The mentors have been absolutely invaluable.  These men and women -- who are a veritable "Who's Who" in the industry -- have taken time out of their busy schedules to get to know us, learn about the technologies we are considering, and offer tactical advice or strategic introductions to help advance our projects.  It's also been fun learning from their stories.  Each mentor has had great successes, but many have taken unpredictable twists and turns to get to where they are today.  It's been fascinating to hear them reflect on their paths and offer their thoughts on the future of the industry.

#### Can you sum up your experience of being a Blavatnik Fellow?
The Blavatnik Fellowship offers an unparalleled opportunity to learn and work at the intersection of academic technology transfer, life sciences technology, and enterprise development.  With the resources of the iLab, the science of Harvard, and the network of Boston-based biotech and venture capital executives, I am fortunate to be situated in the richest ecosystem imaginable for pursuing this kind of work.

*Steve received a BA from Princeton University (2004), an MD with honors
from Harvard Medical School (2011), and an MBA from Harvard Business
School (2011).*
*/?>
## MBA Health Care Career Days Summaries
Each year, the Health Care Initiative and the office of MBA Career &amp; Professional Development co-sponsor Health Care Career Days.  This past October, Career Days included Health Care 101 for students to learn more about the health care industry as a whole, as well as health care sector panel discussions with recent alumni.  The panel's topics included life sciences, health care finance, devices and diagnostics, and care delivery.  [A set of documents have been produced from the content of the panels] and will be available to students to serve as a career resource guide for these particular health care sectors.   

## HBS Health Care Alumni Association Conference a Success
This year's annual HBS Healthcare Alumni Association Conference was held November 7--8, and drew a crowd of over 300 HBS alumni from across the country and around the globe.  The lineup of speakers included prominent health care executives and alumni, such as Kent Thiry, DaVita HealthCare Partners; Scott Huennekens, Volcano Corporation; Don Berwick, former Administrator for Centers for Medicare and Medicaid Services; and the Honorable Ben Sasse, former U.S. Assistant Secretary for the Department of Health & Human Services.  There were also several breakout sessions focused on the pharma, services, IT, and emerging markets sectors of health care.  forty current HBS students also attended to learn and network with alumni.  Next year's conference will be held November 6--7, 2014.  Kudos to the HBS HAA team for a great conference!       

## Student Alumni Mentor Program Matches 234 Pairs
<?php 
beginTable();
left("![speech bubbles][]");
right("Thanks to your support, this past November 234 alumni-student pairs were matched as part of the Health Care Mentor Program! Alumni mentor volunteers: **keep an eye out for a special gift that is on its way to you**! Schedule some time to connect with your mentee during the upcoming holiday season. If your mentee has not contacted you yet, please [let us know](mailto:healthcare_initiative@hbs.edu?subject=HBS%20Health%20Care%20Mentor%20Program). ");
endTable();
?>

<?php section('Health Care Hyperlinks'); ?>

## HBR--NEJM Insight Center --- A Great Resource
The [*Harvard Business Review* -- *New England Journal of Medicine* Insight Center](http://hbr.org/special-collections/insight/leading-health-care-innovation/) recently concluded, accumulating over 70 posts by health care thought leaders. Perhaps the holiday season will present you with an opportunity to peruse this great resource and contemplate what's ahead for health care in the New Year.
 
<?php 
beginTable();
// 
// left("[![Understanding Patient Experience Photo](http://www.hbs.edu/healthcare/images/photos/understanding_the_drivers_of_the_patient_experience.PNG)](http://blogs.hbr.org/2013/09/understanding-the-drivers-of-the-patient-experience/)");
// 
// right("
// The [Harvard Business Review - New England Journal of Medicine Insight Center](http://hbr.org/special-collections/insight/leading-health-care-innovation/) recently concluded with over 70 posts by health care thought leaders. Perhaps the upcoming holiday season will present you with some opportunities to peruse this great resource and contemplate what's ahead for health care in the New Year.
//  
// ");

//left("[![The *Strategy That Will Fix Health Care](http://www.hbs.edu/healthcare/images/photos/strategy_that_will_fix_health_care_porter.PNG)](http://hbr.org/2013/10/the-strategy-that-will-fix-health-care/ar/1)");
left("[![Understanding Patient Experience Photo](http://www.hbs.edu/healthcare/images/photos/understanding_the_drivers_of_the_patient_experience.PNG)](http://blogs.hbr.org/2013/09/understanding-the-drivers-of-the-patient-experience/)");

right("### Big Ideas in Health Care
Be sure to check out the article [\"Why Health Care Is Stuck --- And How to Fix It\"](http://blogs.hbr.org/2013/09/why-health-care-is-stuck-and-how-to-fix-it/) and the webinar *[*Strategy That Will Fix Health Care](http://hbr.org/2013/10/the-strategy-that-will-fix-health-care/ar/1)* by HBS Professor [Michael E. Porter](http://www.hbs.edu/faculty/Pages/profile.aspx?facId=6532&click=bestbet) and Thomas H. Lee.

### Managing Patient Experience Innovation
Don't miss [\"Understanding the Drivers of the Patient Experience\"](http://blogs.hbr.org/2013/09/understanding-the-drivers-of-the-patient-experience/) by James I. Merlino and HBS Professor [Ananth Raman](http://www.hbs.edu/faculty/Pages/profile.aspx?facId=6534&click=bestbet).");
endTable();
?> 

## Recent HBS Faculty Publications  
"[Household Bargaining and Excess Fertility: An Experimental Study in Zambia]"  
[Nava Ashraf], Erica Field, Jean Lee  
*American Economic Review* (forthcoming)  

"[The Hidden Benefits of Keeping Teams Intact]"  
[Robert Huckman], Bradley Staats  
*Harvard Business Review* 91, no. 12 (December 2013): 27--29  

"[Lessons from England's Health Care Workforce Redesign: No Quick Fixes]"  
[Richard Bohmer], Candace Imison  
*Health Affairs* 32, No. 11 (November 2013): 2025--2031  

"[Public Reporting, Consumerism, and Patient Empowerment]"  
[Robert Huckman], Mark A. Kelley, MD  
*New England Journal of Medicine* 369, no. 20 (November 14, 2013): 1875--1877  

"[Increased Speed Equals Increased Wait: The Impact of a Reduction in Emergency Department Ultrasound Order Processing Time]"  
Jillian Berry Jaeker, [Anita Tucker], Michael H. Lee  
*Harvard Business School Working Paper*, No. 14-033, October 2013  

"[Team Scaffolds: How Minimal Team Structures Enable Role-Based Coordination]"  
Melissa A. Valentine, [Amy Edmondson]  
*Harvard Business School Working Paper*, No. 12-062, January 2012 (Revised September 2012, April 2013, October 2013)  

"[The Strategy That Will Fix Health Care]"  
[Michael E. Porter], Thomas H. Lee  
*Harvard Business Review* 91, no. 10 (October 2013): 50--70  

"[Converging to the Lowest Denominator in Physical Health]"  
[Leslie John], Michael I. Norton  
Special Issue on Health Psychology Meets Behavioral Economics. *Health Psychology* 32,  
No. 9 (September 2013): 1023--1028  

## HBS Health Care in the News  
"[The Numbers Stack up for Better, Cheaper Treatment]"  
Re: Regina Herzlinger  
*Financial Times*  
Dec 11, 2013

"[Your Own Medicine]"  
Re: Gene Williams (MBA 1987)  
*HBS Alumni Bulletin*  
Dec 6, 2013

"[Prescription: Free the Data!]"  
Re: Jonathan Bush (MBA 1997)  
*HBS Alumni Bulletin*  
Dec 6, 2013

"[Prescription: Build a Killer App]"  
Re: Nate Gross (MBA 2011) & Halle Tecco (MBA 2011)  
*HBS Alumni Bulletin*  
Dec 6, 2013

"[Prescription: Measure Health Care's Real Costs]"  
Re: Bob Kaplan, Michael E. Porter & Sachin Jain (MBA 2007)  
*HBS Alumni Bulletin*  
Dec 6, 2013

"[Prescription: Make Medicine Personal]"  
Re: Gregory Stock (MBA 1987)  
*HBS Alumni Bulletin*  
Dec 6, 2013

"[Prescription: Leverage Human Nature]"  
Re: Nava Ashraf  
*HBS Alumni Bulletin*  
Dec 6, 2013

"[Prescription: Integrate Preventive Care and Payment]"  
Re: Seth Blackley (MBA 2007), David Bradley (MBA 1977) & Frank Williams (MBA 1995)  
*HBS Alumni Bulletin*  
Dec 6, 2013

"[Encourage Breakthrough Health Care by Competing on Products Rather Than Patents]"  
by Richard Hamermesh  
*HBS Working Knowledge*  
Dec 5, 2013

"[The Mistakes Behind Healthcare.gov Are Probably Lurking in Your Company, Too]"  
by Amy Edmondson  
*HBR Blogs*  
Dec 5, 2013

"[A Mission with Precision: Kathy Giusti's Ambitious Quest to Cure Multiple Myeloma]"  
Re: Kathy Giusti (MBA 1985)  
*Forbes*  
Dec 3, 2013

"[HBS Panel Discusses Health Care]"   
*Harvard Crimson*  
Nov 7, 2013

"[What Obamacare Ignores: Cutting Health Care Costs]"  
Re: Michael E. Porter  
*Fiscal Times*  
Oct 30, 2013

*[FAS]: Harvard Faculty of Arts and Sciences
*[HLS]: Harvard Law School
*[HKS]: Harvard Kennedy School
*[HBR]: Harvard Business Review
*[NEJM]: New England Journal of Medicine
*[BAHM]: Business School Alliance for Health Management 
*[HBSHAA]: Harvard Business School Healthcare Alumni Association
*[FHI]: Forum on Healthcare Innovation
*[HMS]: Harvard Medical School

<?php // links for new issue down here ... 
?>
[Bluestem Brasserie Image]: http://www.hbs.edu/healthcare/images/photos/bluestem_282.png

<?php // in the news links ...
 
?>
[The Numbers Stack up for Better, Cheaper Treatment]: http://www.ft.com/intl/cms/s/0/84a5c49a-5771-11e3-b615-00144feabdc0.html?siteedition=intl#axzz2nB7M9U00
[Your Own Medicine]: https://www.alumni.hbs.edu/stories/Pages/story-bulletin.aspx?num=3032
[Prescription: Free the Data!]: https://www.alumni.hbs.edu/stories/Pages/story-bulletin.aspx?num=3031#prescription1
[Prescription: Build a Killer App]: https://www.alumni.hbs.edu/stories/Pages/story-bulletin.aspx?num=3031#prescription2
[Prescription: Measure Health Care's Real Costs]: https://www.alumni.hbs.edu/stories/Pages/story-bulletin.aspx?num=3031#prescription3
[Prescription: Make Medicine Personal]: https://www.alumni.hbs.edu/stories/Pages/story-bulletin.aspx?num=3031#prescription4
[Prescription: Leverage Human Nature]: https://www.alumni.hbs.edu/stories/Pages/story-bulletin.aspx?num=3031#prescription5
[Prescription: Integrate Preventive Care and Payment]: https://www.alumni.hbs.edu/stories/Pages/story-bulletin.aspx?num=3031#prescription6
[Encourage Breakthrough Health Care by Competing on Products Rather Than Patents]: http://hbswk.hbs.edu/item/7391.html
[The Mistakes Behind Healthcare.gov Are Probably Lurking in Your Company, Too]: http://blogs.hbr.org/2013/12/the-mistakes-behind-healthcare-gov-are-probably-lurking-in-your-company-too/
[A Mission with Precision: Kathy Giusti's Ambitious Quest to Cure Multiple Myeloma]: http://www.forbes.com/sites/vannale/2013/12/03/a-mission-with-precision-kathy-giustis-ambitious-quest-to-cure-multiple-myeloma/
[HBS Panel Discusses Health Care]: http://www.thecrimson.com/article/2013/11/7/hbs-panel-healthcare-kickoff/
[What Obamacare Ignores: Cutting Health Care Costs]: http://www.thefiscaltimes.com/Columns/2013/10/30/What-Obamacare-Ignores-Cutting-Health-Care-Costs

[hbr-nejm]: http://www.hbs.edu/healthcare/images/photos/hbr_nejm.png

<?php // publication authors ... 
?>
[Nava Ashraf]: http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=337273 {.pub .author}
[Robert Huckman]: http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=10653 {.pub .author}
[Richard Bohmer]: http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6424 {.pub .author}
[Robert Huckman]: http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=10653 {.pub .author}
[Elie Ofek]: http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=13407 {.pub .author}
[Anita Tucker]: http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=10668 {.pub .author}
[Amy Edmondson]: http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6451 {.pub .author}
[Michael E. Porter]: http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6532 {.pub .author}
[Leslie John]: http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=589473 {.pub .author}
<?php // publication titles 
?>
[Household Bargaining and Excess Fertility: An Experimental Study in Zambia]: http://www.hbs.edu/faculty/Pages/item.aspx?num=45931
[The Hidden Benefits of Keeping Teams Intact]: http://www.hbs.edu/faculty/Pages/item.aspx?num=45934
[Lessons from England's Health Care Workforce Redesign: No Quick Fixes]: http://www.hbs.edu/faculty/Pages/item.aspx?num=45847
[Public Reporting, Consumerism, and Patient Empowerment]: http://www.hbs.edu/faculty/Pages/item.aspx?num=45889
[Cialis Lifecycle Management: Lilly's BPH Dilemma]: http://www.hbs.edu/faculty/Pages/item.aspx?num=43156
[Increased Speed Equals Increased Wait: The Impact of a Reduction in Emergency Department Ultrasound Order Processing Time]: http://www.hbs.edu/faculty/Pages/item.aspx?num=45790
[Team Scaffolds: How Minimal Team Structures Enable Role-Based Coordination]: http://www.hbs.edu/faculty/Pages/item.aspx?num=42173
[The Strategy That Will Fix Health Care]: http://www.hbs.edu/faculty/Pages/item.aspx?num=45614
[Converging to the Lowest Denominator in Physical Health]: http://www.hbs.edu/faculty/Pages/item.aspx?num=45468

[speech bubbles]: http://www.hbs.edu/healthcare/images/photos/speechbubbles.png
[pill image]: http://www.hbs.edu/healthcare/images/photos/pill_100.png
[steth image]: http://www.hbs.edu/healthcare/images/photos/steth_100.png
[heart image]: http://www.hbs.edu/healthcare/images/photos/heart_100.png
[up_arrow image]: http://www.hbs.edu/healthcare/images/photos/up_arrow_100.png
[dna image]: http://www.hbs.edu/healthcare/images/photos/dna_100.png
[clip image]: http://www.hbs.edu/healthcare/images/photos/clip_100.png

[Steve Porter]: http://www.hbs.edu/healthcare/images/photos/blav_porter_112.png
[Daniel Oliver]: http://www.hbs.edu/healthcare/images/photos/blav_oliver_112.png

[Innovation in Health Care Dinner: Investor &amp; Entrepreneur Perspectives]: http://www.hbshealthalumni.org/article.html?aid=779
[JP Morgan Health Care Conference Reception]: http://www.hbshealthalumni.org/article.html?aid=797
[Crowdfunding Health Care: Secrets from Industry Insiders]: http://www.hbshealthalumni.org/article.html?aid=777
[San Francisco Networking Event for HBS Health Care Alumni]: http://www.hbshealthalumni.org/article.html?aid=801
[New York Networking Event: HBS--Wharton--Columbia]: http://www.hbshealthalumni.org/article.html?aid=798
[HBS Health Care Club Conference 2014]: http://www.hbshealthcareconference.org/health2014/?page_id=89

[December Alumni Bulletin]: https://www.alumni.hbs.edu/bulletin/Pages/table-of-contents.aspx?year=2013&month=12
<!-- [blavatnik logo]: http://www.hbs.edu/healthcare/images/photos/blavatnik_logo.gif -->
[blavatnik logo]: http://www.hbs.edu/healthcare/images/photos/blavatnik_logo_171.png 
[alum newsletter see more blavatnik]:   
http://healthcareinitiative.mkt1960.com/alumni_newsletter_2013-12_blavatnik_see_more

[Lauren Pflepsen]: mailto:lpflepsen@mba2012.hbs.edu
[Elle Pishny]: mailto:epishny@mba2012.hbs.edu
[A set of documents have been produced from the content of the panels]: http://www.hbs.edu/healthcare/pdf/2013%20Health%20Care%20Career%20Days%20Documents.pdf 

<h2>
    
    <a name="_Toc395865181">Barbara</a>
</h2>
<p>
    McNeil, Barbara J. (mcneil@hcp.med.harvard.edu)
</p>
<h2>
    
    <a name="_Toc395865182">Cara</a>
</h2>
<p>
    Sterling, Cara (csterling@hbs.edu)
</p>
<h2>
    
    <a name="_Toc395865183">Joe</a>
</h2>
<p>
    Newhouse, Joseph Paul (newhouse@hcp.med.harvard.edu)
</p>
<h2>
    
    <a name="_Toc395865184">Richard</a>
</h2>
<p>
    Hamermesh, Richard (rhamermesh@hbs.edu)
</p>
<h2>
    
    <a name="_Toc395865185">Rob</a>
</h2>
<p>
    Huckman, Rob (rhuckman@hbs.edu)
</p>
<h1>
    
    <a name="_Toc395865186">Signatures</a>
</h1>
<h2>
    
    <a name="_Toc395865187">Barbara</a>
</h2>
<p>
    <strong>Barbara J. McNeil, M.D.</strong><br/>
    Founding Head, Department of Health Policy and Professor of Radiology<br/>
    Faculty Co-Chair, Forum on Health Care Innovation<br/>
    Harvard Medical School
    <br/>
    180 Longwood Avenue
    <br/>
    Boston, MA 02115
    <br/>
    Phone: 617.432.1909
</p>
<h2>
    <a name="_Toc395865188">Cara</a>
</h2>
<p>
    <strong>Cara Sterling</strong><br/>
    Director, Healthcare Initiative<br/>
    Harvard Business School<br/>
    Soldiers Field<br/>
    Boston, MA 02163<br/>
    Phone: 617.495.6126
</p>
<h2>
    
    <a name="_Toc395865189">Joe</a>
</h2>
<p>
    <strong>Joseph Newhouse, PhD</strong><br/>
    John D. MacArthur Professor of Health Policy and Management<br/>
    Faculty Co-Chair, Forum on Health Care Innovation<br/>
    Harvard Medical School<br/>
    180 Longwood Ave.<br/>
    Boston, MA 02115<br/>
    Phone: 617.432.1325
</p>
<h2>
    
    <a name="_Toc395865190">Richard</a>
</h2>
<p>
    <strong>Richard G. Hamermesh</strong><br/>
    MBA Class of 1961 Professor of Management Practice<br/>
    Faculty Co-Chair, Forum on Health Care Innovation<br/>
    Harvard Business School<br/>
    Rock Center for Entrepreneurship<br/>
    Boston, MA 02163<br/>
    Phone: 617.495.4179
</p>
<h2>
    
    <a name="_Toc395865191">Rob</a>
</h2>
<p>
    <strong>Robert S. Huckman</strong><br/>
    Albert J. Weatherhead III Professor of Business Administration<br/>
    Faculty Co-Chair, Forum on Health Care Innovation<br/>
    Harvard Business School<br/>
    435 Morgan Hall<br/>
    Boston, MA 02163<br/>
    Phone: 617.495.6649
</p>
<h1>
    
    <a name="_Toc395865192">1. HBS Faculty – Monday 11 – from Cara</a>
    <sup></sup>
</h1>
<p>
    Done – from cara<strong></strong>
</p>
<h1>
    
    <a name="_Toc395865193">2. HBS Angels Tuesday 19th – from Richard</a>
</h1>
<table border="0" cellspacing="0" cellpadding="0" width="626">
    <tbody>
        <tr>
            <td width="379" nowrap="" valign="bottom">
                <p>
                    hci-programs.prizes.hac.fhi14-q1.email.08 - 02-hbs angels - richard
                </p>
            </td>
            <td width="247" nowrap="" valign="bottom">
                <p>
                    <u><a href="https://na15.salesforce.com/701i0000000sj6PAAQ">https://na15.salesforce.com/701i0000000sj6PAAQ</a></u>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    From: Hamermesh, Richard (rhamermesh@hbs.edu)
</p>
<p>
    Subject: Please Help Promote Health Acceleration Challenge
</p>
<p>
    <strong></strong>
</p>
<p>
    Dear %%Preferred Name%%,
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Health Acceleration Challenge</a> and we need the help of the HBS Angels and New
    Venture Competition Committees to spread the news!
</p>
<p>
    We are seeking innovations that have demonstrated value for US health care delivery and are ready to be disseminated widely. We are a scale up challenge,
    not a business plan competition.
</p>
<p>
    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Applications</a>
    are open to anyone and due September 29<sup>th</sup>. Our finalists share $150,000 in prize money, meet senior health care leaders at our April 2015
    conference and have an HBS case written about them. All applicants benefit from crowd-sourced feedback on our application platform.
</p>
<p>
    Please help us spread the word using the text below or the attached flyer.
</p>
<ul>
<li>
    Tell health care innovators (past health care pitch companies or applicants)
</li>
<li>
    Tell your screening committees and advisory boards
</li>
<li>
    Include 1-2 sentences about the Challenge in your newsletters (see below)
</li>
<li>
    Share our launch video (<a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.healthaccelerationchallenge.com</a>) on LinkedIn and Twitter
</li>
</ul>
<p>
    Thank you for your support,
</p>
<p>
    Richard
</p>
<p>
    <strong></strong>
</p>
<p>
    <strong>Richard G. Hamermesh</strong><br/>
    MBA Class of 1961 Professor of Management Practice<br/>
    Faculty Co-Chair, Forum on Health Care Innovation<br/>
    Harvard Business School<br/>
    Rock Center for Entrepreneurship<br/>
    Boston, MA 02163<br/>
    Phone: 617.495.4179
</p>
<p>
    <em></em>
</p>
<p>
    <em>***********</em>
</p>
<p>
    <em>If you have a newsletter, please include the following sentences:</em>
</p>
<p>
    <strong><u></u></strong>
</p>
<p>
    <strong><u>What is your health care innovation?</u></strong>
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the Health Acceleration Challenge to award $150,000 to proven health care delivery
innovations ready to scale. Join the conversation and apply by September 29 at    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    <em>[Attachment: Challenge flyer]</em>
</p>
<p>
	<a name="Unsubscribe_1" xt="SPONECLICKOPTOUT" href="#SPONECLICKOPTOUT" target="_blank">Unsubscribe</a>
</p>
<h1>
    
    <a name="_Toc395865194">3. Health Care Initiative Advisory Board Tuesday 19th – from Cara</a>
</h1>
<table border="0" cellspacing="0" cellpadding="0" width="723">
    <tbody>
        <tr>
            <td width="379" nowrap="" valign="bottom">
                <p>
                    hci-programs.prizes.hac.fhi14-q1.email.08 - 03-hci advisory board
                </p>
            </td>
            <td width="344" nowrap="" valign="bottom">
                <p>
                    <u><a href="https://na15.salesforce.com/701i0000000sjNrAAI">https://na15.salesforce.com/701i0000000sjNrAAI</a></u>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    From: Sterling, Cara (csterling@hbs.edu)
</p>
<p>
    Subject: Help Promote the Health Acceleration Challenge
</p>
<p>
    <strong></strong>
</p>
<p>
    Dear %%Preferred Name%%,
</p>
<p>
    I am pleased to tell you that we have launched the HBS-HMS <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Health Acceleration Challenge</a>!
</p>
<p>
    As you may remember, we are seeking innovations that have demonstrated value for US health care delivery and are ready to be disseminated broadly.
    Specifically, we are looking for solutions that have already been implemented at least once and have demonstrated effectiveness. This "scale-up"
    competition seeks to shorten the time frame for innovation dissemination.
</p>
<p>
    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Applications</a>
    are open to anyone and due September 29<sup>th</sup>. Our finalists share $150,000 in prize money, meet senior health care leaders at the Forum on Health
    Care Innovation conference next April and have an HBS case written about them. All applicants benefit from crowd-sourced feedback on our application
    platform.
</p>
<p>
    Please help us spread the word using the text below or the Challenge flyer.
</p>
<ul>
<li>
    Share this information with innovators
</li>
<li>
    Activate intermediaries such as health care investors, hospital executives, accelerators, etc.
</li>
<li>
    Use Twitter or LinkedIn to share our launch video (<a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.healthaccelerationchallenge.com</a>), which includes our Advisory Board stars, Kathy Giusti and Jonathan Bush.
</li>
</ul>
<p>
    Let me know if you have any questions. 
</p>
<p>
	Thank you for your support,
</p>
<p>
    Cara
</p>
<p>
    <strong>Cara Sterling</strong><br/>
    Director, Healthcare Initiative<br/>
    Harvard Business School<br/>
    Soldiers Field<br/>
    Boston, MA 02163<br/>
    Phone: 617.495.6126
</p>
<p>
    <em></em>
</p>
<p>
    <em>***********</em>
</p>
<p>
    <em>To help with email introductions, please feel free to use: </em>
</p>
<p>
    <strong><u></u></strong>
</p>
<p>
    <strong><u>What is your health care innovation?</u></strong>
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the Health Acceleration Challenge to award $150,000 to proven health care delivery
innovations ready to scale. Join the conversation and apply by September 29 at    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    <em>[Attachment: Challenge flyer]</em>
</p>
<h1>
    
    <a name="_Toc395865195">4. FHI Invitees Tuesday 19th</a>
</h1>
<h2>
    
    <a name="_Toc395865196">Barbara</a>
</h2>
<table border="0" cellspacing="0" cellpadding="0" width="723">
    <tbody>
        <tr>
            <td width="379" nowrap="" valign="bottom">
                <p>
                    hci-programs.prizes.hac.fhi14-q1.email.08 - 04-fhi invitees - barbara
                </p>
            </td>
            <td width="344" nowrap="" valign="bottom">
                <p>
                    <u><a href="https://na15.salesforce.com/701i0000000sj6Z">https://na15.salesforce.com/701i0000000sj6Z</a></u>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    <strong></strong>
</p>
<p>
    From: McNeil, Barbara J. (mcneil@hcp.med.harvard.edu)
</p>
<p>
    Subject: Help Scale Up Proven Health Care Innovations
</p>
<p>
    Dear %%Preferred Name%%,
</p>
<p>
Following our successful Forum on Health Care Innovation conference, Harvard Business School and Harvard Medical School are launching the    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Health Acceleration Challenge</a> and we need your help spreading the word!
</p>
<p>
    We are seeking innovations that have demonstrated value for US health care delivery and are ready to scale rapidly. We are not a business plan but a scale
    up competition, as innovators need to have at least one successful implementation.
</p>
<p>
    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Applications</a>
    are open to anyone (not just Harvard Alumni) and due September 29<sup>th</sup>. Our finalists share $150,000 in prize money, meet senior health care
    leaders at our next Forum conference and have an HBS case written about them. All applicants benefit from crowd-sourced feedback on our application
    platform.
</p>
<p>
    Please help us spread the word using the text below and the Challenge flyer:
</p>
<ul><li>
Share this information with innovators
</li>
<li>
Activate intermediaries such as health care investors, hospital executives, accelerators, etc.
</li>
<li>
Use Twitter or LinkedIn to share our launch video (<a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.healthaccelerationchallenge.com</a>)
</li>
<li>
Include us in your organization’s newsletter or send an email with our Challenge flyer
</li></ul>
<p>
    Thank you for your support,
</p>
<p>
    Barbara
</p>
<p>
    P.S. You’ll be receiving your conference invitation in the next couple of months. Hope you can join us in April.
</p>
<p>
    <strong>Barbara J. McNeil, M.D.</strong><br/>
    Founding Head, Department of Health Policy and Professor of Radiology<br/>
    Faculty Co-Chair, Forum on Health Care Innovation<br/>
    Harvard Medical School
    <br/>
    180 Longwood Avenue
    <br/>
    Boston, MA 02115
    <br/>
    Phone: 617.432.1909
</p>
<p>
    <em>***********</em>
</p>
<p>
    <strong><u></u></strong>
</p>
<p>
    <strong><u>What is your health care innovation?</u></strong>
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the Health Acceleration Challenge to award $150,000 to proven health care delivery
innovations ready to scale. Join the conversation and apply by September 29 at    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    <em>[Attachment: Challenge flyer]</em>
</p>
<p>
	<a name="Unsubscribe_1" xt="SPONECLICKOPTOUT" href="#SPONECLICKOPTOUT" target="_blank">Unsubscribe</a>
</p>
<h2>
    
    <a name="_Toc395865197">Richard</a>
</h2>
<table border="0" cellspacing="0" cellpadding="0" width="723">
    <tbody>
        <tr>
            <td width="379" nowrap="" valign="bottom">
                <p>
                    hci-programs.prizes.hac.fhi14-q1.email.08 - 04-fhi invitees - richard
                </p>
            </td>
            <td width="344" nowrap="" valign="bottom">
                <p>
                    <u><a href="https://na15.salesforce.com/701i0000000sj6jAAA">https://na15.salesforce.com/701i0000000sj6jAAA</a></u>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    From: Hamermesh, Richard (rhamermesh@hbs.edu)
</p>
<p>
    Subject: Help Scale Up Proven Health Care Innovations
</p>
<p>
    Dear %%Preferred Name%%,
</p>
<p>
Following our successful Forum on Health Care Innovation conference, Harvard Business School and Harvard Medical School are launching the    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Health Acceleration Challenge</a> and we need your help spreading the word!
</p>
<p>
    We are seeking innovations that have demonstrated value for US health care delivery and are ready to scale rapidly. We are not a business plan but a scale
    up competition, as innovators need to have at least one successful implementation.
</p>
<p>
    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Applications</a>
    are open to anyone (not just Harvard Alumni) and due September 29<sup>th</sup>. Our finalists share $150,000 in prize money, meet senior health care
    leaders at our next Forum conference and have an HBS case written about them. All applicants benefit from crowd-sourced feedback on our application
    platform.
</p>
<p>
    Please help us spread the word using the text below and the Challenge flyer:
</p>
<ul><li>
Share this information with innovators
</li>
<li>
Activate intermediaries such as health care investors, hospital executives, accelerators, etc.
</li>
<li>
Use Twitter or LinkedIn to share our launch video (<a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.healthaccelerationchallenge.com</a>)
</li>
<li>
Include us in your organization’s newsletter or send an email with our Challenge flyer
</li></ul>
<p>
    Thank you for your support,
</p>
<p>
    Richard
</p>
<p>
    <em></em>
</p>
<p>
    P.S. You’ll be receiving your conference invitation in the next couple of months. Hope you can join us in April.
</p>
<p>
    <strong>Richard G. Hamermesh</strong><br/>
    MBA Class of 1961 Professor of Management Practice<br/>
    Faculty Co-Chair, Forum on Health Care Innovation<br/>
    Harvard Business School<br/>
    Rock Center for Entrepreneurship<br/>
    Boston, MA 02163<br/>
    Phone: 617.495.4179
</p>
<p>
    <em>***********</em>
</p>
<p>
    <strong><u></u></strong>
</p>
<p>
    <strong><u>What is your health care innovation?</u></strong>
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the Health Acceleration Challenge to award $150,000 to proven health care delivery
innovations ready to scale. Join the conversation and apply by September 29 at    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    <em>[Attachment: Challenge flyer]</em>
</p>
<p>
	<a name="Unsubscribe_1" xt="SPONECLICKOPTOUT" href="#SPONECLICKOPTOUT" target="_blank">Unsubscribe</a>
</p>
<h2>
    
    <a name="_Toc395865198">Rob</a>
</h2>
<table border="0" cellspacing="0" cellpadding="0" width="723">
    <tbody>
        <tr>
            <td width="379" nowrap="" valign="bottom">
                <p>
                    hci-programs.prizes.hac.fhi14-q1.email.08 - 04-fhi invitees - rob
                </p>
            </td>
            <td width="344" nowrap="" valign="bottom">
                <p>
                    <u><a href="https://na15.salesforce.com/701i0000000sj6oAAA">https://na15.salesforce.com/701i0000000sj6oAAA</a></u>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    From: Huckman, Rob (rhuckman@hbs.edu)
</p>
<p>
    Subject: Help Scale Up Proven Health Care Innovations
</p>
<p>
    Dear %%Preferred Name%%,
</p>
<p>
Following our successful Forum on Health Care Innovation conference, Harvard Business School and Harvard Medical School are launching the    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Health Acceleration Challenge</a> and we need your help spreading the word!
</p>
<p>
    We are seeking innovations that have demonstrated value for US health care delivery and are ready to scale rapidly. We are not a business plan but a scale
    up competition, as innovators need to have at least one successful implementation.
</p>
<p>
    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Applications</a>
    are open to anyone (not just Harvard Alumni) and due September 29<sup>th</sup>. Our finalists share $150,000 in prize money, meet senior health care
    leaders at our next Forum conference and have an HBS case written about them. All applicants benefit from crowd-sourced feedback on our application
    platform.
</p>
<p>
    Please help us spread the word using the text below and the Challenge flyer:
</p>
<ul><li>
Share this information with innovators
</li>
<li>
Activate intermediaries such as health care investors, hospital executives, accelerators, etc.
</li>
<li>
Use Twitter or LinkedIn to share our launch video (<a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.healthaccelerationchallenge.com</a>)
</li>
<li>
Include us in your organization’s newsletter or send an email with our Challenge flyer
</li></ul>
<p>
    Thank you for your support,
</p>
<p>
    Rob
</p>
<p>
    <em></em>
</p>
<p>
    P.S. You’ll be receiving your conference invitation in the next couple of months. Hope you can join us in April.
</p>
<p>
    <strong>Robert S. Huckman</strong><br/>
    Albert J. Weatherhead III Professor of Business Administration<br/>
    Faculty Co-Chair, Forum on Health Care Innovation<br/>
    Harvard Business School<br/>
    435 Morgan Hall<br/>
    Boston, MA 02163<br/>
    Phone: 617.495.6649
</p>
<p>
    <em>***********</em>
</p>
<p>
    <strong><u></u></strong>
</p>
<p>
    <strong><u>What is your health care innovation?</u></strong>
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the Health Acceleration Challenge to award $150,000 to proven health care delivery
innovations ready to scale. Join the conversation and apply by September 29 at    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    <em>[Attachment: Challenge flyer]</em>
</p>
<p>
	<a name="Unsubscribe_1" xt="SPONECLICKOPTOUT" href="#SPONECLICKOPTOUT" target="_blank">Unsubscribe</a>
</p>
<h2>
    
    <a name="_Toc395865199">Joe</a>
</h2>
<table border="0" cellspacing="0" cellpadding="0" width="720">
    <tbody>
        <tr>
            <td width="379" nowrap="" valign="bottom">
                <p>
                    hci-programs.prizes.hac.fhi14-q1.email.08 - 04-fhi invitees - joe
                </p>
            </td>
            <td width="341" nowrap="" valign="bottom">
                <p>
                    <u><a href="https://na15.salesforce.com/701i0000000sj6tAAA">https://na15.salesforce.com/701i0000000sj6tAAA</a></u>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    From: Newhouse, Joseph Paul (newhouse@hcp.med.harvard.edu)
</p>
<p>
    Subject: Help Scale Up Proven Health Care Innovations
</p>
<p>
    Dear %%Preferred Name%%,
</p>
<p>
Following our successful Forum on Health Care Innovation conference, Harvard Business School and Harvard Medical School are launching the    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Health Acceleration Challenge</a> and we need your help spreading the word!
</p>
<p>
    We are seeking innovations that have demonstrated value for US health care delivery and are ready to scale rapidly. We are not a business plan but a scale
    up competition, as innovators need to have at least one successful implementation.
</p>
<p>
    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Applications</a>
    are open to anyone (not just Harvard Alumni) and due September 29<sup>th</sup>. Our finalists share $150,000 in prize money, meet senior health care
    leaders at our next Forum conference and have an HBS case written about them. All applicants benefit from crowd-sourced feedback on our application
    platform.
</p>
<p>
    Please help us spread the word using the text below and the Challenge flyer:
</p>
<ul><li>
Share this information with innovators
</li>
<li>
Activate intermediaries such as health care investors, hospital executives, accelerators, etc.
</li>
<li>
Use Twitter or LinkedIn to share our launch video (<a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.healthaccelerationchallenge.com</a>)
</li>
<li>
Include us in your organization’s newsletter or send an email with our Challenge flyer
</li></ul>
<p>
    Thank you for your support,
</p>
<p>
    Joe
</p>
<p>
    <em></em>
</p>
<p>
    P.S. You’ll be receiving your conference invitation in the next couple of months. Hope you can join us in April.
</p>
<p>
    <strong>Joseph Newhouse, PhD</strong><br/>
    John D. MacArthur Professor of Health Policy and Management<br/>
    Faculty Co-Chair, Forum on Health Care Innovation<br/>
    Harvard Medical School<br/>
    180 Longwood Ave.<br/>
    Boston, MA 02115<br/>
    Phone: 617.432.1325
</p>
<p>
    <em>***********</em>
</p>
<p>
    <strong><u></u></strong>
</p>
<p>
    <strong><u>What is your health care innovation?</u></strong>
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the Health Acceleration Challenge to award $150,000 to proven health care delivery
innovations ready to scale. Join the conversation and apply by September 29 at    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    <em>[Attachment: Challenge flyer]</em>
</p>
<p>
	<a name="Unsubscribe_1" xt="SPONECLICKOPTOUT" href="#SPONECLICKOPTOUT" target="_blank">Unsubscribe</a>
</p>
<h1>
    
    <a name="_Toc395865200">5. Top 100 Executives + Faculty List Tuesday 19th</a>
</h1>
<p>
    <strong></strong>
</p>
<h2>
    
    <a name="_Toc395865201">Barbara</a>
</h2>
<table border="0" cellspacing="0" cellpadding="0" width="626">
    <tbody>
        <tr>
            <td width="378" nowrap="" valign="bottom">
                <p>
                    hci-programs.prizes.hac.fhi14-q1.email.08 - 05-top 100 executives - barbara
                </p>
            </td>
            <td width="248" nowrap="" valign="bottom">
                <p>
                    <u><a href="https://na15.salesforce.com/701i0000000sj6yAAA">https://na15.salesforce.com/701i0000000sj6yAAA</a></u>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    From: McNeil, Barbara J. (mcneil@hcp.med.harvard.edu)
</p>
<p>
    Subject: Help Scale Up Health Care Innovations
</p>
<p>
    Dear %%Preferred Name%%,
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Health Acceleration Challenge</a> to speed up adoption of proven innovations.
</p>
<p>
    We are seeking innovations that have demonstrated value for US health care delivery and are ready to scale broadly. We are not a business plan contest, but
    a scale up competition.
</p>
<p>
    Please help us spread the word.
</p>
<p>
    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Applications</a>
    are open to anyone with a great idea and due September 29<sup>th</sup>. Our finalists share $150,000 in prize money, meet senior health care leaders at our
    invite-only April 2015 conference and have an HBS case written about them. All applicants benefit from crowd-sourced feedback on our application platform.
</p>
<p>
    Please help us spread the word using the text below and the Challenge flyer:
</p>
<ul><li>
Share this information with innovators
</li>
<li>
Activate intermediaries such as health care investors, hospital executives, accelerators, etc.
</li>
<li>
Use Twitter or LinkedIn to share our launch video (<a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.healthaccelerationchallenge.com</a>)
</li>
<li>
Include us in your organization’s newsletter or send an email with our Challenge flyer
</li></ul>
<p>
    Thank you for your support,
</p>
<p>
    Barbara
</p>
<p>
    <strong>Barbara J. McNeil, M.D.</strong><br/>
    Founding Head, Department of Health Policy and Professor of Radiology<br/>
    Faculty Co-Chair, Forum on Health Care Innovation<br/>
    Harvard Medical School
    <br/>
    180 Longwood Avenue
    <br/>
    Boston, MA 02115
    <br/>
    Phone: 617.432.1909
</p>
<p>
    <em>***********</em>
</p>
<p>
    <strong><u></u></strong>
</p>
<p>
    <strong><u>What is your health care innovation?</u></strong>
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the Health Acceleration Challenge to award $150,000 to proven health care delivery
innovations ready to scale. Join the conversation and apply by September 29 at    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    <em>[Attachment: Challenge flyer]</em>
</p>
<p>
	<a name="Unsubscribe_1" xt="SPONECLICKOPTOUT" href="#SPONECLICKOPTOUT" target="_blank">Unsubscribe</a>
</p>
<h2>
    
    <a name="_Toc395865202">Richard</a>
</h2>
<table border="0" cellspacing="0" cellpadding="0" width="725">
    <tbody>
        <tr>
            <td width="378" nowrap="" valign="bottom">
                <p>
                    hci-programs.prizes.hac.fhi14-q1.email.08 - 05-top 100 executives - richard
                </p>
            </td>
            <td width="347" nowrap="" valign="bottom">
                <p>
                    <u><a href="https://na15.salesforce.com/701i0000000sj78AAA">https://na15.salesforce.com/701i0000000sj78AAA</a></u>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    From: Hamermesh, Richard (rhamermesh@hbs.edu)
</p>
<p>
    Subject: Help Scale Up Health Care Innovations
</p>
<p>
    Dear %%Preferred Name%%,
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Health Acceleration Challenge</a> to speed up adoption of proven innovations.
</p>
<p>
    We are seeking innovations that have demonstrated value for US health care delivery and are ready to scale broadly. We are not a business plan contest, but
    a scale up competition.
</p>
<p>
    Please help us spread the word.
</p>
<p>
    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Applications</a>
    are open to anyone with a great idea and due September 29<sup>th</sup>. Our finalists share $150,000 in prize money, meet senior health care leaders at our
    invite-only April 2015 conference and have an HBS case written about them. All applicants benefit from crowd-sourced feedback on our application platform.
</p>
<p>
    Please help us spread the word using the text below and the Challenge flyer:
</p>
<ul><li>
Share this information with innovators
</li>
<li>
Activate intermediaries such as health care investors, hospital executives, accelerators, etc.
</li>
<li>
Use Twitter or LinkedIn to share our launch video (<a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.healthaccelerationchallenge.com</a>)
</li>
<li>
Include us in your organization’s newsletter or send an email with our Challenge flyer
</li></ul>
<p>
    Thank you for your support,
</p>
<p>
    Richard
</p>
<p>
    <strong></strong>
</p>
<p>
    <strong>Richard G. Hamermesh</strong><br/>
    MBA Class of 1961 Professor of Management Practice<br/>
    Faculty Co-Chair, Forum on Health Care Innovation<br/>
    Harvard Business School<br/>
    Rock Center for Entrepreneurship<br/>
    Boston, MA 02163<br/>
    Phone: 617.495.4179
</p>
<p>
    <em></em>
</p>
<p>
    <em>***********</em>
</p>
<p>
    <strong><u></u></strong>
</p>
<p>
    <strong><u>What is your health care innovation?</u></strong>
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the Health Acceleration Challenge to award $150,000 to proven health care delivery
innovations ready to scale. Join the conversation and apply by September 29 at    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    <em>[Attachment: Challenge flyer]</em>
</p>
<p>
	<a name="Unsubscribe_1" xt="SPONECLICKOPTOUT" href="#SPONECLICKOPTOUT" target="_blank">Unsubscribe</a>
</p>
<h2>
    
    <a name="_Toc395865203">Rob</a>
</h2>
<table border="0" cellspacing="0" cellpadding="0" width="725">
    <tbody>
        <tr>
            <td width="378" nowrap="" valign="bottom">
                <p>
                    hci-programs.prizes.hac.fhi14-q1.email.08 - 05-top 100 executives - rob
                </p>
            </td>
            <td width="347" nowrap="" valign="bottom">
                <p>
                    <u><a href="https://na15.salesforce.com/701i0000000sj7DAAQ">https://na15.salesforce.com/701i0000000sj7DAAQ</a></u>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    From: Huckman, Rob (rhuckman@hbs.edu)
</p>
<p>
    Subject: Help Scale Up Health Care Innovations
</p>
<p>
    Dear %%Preferred Name%%,
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Health Acceleration Challenge</a> to speed up adoption of proven innovations.
</p>
<p>
    We are seeking innovations that have demonstrated value for US health care delivery and are ready to scale broadly. We are not a business plan contest, but
    a scale up competition.
</p>
<p>
    Please help us spread the word.
</p>
<p>
    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Applications</a>
    are open to anyone with a great idea and due September 29<sup>th</sup>. Our finalists share $150,000 in prize money, meet senior health care leaders at our
    invite-only April 2015 conference and have an HBS case written about them. All applicants benefit from crowd-sourced feedback on our application platform.
</p>
<p>
    Please help us spread the word using the text below and the Challenge flyer:
</p>
<ul><li>
Share this information with innovators
</li>
<li>
Activate intermediaries such as health care investors, hospital executives, accelerators, etc.
</li>
<li>
Use Twitter or LinkedIn to share our launch video (<a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.healthaccelerationchallenge.com</a>)
</li>
<li>
Include us in your organization’s newsletter or send an email with our Challenge flyer
</li></ul>
<p>
    Thank you for your support,
</p>
<p>
    Rob
</p>
<p>
    <strong>Robert S. Huckman</strong><br/>
    Albert J. Weatherhead III Professor of Business Administration<br/>
    Faculty Co-Chair, Forum on Health Care Innovation<br/>
    Harvard Business School<br/>
    435 Morgan Hall<br/>
    Boston, MA 02163<br/>
    Phone: 617.495.6649
</p>
<p>
    <em></em>
</p>
<p>
    <em>***********</em>
</p>
<p>
    <strong><u></u></strong>
</p>
<p>
    <strong><u>What is your health care innovation?</u></strong>
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the Health Acceleration Challenge to award $150,000 to proven health care delivery
innovations ready to scale. Join the conversation and apply by September 29 at    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    <em>[Attachment: Challenge flyer]</em>
</p>
<p>
	<a name="Unsubscribe_1" xt="SPONECLICKOPTOUT" href="#SPONECLICKOPTOUT" target="_blank">Unsubscribe</a>
</p>
<h2>
    
    <a name="_Toc395865204">Joe</a>
</h2>
<table border="0" cellspacing="0" cellpadding="0" width="725">
    <tbody>
        <tr>
            <td width="378" nowrap="" valign="bottom">
                <p>
                    hci-programs.prizes.hac.fhi14-q1.email.08 - 05-top 100 executives - joe
                </p>
            </td>
            <td width="347" nowrap="" valign="bottom">
                <p>
                    <u><a href="https://na15.salesforce.com/701i0000000sj7IAAQ">https://na15.salesforce.com/701i0000000sj7IAAQ</a></u>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    From: Newhouse, Joseph Paul (newhouse@hcp.med.harvard.edu)
</p>
<p>
    Subject: Help Scale Up Health Care Innovations
</p>
<p>
    Dear %%Preferred Name%%,
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Health Acceleration Challenge</a> to speed up adoption of proven innovations.
</p>
<p>
    We are seeking innovations that have demonstrated value for US health care delivery and are ready to scale broadly. We are not a business plan contest, but
    a scale up competition.
</p>
<p>
    Please help us spread the word.
</p>
<p>
    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Applications</a>
    are open to anyone with a great idea and due September 29<sup>th</sup>. Our finalists share $150,000 in prize money, meet senior health care leaders at our
    invite-only April 2015 conference and have an HBS case written about them. All applicants benefit from crowd-sourced feedback on our application platform.
</p>
<p>
    Please help us spread the word using the text below and the Challenge flyer:
</p>
<ul><li>
Share this information with innovators
</li>
<li>
Activate intermediaries such as health care investors, hospital executives, accelerators, etc.
</li>
<li>
Use Twitter or LinkedIn to share our launch video (<a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.healthaccelerationchallenge.com</a>)
</li>
<li>
Include us in your organization’s newsletter or send an email with our Challenge flyer
</li></ul>
<p>
    Thank you for your support,
</p>
<p>
    Joe
</p>
<p>
    <strong>Joseph Newhouse, PhD</strong><br/>
    John D. MacArthur Professor of Health Policy and Management<br/>
    Faculty Co-Chair, Forum on Health Care Innovation<br/>
    Harvard Medical School<br/>
    180 Longwood Ave.<br/>
    Boston, MA 02115<br/>
    Phone: 617.432.1325
</p>
<p>
    <em></em>
</p>
<p>
    <em>***********</em>
</p>
<p>
    <strong><u></u></strong>
</p>
<p>
    <strong><u>What is your health care innovation?</u></strong>
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the Health Acceleration Challenge to award $150,000 to proven health care delivery
innovations ready to scale. Join the conversation and apply by September 29 at    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    <em>[Attachment: Challenge flyer]</em>
</p>
<p>
	<a name="Unsubscribe_1" xt="SPONECLICKOPTOUT" href="#SPONECLICKOPTOUT" target="_blank">Unsubscribe</a>
</p>
<h1>
    
    <a name="_Toc395865205">6. HC Entrepreneurs + Faculty List Tuesday 19th – from Richard</a>
</h1>
<table border="0" cellspacing="0" cellpadding="0" width="626">
    <tbody>
        <tr>
            <td width="378" nowrap="" valign="bottom">
                <p>
                    hci-programs.prizes.hac.fhi14-q1.email.08 - 06-hc enterpreneurs - richard
                </p>
            </td>
            <td width="248" nowrap="" valign="bottom">
                <p>
                    <u><a href="https://na15.salesforce.com/701i0000000sj7NAAQ">https://na15.salesforce.com/701i0000000sj7NAAQ</a></u>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    <strong></strong>
</p>
<p>
    From: Hamermesh, Richard (rhamermesh@hbs.edu)
</p>
<p>
    Subject: Scale Up Your Health Care Innovation
</p>
<p>
    Dear %%Preferred Name%%,
</p>
<p>
    As CEO of your company you know firsthand how difficult it is to scale up your health care innovation.
</p>
<p>
    That is why Harvard Business School and Harvard Medical School are launching the <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Health Acceleration Challenge</a> seeking innovations that have
    demonstrated value for US health care delivery and are ready to scale rapidly.
</p>
<p>
    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Applications</a>
    are open to anyone with a great idea and due September 29<sup>th</sup>. Our finalists share $150,000 in prize money, meet senior health care leaders at our
    invite-only April 2015 conference and have an HBS case written about them. All applicants benefit from crowd-sourced feedback on our application platform.
</p>
<p>
    To find out more and to apply, go to <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    Best,
</p>
<p>
    Richard
</p>
<p>
    <strong>Richard G. Hamermesh</strong><br/>
    MBA Class of 1961 Professor of Management Practice<br/>
    Faculty Co-Chair, Forum on Health Care Innovation<br/>
    Harvard Business School<br/>
    Rock Center for Entrepreneurship<br/>
    Boston, MA 02163<br/>
    Phone: 617.495.4179
</p>
<p>
    <em>[Attachment: Challenge flyer]</em>
</p>
<p>
	<a name="Unsubscribe_1" xt="SPONECLICKOPTOUT" href="#SPONECLICKOPTOUT" target="_blank">Unsubscribe</a>
</p>
<h1>
    
    <a name="_Toc395865206">7. HC Investor List + Faculty List Tuesday 19th – from Rob</a>
</h1>
<table border="0" cellspacing="0" cellpadding="0" width="626">
    <tbody>
        <tr>
            <td width="379" nowrap="" valign="bottom">
                <p>
                    hci-programs.prizes.hac.fhi14-q1.email.08 - 07-hc investor list - rob
                </p>
            </td>
            <td width="247" nowrap="" valign="bottom">
                <p>
                    <u><a href="https://na15.salesforce.com/701i0000000sj7SAAQ">https://na15.salesforce.com/701i0000000sj7SAAQ</a></u>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    From: Huckman, Rob (rhuckman@hbs.edu)
</p>
<p>
    Subject: Scale Up Your Portfolio Companies
</p>
<p>
    Dear %%Preferred Name%%,
</p>
<p>
    As investors in health care start-ups you know firsthand how difficult it is to scale up a good idea in health care.
</p>
<p>
    That is why Harvard Business School and Harvard Medical School are launching the <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Health Acceleration Challenge</a>, seeking innovations that have
    demonstrated value for US health care delivery and are ready to scale rapidly.
</p>
<p>
    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Applications</a>
    are open to anyone (NOT just Harvard alumni) and due September 29<sup>th</sup>. Finalists share $150,000 in prize money, meet senior health care leaders at
    our invite-only April 2015 conference and have an HBS case written about them. All applicants benefit from crowd-sourced feedback on our application
    platform.
</p>
<p>
    Please forward our Challenge flyer to your portfolio companies and to other innovators that you think should apply.
</p>
<p>
    To find out more and apply, go to <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    Best regards,
</p>
<p>
    Rob
</p>
<p>
    <strong>Robert S. Huckman</strong><br/>
    Albert J. Weatherhead III Professor of Business Administration<br/>
    Faculty Co-Chair, Forum on Health Care Innovation<br/>
    Harvard Business School<br/>
    435 Morgan Hall<br/>
    Boston, MA 02163<br/>
    Phone: 617.495.6649
</p>
<p>
    <em>[Attachment: Challenge flyer]</em>
</p>
<p>
	<a name="Unsubscribe_1" xt="SPONECLICKOPTOUT" href="#SPONECLICKOPTOUT" target="_blank">Unsubscribe</a>
</p>
<h1>
    
    <a name="_Toc395865207">8. Healthcare Executives Tuesday 19th</a>
</h1>
<h2>
    
    <a name="_Toc395865208">From Cara, With First Name</a>
</h2>
<table border="0" cellspacing="0" cellpadding="0" width="626">
    <tbody>
        <tr>
            <td width="379" nowrap="" valign="bottom">
                <p>
                    hci-programs.prizes.hac.fhi14-q1.email.08 - 08-hc execs - fname - cara
                </p>
            </td>
            <td width="247" nowrap="" valign="bottom">
                <p>
                    <u><a href="https://na15.salesforce.com/701i0000000sj7XAAQ">https://na15.salesforce.com/701i0000000sj7XAAQ</a></u>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    From: Sterling, Cara (csterling@hbs.edu)
</p>
<p>
    Subject: Disseminate Your Innovations
</p>
<p>
    Dear %%Preferred Name%%,
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Health Acceleration Challenge</a> seeking innovations that have demonstrated value
    for US health care delivery and are ready to scale rapidly.
</p>
<p>
    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Applications</a>
    are open to anyone with a great idea and we particularly seek innovations from health system teams that have implemented a solution locally that other health systems could benefit from. 
</p>
<p>
    Our finalists share $150,000 in prize money, meet senior health care leaders at our
    April 2015 conference and have an HBS case written about them. All applicants benefit from crowd-sourced feedback on our application platform.
</p>
<p>
    Please help us spread the word so that your organization’s ideas gain traction across the country. Email innovators, include us in your newsletter (see
    below), forward our Challenge flyer (attached) and share our launch video.
</p>
<p>
    To find out more and apply by September 29th, go to <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    Thank you for your support,
</p>
<p>
    Cara
</p>
<p>
    <strong>Cara Sterling</strong><br/>
    Director, Healthcare Initiative<br/>
    Harvard Business School<br/>
    Soldiers Field<br/>
    Boston, MA 02163<br/>
    Phone: 617.495.6126
</p>
<p>
    <em></em>
</p>
<p>
    <em>***********</em>
</p>
<p>
    <strong><u></u></strong>
</p>
<p>
    <strong><u>What is your health care innovation?</u></strong>
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the Health Acceleration Challenge to award $150,000 to proven health care delivery
innovations ready to scale. Join the conversation and apply by September 29 at    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    <em>[Attachment: Challenge flyer]</em>
</p>
<p>
	<a name="Unsubscribe_1" xt="SPONECLICKOPTOUT" href="#SPONECLICKOPTOUT" target="_blank">Unsubscribe</a>
</p>
<h2>
    
    <a name="_Toc395865209">From Cara, Without First Name</a>
</h2>
<table border="0" cellspacing="0" cellpadding="0" width="724">
    <tbody>
        <tr>
            <td width="379" nowrap="" valign="bottom">
                <p>
                    hci-programs.prizes.hac.fhi14-q1.email.08 - 08-hc execs - no fname - cara
                </p>
            </td>
            <td width="345" nowrap="" valign="bottom">
                <p>
                    <u><a href="https://na15.salesforce.com/701i0000000sj7cAAA">https://na15.salesforce.com/701i0000000sj7cAAA</a></u>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    From: Sterling, Cara (csterling@hbs.edu)
</p>
<p>
    Subject: Disseminate Your Innovations
</p>
<p>
    Good morning,
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Health Acceleration Challenge</a> seeking innovations that have demonstrated value
    for US health care delivery and are ready to scale rapidly.
</p>
<p>
    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Applications</a>
    are open to anyone with a great idea  idea and we particularly seek innovations from health system teams that have implemented a solution locally that other health systems could benefit from. 
</p>
<p> Our finalists share $150,000 in prize money, meet senior health care leaders at our
    April 2015 conference and have an HBS case written about them. All applicants benefit from crowd-sourced feedback on our application platform.
</p>
<p>
    Please help us spread the word so that your organization’s ideas gain traction across the country. Email innovators, include us in your newsletter (see
    below), forward our Challenge flyer (attached) and share our launch video.
</p>
<p>
    To find out more and apply by September 29<sup>th</sup>, go to <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    Thank you for your support,
</p>
<p>
    Cara
</p>
<p>
    <strong>Cara Sterling</strong><br/>
    Director, Healthcare Initiative<br/>
    Harvard Business School<br/>
    Soldiers Field<br/>
    Boston, MA 02163<br/>
    Phone: 617.495.6126
</p>
<p>
    <em></em>
</p>
<p>
    <em>***********</em>
</p>
<p>
    <strong><u></u></strong>
</p>
<p>
    <strong><u>What is your health care innovation?</u></strong>
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the Health Acceleration Challenge to award $150,000 to proven health care delivery
innovations ready to scale. Join the conversation and apply by September 29 at    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    <em>[Attachment: Challenge flyer]</em>
</p>
<p>
	<a name="Unsubscribe_1" xt="SPONECLICKOPTOUT" href="#SPONECLICKOPTOUT" target="_blank">Unsubscribe</a>
</p>
<h2>
    
    <a name="_Toc395865210">From Barbara, WITH First Name (Note – first names HAVE been prepared for this mailing as of 2014-08-18)</a>
</h2>
<table border="0" cellspacing="0" cellpadding="0" width="724">
    <tbody>
        <tr>
            <td width="379" nowrap="" valign="bottom">
                <p>
                    hci-programs.prizes.hac.fhi14-q1.email.08 - 08-hc execs - barbara
                </p>
            </td>
            <td width="345" nowrap="" valign="bottom">
                <p>
                    <u><a href="https://na15.salesforce.com/701i0000000sj7hAAA">https://na15.salesforce.com/701i0000000sj7hAAA</a></u>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    From: McNeil, Barbara J. (mcneil@hcp.med.harvard.edu)
</p>
<p>
    Subject: Disseminate Your Innovations
</p>
<p>
    Dear %%Preferred Name%%,
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Health Acceleration Challenge</a> seeking innovations that have demonstrated value
    for US health care delivery and are ready to scale rapidly.
</p>
<p>
    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Applications</a>
    are open to anyone with a great idea and we particularly seek innovations from health system teams that have implemented a solution locally that other health systems could benefit from. 
</p>
<p> Our finalists share $150,000 in prize money, meet senior health care leaders at our
    April 2015 conference and have an HBS case written about them. All applicants benefit from crowd-sourced feedback on our application platform.
</p>
<p>
    Please help us spread the word so that your organization’s ideas gain traction across the country. Email innovators, include us in your newsletter (see
    below), forward our Challenge flyer (attached) and share our launch video.
</p>
<p>
    To find out more and apply by September 29<sup>th</sup>, go to <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    Thank you for your support,
</p>
<p>
    Barbara
</p>
<p>
    <strong>Barbara J. McNeil, M.D.</strong><br/>
    Founding Head, Department of Health Policy and Professor of Radiology<br/>
    Faculty Co-Chair, Forum on Health Care Innovation<br/>
    Harvard Medical School
    <br/>
    180 Longwood Avenue
    <br/>
    Boston, MA 02115
    <br/>
    Phone: 617.432.1909
</p>
<p>
    <em>***********</em>
</p>
<p>
    <strong><u></u></strong>
</p>
<p>
    <strong><u>What is your health care innovation?</u></strong>
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the Health Acceleration Challenge to award $150,000 to proven health care delivery
innovations ready to scale. Join the conversation and apply by September 29 at    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    <em>[Attachment: Challenge flyer]</em>
</p>
<p>
	<a name="Unsubscribe_1" xt="SPONECLICKOPTOUT" href="#SPONECLICKOPTOUT" target="_blank">Unsubscribe</a>
</p>
<h1>
    
    <a name="_Toc395865211">9. MDMBA programs Tuesday 19 – from Cara</a>
</h1>
<table border="0" cellspacing="0" cellpadding="0" width="626">
    <tbody>
        <tr>
            <td width="377" nowrap="" valign="bottom">
                <p>
                    hci-programs.prizes.hac.fhi14-q1.email.08 - 09-mdmba programs - cara
                </p>
            </td>
            <td width="249" nowrap="" valign="bottom">
                <p>
                    <u><a href="https://na15.salesforce.com/701i0000000sj7mAAA">https://na15.salesforce.com/701i0000000sj7mAAA</a></u>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    From: Sterling, Cara (csterling@hbs.edu)
</p>
<p>
    Subject: Help Promote Our Health Acceleration Challenge
</p>
<p>
    Dear %%Preferred Name%%,
</p>
<p>
    I am the Director of the Health Care Initiative at Harvard Business School and I wanted to ask for your help spreading the word about our scale up
    competition.
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Health Acceleration Challenge</a> seeking innovations that have demonstrated value
    for US health care delivery and are ready to scale rapidly.
</p>
<p>
    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Applications</a>
    are open to anyone with a great idea and due September 29<sup>th</sup>. Our finalists share $150,000 in prize money, meet senior health care leaders at our
    invite-only April 2015 conference and have an HBS case written about them. All applicants benefit from crowd-sourced feedback on our application platform.
</p>
<p>
    Please include this Challenge in your newsletters to your students (see below) or, even better, forward the Challenge flyer to innovators that you think
    should apply!
</p>
<p>
    To find out more and to apply, go to <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    Many thanks,
</p>
<p>
    Cara Sterling
</p>
<p>
    <strong>Cara Sterling</strong><br/>
    Director, Healthcare Initiative<br/>
    Harvard Business School<br/>
    Soldiers Field<br/>
    Boston, MA 02163<br/>
    Phone: 617.495.6126
</p>
<p>
    <em>[Attachment: Challenge flyer]</em>
</p>
<p>
	<a name="Unsubscribe_1" xt="SPONECLICKOPTOUT" href="#SPONECLICKOPTOUT" target="_blank">Unsubscribe</a>
</p>
<h1>
    
    <a name="_Toc395865212">10. Press Tuesday 19th – from Klaus</a>
</h1>
<table border="0" cellspacing="0" cellpadding="0" width="626">
    <tbody>
        <tr>
            <td width="381" nowrap="" valign="bottom">
                <p>
                    hci-programs.prizes.hac.fhi14-q1.email.08 - 10-press - Klaus
                </p>
            </td>
            <td width="245" nowrap="" valign="bottom">
                <p>
                    <u><a href="https://na15.salesforce.com/701i0000000sj7rAAA">https://na15.salesforce.com/701i0000000sj7rAAA</a></u>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    Dear %%Preferred Name%%,
</p>
<p>
    Harvard Business School and Harvard Medical School are pleased to announce the launch of the Health Acceleration Challenge. 
</p>
<p>
    For more information, please find our press release below and visit us at <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    Best regards,
</p>
<p>
    Klaus 
</p>
<p>
    <strong>Klaus Koenigshausen</strong><br/>
    Health Care Initiative<br/>
    Harvard Business School<br/>
    Soldiers Field<br/>
    Boston, MA 02163<br/>
    Phone: 617.495.6126
</p>
<p>
    <strong>Press Release - For Immediate Release</strong>
</p>
<p>
    <strong>
        Harvard Business School and Harvard Medical School Launch “The Health Acceleration Challenge”
    </strong>
</p>
<p>
    <strong>Boston - </strong>
    Over the last half-century, some of the greatest health care innovations have taken decades to reach widespread adoption. Why does it take so long?
</p>
<p>
    Dr. Toby Cosgrove, chief executive officer, Cleveland Clinic, noted that it takes an average of thirteen years for a proven therapy to become a standard
    practice.
</p>
<p>
    Adding to that Dr. Barbara McNeil, Professor at Harvard Medical School points out: "We can't afford to develop improvements exclusively for one site when
    they could be applicable to a much larger number of patients." Therefore, in a world of full of innovations that we already struggle to
    absorb, perhaps what we need is not simply another 'big idea,' but rather better ways of distributing smaller ideas.
</p>
<p>
That is why the <strong>Forum on Health Care Innovation</strong>, a collaboration between <strong>Harvard Business School (HBS)</strong> and <strong>Harvard Medical School (HMS)</strong> are launching the <strong>Health Acceleration Challenge</strong> to identify innovations that have
    demonstrated effectiveness for U.S. health care delivery and shorten the time frame for their widespread dissemination.
</p>
<p>
    Finalists will share $150,000 in prize money, meet senior health care leaders at the HBS-HMS Forum on Health Care Innovation Conference in April 2015, and
    have an HBS case written about them. Applications are open to anyone and due on September 29<sup>th</sup>.
</p>
<p>
    All applicants can benefit from crowd-sourced feedback on our application platform.
</p>
<p>
    To find out more, go to <a target="_blank" href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    <strong>About the Forum on Health Care Innovation</strong>
</p>
<p>
    The Forum on Health Care Innovation is a collaboration between Harvard Business School and Harvard Medical School. Its purpose is to unite leading
    executives, policymakers, and academics in a cross-disciplinary exploration of innovative actions to improve quality, reduce costs, and, ultimately,
increase value in the health care industry. The Forum's initial event was a conference and survey that were summarized in <a target="_blank" href="http://www.healthaccelerationchallenge.com/conference/?%%SP_CRM_BLOCK%%#thought-leadership">Five Key Imperatives</a>. 
The Forum has now launched the Health Acceleration Challenge to identify and promote demonstrated health care innovations ready to scale.
</p>
<p>
    <strong>Press Contact:</strong>
</p>
<p>
    Klaus Koenigshausen, <a href="mailto:kkoenigshausen@hbs.edu">kkoenigshausen@hbs.edu</a>, (617) 475-0424
</p>
<p>
	<a name="Unsubscribe_1" xt="SPONECLICKOPTOUT" href="#SPONECLICKOPTOUT" target="_blank">Unsubscribe</a>
</p>
<h1>
    <a name="_Toc395865213">11. Challenge Launch Rob Tuesday 19 – from Rob</a>
</h1>
<table border="0" cellspacing="0" cellpadding="0" width="725">
    <tbody>
        <tr>
            <td width="378" nowrap="" valign="bottom">
                <p>
                    hci-programs.prizes.hac.fhi14-q1.email.08 - 11-challenge launch - rob
                </p>
            </td>
            <td width="347" nowrap="" valign="bottom">
                <p>
                    <u><a href="https://na15.salesforce.com/701i0000000sj7wAAA">https://na15.salesforce.com/701i0000000sj7wAAA</a></u>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    From: Huckman, Rob (rhuckman@hbs.edu)
</p>
<p>
    Subject: Health Acceleration Challenge Launch
</p>
<p>
    Dear %%Preferred Name%%,
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Health Acceleration Challenge</a> seeking innovations that have demonstrated value
    for US health care delivery and are ready to scale rapidly.
</p>
<p>
    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Applications</a>
    are open to anyone with a great idea and due September 29<sup>th</sup>. Our finalists share $150,000 in prize money, meet senior health care leaders at our
    April 2015 conference and have an HBS case written about them. All applicants benefit from crowd-sourced feedback on our application platform.
</p>
<p>
    Please help us spread the word. Email innovators, include us in your newsletter (see below), forward our Challenge flyer (attached) and share our launch
    video.
</p>
<p>
    To find out more or apply, go to <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    Thank you for your support,
</p>
<p>
    Rob
</p>
<p>
    <strong>Robert S. Huckman</strong><br/>
    Albert J. Weatherhead III Professor of Business Administration<br/>
    Faculty Co-Chair, Forum on Health Care Innovation<br/>
    Harvard Business School<br/>
    435 Morgan Hall<br/>
    Boston, MA 02163<br/>
    Phone: 617.495.6649
</p>
<p>
    <em></em>
</p>
<p>
    <em>***********</em>
</p>
<p>
    <strong><u></u></strong>
</p>
<p>
    <strong><u>What is your health care innovation?</u></strong>
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the Health Acceleration Challenge to award $150,000 to proven health care delivery
innovations ready to scale. Join the conversation and apply by September 29 at    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    <em>[Attachment: Challenge flyer]</em>
</p>
<p>
	<a name="Unsubscribe_1" xt="SPONECLICKOPTOUT" href="#SPONECLICKOPTOUT" target="_blank">Unsubscribe</a>
</p>
<h1>
    
    <a name="_Toc395865214">12. Challenge Launch Joe Tuesday 19 – from Joe</a>
</h1>
<table border="0" cellspacing="0" cellpadding="0" width="722">
    <tbody>
        <tr>
            <td width="378" nowrap="" valign="bottom">
                <p>
                    hci-programs.prizes.hac.fhi14-q1.email.08 - 12-applicants - joe
                </p>
            </td>
            <td width="344" nowrap="" valign="bottom">
                <p>
                    <u><a href="https://na15.salesforce.com/701i0000000sj81AAA">https://na15.salesforce.com/701i0000000sj81AAA</a></u>
                </p>
            </td>
        </tr>
    </tbody>
</table>
<p>
    From: Newhouse, Joseph Paul (newhouse@hcp.med.harvard.edu)
</p>
<p>
    Subject: Health Acceleration Challenge Launch
</p>
<p>
    Dear %%Preferred Name%%,
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Health Acceleration Challenge</a> seeking innovations that have demonstrated value
    for US health care delivery and are ready to scale rapidly.
</p>
<p>
    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Applications</a>
    are open to anyone with a great idea and due September 29<sup>th</sup>. Our finalists share $150,000 in prize money, meet senior health care leaders at our
    April 2015 conference and have an HBS case written about them. All applicants benefit from crowd-sourced feedback on our application platform.
</p>
<p>
    Please help us spread the word. Email innovators, include us in your newsletter (see below), forward our Challenge flyer (attached) and share our launch
    video.
</p>
<p>
    To find out more or apply, go to <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    Thank you for your support,
</p>
<p>
    Joe
</p>
<p>
    <strong>Joseph Newhouse, PhD</strong><br/>
    John D. MacArthur Professor of Health Policy and Management<br/>
    Faculty Co-Chair, Forum on Health Care Innovation<br/>
    Harvard Medical School<br/>
    180 Longwood Ave.<br/>
    Boston, MA 02115<br/>
    Phone: 617.432.1325
</p>
<p>
    <em></em>
</p>
<p>
    <em>***********</em>
</p>
<p>
    <strong><u></u></strong>
</p>
<p>
    <strong><u>What is your health care innovation?</u></strong>
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the Health Acceleration Challenge to award $150,000 to proven health care delivery
innovations ready to scale. Join the conversation and apply by September 29 at    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    <em>[Attachment: Challenge flyer]</em>
</p>
<p>
	<a name="Unsubscribe_1" xt="SPONECLICKOPTOUT" href="#SPONECLICKOPTOUT" target="_blank">Unsubscribe</a>
</p>
<h1>
    
    <a name="_Toc395865215">Email to HBR Advisory Members– Tuesday 12th – from Cara</a>
</h1>
<p>
    From: Sterling, Cara (csterling@hbs.edu)
</p>
<p>
    Subject: Help Promote the Health Acceleration Challenge
</p>
<p>
    <strong></strong>
</p>
<p>
    Steve and Gardiner -
</p>
<p>
    I am pleased to tell you that we successfully launched the <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Health Acceleration Challenge</a> to the public yesterday. As we discussed, could you please
    distribute this information to your HBR Advisory Board?
</p>
<p>
    As you remember, we are seeking innovations that have demonstrated value for US health care delivery and are ready to scale rapidly. We are not a business
    plan contest but a scale up competition!
</p>
<p>
    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Applications</a>
    are open to anyone and due September 29<sup>th</sup>. Our finalists share $150,000 in prize money, meet senior health care leaders at the Forum on Health
    Care Innovation’s conference next April and have an HBS case written about them. All applicants benefit from crowd-sourced feedback on our application
    platform.
</p>
<p>
    We would like to ask your Advisory Board members to spread the word using the text below and Challenge flyer. For example:
</p>
<ul><li>
Share this information with innovators
</li>
<li>
Activate intermediaries such as health care investors, hospital executives, accelerators, etc.
</li>
<li>
Use Twitter or LinkedIn to share our launch video (<a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.healthaccelerationchallenge.com</a>)
</li>
<li>
Include us in your organization’s newsletter or send an email with our Challenge flyer
</li></ul>
<p>
    Let me know if you need anything else!
</p>
<p>
    Best,
</p>
<p>
    Cara Sterling
</p>
<p>
    <strong>Cara Sterling</strong><br/>
    Director, Healthcare Initiative<br/>
    Harvard Business School<br/>
    Soldiers Field<br/>
    Boston, MA 02163<br/>
    Phone: 617.495.6126
</p>
<p>
    <em></em>
</p>
<p>
    <em>***********</em>
</p>
<p>
    <em>To help spread the word, please feel free to use: </em>
</p>
<p>
    <strong><u></u></strong>
</p>
<p>
    <strong><u>What is your health care innovation?</u></strong>
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the Health Acceleration Challenge to award $150,000 to proven health care delivery
innovations ready to scale. Join the conversation and apply by September 29 at    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    <em>[Attachment: Challenge flyer]</em>
</p>
<p>
    <em>Myer Tracking Link</em>
    :
</p>
<p>
    <u>http://healthaccelerationchallenge.com?utm_source=hbradvisoryboard&amp;utm_medium=email&amp;utm_campaign=HAC+launch</u>
    <u></u>
</p>
<p>
	<a name="Unsubscribe_1" xt="SPONECLICKOPTOUT" href="#SPONECLICKOPTOUT" target="_blank">Unsubscribe</a>
</p>
<h1>
    
    <a name="_Toc395865216">Accelerator List – August 19th</a>
</h1>
<p>
    From: Klaus Koenigshausen
</p>
<p>
    Subject: Scale Your Portfolio Companies
</p>
<p>
    Dear %%Preferred Name%%,
</p>
<p>
    Harvard Business School and Harvard Medical School are launching the <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Health Acceleration Challenge</a> seeking innovations that have demonstrated value
    for US health care delivery and are ready to scale rapidly.
</p>
<p>
    <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">Applications</a>
    are open to anyone and due September 29<sup>th</sup>. Our finalists share $150,000 in prize money, meet senior health care leaders at our invite-only April
    2015 conference and have an HBS case written about them. All applicants benefit from crowd-sourced feedback on our application platform.
</p>
<p>
    Please forward our Challenge flyer to your portfolio companies and to other innovators that you think should apply!
</p>
<p>
    To find out more and apply, go to <a href="http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%">www.HealthAccelerationChallenge.com</a>.
</p>
<p>
    Best regards,
</p>
<p>
    Klaus
</p>
<p>
    <em>[Attachment: Challenge flyer]</em>
</p>
<p>
    <em>Myer Tracking Link</em>
    :
</p>
<p>
    <a
        href="https://www.google.com/url?q=http://healthaccelerationchallenge.com/%23utm_campaign%3Dhac_launch%26utm_source%3Daccelerators%26utm_medium%3Demail&amp;usd=2&amp;usg=ALhdy2_HMMx6mrflKUc55LsCXkNbetbbTQ"
        target="_blank"
    >
        http://healthaccelerationchallenge.com/#utm_campaign=hac_launch&amp;utm_source=accelerators&amp;utm_medium=email
    </a>
</p>
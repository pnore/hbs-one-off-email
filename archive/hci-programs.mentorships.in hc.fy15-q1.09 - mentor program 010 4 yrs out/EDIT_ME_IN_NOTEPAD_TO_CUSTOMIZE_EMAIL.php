<?php
///////////////////////// google analytics: (* = required) 
///////////////////////// for fhi only //////////////////////
$use_google_analytics = true;

// an instance of this string in a url will be replaced by the google analytics code
$ga_replace = '%%GOOGLE_ANALYTICS%%';

//  Campaign Source* (referrer: google, yahoo, newsletter_0602)
$ga_source = 'blogpost-2013-10';

// Campaign Medium* (marketing medium: cpc, banner, email)
$ga_medium = 'email';

// Campaign Name* (product, promo code, or slogan)
$ga_name = "Let's open the licenses and compete on solutions";

// Campaign Term (keywords)
$ga_term = '';

// Campaign Content* (use to differentiate ads)
$ga_content = '';
///////////////////////////////////////////////////////////////

// open survey link (does not require login)
$survey_link = "https://secure.hbs.edu/poll/open/pollTakerOpen.jsp?poll=134402";
$main_link_url = "http://projects.iq.harvard.edu/forum-on-healthcare-innovation/blog/improving-quality-care-united-states?%%GOOGLE_ANALYTICS%%";

// open survey link (does not require login)
$survey_link = "https://secure.hbs.edu/poll/open/pollTakerOpen.jsp?poll=134402";

/**
 * Edit the values below to customize the email. Each value has a variable
 * name that begins with a dollar sign on the left, followed by an equals sign, 
 * followed by the value in quotation marks. Every assignment statement should end with a semicolon. 
 */
 
$email_subject = "[HBS Health Care] Mentor an MBA Student";

/**
 * This is the text that should appear in the email preview pane after the subject. 
 * See also: goo.gl/IJaxHy and goo.gl/hf4h6P
 */
$email_preheader = "Are you willing to share your Business and Environment industry expertise?";

/**
 * If true, the header image will show. If false, it will not. 
 */
$show_header_image = true;

	/**
	 * This is a list of possible header images for the email. 
	 * Each image has a name on the left inside single quotes,
	 * and a url on the right inside single quotes with a homebrew 
	 * double arrow pointing from the name to the url like this: 
	 * name => url
	 * URLs should be separated by commas. 
	 */
	$possible_header_images = array(
		"health care event" => "http://www.hbs.edu/healthcare/images/photos/health-care-event-topbanner.gif",
		"welcome!" => "http://www.hbs.edu/healthcare/images/photos/welcome-topbanner-472x102.gif",
		"mentor program orange" => "http://www.hbs.edu/healthcare/images/photos/mentor-program-topbanner_472x102_orange.png",
		"mentor program blue" => 
"http://www.hbs.edu/healthcare/images/photos/mentor-program-topbanner_472x102_blue.gif"
	);
	
	/**
	 * This should be a valid name of a header image defined in the $possible_header_images array.
	 */
	$header_image_name = "mentor program blue";

$possible_departments = array(
	"hci" => array(
		"url" => 'http://www.hbs.edu/healthcare/',
		"email" => 'mailto:healthcare_initiative@hbs.edu',
		"image" => "http://www.hbs.edu/healthcare/images/photos/health-care-initiative-logo-x000-470x76.png",
		"topmessage" => "You are receiving this update because you have expressed an interest in health care activities at HBS."
	), 
	"bei" => array(
		"url" => 'http://www.hbs.edu/environment/',
		"email" => 'bei@hbs.edu',
		"image" => 'http://www.hbs.edu/healthcare/images/photos/business-and-environment-logo-470x76.png',
		// You are receiving this update because you have participated in the BEI mentor program in the past.
		// You are receiving this update because you are a member of the Green Business Alumni Association, which has partnered with HBS on this alumni mentor opportunity.
		"topmessage" => "%%top_phrase%%"
	)
);

$department = "hci";

/**
 * This is the date phrase that will appear at the top right of the email.
 */
$header_date_phrase = "TUE SEP 09";

/**
 * The following options concern an optional top description area in the body. 
 * The top description area contains a picture, title, date, and calendar button. 
 */
$show_body_topdescription_row = false;
	$body_topdescription_title = "Health Care Welcome Reception";
	$body_topdescription_date = "Thursday, September 12, 2013";
	$body_topdescription_time = "3:30pm - 5:00pm";
	$body_topdescription_location = "Gallatin Lounge";
	$show_body_topdescription_download_calendar_event_button = false;

/**
 * This button appears at the bottom of the email. The button is just a link to a URL. 
 */
$show_bottom_button = true;
	$bottom_button_text = "Sign Up";
	$bottom_button_url = "$survey_link";

/**
 * Every link in the links array has a name on the left and a URL on the right. 
 * Every occurance of the phrase on the left will be made into a link to the right. 
 * Between link lines there should be a comma. For mailto links, use http://goo.gl/0LduFn
 */
$links = array(
	"contact the HBS Business and Environment Initiative" => "mailto:bei%40hbs.edu?subject=Business%20and%20Environment%20Initiative%20Mentor%20Program&cc=pnore%40hbs.edu"
);

$possible_colors = array(
	"mint" => "#c4e4dd",
	"white" => "#ffffff",
	"black" => "#000000",
	"darkblue" => "#0D667F",
	"peach" => "#fcd4a1",
	"crimson" => "#a41034"
);

$use_excel = false;

$background_color = "darkblue";
$body_background_color = "white";
$default_text_color = "black";
$heading_text_color = "darkblue";
$highlight_text_color = "crimson";
$header_date_color = "white";
$footer_background_color = "white";
$button_text_color = "white";
$topheader_color = "white";

$template_name = "one-off";

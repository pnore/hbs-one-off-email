<?php 
// MAILING INFO
$title = "FY15-q4 04 student newsletter";
$campaignName = "hci-programs.newsletters.student.fy15-q4.email.04 - 01 to students dist fac";
$campaignUrl = "https://na15.salesforce.com/701i0000001R5Eb/e?retURL=%2F701i0000001R5EbAAK";
$templateUrl = "";
$from = null;
$sig = null;
$subject = "";
$containsFirstName = false;
$relationalTableQuery = "";

/**************  BEGIN EMAIL HEADER ******************
	This section gets echoed into the source code of the email
	but is not visible to the viewer of the email. It can be used 
	to quickly locate the exact email that was sent. 
*/
?>

<!--
# <?php echo $title ?>  
**Campaign:** <?php echo $campaignName ?>       
**Campaign Link:** <?php echo $campaignUrl ?>  
**From:** <?php echo $from; ?>
**Subject:** <?php echo $subject; ?> 
-->

<?php
/**************  BEGIN EMAIL BODY ******************/
?>

<?php section("Health Care Announcements"); ?>

## HIMSS Reception in Chicago 
<?php
beginTable();
left("Sun Apr 12   
<span class='time'>6:00 pm -- 9:00 pm</span>   
<span class='place'>Chicago, IL -- [Zed451](http://www.zed451.com/)</span>");


right("<img src='http://www.hbs.edu/healthcare/PublishingImages/thumbnails/hims-convention.png' alt='HIMSS logo'/>  
	Network with Chicago alumni as well as those traveling to the Windy City to attend the annual HIMSS Conference. We've heard that quite a few people will be participating in the pre-event workshop at McCormick place or flying into Chicago Sunday afternoon and we wanted to provide the opportunity to mix and mingle. There is no cost to attend." 
	. button("RSVP -- HIMSS", "http://www.hbshealthalumni.org/store.html?event_id=949"));

endTable();
?>  

## Alumni and Student Mixer at Park Restaurant in Harvard Square
<?php
beginTable();

left("Thu Apr 16   
<span class='time'>6:00 pm -- 9:00 pm</span>   
<span class='place'>Harvard Square: Park Restaurant and Bar     
59 JFK St,   
Cambridge, MA 02138  
</span>");


right("You are invited to attend the annual health care mixer organized by HBS Health Care Club students, the HBS Health Care Initiative and the Healthcare Alumni Association. We had a great turn out last year and we are anticipating an lively evening this year!  There is no cost to attend, but please [RSVP](http://www.hbshealthalumni.org/store.html?show_item=1913) so we can plan accordingly. We look forward to seeing you there! " 
	. button("RSVP -- Mixer", "http://www.hbshealthalumni.org/store.html?show_item=1913"));

endTable();
?>  

## Time to Reconnect through the Student-Alumni Mentor Program
Schedule some time to connect with your mentee during the upcoming spring interview season. We highly recommend reading [The Art of Giving and Receiving Advice](https://hbr.org/2015/01/the-art-of-giving-and-receiving-advice) by David A. Garvin and Joshua D. Margolis from the January 2015 issue of HBR for tips on how to help your mentee reach their potential. If you have any questions about mentoring, please email [healthcare_initiative@hbs.edu](mailto:healthcare_initiative@hbs.edu?subject=Health%20Care%20Mentor%20Program). 

## Sign up for the Innovating in Health Care MOOC with Regina Herzlinger 
Problems with health care quality, access, and costs bedevil all countries. [Innovating in Health Care](https://www.edx.org/course/innovating-health-care-harvardx-bus5-1#.VRBdFPnF_dg) is an online course that explores how to create successful global business innovations in health care that can better meet consumer and social needs. Upon conclusion, you will learn to evaluate opportunities and gain an understanding of the elements necessary for viable health care business models.  Through case study analysis and application of the "Innovating in Health Care Framework," this course focuses on evaluating and crafting health care startup business models that focus on the six factors that critically shape new health care ventures.  The course begins May 5, 2015 and runs for 9 weeks.

## Health Care Entrepreneur in Residence and Mentor Help Needed
Interested in being a mentor/entrepreneur in residence to help bridge health care's innovation education gap?  If so, please send your resume with your specific interest, location, and availability to Prof. Regina Herzlinger, [rherzlinger@hbs.edu](mailto:rherzlinger@hbs.edu?subject=Health%20Care%20Entrepreneur%20in%20Residence). You can read more about this call-to-action in the HBR article, [Bridging Health Care's Innovation-Education Gap](https://hbr.org/2014/11/bridging-health-cares-innovation-education-gap) and the [Health Affairs blog](http://healthaffairs.org/blog/2015/01/29/innovation-in-health-care-education-a-call-to-action/).  

Prof. Herzlinger will post it on the web site of her HarvardX MOOC, [Innovating in Health Care](https://www.edx.org/course/innovating-health-care-harvardx-bus5-1x#.VN4jXvnF98E), which attracted 20,000 participants in its first year of airing, and [GENiE](http://www.thegeniegroup.org/), the organization she formed as a Global Educators Network for Innovating Health Care Education.  In its two years of existence, GENiE has helped spur the adoption of an Innovating in Health Care degree program or course of study in dozens of schools and online education sites. 

The MOOC's thousands of participants, who are urged to create a business plan for an innovative venture, and GENiE's hundreds of member academics urgently need your help as mentors/entrepreneurs in residence to help advance this important movement.

Don't miss Professor Herzlinger's newest *New England Journal of Medicine Article*, [Market-Based Solutions to Antitrust Threats — The Rejection of the Partners Settlement](http://www.nejm.org/doi/full/10.1056/NEJMp1501782).

## Blavatnik Fellowship Updates

<?php 
beginTable();

left("
### Subscribe to New Newsletter
To learn more about the Fellowship, **[view the spring Blavatnik Quarterly Newsletter](http://healthcareinitiative.mkt1960.com/blavatnik-newsletter)** pictured here and [email Tracy Perry to sign up](mailto:tperry@hbs.edu?subject=Subscribe%20to%20Blavatnik%20Quarterly%20Newsletter) to receive future editions.  

### Fellow Reaches HBS NVC Finals 

", "450px", "normal");
right("[![blav-newsletter-preview][]](http://healthcareinitiative.mkt1960.com/blavatnik-newsletter)", "130px", "normal");

endTable();
?>

Congratulations to Alexandra Dickson, Blavatnik Fellow ’14, on reaching the finals of 
the HBS Alumni New Venture Competition.  In a new twist, there are three competition 
rounds--- Most Innovative, Greatest Impact, and Best Investment.  For Most Innovative 
and Greatest Impact, students and alumni choose the winner.  To view the finalist’s videos 
and vote, please follow [this link](https://www.alumni.hbs.edu/events/nvc/Pages/default.aspx).





<?php section("Health Care Events"); ?>

Below is a listing of upcoming events including HBS happenings, alumni events, upcoming opportunity deadlines and conferences at which the Health Care Initiative is hosting a reception.

<?php 
beginTable();

left("Fri Apr 03   
<span class='time'>12:00 pm -- 1:30 pm</span>   
 <span class='place'>HBS, Baker Library, Bloomberg 102</span>");
right("[Science-Based Business Seminar: Cubist - Battling Superbugs: The Intersection of Science and Policy. Mike Bonney](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=117)  
Speaker: Mike Bonney, Cubist Pharmaceuticals. Cubist - Battling Superbugs: The Intersection of Science and Policy. Cubist, the world’s largest company focused on the discovery, development and commercialization of antibiotics with activity against highly resistant bacteria, located in Lexington MA, was recently acquired by Merck.  Michael Bonney, the CEO of the company from 2003 will discuss the development of the company and the current state of R&D in this critical area of public health.  Particular focus will be paid to the inherent challenges of attracting capital to support the investment in R&D given the appropriate use of antibiotics and the resultant international public health crisis as bacteria continue to develop resistance and the investment in R&D has dropped precipitously over the past 20-30 years.  *Hosted by Harvard Business School.*  *Open to the HBS community.*");

left("Thu Apr 09   
<span class='time'>3:30 pm -- 5:00 pm</span>   
 <span class='place'>TBD</span>");
right("[Care Delivery Sessions with Dr. Thomas Feeley: Regional and National Expansion Strategies – Zach Landman, Vanderbilt](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=148)  
All MBA students interested in Care Delivery are invited to participate in a series of sessions organized by Dr. Thomas Feeley, Senior Fellow, Institute for Strategy and Competitiveness.  To register for any or all, please contact Melissa Higdon at mhigdon@hbs.edu. Upcoming sessions: 
Thursday April 9, 3:30-5 PM, Regional and National Expansion Strategies with Zach Landman, Vanderbilt; Wednesday April 15, 3:30-5 PM, Future Leadership in Healthcare with Dr. Eleanor Herriman, Cleveland Clinic, Dr. Mark Kelley, and Tom Feeley discussants.  
 *Hosted by Harvard Business School.*  *Open to all HBS students.*");

left("Fri Apr 10   
<span class='time'>12:00 pm -- 1:30 pm</span>   
 <span class='place'>HBS, Baker Library, Bloomberg 102</span>");
right("[Science-Based Business Seminar](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=158)  
Speaker: Jim Manzi, Chairman, Applied Predictive Technologies. The Experimental Revolution in Business. *Hosted by Harvard Business School.*  *Open to the HBS community.*");

left("Wed Apr 15   
<span class='time'>3:30 pm -- 5:00 pm</span>   
 <span class='place'>TBD</span>");
right("[Care Delivery Sessions with Dr. Thomas Feeley: Future Leadership in Healthcare – Dr. Eleanor Herriman, Cleveland Clinic](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=149)  
All MBA students interested in Care Delivery are invited to participate in a series of sessions organized by Dr. Thomas Feeley, Senior Fellow, Institute for Strategy and Competitiveness.  To register for any or all, please contact Melissa Higdon at mhigdon@hbs.edu. Upcoming sessions: 
Wednesday April 15, 3:30-5 PM, Future Leadership in Healthcare with Dr. Eleanor Herriman, Cleveland Clinic, Dr. Mark Kelley, and Tom Feeley discussants.  
 *Hosted by Harvard Business School.*  *Open to all HBS students.*");

left("Thu Apr 16   
<span class='time'>6:00 pm -- 9:00 pm</span>   
 <span class='place'>Cambridge</span>");
right("[2015 HBS Student & Alumni Mixer](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=163)  
See friends and make connections. You are invited to attend the annual health care mixer organized by HBS Health Care Club students, the HBS Health Care Initiative and the Healthcare Alumni Association. We had a great turn out last year and we are anticipating another lively evening! Please RSVP so we can plan accordingly.  *Hosted by HBS Health Care Initiative, HBS Health Care Alumni Association, HBS Student Health Care Club.*  *Open to all HBS students.*");

left("Fri Apr 17   
<span class='time'>12:00 pm -- 1:30 pm</span>   
 <span class='place'>HBS, Baker Library, Bloomberg 102</span>");
right("[Science-Based Business Seminar](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=160)  
Speaker: Timothy K. Lu, MD, PhD, MIT Synthetic Biology Center, Co-founder, Synlogic, Inc., Co-Founder, Sample6, Inc. Synthetic Biology for Real-World Applications. *Hosted by Harvard Business School.*  *Open to the HBS community.*");

left("Fri Apr 24   
<span class='time'>12:00 pm -- 1:30 pm</span>   
 <span class='place'>HBS, Baker Library, Bloomberg 102</span>");
right("[Science-Based Business Seminar](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=161)  
Speaker: Adam Isen, US Dept of the Treasury. The Effect of High-Skilled Immigration on Patenting and Employment: Evidence from H-1B Visa Lotteries. *Hosted by Harvard Business School.*  *Open to the HBS community.*");



endTable();

?>

This listing may not include all health care recruiting events; please be sure to check the [MBA Event Calendar](http://beech.hbs.edu/eventCalendar/user/mainCalendar.do) for more company presentations. Also, these events can be downloaded at the Health Care Initiative event calendar [here](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx).

<?php 
section("Selected Health Care Events Around Harvard University and Boston"); 

beginTable();


left("Thu Apr 02   
<span class='time'>7:30 am -- 10:00 am</span>   
 <span class='place'>Mass Medical Society</span>");
right("[MassMEDIC Women in MedTech Forum: Disruptive Technologies in Medicine Kick-Off Event](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=138)  
Special Guest: Robin Strongin, President & CEO of Amplify Public Affairs (a woman-owned small business), and Creator & Founder of the award winning Disruptive Women in Health Care.  *Hosted by Mass Medical Society.* *Open to the public.*");

left("Mon Apr 06   
<span class='time'>5:00 pm -- 7:00 pm</span>   
 <span class='place'>HSPH, Griswold Hall, Room 110</span>");
right("[Health Law Workshop: Medicare Advantage, Accountable Care Organizations, and Traditional Medicare: Synchronization or Collision?](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=126)  
Speaker: Thomas Greaney, Chester A. Myers Professor of Law and Co-Director of the Center for Health Law Studies, St. Louis University School of Law. Medicare Advantage, Accountable Care Organizations, and Traditional Medicare: Synchronization or Collision? *Hosted by Harvard Law School Petrie Flom Center.*  *Open to the public.*");

left("Tue Apr 07   
<span class='time'>10:00 am -- 11:00 am</span>   
 <span class='place'>Dana-Farber, Yawkey Conference Center, 306/307</span>");
right("[2015 Cancer Disparities Research Symposium](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=165)  
Speaker: Dr. Francesca Gany, MD, MS, Chief of the Immigrant Health and Cancer Disparities Service at Memorial Sloan Kettering Cancer Center, the Director of the Center for Immigrant Health and Cancer Disparities, and a Director of the community based participatory South Asian Health Initiative.  *Hosted by Dana Farber / Harvard Cancer Center.* *Open to the public.*");

left("Wed Apr 08   
<span class='time'>1:30 pm -- 6:30 pm</span>   
 <span class='place'>Broad Institute, Auditorium</span>");
right("[Xconomy Forum: What's Hot in Boston Biotech](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=173)  
Life sciences innovation is moving fast these days, and perhaps nowhere is this more evident than in Boston. Local companies, researchers, and entrepreneurs are pursuing some of the boldest ideas in the field—T-cell therapy, gene therapy, RNA interference, and microbiome analysis, among others. What's more, they're seeing real results, in human trials, that indicate these approaches might soon change the way a number of diseases are treated. But a whole host of hurdles still have to be cleared. How can we avoid the pitfalls of the past, and turn these innovative visions into a reality? What’s it going to take to get new types of treatments approved? And how will we pay for them? Join Xconomy as they bring together a group of Boston biotech’s top innovators, entrepreneurs, investors, and leaders as they explore the brightest spots on the life sciences horizon. *Hosted by Broad Institute.*  *Open to the Harvard community.*");

left("Wed Apr 08   
<span class='time'>4:00 pm -- 4:00 pm</span>   
 <span class='place'>HKS, Taubman Building Room 102, Women and Public Policy Program Cason Conference</span>");
right("[Healthcare.gov: A Case Study with Nick Sinai and Ryan Panchadsaram](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=169)  
The failures and subsequent rescue of Healthcare.gov is a compelling story in its own right. But it also speaks to the failures of government digital services and government IT more broadly. By one study, over 90 percent of government IT projects underperform or fail outright. What lessons can be learned by the failure and rescue of Healthcare.gov? How did it inform the creation of the new U.S. Digital Services in the White House and the formation of the 18F digital service unit in the General Services Administration? What does it mean for procurement and government IT reform? Ryan Panchadsaram, U.S. Deputy Chief Technology Officer will answer these questions. Seminars are for students only (graduate and undergraduate), not-for-credit, and will be restricted to 40 attendees. Please register online to reserve your space. *Hosted by Harvard Kennedy School.*  *Open to the Harvard community.*");

left("Thu Apr 09   
<span class='time'>2:30 pm -- 3:30 pm</span>   
 <span class='place'>HMS, Benjamin Waterhouse Room, Gordon Hall.</span>");
right("[Learning Healthcare at Scale (or how 74,000 patients in 74 ICUs changed infection prevention in two years)](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=171)  
The Department of Population Medicine will host Jonathan Perlin, MD, keynote speaker at the 2015 Thomas S. Inui Lecture. Space is limited, so please RSVP to Meg Powers. *Hosted by Harvard Medical School.*  *Open to the Harvard community.*");

left("Thu Apr 09   
<span class='time'>5:30 pm -- 8:30 pm</span>   
 <span class='place'>HBS, Hawes 101</span>");
right("[The Business of the Brain, Part II](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=154)  
This is part II of a very special program co-hosted with The Harvard Stem Cell Institute (HSCI), Harvard NeuroDiscovery Center and the HBSAB Healthcare Shared Interest Group (SIG). The Harvard community and its affiliates are doing tremendous work uncovering the mysteries of the brain.  This year’s Business of the Brain 2.0 highlights that work, and builds on last year’s program with another wonderful panel to provide unique perspectives on the future of brain research, with a specific focus on approaches to accelerate treatments and cures for brain related conditions and diseases. The panel will be moderated by HBS Professor William Sahlman. *Hosted by HBS Health Care Alumni Association, HBS Association of Boston.*  *Open to HBS Alumni.*");

left("Fri Apr 10   
<span class='time'>8:00 am -- 10:30 am</span>   
 <span class='place'>Mass Medical Society</span>");
right("[Designing for Human Behavior - Developing Medical Devices for Regulatory Approvals and Market Adoption](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=172)  
The session will deliver an overview of human factors requirements and successful methodologies for exploration of user needs and desires, including how these can be integrated within the medical device development process. This overview will be followed by case studies that explore how this development process has been employed in two different categories of medical devices: consumer health and drug delivery.  *Hosted by MassMEDIC.* *Open to the public.*");

left("Mon Apr 13   
<span class='time'>12:30 pm -- 1:30 pm</span>   
 <span class='place'>HSPH, FXB G13</span>");
right("[Addressing Ebola: A Series on the 2014 Outbreak](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=166)  
Speaker: Rupa Kanapathipillai, MPH, DTM&H, Editorial Fellow at New England Journal of Medicine, Infectious Disease Physician. On the Ground Reality of Practice and Patient Treatment. *Hosted by Harvard School of Public Health.*  *Open to the Harvard community.*");

left("Mon Apr 13   
<span class='time'>5:00 pm -- 7:00 pm</span>   
 <span class='place'>HSPH, Griswold Hall, Room 110</span>");
right("[Health Law Workshop: Prescription Drug Reimbursement as Innovation Incentive.](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=127)  
Speaker: Rachel Sachs, Academic Fellow, Petrie-Flom Center. Prescription Drug Reimbursement as Innovation Incentive. *Hosted by Harvard Law School Petrie Flom Center.*  *Open to the public.*");

left("Tue Apr 14   
<span class='time'>12:00 pm -- 1:00 pm</span>   
 <span class='place'>Virtual</span>");
right("[Consumerism in Healthcare](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=120)  
Professor Robert Huckman, the faculty co-chair of the Harvard Business School Healthcare Initiative, will discuss what it takes for consumer health to succeed in the current healthcare environment. He will draw on his research on the topic to focus on the structural changes occurring in the healthcare system that are driving increased consumerism and will help explain the opportunities and challenges facing the rise of consumerism in health.  *Hosted by HBS Health Care Alumni Association.*  *Open to the HBS community.*");

left("Tue Apr 14   
<span class='time'>12:00 pm -- 1:00 pm</span>   
 <span class='place'>HSPH, Griswold Hall, Room 110</span>");
right("[FDA's Impact on Pharmaceutical Innovation: A lecture by Neil Flanzraich](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=128)  
Speaker: Neil Flanzraich, Executive Chairman, Kirax Corporation and the Executive Chairman, ParinGenix. He was also appointed by Dean Martha Minow as an Expert in Residence at the Harvard Innovation Lab. *Hosted by Harvard Law School.*  *Open to the public.*");

left("Tue Apr 14   
<span class='time'>6:00 pm -- 7:00 pm</span>   
 <span class='place'>Harvard Innovation Lab</span>");
right("[User Research Seminar for Medical Device Startups](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=174)  
The importance of user research cannot be underestimated, but how to integrate user research activities within a typical product development process is equally important. This workshop will outline tangible user research activities with structured goals and deliverable outcomes that can dovetail with the evolving needs of the product development. We will address common questions from medical device entrepreneurs, including how to determine what research methods to use, how to make data actionable and prioritized, and what makes user research in the field of healthcare different.
 *Hosted by the Harvard iLab.*  *Open to the public.*");

left("Mon Apr 27   
<span class='time'>8:00 am -- 5:00 pm</span>   
 <span class='place'>Westin Copley Place Hotel</span>");
right("[World Medical Innovation Forum](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=175)  
The World Medical Innovation Forum features CEOs of industry-leading multinational medical companies, Partners HealthCare top clinicians and scientists—all Harvard Medical School faculty members—and global technology leaders. Join 1,000 international innovation decision-makers and be briefed on the state of the art in neuroscience technology, care and research directly from the CEOs bringing game-changing products to market, world-class clinicians delivering the highest-quality care and the scientists rapidly propelling this crucial field forward through their CNS insights and discoveries.  *Hosted by World Medical Innovation Forum.* *Open to the public.*");

endTable();

?>

These events can be downloaded at the Health Care Initiative event calendar [here](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx).

<?php section("HBS Health Care In the News"); ?>

[Company Thinks it Has Answer for Lower Health Costs: Customer Service](http://www.nytimes.com/2015/03/29/upshot/small-company-has-plan-to-provide-primary-care-for-the-masses.html?abt=0002&abg=1)  
Re: Frances Frei  
*New York Times*  
Mar 27, 2015

[Who Are the World''s Best Leaders?](http://fortune.com/2015/03/26/best-leaders-employee-well-being/)  
Re: Joel Goh  
*Fortune*  
Mar 26, 2015

[NextGen Jane Launches Pre-Sale After Three-City Road Show](http://www.nextgenjane.com/agency)  
Re: Ridhi Tariyal, Blavatnik Fellow '13  
*nextgenjane.com*  
Mar 24, 2015  

[Paying More for Comparable Outcomes in Prostate Treatment](http://www.dotmed.com/news/story/25295)  
Re: Robert Steven Kaplan  
*DotMed Daily News*  
Mar 11, 2015

[Cost of Care for a Common Prostate Condition Measured for the First Time](http://www.sciencedaily.com/releases/2015/03/150310143927.htm)  
Re: Robert Steven Kaplan  
*ScienceDaily*  
Mar 10, 2015

[UCLA Researchers for the First Time Measure the Cost of Care for a Common Prostate Condition](http://www.newswise.com/articles/ucla-researchers-for-the-first-time-measure-the-cost-of-care-for-a-common-prostate-condition)  
Re: Robert Steven Kaplan  
*Newswise*  
Mar 10, 2015

[Innovation and Implementation in Cardiovascular Medicine](http://jama.jamanetwork.com/article.aspx?articleid=2190995)  
Re: Robert Huckman & Richard Hamermesh  
*Journal of the American Medical Association*  
Mar 10, 2015

[Twenty Team Finalists Named in Deans' Challenges](http://news.harvard.edu/gazette/story/2015/03/twenty-team-finalists-named-in-deans-challenges/?utm_source=Harvard+i-lab&utm_campaign=ce759c8cd4-3%2F11%2F15&utm_medium=email&utm_term=0_f2b82a2fec-ce759c8cd4-281914273)  
Re: Nitin Nohria, Alexandra Dickson and Meridith Unger, Blavatnik Fellows '14  
*Harvard Gazette*  
Mar 9, 2015

[MBA Graduates' Tech 'start-Ups Add to innovation In Healthcare](http://www.businessbecause.com/news/mba-entrepreneurs/3156/mba-entrepreneurs-drive-innovation-in-healthcare)  
Re: Halle Tecco (MBA 2011)  
*Business Because*  
Mar 9, 2015

[Doximity: MD/MBA Launches Healthcare Revolution From Harvard Business School](http://poetsandquants.com/2015/03/02/doximity-mdmba-launches-healthcare-revolution-from-harvard/)  
Re: Nathan Gross (MBA 2011)  
*Poets & Quants*  
Mar 6, 2015

[Halting Hospital Mergers Not the Same as Boosting Competition](http://www.modernhealthcare.com/article/20150306/blog/150309916)  
Re: Regina Herzlinger  
*Modern Healthcare*  
Mar 6, 2015  

[In-Q-Tel Invests in 3D Printing Innovator Voxel8]  
Re: Dan Oliver, Blavatnik Fellow '13  
*Business Wire*  
Mar 5, 2015  

[Market-Based Solutions to Antitrust Threats — the Rejection of the Partners Settlement](http://www.nejm.org/doi/full/10.1056/NEJMp1501782?query=featured_home&)  
By: Regina Herzlinger  
*New England Journal of Medicine*  
Mar 4, 2015

[Challenging the Rate of Digital Health Care](http://www.laboratoryequipment.com/articles/2015/03/challenging-rate-digital-health-care)  
Re: Robert Huckman & Richard Hamermesh  
*Labratory Equipment*  
Mar 4, 2015  

[Macrolide Pharmaceuticals, Inc. Launches with $22 Million Series A Financing to Develop Novel Macrolide Antibiotics]  
Re: Steve Porter, Blavatnik Fellow '13  
*FierceBiotech*  
Mar 4, 2015  

[Milton Hospital Grows as Part of Beth Israel Deaconess Network](http://www.bostonglobe.com/business/2015/03/03/once-struggling-milton-hospital-grows-part-beth-israel-deaconess-network/H3nHMyKmtggHLmZCQ6J5FJ/story.html?event=event12)  
Re: Robert Huckman  
*Boston Globe*  
Mar 3, 2015

[Scholars and Students Unpack the Digital Business Revolution](http://hbswk.hbs.edu/item/7517.html)  
Re: Karim Lakhani & Marco Iansiti  
*HBS Working Knowledge*  
Feb 25, 2015

[Improving Patient Care](http://healthcare.utah.edu/healthfeed/postings/2015/02/021815_nyt.php)  
Re: Michael Porter  
*HealthFeed*  
Feb 18, 2015

[An Afternoon With Michael Porter](http://healthsciences.utah.edu/notes/postings/2015/02/021815_porter.lee.php#.VQiJ547F98H)  
Re: Michael Porter  
*Notes*  
Feb 17, 2015

[Business Strategist as Coach: Michael Porter on Building Health Care Champions](http://healthsciences.utah.edu/innovation/blog/2014/12/michael-porter-15.php)  
Re: Michael Porter  
*Algorithms for Innovation*  
Feb 13, 2015



<?php section("Recent Faculty Publications in Health Care"); ?>

'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=36929' target='_blank'>Cleveland Clinic: Growth Strategy 2014</a>'<br>Michael Porter<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48260' target='_blank'>DaVita HealthCare Partners and the Denver Public Schools: Creating Connections</a>'<br>John Kim and Christine  An<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=46020' target='_blank'>GenapSys: Business Models for the Genome</a>'<br>Richard Hamermesh, Joseph  Fuller, and Matthew  Preble<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48246' target='_blank'>Improving Melanoma Screening: MELA Sciences</a>'<br>Regina Herzlinger and Kevin  Schulman<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.nejm.org/doi/full/10.1056/NEJMp1501782' target='_blank'>Market-Based Solutions to Antitrust Threats — The Rejection of the Partners Settlement</a>'<br>Regina E. Herzlinger, D.B.A., Barak D. Richman, J.D., Ph.D., and Kevin A. Schulman, M.D.<br><em>New England Journal of Medicine</em><br><br>

'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=47595' target='_blank'>No Margin, No Mission? A Field Experiment on Incentives for Public Services Deli</a>'<br>B. Kelsey Jack, Oriana Bandiera and Nava Ashraf<br><em>Journal of Public Economics</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=45651' target='_blank'>PepsiCo, Profits, and Food: The Belt Tightens</a>'<br>Matthew Preble and Joseph L. Badaracco<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48341' target='_blank'>Social Business at Novartis: Arogya Parivar</a>'<br>Michael Porter<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=46887' target='_blank'>Vision 2020: Takeda and the Vaccine Business</a>'<br>John Quelch and Margaret  Rodriguez<br><em>Harvard Business School Case</em><br><br>


<?php 
/**************  END EMAIL BODY ******************/

/**************  BEGIN LINKS ******************/
?>


<?php // logos ?>
[blavatnik logo]: http://www.hbs.edu/healthcare/images/photos/blav-in-life-science-entrepreneurship.png
[blavatnik logo square]: http://www.hbs.edu/healthcare/PublishingImages/thumbnails/blav_sm_75x75.png
[exec ed]: http://www.hbshealthalumni.org/images/article_images/436.jpg

<?php // one-time images ?>


<?php // one time links for blavatnik fellowship ?>
[In-Q-Tel Invests in 3D Printing Innovator Voxel8]: http://www.businesswire.com/news/home/20150305005064/en/In-Q-Tel-Invests-3D-Printing-Innovator-Voxel8#.VQHXwvnF9fi
[Macrolide Pharmaceuticals, Inc. Launches with $22 Million Series A Financing to Develop Novel Macrolide Antibiotics]: http://www.fiercebiotech.com/press-releases/macrolide-pharmaceuticals-inc-launches-22-million-series-financing-develop
[pre-sale]: http://www.nextgenjane.com/agency?utm_source=Website+Subscribers&amp;utm_campaign=4c832633e0-March_2015&amp;utm_medium=email&amp;utm_term=0_a7226bcf7a-4c832633e0-216897193&amp;ct=t(Feb_20151_31_2015)
[Alexandra Dickson and Meridith Unger Named Finalists]: http://news.harvard.edu/gazette/story/2015/03/twenty-team-finalists-named-in-deans-challenges/?utm_source=Harvard+i-lab&amp;utm_campaign=ce759c8cd4-3%2F11%2F15&amp;utm_medium=email&amp;utm_term=0_f2b82a2fec-ce759c8cd4-300591361
[blav-newsletter-preview]: http://www.hbs.edu/healthcare/PublishingImages/thumbnails/blav-newsletter-130.png

<?php // one-time event links ?>
[ReSourcing Big Data: A Symposium & Collaboration Opportunity]: http://catalyst.harvard.edu/programs/reactor/
[US Secretary of Veterans Affairs Robert A. McDonald]: http://www.hbs.edu/healthcare/download-event.aspx?title=The+Leadership+Journey+From+Corporate+CEO+to+the+President%27s+Cabinet&dtstart=20150325T173000&dtend=20150325T190000&location=Spangler+Auditorium&description=%3cdiv+class%3d%22ExternalClass389B1DD15BC04CD6832229ACC5503205%22%3e%3cp%3eThe+HBS+Community+is+invited+to+hear+a+talk+by%c2%a0Robert+A.+McDonald%2c+U.S.+Secretary+of+Veterans+Affairs+and+former+CEO+of+Procter+%26amp%3b+Gamble%2c+with+an+introduction+by+Professor+Rosabeth+Moss+Kanter.%c2%a0He%c2%a0will+discuss%c2%a0his+journey+from+the+business+world+to+the+White+House.+The+event+will+take+place+on+March+25%2c%c2%a05%3a30+-+7%3a00+p.m.%2c+in+Spangler+Auditorium.%3c%2fp%3e%3c%2fdiv%3e&status=TENTATIVE&allday=false
[TOM Seminar: Critical Care in Hospitals: When to Introduce a Step Down Unit?]: http://www.hbs.edu/faculty/units/tom/Pages/events.aspx
[The Future of Innovation in Pharma and Biotech]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=92308&filter=month&viewDay=1&viewMonth=2&viewYear=2015
[2nd Annual State of Global Health Symposium]: http://www.hsph.harvard.edu/global-health-and-population/events/2nd-annual-state-of-global-health-symposium/
[The Business of the Brain, Part II]: http://www.hbsab.org/article.html?aid=950
[Reception at HIMSS 2015]: http://www.hbshealthalumni.org/events.html
[Consumerism in Healthcare]: http://www.hbshealthalumni.org/article.html?aid=946
[FDA's Impact on Pharmaceutical Innovation: Neil Flanzraich]: http://petrieflom.law.harvard.edu/events/details/fdas-impact-on-pharmaceutical-innovation
[Reception at BIO International Convention]: http://www.hbshealthalumni.org/article.html?aid=947


<?php // one-time links for in the news ?>
[Twenty Team Finalists Named in Deans' Challenges]: http://news.harvard.edu/gazette/story/2015/03/twenty-team-finalists-named-in-deans-challenges/?utm_source=Harvard+i-lab&utm_campaign=ce759c8cd4-3%2F11%2F15&utm_medium=email&utm_term=0_f2b82a2fec-ce759c8cd4-281914273
[Doximity: MD/MBA Launches Healthcare Revolution From Harvard Business School]: http://poetsandquants.com/2015/03/02/doximity-mdmba-launches-healthcare-revolution-from-harvard/
[Halting Hospital Mergers Not the Same as Boosting Competition]: http://www.modernhealthcare.com/article/20150306/blog/150309916
[Market-Based Solutions to Antitrust Threats -- the Rejection of the Partners Settlement]: http://www.nejm.org/doi/full/10.1056/NEJMp1501782?query=featured_home&
[Milton Hospital Grows as Part of Beth Israel Deaconess Network]: http://www.bostonglobe.com/business/2015/03/03/once-struggling-milton-hospital-grows-part-beth-israel-deaconess-network/H3nHMyKmtggHLmZCQ6J5FJ/story.html?event=event12
[Harvard Health Acceleration Challenge: Meet the Finalists]: https://medtechboston.medstro.com/harvard-health-acceleration-challenge-meet-the-finalists/
[Improving Patient Care]: http://healthcare.utah.edu/healthfeed/postings/2015/02/021815_nyt.php
[An Afternoon With Michael Porter]: http://healthsciences.utah.edu/notes/postings/2015/02/021815_porter.lee.php#.VQiJ547F98H
[Business Strategist as Coach: Michael Porter on Building Health Care Champions]: http://healthsciences.utah.edu/innovation/blog/2014/12/michael-porter-15.php
[Athenahealth Plans Growth in 2015 After Successful 2014]: http://www.bizjournals.com/boston/blog/health-care/2015/02/athenahealth-plans-growth-in-2015-after-successful.html
[Should Harvard Business School Hit Refresh?]: http://www.wsj.com/articles/should-harvard-business-school-hit-refresh-1423100330
[Founder of Oscar Health: 'A Lot of People in This Industry Are Just Evil']: http://www.forbes.com/sites/danmunro/2015/02/02/founder-of-oscar-health-a-lot-of-people-in-this-industry-are-just-evil/?utm_source=followingweekly&utm_medium=email&utm_campaign=20150202
[Innovation In Health Care Education: a Call to Action]: http://healthaffairs.org/blog/2015/01/29/innovation-in-health-care-education-a-call-to-action/
[Boost the Right Kind of Innovation]: http://www.modernhealthcare.com/article/20150124/MAGAZINE/301249984/boost-the-right-kind-of-innovation
[Soledad O'Brien, Paula Johnson, MD, and Amy Cuddy, Phd, Join together to Inspire Women to Lead Healthcare and Academic Medicine]: http://www.pressreleaserocket.net/soledad-obrien-paula-johnson-md-and-amy-cuddy-phd-join-together-to-inspire-women-to-lead-healthcare-and-academic-medicine/43900/
[Merck Exec Moves to Caremore]: http://www.modernhealthcare.com/article/20150116/NEWS/301169935
[Cms Administrator Marilyn Tavenner Is Stepping Down]: http://www.washingtonpost.com/blogs/wonkblog/wp/2015/01/16/cms-administrator-marilyn-tavenner-is-stepping-down/
[Andy Slavitt's Journey: From Contractor for Healthcare.Gov to Head of Cms]: http://www.bizjournals.com/bizjournals/washingtonbureau/2015/01/andy-slavitts-journey-from-contractor-to-cms-head.html
[Cambridge-Based Company Aims to Bridge Doctor-Patient Gap]: http://radioboston.wbur.org/2015/01/14/twine-health
[Merck's Ex-Innovation Chief Jain Heads to Anthem Subsidiary Caremore]: http://mobihealthnews.com/39704/mercks-ex-innovation-chief-jain-heads-to-anthem-subsidiary-caremore/
[Bidding for Blood, the Priceline Way]: http://www.marketplace.org/topics/business/bidding-blood
[Report: Don'T Let Integration Cause Physician Micromanagement]: http://www.healthcarefinancenews.com/news/report-dont-let-integration-cause-physician-micromanagement
[Healthcare Strategy 2015 -- Back to the Basics: 12 Key Thoughts]: http://www.beckershospitalreview.com/hospital-management-administration/healthcare-strategy-2015-back-to-the-basics-12-key-thoughts.html
[Better Path to Health Care Reform]: http://www.rutlandherald.com/article/20150108/OPINION04/701089906
[Wanted: the It-Enabled Health Professional]: https://www.linkedin.com/pulse/wanted-it-enabled-health-professional-sarah-murray
[Delivering Higher Value Care Means Spending More Time With Patients]: https://hbr.org/2014/12/delivering-higher-value-care-means-spending-more-time-with-patients
[The Future of Green China]: http://www.hbs.edu/news/articles/Pages/future-of-green-china.aspx
[Curbing the Cost of Cancer Care]: http://www.dallasnews.com/business/columnists/jim-landers/20141215-curbing-the-cost-of-cancer-care.ece


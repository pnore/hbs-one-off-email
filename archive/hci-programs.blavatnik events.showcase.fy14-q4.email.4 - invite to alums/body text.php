# Upcoming Events at Harvard Business School
<?php
beginTable();

left("[![blav fellows][]](http://www.hbs.edu/healthcare/images/photos/blav-pic-1.jpg)  
### **Wed May 21**
### 3:30 p.m. -- 5:30 p.m.
### Harvard Innovation Lab, HBS");
right("## Blavatnik Fellows: Achievements & Lessons in Life Science Entrepreneurship

The Blavatnik Key Advisory Board invites you to join the inaugural group of Blavatnik Fellows to discuss their experiences in the Fellowship and their project progress.  Prof. Vicki Sato, Faculty Chair of the Fellowship, will moderate a panel discussion and afterwards you will have the opportunity to talk one-on-one with the Fellows to learn more about their projects.  We would really value your insight and feedback to the Fellows. Please [RSVP here](mailto:tsaxton@hbs.edu?subject=Blavatnik%20Fellows%20Event).
");

left("[![sophie][]](http://www.roche.com/about_roche/management/executive_committee/executive_commitee-sophie-kornowski-bonnet.htm)  
### **Thu May 8**
### 4:00 p.m. -- 6:00 p.m.
### Meredith Room, Spangler Center, HBS");
right("## A Glimpse at the Pharmaceutical Industry from an Insider's Perspective:  From Business to Business Development  
### Sophie Kornowski- Bonnet, Head of Roche Partnering  
 
On May 8th from 4-6pm, please join Sophie Kornowski- Bonnet, Head of Roche Partnering, for a talk about the pharmaceutical business.  Sophie will provide her perspective about the challenges associated with this industry and share anecdotes and tips for succeeding in this business, earned throughout her rich career.  The talk will be held in the Meredith Room, Spangler Center on the HBS campus.  Light refreshments will be served.  Please [RSVP](https://docs.google.com/forms/d/1_m5Vv0XUn4s7grSZcb-Le1v1oPnVI9RK6XzuZ9Fj4dU/viewform) by May 5th.  
");

left("[![hbs nvc][]](http://www.hbs.edu/newventurecompetition/Pages/default.aspx)  
### **Tue April 29**
### 4:30 p.m. -- 6:30 p.m.
### Burden Auditorium, HBS");
right("## New Venture Competition Finale
The New Venture Competition is an annual competition sponsored by Harvard Business School's Rock Center for Entrepreneurship and Social Enterprise Initiative. Attend this year's ceremony to get to know this year's revolutionary ventures:

*   Alumni Competition winners for Most Innovative, Greatest Impact, and Best Investment 
*   Student Competition finalists (8 teams make their final pitch!) for Grand Prize & Runner-Up awards

Vote for your favorite team as the Crowd Favorite ... and then watch as Dean Nohria reveals the winners!  
");

endTable();
?>  

[blav group pic]: http://www.hbs.edu/healthcare/images/photos/blav-pic-1.jpg
[blavatnik]: http://www.hbs.edu/healthcare/images/photos/blavatnik_logo.gif
[blav fellows]: http://www.hbs.edu/healthcare/images/photos/blav-pic-200w.jpg
[sophie]: http://www.hbs.edu/healthcare/images/photos/sophie-kb-200.jpg
[hbs nvc]: http://www.hbs.edu/healthcare/images/photos/new-venture-competition.png

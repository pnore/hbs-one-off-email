<?php
beginTable();
left('[![health care career days logo 100w][]](http://www.hbs.edu/healthcare/pdf/2013%20Health%20Care%20Career%20Days%20-%20All%20Guides.pdf)');
right("In October we co-sponsored five alumni panels as part of our annual Health Care Career Days. Documents have been produced that summarize the advice of each panel to serve as a career resource guide for students. You can now download individual guides in:  

* [care delivery](http://www.hbs.edu/healthcare/pdf/2013%20Health%20Care%20Career%20Days%20-%20Care%20Delivery.pdf)  
* [devices and diagnostics](http://www.hbs.edu/healthcare/pdf/2013%20Health%20Care%20Career%20Days%20-%20Devices%20and%20Diagnostics.pdf)  
* [digital health](http://www.hbs.edu/healthcare/pdf/2013%20Health%20Care%20Career%20Days%20-%20Digital%20Health.pdf)  
* [health care finance](http://www.hbs.edu/healthcare/pdf/2013%20Health%20Care%20Career%20Days%20-%20Health%20Care%20Finance.pdf)  
* [life sciences](http://www.hbs.edu/healthcare/pdf/2013%20Health%20Care%20Career%20Days%20-%20Life%20Sciences.pdf)  

Alternatively, you can [download the combined PDF with all of the guides](http://www.hbs.edu/healthcare/pdf/2013%20Health%20Care%20Career%20Days%20-%20All%20Guides.pdf).  
Enjoy---and please send feedback to [healthcare_initiative@hbs.edu](mailto:healthcare_initiative@hbs.edu?subject=Health%20Care%20Career%20Days%20Guides).");

endTable();
?>  

[health care career days logo 100w]: http://www.hbs.edu/healthcare/images/photos/2013_health_care_career_days_icon_100w.png

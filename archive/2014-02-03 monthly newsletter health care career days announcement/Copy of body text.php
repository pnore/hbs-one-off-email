## Welcome Back to All!  
We hope that everyone had a great break, and congratulate RCs on their completion of __ student projects in __ cities and __ countries as part of HBS's Global Immersion FIELD study program.  

## Admitted Student Health Care Mixer  
The Health Care Club and the Health Care Initiative are co-sponsoring an student health care mixer at First Printer (<a href="https://maps.google.com/maps?q=First+Printer&fb=1&gl=us&hq=first+printer+15+dunster+street+cambridge+ma&cid=1651955807824331829&t=h&z=16&iwloc=A">15 Dunster Street</a>) from 8:30 p.m. to 10:30 p.m. on Thursday, February 6th. We invite you to come meet the newly admitted students that are interested in health care and to eat, drink and be merry. 

## Health Care Conference  
2014 Health Care Club Conference: <em>Redefining Access in Modern Health Care</em> is on <strong>Saturday, February 8</strong>! Tickets are selling out fast, so <a href="http://www.hbshealthcareconference.org/health2014/">buy your ticket today</a>! There will be panels on the Accountable Care Act, Emerging Markets, Devices and Diagnostics, Consumer Health, IT, Finance, Payer/Providers and Pharma/Biotech. Featured keynote speakers include Mr. Mark Bertolini - Chairman, CEO, and President of Aetna; Dr. Elizabeth Nabel, President of Brigham and Women's Hospital; and Mr. John Brooks, CEO of Joslin Diabetes Center.

## What I Wish an HBS Alum Had Told Me: Practical Tips from HBS Health Care Career Days

<?php
beginTable();
left('[![health care career days logo 100w][]](http://www.hbs.edu/healthcare/pdf/2013%20Health%20Care%20Career%20Days%20-%20All%20Guides.pdf)');
right("In October we co-sponsored five alumni panels as part of our annual Health Care Career Days. Documents have been produced that summarize the advice of each panel to serve as a career resource guide for students. You can now download individual guides in:  

* [care delivery](http://www.hbs.edu/healthcare/pdf/2013%20Health%20Care%20Career%20Days%20-%20Care%20Delivery.pdf)  
* [devices and diagnostics](http://www.hbs.edu/healthcare/pdf/2013%20Health%20Care%20Career%20Days%20-%20Devices%20and%20Diagnostics.pdf)  
* [digital health](http://www.hbs.edu/healthcare/pdf/2013%20Health%20Care%20Career%20Days%20-%20Digital%20Health.pdf)  
* [health care finance](http://www.hbs.edu/healthcare/pdf/2013%20Health%20Care%20Career%20Days%20-%20Health%20Care%20Finance.pdf)  
* [life sciences](http://www.hbs.edu/healthcare/pdf/2013%20Health%20Care%20Career%20Days%20-%20Life%20Sciences.pdf)  

Alternatively, you can [download the combined PDF with all of the guides](http://www.hbs.edu/healthcare/pdf/2013%20Health%20Care%20Career%20Days%20-%20All%20Guides.pdf).  
Enjoy---and please send feedback to [healthcare_initiative@hbs.edu](mailto:healthcare_initiative@hbs.edu?subject=Health%20Care%20Career%20Days%20Guides).");

endTable();
?>  

[health care career days logo 100w]: http://www.hbs.edu/healthcare/images/photos/2013_health_care_career_days_icon_100w.png

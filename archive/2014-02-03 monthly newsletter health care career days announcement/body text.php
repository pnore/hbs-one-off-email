<?php
beginTable();
left('[![health care career days logo 100w][]](http://www.hbs.edu/healthcare/pdf/2013%20Health%20Care%20Career%20Days%20-%20All%20Guides.pdf)');
right("In October HCI sponsored five alumni panels as part of our annual Health Care Career Days. Documents have been produced that summarize the advice of each panel to serve as a career resource guide for students. [The PDF](http://www.hbs.edu/healthcare/pdf/2013%20Health%20Care%20Career%20Days%20-%20All%20Guides.pdf) contains guides in: 

* care delivery  
* devices and diagnostics
* digital health
* health care finance  
* life sciences

Enjoy---and please send feedback to [healthcare_initiative@hbs.edu](mailto:healthcare_initiative@hbs.edu?subject=Health%20Care%20Career%20Days%20Guides).");

endTable();
?>  

[health care career days logo 100w]: http://www.hbs.edu/healthcare/images/photos/2013_health_care_career_days_icon_100w.png

# Networking Event: Health Care Student-Alumni Mixer -- December 3

You are invited to join HBS students and alumni at this year's health care mixer, 
which will be on Tuesday, December 3. Last year this event was popular, 
so we encourage you to [RSVP] to attend.


# Details ([add to calendar])
|               |                                     |
|---------------|-------------------------------------|
| **Date:**     | Tuesday, December 3, 2013           |
| **Time:**     | 6:00 to 9:00 pm                     |
| **Place:**    | Harvard Business School, Room TBA   |
| **Cost:**     | There is no cost to attend          |

# Please RSVP at the [Healthcare Alumni Association event page]. 
If you have questions, please email [Cara Sterling]. 

Sincerely,  
Cara Sterling  
Director, Health Care Initiatve

[Cara Sterling]: mailto:csterling@hbs.edu?subject=Health%20Care%20Student-Alumni%20Mixer
[RSVP]: http://www.hbshealthalumni.org/article.html?aid=793
[Healthcare Alumni Association event page]: http://www.hbshealthalumni.org/article.html?aid=793
[add to calendar]: http://www.hbshealthalumni.org/events.html?vcal=793
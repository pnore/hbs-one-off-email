  
  
## Dear %%First Name%%, 

Thank you for signing up for a health care alumni mentor. 
This year we had so many alumni volunteers that we have the opportunity 
to give you not just 1, but 2 mentors!  This is a great way to 
broaden your mentorship experience and connect with more alumni. If you would 
prefer to only have one mentor please let me know by noon on Monday, 11/4. 
Mentor matches will be emailed on Tue 11/5. 

Sincerely, 

P. Myer Nore  
HBS Health Care Initiative  
[pnore@hbs.edu]

[pnore@hbs.edu]: mailto:pnore@hbs.edu?subject=Mentor%20Program%20One%20Match
# Dear %%alumni_first_name%%, 

Thank you for volunteering to participate in the HBS Business and Environment Mentor Program! 
We are thrilled with your willingness to engage with HBS students.

We are writing to let you know that students have been matched with their mentors.  
Because there were fewer students than alumni who opted into the program, 
it was not possible to match all the alumni who signed up.  As a result, 
we were unable to match you with a student this year.

As a gesture of our gratitude, we would like to send you a gift.
If you have not updated [your alumni profile] in a while, please 
[click here to update your address] by December 10. 

On behalf of the Business and Environment Initiative, we want to thank you once again for volunteering. 
We hope that you will be a part of the mentor program again next year. 

## Thank you,  
Melissa Paschall  
Director, Business & Environment Initiative

Libby Hamer  
Coordinator, Business & Environment Initiative


[alumni profile]: %%hbs_alumni_profile_url%%
[your alumni profile]: https://www.alumni.hbs.edu/community/Pages/edit-profile.aspx
[click here to update your address]: https://www.alumni.hbs.edu/community/Pages/edit-profile.aspx
[contact us]: mailto:bei@hbs.edu?subject=Update%20Alumni%20Address%20for%20Mentor%20Program
<?php
beginTable();



left("[![blav fellows][]](http://www.hbs.edu/healthcare/images/photos/blav-pic-1.jpg)");
right("## [2014-15 EC Health Care Courses]  
Next year there are 10 terrific courses focused on the health care industry taught by 10 HBS faculty members. See [this document](http://j.mp/fy14-hc-ec-course-prereg) for summary information on courses, other suggested courses and individual project information.");

left("[![wildside][]](https://docs.google.com/forms/d/14EwhKbk7nVxn5wkw3KIdTCmh3IUPcaBz5QiFQaigYRA/viewform)");
right("## [Health Care EC Send-off with Wildside]  
**TONIGHT -- Wed Apr 16** <span class='time'>8:00 p.m.  -- 10:00 p.m.,</span> <span class='place'>Lizard Lounge, Cambridge, MA</span>  
This year in honor of our departing 2014 health care students the HCI will be hosting WildSide at the Lizard Lounge. Just bring your Harvard ID. Attendance is capped at 100 people, but you can [RSVP](https://docs.google.com/forms/d/14EwhKbk7nVxn5wkw3KIdTCmh3IUPcaBz5QiFQaigYRA/viewform) by 3:30 PM to have your name at the door. More [INFO HERE](http://www.hbs.edu/healthcare/images/photos/EC-Send-Off-16.png). *Hosted by the Health Care Initiative*. Open to MBA Students. 
");

left("[![first printer asw][]](http://thefirstprinter.com/)");
right("## [Health Care Student Mixer at First Printer]  
**TOMORROW -- Thu Apr 17** <span class='time'>9:00 p.m.  -- 11:00 p.m,</span>, <span class='place'>Boston</span>  
The Health Care Club is sponsoring a mixer at First Printer (15 Dunster Street, Cambridge) from 9:00 p.m. to 11:00 p.m. on Thursday, April 17th. You are invited to come meet admitted students interested in health care and enjoy drinks and light appetizers.  
");



endTable();
?>  
[fleece]: http://www.hbs.edu/healthcare/images/photos/fleece.PNG
[word doc]: http://www.hbs.edu/healthcare/images/photos/microsoft-word-2010-logo-for-downloadable-file-template.png
[wildside]: http://www.hbs.edu/healthcare/images/photos/wildside.png
[first printer asw]: http://www.hbs.edu/healthcare/images/photos/first-printer.png

[Have a Health Care Mentor? Win an HBS Health Care Fleece or T-Shirt!]: https://secure.hbs.edu/poll/open/pollTakerOpen.jsp?poll=135058
[2014-15 EC health care courses]: http://j.mp/fy14-hc-ec-course-prereg
[Health Care EC Send-off with Wildside]: https://docs.google.com/forms/d/14EwhKbk7nVxn5wkw3KIdTCmh3IUPcaBz5QiFQaigYRA/viewform
[Health Care Student Mixer at First Printer]: http://thefirstprinter.com/

[blav fellows]: http://www.hbs.edu/healthcare/images/photos/blav-pic-200w.jpg

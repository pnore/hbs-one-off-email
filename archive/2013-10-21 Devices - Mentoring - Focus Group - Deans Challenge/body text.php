## [Career Days: Devices and Diagnostics Panel] -- MON OCT 21 
<span class='time'>4:30--5:30 p.m.</span>, <span class='place'>Cumnock 230</span>  
Join HBS alums as they speak about their career paths in the devices and diagnostics health care sectors.

## [Alumni Mentor Preferences Due] -- SUN OCT 27
Don't miss this unique opportunity to be paired with a 
senior alum from the health care sector that interests you.

## [Focus Group on Health Care Events] -- MON OCT 28
<span class='time'>3:30--4:30 p.m.</span>, <span class='place'>Cotting Lounge</span>  
Did you attend health care events such as *Health Care 101* or *Health Care Career Days*? We want to hear from you. 
Email [pnore@hbs.edu] by 10/28 to RSVP for our focus group or to give us your feedback. *Refreshments served*.

## [Deans' Health and Life Sciences Challenge Kickoff] -- MON OCT 28
<span class='time'>6:00--8:00 p.m.</span>, <span class='place'>i-Lab</span>  
You are all invited to the kickoff celebration of the second Deans' Health and Life Sciences Challenge at the i-lab.
Food and refreshments will be served. Please contact Alice Ly, i-Lab Assistant Director 
([aly@harvard.edu]), with questions. RSVP [here](https://www.eventbrite.com/event/8767861911). 

[speech bubbles]: http://www.hbs.edu/healthcare/images/photos/speechbubbles.png
[pill image]: http://www.hbs.edu/healthcare/images/photos/pill_100.png
[steth image]: http://www.hbs.edu/healthcare/images/photos/steth_100.png
[heart image]: http://www.hbs.edu/healthcare/images/photos/heart_100.png
[up_arrow image]: http://www.hbs.edu/healthcare/images/photos/up_arrow_100.png
[dna image]: http://www.hbs.edu/healthcare/images/photos/dna_100.png
[clip image]: http://www.hbs.edu/healthcare/images/photos/clip_100.png

[Deans' Health and Life Sciences Challenge Kickoff]: http://ilab.harvard.edu/calendar-event/content/2014-deans-health-life-sciences-challenge-kick
[pnore@hbs.edu]: mailto:pnore@hbs.edu?Subject=Student%20Focus%20Group
[aly@harvard.edu]: mailto:aly@harvard.edu
[Focus Group on Health Care Events]: mailto:pnore@hbs.edu?Subject=Student%20Focus%20Group
[Alumni Mentor Preferences Due]: https://secure.hbs.edu/poll/taker/pollTaker.jsp?poll=1,134531
[j.mp/2014-hbs-hc-mentor]: https://secure.hbs.edu/poll/taker/pollTaker.jsp?poll=1,134531
[Care Delivery Panel]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=73289&filter=month&viewDay=15&viewMonth=9&viewYear=2013
[RSVP]: https://secure.hbs.edu/poll/taker/pollTaker.jsp?poll=134521
[Life Sciences Panel]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=70952&filter=month&viewDay=16&viewMonth=9&viewYear=2013
[Health Care Finance Panel]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=70957&filter=month&viewDay=16&viewMonth=9&viewYear=2013
[Career Days: Devices and Diagnostics Panel]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=73290

[![![up_arrow image][]]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=73289&filter=month&viewDay=15&viewMonth=9&viewYear=2013

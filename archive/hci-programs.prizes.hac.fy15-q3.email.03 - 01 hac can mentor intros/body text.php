<?php 
// MAILING INFO
$title = "Instructions to CAN Advisors";
$campaignName = "hci-programs.prizes.hac.fy15-q3.email.03 - 01 hac can mentor intros";
$campaignUrl = "https://na15.salesforce.com/701i0000001QybZ";
$from = $caraFrom;
$sig = $caraSig;
$subject = "[HBS Health Care] Challenge Advisory Network Instructions";
$containsFirstName = true;
$baseLink = '';
$baseLinkName = "";
$relationalTableQuery = "/Shared/hac advising query 6";


/**************  BEGIN EMAIL ////////////////

# <?php echo $title ?>  
**Campaign:** <?php echo $campaignName ?>       
**Campaign Link:** <?php echo $campaignUrl ?>  
**From:** <?php echo $from; ?>
**Subject:** <?php echo $subject; ?> 
*/
?>

## Thank you for volunteering to be an advisor for the Health Acceleration Challenge!

A couple of weeks ago you identified the organizations that you were most interested in advising. 
Those organizations have received an email letting them know of your interest.  

Please reach out to them by **Tuesday, March 31st** to make an initial contact. I strongly encourage you 
to review the organization's website and Health Acceleration Challenge application first.  

Please click [here] for a great article from *Harvard Business Review* entitled "[The Art of Giving and Receiving Advice]." 
I hope it will be useful to you as you determine your role in advising the organization.  

You have volunteered to advise the following finalists:

<?php 
beginTable( "$relationalTableQuery", "12", "leftright", "%%Mentee_Company%%");

// note, somehow this gets translated from & to &amp; and it messes the merge up. you have to manually fix it!
row( "<a xt='SPNOTRACK' name='mentor_match_%%Mentor_Index%%' href='%%Mentee_Challenge_Application_URL%%' target='_blank'>%%Mentee_Company%%</a>:  
	%%Mentee_First_Name%% %%Mentee_Last_Name%% (<a href=\"mailto:%%Mentee_Email%%?subject=Challenge%20Advisory%20Network\">%%Mentee_Email%%</a>) from %%Mentee_Company%% would like help with: %%Mentee_Needs%%  
	");

endTable();
?>

For more context about the Challenge, please visit [www.HealthAccelerationChallenge.com].  

Thank you so much for sharing your time and expertise.  

<?php echo $sig; ?>



<?php // LINKS FILLED IN BY MARKDOWN ?>
[Health Acceleration Challenge]: http://forumonhealthcareinnovation.org/
[www.HealthAccelerationChallenge.com]: http://forumonhealthcareinnovation.org/
[The Art of Giving and Receiving Advice]: https://cb.hbsp.harvard.edu/cbmp/pl/35115216/35115218/6a9dcede3c5b8bf2eae6f9d048920861
[here]: https://cb.hbsp.harvard.edu/cbmp/pl/35115216/35115218/6a9dcede3c5b8bf2eae6f9d048920861
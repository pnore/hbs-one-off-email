<?php 
// MAILING INFO
$title = "FY15-q3 alumni newsletter";
$campaignName = "hci-programs.newsletters.alumni.fyXX-qX.email.MM - 01 to alumni";
$campaignUrl = "";
$templateUrl = "https://engage1.silverpop.com/composeMailing7.do?action=displayLoadTemplate&mailingid=11713732&templateType=0";
$from = "HBS Health Care Initiative <healthcare_initiative@hbs.edu>";
$sig = null;
$subject = "[HBS Health Care] Alumni Update";
$containsFirstName = false;


/**************  BEGIN EMAIL HEADER ******************/
?>

<!--
# <?php echo $title ?>  
**Campaign:** <?php echo $campaignName ?>       
**Campaign Link:** <?php echo $campaignUrl ?>  
**From:** <?php echo $from; ?>
**Subject:** <?php echo $subject; ?> 
-->

<?php
/**************  BEGIN EMAIL BODY ******************/
?>

<?php section("Health Care Happenings"); ?>

## Secretary of Vet Affairs [Frank]
<?php 
beginTable();
left("
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.  ", "280px", "normal");

right('<img src="http://dummyimage.com/282x183/4d494d/686a82.gif&text=placeholder+image" alt="placeholder+image">', "292px", "normal", "middle");
endTable();
?>

## BIO and HIMS [Frank]

<?php
beginTable();

left("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium." . button('RSVP', 'http://www.hbshealthalumni.org/store.html?event_id=913'), "290px", "normal");
right('<img src="http://dummyimage.com/282x183/4d494d/686a82.gif&text=bio+and+hims" alt="placeholder+image">', "282px");

endTable();
?>  


## Alumni Mentor Program [Myer]
<?php 
beginTable();

left('<img src="http://dummyimage.com/102x52/4d494d/686a82.gif&text=placeholder+image" alt="logo+from+HBR">');
right("Mention the mugs / schedule some time to connect in the Spring. Read [The Art of Giving and Receiving Advice](https://hbr.org/2015/01/the-art-of-giving-and-receiving-advice) by David A. Garvin and Joshua D. Margolis from the January 2015 issue of HBR.");

endTable();
?>


## BAHM [Frank]
<?php 
beginTable();
left("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
", "438px", "normal");
right('<img src="http://dummyimage.com/102x52/4d494d/686a82.gif&text=bahm+logo" alt="BAHM+logo">',  "103px", "normal", "middle");
endTable();
?>

## Blavatnik [Tracy]
<?php beginTable();
left("[![blavatnik logo][]](http://www.biomedicalaccelerator.org/fellowship/)","171px", "normal", "middle");
right(
"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.", "465px", "normal");
endTable(); ?>

<?php section("Health Care Events [Myer]"); ?>

Below is a listing of upcoming events including HBS happenings, alumni events, upcoming opportunity deadlines and conferences at which the Health Care Initiative is considering hosting a reception.  

<?php 
beginTable();

left("Fri Apr 03  
 <span class='place'>Virtual</span>");
right("[Sample Event]  
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. *Open to the public.*");

left("Fri Apr 03  
 <span class='place'>Virtual</span>");
right("[Sample Event]  
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. *Open to the public.*");

left("Fri Apr 03  
 <span class='place'>Virtual</span>");
right("[Sample Event]  
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. *Open to the public.*");

left("Fri Apr 03  
 <span class='place'>Virtual</span>");
right("[Sample Event]  
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. *Open to the public.*");

left("Fri Apr 03  
 <span class='place'>Virtual</span>");
right("[Sample Event]  
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. *Open to the public.*");

left("Fri Apr 03  
 <span class='place'>Virtual</span>");
right("[Sample Event]  
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. *Open to the public.*");

endTable();
?>

## HBP Highlights [Cara]
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
 
<?php 
beginTable();

left("[![medical case image](http://www.hbs.edu/healthcare/images/photos/medical_case.png)](https://hbr.org/insight-center/innovating-for-value-in-health-care)");

right("### Bridging Health Care's Innovation-Education Gap
Be sure to check out the article \"[Bridging Health Care's Innovation-Education Gap](https://hbr.org/2014/11/bridging-health-cares-innovation-education-gap)\" by HBS Professor Regina Herzlinger along with Vasant Kumar Ramaswamy and Kevin A. Schulman. Also, see the [accompanying videos here](https://www.youtube.com/channel/UCEHiAeuRb5J2sCIfunvUdLQ/videos). 

### Sample Article 2
Don't miss \"[Sample HBP Highlight](https://hbr.org/2014/11/health-care-needs-less-innovation-and-more-imitation)\" by Jane Doe.");
endTable();
?>

<?php section("Recent Faculty Publications [Myer]"); ?>

<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48227' target='_blank'>Sample Publication</a>
<br><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6487'>Jane Doe</a>, John Doe
<br>Publications 92, no. 11 (November 2014): 116-122<br><br>

<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48227' target='_blank'>Sample Publication</a>
<br><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6487'>Jane Doe</a>, John Doe
<br>Publications 92, no. 11 (November 2014): 116-122<br><br>

<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48227' target='_blank'>Sample Publication</a>
<br><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6487'>Jane Doe</a>, John Doe
<br>Publications 92, no. 11 (November 2014): 116-122<br><br>

<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48227' target='_blank'>Sample Publication</a>
<br><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6487'>Jane Doe</a>, John Doe
<br>Publications 92, no. 11 (November 2014): 116-122<br><br>

<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48227' target='_blank'>Sample Publication</a>
<br><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6487'>Jane Doe</a>, John Doe
<br>Publications 92, no. 11 (November 2014): 116-122<br><br>

<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48227' target='_blank'>Sample Publication</a>
<br><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6487'>Jane Doe</a>, John Doe
<br>Publications 92, no. 11 (November 2014): 116-122<br><br>

<?php section("HBS Health Care In the News [Myer]"); ?>

"[Sample Health Care In the News Article][]"  
Re: Robert Huckman  
*Boston Globe*  
Dec 11, 2014

"[Sample Health Care In the News Article][]"  
Re: Robert Huckman  
*Boston Globe*  
Dec 11, 2014

"[Sample Health Care In the News Article][]"  
Re: Robert Huckman  
*Boston Globe*  
Dec 11, 2014

"[Sample Health Care In the News Article][]"  
Re: Robert Huckman  
*Boston Globe*  
Dec 11, 2014

"[Sample Health Care In the News Article][]"  
Re: Robert Huckman  
*Boston Globe*  
Dec 11, 2014

"[Sample Health Care In the News Article][]"  
Re: Robert Huckman  
*Boston Globe*  
Dec 11, 2014

"[Sample Health Care In the News Article][]"  
Re: Robert Huckman  
*Boston Globe*  
Dec 11, 2014

"[Sample Health Care In the News Article][]"  
Re: Robert Huckman  
*Boston Globe*  
Dec 11, 2014

---

### Update Your Alumni Profile
<a name='update_alumni_info' href='https://www.alumni.hbs.edu/community/Pages/edit-profile.aspx' target='_blank' style='color: rgb(229, 102, 93); background-color: rgb(255, 255, 255);'>Update your career profile in the alumni directory today</a> today so you can be found by HBS alumni, faculty and students that share your interest in health care. 

<?php 
/**************  END EMAIL BODY ******************/

/**************  BEGIN LINKS FILLED IN BY MARKDOWN  ******************/
?>


*[FAS]: Harvard Faculty of Arts and Sciences
*[HLS]: Harvard Law School
*[HKS]: Harvard Kennedy School
*[HBR]: Harvard Business Review
*[NEJM]: New England Journal of Medicine
*[BAHM]: Business School Alliance for Health Management 
*[HBSHAA]: Harvard Business School Healthcare Alumni Association
*[FHI]: Forum on Healthcare Innovation
*[HMS]: Harvard Medical School

<?php // reusable images ?>
[speech bubbles]: http://www.hbs.edu/healthcare/images/photos/speechbubbles.png
[pill image]: http://www.hbs.edu/healthcare/images/photos/pill_100.png
[steth image]: http://www.hbs.edu/healthcare/images/photos/steth_100.png
[heart image]: http://www.hbs.edu/healthcare/images/photos/heart_100.png
[up_arrow image]: http://www.hbs.edu/healthcare/images/photos/up_arrow_100.png
[dna image]: http://www.hbs.edu/healthcare/images/photos/dna_100.png
[clip image]: http://www.hbs.edu/healthcare/images/photos/clip_100.png

<?php // logos ?>
[blavatnik logo]: http://www.hbs.edu/healthcare/images/photos/blav-in-life-science-entrepreneurship.png
[exec ed]: http://www.hbshealthalumni.org/images/article_images/436.jpg

<?php // one-time images ?>
[Health Acceleration Challenge Poster]: http://www.hbs.edu/healthcare/images/photos/hac-img-290x278.png
[Ross Leimberg]: http://www.hbs.edu/news/PublishingImages/photos/leimberg.jpg
[Daniel Oliver]: http://www.hbs.edu/news/PublishingImages/photos/oliver.jpg
[Steve Porter]: http://www.hbs.edu/news/PublishingImages/photos/porter.jpg
[John Strenkowski]: http://www.hbs.edu/news/PublishingImages/photos/strenkowski.jpg
[Ridhi Tariyal]: http://www.hbs.edu/news/PublishingImages/photos/tariyal.jpg
[Bluestem Brasserie Image]: http://www.hbs.edu/healthcare/images/photos/bluestem_282.png

<?php // one-time event links ?>
[Hacking Healthcare, Luc Sirois]: http://www.hbshealthalumni.org/article.html?aid=907
[Networking Event: HBS Healthcare Alumni]: http://www.hbshealthalumni.org/article.html?aid=921
[Sample Event]: http://www.hbshealthalumni.org/article.html?aid=909
[Reception at JP Morgan 33rd Annual Conference]: http://www.hbshealthalumni.org/article.html?aid=913
[J.P. Morgana 33rd Annual Healthcare Conference]: http://sternir.com/ai1ec_event/j-p-morgan-33rd-annual-healthcare-conference/
[Is That a Doctor in Your Pocket? (Roy Schoenberg)]: http://www.hbshealthalumni.org/article.html?aid=916
[Drug Development Dynamics: Discovery, Delivery & Demand]: http://www.hbshealthalumni.org/article.html?aid=912
[Ebola: What Can Be Done? (Marcus Lovell Smith)]: http://www.hbshealthalumni.org/article.html?aid=922
[Seeking Alumni Applicants: Blavatnik Fellowship]: http://otd.harvard.edu/accelerators/blavatnik-biomedical-accelerator/blavatnik-fellowship/
[Reception at HIMSS 2015]: http://www.hbshealthalumni.org/events.html
[Reception at BIO International Convention]: http://www.hbshealthalumni.org/events.html

<?php // one-time links for in the news ?>
[Sample Health Care In the News Article]: https://www.bostonglobe.com/business/2014/12/11/tufts/yaRVe0uBCrfE4FZlFqPFOJ/story.html
[In the Market for Blood? Here's How to Keep From Getting Squeezed]: http://www.bizjournals.com/dallas/blog/2014/11/in-the-market-for-blood-here-s-how-to-keep-from.html
[Finalists Announced in Harvard Business School/Harvard Medical School Health Acceleration Challenge]: http://www.hbs.edu/news/releases/Pages/hac-finalists.aspx
[Boston Health Tech Groups Are Finalists in Harvard's Health Acceleration Challenge]: http://betaboston.com/news/2014/11/20/boston-health-tech-groups-are-finalists-in-harvards-health-acceleration-challenge/
[Medalogix Chosen as Finalist in Harvard Challenge, Wins $37.5K]: https://www.nashvillepost.com/blogs/postbusiness/2014/11/20/medalogix_chosen_as_finalist_in_harvard_challenge_wins_375k
[Bridging Health Care's Innovation-Education Gap]: https://hbr.org/2014/11/bridging-health-cares-innovation-education-gap
[Health Acceleration Challenge Contestants Aim to Improve U.S. Health Care]: http://www.hbs.edu/news/releases/Pages/hac-applicants-eager.aspx
[A Recap of the U.S. News Hospital of Tomorrow Forum 2014]: http://health.usnews.com/health-news/hospital-of-tomorrow/articles/2014/11/03/a-recap-of-the-us-news-hospital-of-tomorrow-forum-2014
[The Businessman Disrupting Organ Transplantation]: http://www.theatlantic.com/health/archive/2014/10/the-businessman-disrupting-organ-transplantation/381843/?single_page=true&print=
[Ebola as a Wake-Up Call]: http://www.hbs.edu/news/articles/Pages/ebola-as-a-wake-up-call.aspx
[Harvard Challenge Testing Healthcare Innovation Ideas That Are Ready to Scale]: http://medcitynews.com/2014/10/can-harvard-help-make-primary-care-better-patients/
[Economic Costs of Ebola Rising as People Shun Human Contact]: http://www.economywatch.com/features/Economic-costs-of-Ebola-rising-as-people-shun-human-contact.10-15-14.html
[The Unanticipated Risks of Maximizing Shareholder Value]: http://www.forbes.com/sites/stevedenning/2014/10/14/the-unanticipated-risks-of-maximizing-shareholder-value/
[Science Innovation Contest to Begin Mid-Oct.]: http://www.thecrimson.com/article/2014/10/7/ilab-challenge-health-sciences/
[Michael Porter: Disrupt Health Care to Save it]: http://health.usnews.com/health-news/hospital-of-tomorrow/articles/2014/10/07/michael-porter-disrupt-health-care-to-save-it
[The Rise of the M.D./M.B.a. Degree]: http://www.theatlantic.com/education/archive/2014/09/the-rise-of-the-mdmba-degree/380683/
[WATCH: Live From the 2014 Hospital of Tomorrow Conference]: http://health.usnews.com/health-news/hospital-of-tomorrow/articles/2014/09/30/watch-live-from-the-2014-hospital-of-tomorrow-conference
[The Shortcomings of the Corporate Wellness Program]: http://www.marketplace.org/topics/health-care/shortcomings-corporate-wellness-program
[Nanobiosym Global Summit Convenes World Experts in Cambridge to Democratize Health Care]: http://www.marketwired.com/press-release/nanobiosym-global-summit-convenes-world-experts-cambridge-democratize-healthcare-1948278.htm
[If Your Kids Get Free Health Care, You'Re More Likely to Start a Company]: http://blogs.hbr.org/2014/09/if-your-kids-get-free-health-care-youre-more-likely-to-start-a-company/

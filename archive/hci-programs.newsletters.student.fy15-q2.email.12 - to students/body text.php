<?php 
// MAILING INFO
$title = "";
$campaignName = "";
$campaignUrl = "";
$templateUrl = "";
$from = null;
$sig = null;
$subject = "";
$containsFirstName = false;


/**************  BEGIN EMAIL HEADER ******************/

/*# <?php echo $title ?>  
**Campaign:** <?php echo $campaignName ?>       
**Campaign Link:** <?php echo $campaignUrl ?>  
**From:** <?php echo $from; ?>
**Subject:** <?php echo $subject; ?> 
*/

/**************  BEGIN EMAIL BODY ******************/
?>
# Seeking EC Applicants: Blavatnik Fellowship in Life Science Entrepreneurship
<?php 

beginTable();

beginTable();

left("[![blavatnik][]](http://www.hbs.edu/healthcare/images/photos/blavatnik_logo.gif)");
right("The Blavatnik Fellowship in Life Science Entrepreneurship is seeking entrepreneurial HBS alumni who have graduated within the last seven years, including
2015 graduates, to foster the commercialization of biomedical innovation from laboratories throughout Harvard University. Fellows work for one year in the
vibrant Harvard Innovation Lab on the HBS campus and receive a $95K stipend for 12 months with extra resources for due diligence, market research, and
licensing options. To <a href=\"http://otd.harvard.edu/accelerators/blavatnik-biomedical-accelerator/blavatnik-fellowship/\">apply to the program</a>, submit
a completed application to <a href=\"mailto:blavatnikfellowship@hbs.edu\">blavatnikfellowship@hbs.edu</a> by March 5, 2015.");

endTable();


?>

<?php 
/**************  END EMAIL BODY ******************/

/**************  BEGIN LINKS FILLED IN BY MARKDOWN  ******************/
?>
[blav group pic]: http://www.hbs.edu/healthcare/images/photos/blav-pic-1.jpg
[blavatnik]: http://www.hbs.edu/healthcare/images/photos/blavatnik_logo_171.png
[blav fellows]: http://www.hbs.edu/healthcare/images/photos/blav-pic-200w.jpg
[sophie]: http://www.hbs.edu/healthcare/images/photos/sophie-kb-200.jpg
[hbs nvc]: http://www.hbs.edu/healthcare/images/photos/new-venture-competition.png
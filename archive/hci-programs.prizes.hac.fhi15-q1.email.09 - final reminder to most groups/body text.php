<?php 

// hci-programs.prizes.hac.fhi15-q1.email.09 - final reminder to most groups
// https://na15.salesforce.com/701i0000000stWd

// NUMBERS
$number_of_applications = "187";
$number_participators = "940";
$days_left = "5";

$unsubscribe = "<a name='Unsubscribe_1' xt='SPONECLICKOPTOUT' href='#SPONECLICKOPTOUT' target='_blank'>Unsubscribe</a>";

// RE-USED PARAGRAPHS AND SENTENCES
$we_already_have_applications = "We already have $number_of_applications applications and over $number_participators registrations from numerous entrepreneurs and institutions including the Cleveland Clinic, Mayo Clinic, Kaiser Permanente! ";

$closing_thank_you_let_know_questions = "Please let me know if you have any questions. Thank you,  
";

// LINKS
$link_to_all_applications = "http://openforum.hbs.org/challenge/hbs-hms-health-acceleration-challenge/innovations";



// SIGNATURES
$barbaraFrom = "McNeil, Barbara J. (mcneil@hcp.med.harvard.edu)  
";
$barbaraSig = "
Barbara  

Barbara J. McNeil, M.D.  
Founding Head, Department of Health Policy and Professor of Radiology  
Faculty Co-Chair, Forum on Health Care Innovation  
Harvard Medical School  
180 Longwood Avenue  
Boston, MA 02115  
Phone: 617.432.1909  

$unsubscribe
";

$caraFrom = "Sterling, Cara (csterling@hbs.edu)  
";
$caraSig = "
Cara

Cara Sterling  
Director, Health Care Initiative  
Harvard Business School  
Phone: 617-495-6126  
Email: [csterling@hbs.edu](mailto:csterling@hbs.edu)  

$unsubscribe
";

$joeFrom = "Newhouse, Joseph Paul (newhouse@hcp.med.harvard.edu)  
";
$joeSig = "
Joe  

Joseph Newhouse, PhD  
John D. MacArthur Professor of Health Policy and Management  
Faculty Co-Chair, Forum on Health Care Innovation  
Harvard Medical School  
180 Longwood Ave.  
Boston, MA 02115  
Phone: 617.432.1325  

$unsubscribe
";


$richardFrom = "Hamermesh, Richard (rhamermesh@hbs.edu)  
";
$richardSig = "
Richard  

Richard G. Hamermesh  
MBA Class of 1961 Professor of Management Practice  
Faculty Co-Chair, Forum on Health Care Innovation  
Harvard Business School  
Rock Center forEntrepreneurship  
Boston, MA 02163  
Phone: 617.495.4179  

$unsubscribe
";

$robFrom = "Huckman, Rob (rhuckman@hbs.edu)  
";
$robSig = "
Rob

Robert S. Huckman  
Albert J. Weatherhead III Professor of Business Administration  
Faculty Co-Chair, Forum on Health Care Innovation  
Harvard Business School  
435 Morgan Hall  
Boston, MA 02163  
Phone: 617.495.6649  

$unsubscribe
";

?>


# Email deadline reminder to all - Cara
**Note:** going to a. FHI Invitees, b. HBS Angels, c. Top 100 Executives, d. Entrepreneurs, e. Investors, f. Healthcare Executives, g. subscribers to FHI, h. hc alumni  
**Campaign:** hci-programs.prizes.hac.fhi15-q1.email.09 - final reminder to most groups     
**Campaign Link:** https://na15.salesforce.com/701i0000000stWd    
**From:** <?php echo $caraFrom; ?>
**Subject:** <?php echo $days_left; ?> Days Left - Health Acceleration Challenge!


Good Afternoon,

<?php echo $we_already_have_applications; ?> And next **Monday September 29th** is the application deadline!  

If you are a team that wants to apply, an investor or health system executive who has an innovation in mind, please make sure you or the teams you want to nominate submit the applications before 11.59pm EST on Monday.  

As a reminder, Harvard Business School and Harvard Medical School are awarding $150,000 to proven innovations for US health care delivery that are ready to scale.  

To find out more and apply, please visit [www.HealthAccelerationChallenge.com].

<?php echo $closing_thank_you_let_know_questions; ?>

<?php echo $caraSig; ?>



<?php // LINKS FILLED IN BY MARKDOWN ?>
[Boston Magazine]: http://www.bostonmagazine.com/health/blog/2014/08/25/harvard-health-acceleration-challenge/
[Forbes]: http://www.forbes.com/sites/hbsworkingknowledge/2014/08/18/harvard-wants-your-proven-ideas-to-improve-health-care/
[Health Acceleration Challenge]: http://forumonhealthcareinnovation.org/?utm_campaign=hac_launch&utm_source=hci-programs.prizes.hac.fhi15-q1.email.09_-_final_reminder_to_most_groups&utm_medium=social
[www.HealthAccelerationChallenge.com]: http://forumonhealthcareinnovation.org/?utm_campaign=hac_launch&utm_source=hci-programs.prizes.hac.fhi15-q1.email.09_-_final_reminder_to_most_groups&utm_medium=social
<?php /*********  BEGIN OPTIONAL MAIL INFORMATION - FILL IN ********/

$title = "HCI Student Newsletter 2015-05-01";
$campaignName = "hci-programs.newsletters.student.fy15-q4.email.05 - 01 to students dist fac";
$campaignUrl = "https://na15.salesforce.com/701i0000001RAYoAAO";
$templateUrl = "https://engage1.silverpop.com/composeMailing7.do?action=displayLoadTemplate&mailingid=11846610&templateType=0";
$from = null;
$sig = null;
$subject = "[HBS Health Care] Congrats; Events; Bye for the Summer! ";
$containsFirstName = false;

/**************  END OPTIONAL MAIL INFORMATION - FILL IN ***********/
/**************  BEGIN EMAIL HEADER (DON'T TOUCH) ****************/?>

<!--
# <?php echo $title ?>  
**Campaign:** <?php echo $campaignName ?>       
**Campaign Link:** <?php echo $campaignUrl ?>  
**From:** <?php echo $from; ?>
**Subject:** <?php echo $subject; ?> 
-->

<?php /********  END EMAIL HEADER (DON'T TOUCH) ********************/
/**************  BEGIN EMAIL BODY ******************/?>


<?php section("Health Care Announcements"); ?>


## So Long, But Not Goodbye  
This newsletter will be the last from the HCI until the Fall.  Graduating ECs – Please stay in touch and let us know how you are doing! Rising ECs – We look forward to seeing you in September.

## EC Health Care Course Overview Report 
On Friday, May 1 at 12:15 in Cumnock 102 faculty who are teaching EC health care courses will present an overview of their curriculum for the upcoming academic year. If you are unable to make it to the event, please email the [Health Care Initiative](mailto:healthcare_initiative@hbs.edu?subject=EC%20Health%20Care%20Course%20PDF) and we will provide you with a PDF of the courses.

## HBS Receptions at Health Care Conferences 

The Health Care Initiative
hosted a reception for alumni in Chicago at HIMSS. It was fun evening of
networking with more than 50 alumni in attendance.

The Health Care Initiative will also be hosting a reception with the Health Care Alumni Association in
Philadelphia to kick off the BIO conference. If you are in the area and would
like to attend, please click [here](http://www.hbshealthalumni.org/article.html?aid=947) too sign
up. The event is scheduled for Sunday, June 14th at 6:30 p.m. at the Academy of Natural Sciences.

<?php 
beginTable();
left('<img src="http://www.hbs.edu/healthcare/PublishingImages/photos/HIMSS15-2_277.png" alt="HIMSS Networking Photo 1">', "292px", "normal", "middle");
right('<img src="http://www.hbs.edu/healthcare/PublishingImages/photos/HIMSS15-3_277.png" alt="HIMSS Networking Photo 2">', "292px", "normal", "middle");
endTable();
?>

## Business Alliance for Health Care Management (BAHM) Update

<?php 
beginTable();

left("
This year’s BAHM Case Competition was in Denver at the University of Colorado, Denver. HBS was represented by MBA 2015 students, Shaan Gandhi, Nworah Ayogu and Clay Wiske (pictured here) with Prof. Regina Herzlinger as their faculty advisor. While the HBS team did not win the competition, they did an outstanding job presenting their case. The winning teams came from Yale (first place), Kellogg (second place) and Berkeley (third place). Next year’s case competition will be in Minneapolis at University of Minnesota. 
", "280px", "normal");
right('<img src="http://www.hbs.edu/healthcare/PublishingImages/photos/BAHM_Competition_277.png" alt="BAHM Competition">', "292px", "normal", "middle");

endTable();
?>

## Blavatnik Fellowship Updates
Summer won’t slow down the Blavatnik Fellows! As the fellows enter the last four month of the program, we have exciting news and opportunities to share. If you have any questions about the Fellowship, please contact Tracy Perry, [tperry@hbs.edu](mailto:tperry@hbs.edu) or come to the i-lab to chat with the Fellows.

### Two Blavatnik Fellows Win in Deans’ Challenges
Congratulations to Alexandra Dickson and Meridith Unger, Blavatnik Fellows ’14, for winning in two of the four Deans’ Challenges tracks.  Alexandra and the LuminOva team won the Health and Life Sciences track.  Meridith Unger and the Nix team won the Innovation in Sports track.  Each team was awarded $40,000.

### MBA Summer Internship at LuminOva  
LuminOva is a medical device startup in the women’s health space emerging from the Blavatnik Fellowship program at HBS.  We have developed a device to assess egg and embryo quality with the ultimate goal of improving success rates of in vitro fertilization.  We are early-stage, so are looking for an intern who is interested in working across functions, with job responsibilities in business development, marketing, and strategy. To apply, please send your resume to [adickson@hbs.edu](mailto:adickson@hbs.edu).

### Summer Internship at Nix
Nix is an early-stage consumer diagnostic company currently developing its initial product: a wearable hydration sensor that empowers athletes to manage their hydration status in real-time. Nix is seeking an energetic, motivated Business Development intern to complete critical projects around go-to-market strategy. Ideal candidates have consumer product and/or market research experience. Bonus points if you’re an (amateur) athlete.  To apply, please send your resume to [munger@hbs.edu](mailto:munger@hbs.edu).


<?php section("Health Care Events"); ?>

Below is a listing of upcoming events including HBS happenings, alumni events, upcoming opportunity deadlines and conferences at which the Health Care Initiative is hosting a reception.

<?php 
beginTable();

left("Fri May 01   
<span class='time'>12:00 pm -- 1:30 pm</span>   
 <span class='place'>HBS, Baker Library, Bloomberg 102</span>");
right("[Science-Based Business Seminar](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=162)  
Speaker: Ajay K. Agrawal, Peter Munk Professor of Entrepreneurship, University of Toronto and NBER. Collaboration, Stars, and the Changing Organization of Science: Evidence from Evolutionary Biology. *Hosted by Harvard Business School.*  *Open to the Harvard community.*");

left("Fri May 01   
<span class='time'>12:15 pm -- 1:00 pm</span>   
 <span class='place'>HBS, Cumnock 102</span>");
right("[Healthcare EC Course Overview](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=195)  
Faculty Present Health Care Electives -- Don’t miss this opportunity to see faculty present on their EC health care courses for the upcoming academic year. Lunch will be served. Please RSVP to Frank Sutter, fsutter@hbs.edu, so meals can be planned accordingly. *Hosted by HBS Health Care Initiative.*  *Open to HBS RC Students.*");

left("Fri May 08   
<span class='time'>12:00 pm -- 1:30 pm</span>   
 <span class='place'>HBS, Baker Library, Bloomberg Center 102</span>");
right("[Science-Based Business Seminar](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=177)  
Speaker: Timothy K. Lu, M.D., PhD. (MIT Synthetic Biology Center, Co-Founder, Synlogic, Inc., Co-Founder, Sample6, Inc., MIT. Synthetic Biology for Real-World Applications. *Hosted by Harvard Business School.*  *Open to the HBS community.*");

left("Tue May 12   
<span class='time'>12:00 pm -- 1:00 pm</span>   
 <span class='place'>Virtual</span>");
right("[Emerging Business Models and Trends in M&A](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=178)  
Key trends to be discussed include the convergence between health plans and providers, horizontal and vertical integration within the provider space, new risk transfer models, consolidation and portfolio rationalization among life sciences companies and globalization.  This session features Deloitte’s health plan leader, provider leader, life sciences M&A strategy leader, and LS&HC corporate finance leaders. *Hosted by HBS Health Care Alumni Association.*  *Open to the HBS community.*");

left("Sat May 30   
<span class='time'>4:00 pm -- 5:00 pm</span>   
 <span class='place'>HBS, Room TBA</span>");
right("[HBS Healthcare Alumni Reception at Reunion 2015](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=179)  
At this year's Spring Reunion, we will have the chance to informally gather alumni interested in healthcare. With 2,000+ graduates attending reunions, there are sure to be many old faces to see and new friends to welcome. *Hosted by Harvard Business School.*  *Open to HBS Alumni.*");

left("Sun Jun 14   
<span class='time'>6:30 pm -- 8:30 pm</span>   
 <span class='place'>Drexel University, Philadelphia, PA</span>");
right("[Reception at BIO International Convention](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=155)  
Join fellow HBS Healthcare Alumni for a cocktail reception to kick-off the 2015 BIO International Convention. Enjoy conversation and networking with friends old and new the evening prior to the event. *Hosted by HBS Health Care Initiative, HBS Health Care Alumni Association.*  *Open to HBS Alumni.*");

endTable();

?>

This listing may not include all health care recruiting events; please be sure to check the [MBA Event Calendar](http://beech.hbs.edu/eventCalendar/user/mainCalendar.do) for more company presentations. Also, these events can be downloaded at the Health Care Initiative event calendar [here](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx).

<?php 
section("Selected Health Care Events Around Harvard University and Boston"); 

beginTable();


left("Mon May 04   
<span class='time'>4:00 pm -- 6:00 pm</span>   
 <span class='place'>HSPH, Kresge G2</span>");
right("[Health Equity in an Unequal World](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=181)  
Speaker: Professor Sir Michael G. Marmot MBBS, MPH, PhD, FRCP, FFPHM, FMedSci, FBA, Director, UCL Institute of Health Equity (Marmot Institute). Welcome and discussion moderated by: Dean Julio Frenk, MD, MPH, PhD. Dean of the Faculty, Harvard T.H. Chan School of Public Health, T&G Angelopoulos Professor of Public Health and International Development, Harvard T.H. Chan School of Public Health and Harvard Kennedy School. *Hosted by Harvard School of Public Health.*  *Open to the Harvard community.*");

left("Mon May 04   
<span class='time'>4:00 pm -- 5:00 pm</span>   
 <span class='place'>Longwood Campus, Countway Library, Ballard Room</span>");
right("[Rebalancing Health and Healthcare: Building a Global Grassroots Movement for Change](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=182)  
In every country, healthcare systems are undergoing rapid change.  At the same time, the importance of social determinants of health is increasingly being recognized. Imagining and implementing health systems that integrate high-quality healthcare with community-based health promotion will require revolutionary approaches. Featured speakers: Vikas Saini, MD, President, and Shannon Brownlee, MS, Sr. Vice President, Lown Institute. *Hosted by Harvard School of Public Health.*  *Open to the Harvard community.*");

left("Wed May 06   
<span class='time'>8:00 am -- 12:00 pm</span>   
 <span class='place'>Boston Convention & Exhibition Center, Room 160</span>");
right("[MassMEDIC’s 19th Annual Conference](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=186)  
Keynote: Dean Kamen, Inventor and Entrepreneur, Deka Research. MassMEDIC is teaming up with UBM Canon and holding its 19th Annual Conference in conjunction with the Boston BIOMEDevice Show. In addition to a great line-up of speakers and unparalleled networking opportunities, your conference registration fee provides access to the BIOMEDevice Exhibit Hall, with product demonstrations, specialized tours and over 200 exhibitors. All this plus a voucher for lunch at the show! *Hosted by MassMEDIC.*  *Open to the public.*");

left("Wed May 06   
<span class='time'>9:00 am -- 4:00 pm</span>   
 <span class='place'>Boston Convention & Exhibition Center</span>");
right("[BIOMEDevice Boston Exhibition](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=187)  
The exhibition features two full days of networking, product design innovation, and educational opportunities under one roof. *Hosted by BIOMEDevice.*  *Open to the public.*");

left("Wed May 06   
<span class='time'>12:00 pm -- 3:00 pm</span>   
 <span class='place'>Bullfinch Tent, MGH</span>");
right("[4th Annual Mass General Global Health Expo](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=167)  
The 4th Annual MGH Global Health Expo will offer an opportunity for the MGH community to learn more about the exceptional breadth of global health activities happening across our campus, including opportunities available to all levels of Mass General staff, as well as to spur new collaborations and conversations among those interested in Global Health. Over 40 organizations, representing multiple departments and specialties, will be on hand to discuss their activities. All are welcome to join us for refreshments and to learn more about Global Health at Mass General.  *Open to the public.*");

left("Wed May 06   
<span class='time'>12:00 pm -- 2:00 pm</span>   
 <span class='place'>Countway Library, Lahey Room, 5th Floor</span>");
right("[Vital Conversations: Improving Communication Between Doctors and Patients](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=184)  
Health Information Lunchtime Lecture Series where Dennis Rosen, MD, Associate Medical Director of the Center for Pediatric Sleep Disorders at Boston Children's Hospital and Associate Professor of Pediatrics at Harvard Medical School, will discuss his book. *Hosted by Harvard Medical School.*  *Open to the public.*");

left("Thu May 07   
<span class='time'>4:00 pm -- 6:00 pm</span>   
 <span class='place'>MGH, Thier Conference Room </span>");
right("[The Healthcare Transformation Lab - Annual Event 2015](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=194)  
Speaker Topic:   The Healthcare Innovation Bubble. Speaker: Sachin Jain, MD, MBA - Chief Medical Officer at CareMore Health System, an innovative health plan and care delivery system and former Chief Medical Information and Innovation Officer at Merck & Co.  *Hosted by MGH.*  *Open to the Harvard community.*");

left("Fri May 08   
<span class='time'>8:00 am -- 3:00 pm</span>   
 <span class='place'>HLS, Wasserstein Hall, Milstein East BC</span>");
right("[Annual Conference: Law, Religion, and Health in America](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=164)  
This conference will: (1) identify the various ways in which law intersects with religion and health care in the United States; (2) understand the role of law in creating or mediating conflict between religion and health care; and (3) explore potential legal solutions to allow religion and health care to simultaneously flourish in a culturally diverse nation.  *Hosted by Harvard Law School Petrie Flom Center.*  *Open to the public.*");

left("Fri May 08   
<span class='time'>12:00 pm -- 1:00 pm</span>   
 <span class='place'>TBD</span>");
right("[Demystifying Drug Development Series](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=193)  
Speaker: Humphrey Gardner, MD.  *Hosted by Brigham and Women's Hospital.*  *Open to the public.*");

left("Tue May 12   
<span class='time'>6:00 pm -- 7:30 pm</span>   
 <span class='place'>Countway Library, Cannon Room, Building C</span>");
right("[Routine Medical Care: Perspectives from the Practice of Dr. Richard Cabot](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=183)  
Speakers: Christopher Crenner, MD, PhD, Robert Hudson and Ralph Major Professor and Chair of the Department of History and Philosophy of Medicine at the University of Kansas Medical Center. Dr. Richard Cabot, a leader of the early twentieth-century profession, was by turns both a prominent advocate for and an incisive critic of medicine at the turn of the century. His writings and letters help to document new and persisting dilemmas emerging in the daily practice of medicine.  *Hosted by Harvard Medical School.*  *Open to the Harvard community.*");

left("Wed May 13   
<span class='time'>2:00 pm -- 6:00 pm</span>   
 <span class='place'>Microsoft NERD, 1 Memorial Drive #1, Cambridge, MA 02142</span>");
right("[Life Sciences Informatics: The Massachusetts Opportunity for Global Leadership](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=189)  
Across Massachusetts, life sciences companies and healthcare organizations are collecting data: genome sequences, drug screens, clinical trial data, electronic medical records, claims data, and even patient-reported data. How we connect, analyze, organize and use that data presents an opportunity for our emerging life sciences informatics cluster that’s second to none. What is the market opportunity at the intersection of life sciences and technology and how can Massachusetts position itself as a world-leader in LSIX? *Hosted by MassBio.*  *Open to the public.*");

left("Fri May 22   
<span class='time'>8:00 am -- 10:00 am</span>   
 <span class='place'>MassBio, 300 Technology Square, 8th Floor, Cambridge, MA 02139</span>");
right("[Changes & Future Trends in the Cambridge Vector of Life Sciences](http://www.massbio.org/events/calendar/3205-changes_future_trends_in_the_cambridge_vector/event_detail)  
The landscape in greater Kendall Square and MA has changed dramatically in the last decade. With the arrival of big pharma R&D centers starting with Novartis in 2002, the explosion of incubators across MA, and the migration of technology and digital media companies to Cambridge and Boston, we have seen profound and sweeping changes. From accelerating rents to massive new construction projects housing campuses for big pharma R&D, nowhere has this change been more profound than in Cambridge, and the face and fate of Kendall Square has and continues to evolve rapidly. What do these seismic evolutions augur for our future? *Hosted by MassBio.*  *Open to the public.*");

left("Tue Jun 02   
<span class='time'>8:00 am -- 7:00 pm</span>   
 <span class='place'>Harvard Club of Boston</span>");
right("[Massachusetts Life Sciences Innovation Day 2015](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=188)  
Weathering Healthcare Climate Change. The 8th Massachusetts Life Sciences Innovation (MALSI) Day is the biggest day for life sciences startups and innovation in the Commonwealth of Massachusetts. This is a high-energy, hands-on event which brings together scientific leaders and business experts to mingle with scientists, post-docs, professors, entrepreneurs, innovators, and venture capitalists. *Hosted by The Massachusetts Technology Transfer Center.*  *Open to the public.*");

left("Fri Jun 05   
<span class='time'>8:00 am -- 10:00 am</span>   
 <span class='place'>MassBio</span>");
right("[Making the Connection: Biomanufacturing & Supporting Resources in Massachusetts](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=191)  
Come learn how to tap into the power of the Commonwealth’s Biomanufacturing resources to help your business prosper.  *Hosted by MassBio.*  *Open to the public.*");

left("Wed Jun 24   
<span class='time'>8:30 am -- 12:30 pm</span>   
 <span class='place'>HMS, Joseph Martin Conference Center</span>");
right("[Precision Medicine 2015: Patient Driven](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=185)  
This one and a half day meeting will include expert panels on how we are going to pay for precision medicine, on the ethical and regulatory challenges, delivering precision medicine to the point of care, and several inspiring examples of patient-driven successes. *Hosted by Harvard Medical School.*  *Open to the public.*");

left("Tue Jun 30   
<span class='time'>8:00 am -- 5:00 pm</span>   
 <span class='place'>HLS, Wasserstein Hall, Milstein West A</span>");
right("[Visible Solutions: How Neuroimaging Helps Law Re-envision Pain](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx?eventid=180)  
Can brain imaging be a “pain-o-meter” that tells courts when a person is in pain?  Can fMRI help us discern whether intractable chronic pain is “all in your head” or all in the brain – or will it require us to reconsider that distinction? Leading neuroscientists, legal scholars, and bioethicists will debate standards and limits on how the law can use brain science to get smarter about a subject that touches everyone. *Hosted by Harvard Law School Petrie Flom Center.*  *Open to the public.*");


endTable();

?>

These events can be downloaded at the Health Care Initiative event calendar [here](http://www.hbs.edu/healthcare/mba-experience/Pages/event-calendar.aspx).

<?php section("HBS Health Care In the News"); ?>

[A Peek Inside the Harvard Forum on Health Care Innovation](https://medtechboston.medstro.com/harvard-forum-on-healthcare-innovation/)  
Re: John Quelch  
*MedTech Boston*  
Apr 27, 2015

[Ariad Pharmaceuticals' Chief Fights to Hold On](http://www.bostonglobe.com/business/2015/04/08/ariad-pharmaceutical-chief-struggles-keep-control-cancer-drug-company-founded/XqdsnB9xdoU5jNBOyoeeRK/story.html)  
Re: Gary Pisano  
*Boston Globe*  
Apr 9, 2015

[3 Key Skills Clinicians Need in Executive Roles](http://www.beckershospitalreview.com/hospital-management-administration/3-key-skills-clinicians-need-in-executive-roles.html)  
Re: Michael Porter  
*Becker's Hospital Review*  
Apr 8, 2015

[Digital Summit Explores the New Economy](http://hbswk.hbs.edu/item/7764.html)  
Re:  HBS Digital Initiative
*HBS Working Knowledge*  
Apr 7, 2015

[The Skills Doctors and Nurses Need to Be Effective Executives](https://hbr.org/2015/04/the-skills-doctors-need-to-be-effective-executives)  
Re: Sachin Jain (MBA 2007)  
*Harvard Business Review*  
Apr 7, 2015

[The Slow, Steady Battle to Fix Cancer Care](http://hbswk.hbs.edu/item/7649.html)  
Re: Thomas Feeley  
*HBS Working Knowledge*  
Apr 1, 2015


<?php section("Recent Faculty Publications in Health Care"); ?>

<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48901' target='_blank'>Bloodbuy</a><br>Michael Norris and Richard G. Hamermesh<br><em>Harvard Business School Case</em><br><br>
<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48493' target='_blank'>Bonitas</a><br>Natalie Kindred and Regina E. Herzlinger<br><em>Harvard Business School Case</em><br><br>
<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48804' target='_blank'>CV Ingenuity (A)</a><br>Andrew Otazo and Regina E. Herzlinger<br><em>Harvard Business School Case</em><br><br>
<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48805' target='_blank'>CV Ingenuity (B)</a><br>Andrew Otazo and Regina E. Herzlinger<br><em>Harvard Business School Supplement</em><br><br>
<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48740' target='_blank'>Intrapreneurship at DaVita HealthCare Partners</a><br>Matthew G. Preble, Joseph B. Fuller and David J. Collis<br><em>Harvard Business School Case</em><br><br>
<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=49027' target='_blank'>Market-Based Solutions to Antitrust Threats—The Rejection of the Partners Settlement</a><br>Kevin A. Schulman, Barak D. Richman and Regina E. Herzlinger<br><em>New England Journal of Medicine</em><br><br>
<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48868' target='_blank'>Medalogix</a><br>Matthew G. Preble and Richard G. Hamermesh<br><em>Harvard Business School Case</em><br><br>
<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=49050' target='_blank'>Novartis Pharmaceuticals Corp: Redefining Success in the U.S. (A)</a><br>Shawn Tuli, Jared Katseff, Sean Liu, Philip Berkman, Arthur I Segel and Nicolas P. Retsinas<br><em>Harvard Business School Case</em><br><br>
<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=49067' target='_blank'>Novartis Pharmaceuticals Corp: Redefining Success in the U.S. (B)</a><br>Shawn Tuli, Jared Katseff, Sean Liu, Philip Berkman, Arthur I Segel and Nicolas P. Retsinas<br><em>Harvard Business School Supplement</em><br><br>
<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48999' target='_blank'>Philips Healthcare Latin America</a><br>Sunil Gupta<br><em>Harvard Business School Case</em><br><br>
<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48549' target='_blank'>The Affordable Care Act (F): Regaining Momentum</a><br>Michael Norris and Joseph L. Bower<br><em>Harvard Business School Supplement</em><br><br>
<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48554' target='_blank'>The Affordable Care Act (J): Healthcare.gov</a><br>Michael Norris and Joseph L. Bower<br><em>Harvard Business School Case</em><br><br>
<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48963' target='_blank'>The I-PASS Patient Handoff Program</a><br>Michael Norris and Robert S. Huckman<br><em>Harvard Business School Case</em><br><br>
<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48840' target='_blank'>Transforming Care at UnityPoint Health – Fort Dodge</a><br>Ashley-Kay Fryer, Amy C. Edmondson and Morten T. Hansen<br><em>Harvard Business School Case</em><br><br>
<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48888' target='_blank'>Twine Health</a><br>Matthew G. Preble, Ariel D. Stern and Robert S. Huckman<br><em>Harvard Business School Case</em><br><br>
<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=49007' target='_blank'>Zipongo: Improving Health by Redesigning the Food Chain</a><br>Ray A. Goldberg<br><em>Harvard Business School Case</em><br><br>




<?php /*********  END EMAIL BODY ******************/?>
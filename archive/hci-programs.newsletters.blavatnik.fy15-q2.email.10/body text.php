<?php 
// MAILING INFO
$title = "Blavatnik Fellowship October newsletter";
$campaignName = "hci-programs.newsletters.blavatnik.fy15-q2.email.10";
$campaignUrl = "https://na15.salesforce.com/701i0000000sy1v";
$templateUrl = "https://engage1.silverpop.com/composeMailing7.do?action=displayLoadTemplate&mailingid=11324627&templateType=0";
$from = null;
$sig = null;
$subject = "Blavatnik Fellowship October newsletter ";
$containsFirstName = false;


/**************  BEGIN EMAIL ////////////////

# <?php echo $title ?>  
**Campaign:** <?php echo $campaignName ?>       
**Campaign Link:** <?php echo $campaignUrl ?>  
**From:** <?php echo $from; ?>
**Subject:** <?php echo $subject; ?> 
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Blavatnik</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>         <style type="text/css">
#outlook a {padding:0;} 
body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
.ExternalClass {width:100%;}
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
a,a:link,a:active,a:visited{color:#48c4b7;text-decoration:underline;padding:0px;margin:0px !important;}
#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
table td {border-collapse: collapse;}
img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}
a img {border:none;}
.image_fix {display:block;}
p {margin: 0px 0px 1em 0px; width:100%;}
h1, h2, h3, h4, h5, h6 {color: black !important;}
h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color:#48c4b7 !important;}
h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {color: red !important;}
h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {color: purple !important;}
</style>
    </head>
    <body>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" id="backgroundTable">
            <tbody>
                <tr>
                    <td bgcolor="#eee29f" align="center"><a name="Event_photo" xt="SPNOTRACK"></a>
                    <table cellspacing="0" cellpadding="0" width="580" border="0">
                        <tbody>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="580">
                                <table cellspacing="0" cellpadding="5" width="580" border="0" style="border:none; mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse:collapse;">
                                    <tbody>
                                        <tr>
                                            <td width="81%" valign="bottom" align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><span style="color: rgb(128, 130, 133); font-size: 11px; line-height: 15px;">You are receiving this update because you have expressed an interest in health care activities at HBS.</span></td>
                                            <td width="19%" valign="bottom" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:11px;"><a target="_blank" xt="SPCLICKTOVIEW" href="#SPCLICKTOVIEW" name="viewinbrowser_1">View in browser</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="580" bgcolor="#FFFFFF">
                                <table cellspacing="0" cellpadding="0" width="580" border="0" style="border:none; mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse:collapse;">
                                    <tbody>
                                        <tr>
                                            <td width="20" valign="middle" height="112" align="left">&nbsp;</td>
                                            <td valign="middle" align="left"><img width="290" height="72" spname="images_HBS_Blavatnik_logo.png" xt="SPIMAGE" contentid="2cb83a72-148f68b0a5a-ce240116a00f91fcaa71fbe5df141a8a" style="display:block;" alt="BLAVATNIK FELLOWSHIP IN LIFE SCIENCE ENTREPRENEURSHIP" name="images_HBS_Blavatnik_logo.png" src="images_HBS_Blavatnik_logo.png" /></td>
                                            <td valign="middle" align="right">
                                            <table cellspacing="0" cellpadding="10" width="120" border="0" style="border:none; mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse:collapse;">
                                                <tbody>
                                                    <tr>
                                                        <td valign="middle" bgcolor="#a41034" align="center" style="color:#ffffff; font-size:14px; font-family:Arial, Helvetica, sans-serif;"><strong>OCT 2014</strong></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="580" bgcolor="#FFFFFF" align="center">
                                <table cellspacing="0" cellpadding="0" width="540" border="0" align="center" style="min-width:540px;border:none; mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse:collapse;">
                                    <tbody>
                                        <tr>
                                            <td width="540" align="left">
                                            <table cellspacing="0" cellpadding="0" width="540" border="0">
                                                <tbody>
                                                    <tr>
                                                        <td width="540" style="font-family:Arial, Helvetica, sans-serif; color:#000000; font-size:14px; line-height:18px; white-space: normal;">
                                                        <h1 style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 20px; line-height: 20px; white-space: normal; margin-top: 0px; margin-bottom: 10px;">Welcome</h1>
                                                        <p>Welcome to the inaugural issue of the Blavatnik Fellowship in Life Science Entrepreneurship newsletter. Each quarter, we will update you with the latest news from the Fellows, recap a featured event, post upcoming workshops, and highlight a guest blogger. As a friend of the Blavatnik Fellowship, this newsletter seeks to connect you to the Fellowship and the life science entrepreneurship community at Harvard Business School.</p>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="10" style="line-height:10px;"></td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="font-size:20px; color:#0d667f; font-family:Arial, Helvetica, sans-serif;"><strong>New Fellows 2014-2015</strong></td>
                                        </tr>
                                        <tr>
                                            <td height="10" style="line-height:10px;"></td>
                                        </tr>
                                        <!-- begin profile row 1 -->
                                        <tr>
                                            <td>
                                            <table cellspacing="0" cellpadding="0" width="540" border="0" style="border:none; mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse:collapse;">
                                                <tbody>
                                                    <tr>
                                                        <td width="110" valign="top" style="border-right:1px solid #ffffff;"><img alt="Alexandra Dickson" width="110" height="165" spname="Dickson.gif" xt="SPIMAGE" contentid="6e8a732f-14914eca889-c2a8af96738cc20073a7254c07aedd78" style="display:block;font-size:8px;" alt="Alexandra Dickson" name="Dickson.gif" src="Dickson.gif" title="Alexandra Dickson" /></td>
                                                        <td width="429" valign="top" height="126" bgcolor="#0d667f">
                                                        <table cellspacing="0" cellpadding="15" width="100%" border="0" style="border:none; mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse:collapse;">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#ffffff; line-height:18px; padding-bottom:0px;">
                                                                    <p style="font-size: 17px; line-height: 15px; text-transform: uppercase; margin-top: 0px; margin-bottom: 10px;"><b>alexandra dickson</b></p>
                                                                    <p>Alexandra is a biologist skilled in life science strategy and analytics.</p>
                                                                    <p>Most recently, Alexandra worked in Biogen Idec&rsquo;s business insights and analytics group to build the infrastructure to support and monitor the launch of the company&rsquo;s hemophilia franchise. She also led hemophilia salesforce analytics, acting as the source of performance insights for the salesforce. 
                                                                    	<br/><a style="color:#48c4b7" name="Full biography_1" xt="SPCLICK" href="http://www.hbs.edu/news/releases/Pages/blavatnik-fellows-2014.aspx" target="_blank">Full biography<br/></a>
                                                                    </p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="10" style="line-height:10px;"></td>
                                        </tr>
                                        <!-- end profile row 1 -->                           <!-- begin profile row 2 -->
                                        <tr>
                                            <td>
                                            <table cellspacing="0" cellpadding="0" width="540" border="0" style="border:none; mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse:collapse;">
                                                <tbody>
                                                    <tr>
                                                        <td width="110" valign="top" style="border-right:1px solid #ffffff;">
                                                        	<img width="110" height="165" spname="Jaeker.gif"             xt="SPIMAGE" contentid="6e8a732f-14914d09f85-c2a8af96738cc20073a7254c07aedd78" style="display:block;font-size:8px;" alt="Christoph Jaeker" name="Jaeker.gif"             src="Jaeker.gif"             title="Christoph Jaeker" />
                                                        </td>
                                                        <td width="429" valign="top" height="126" bgcolor="#0d667f">
                                                        <table cellspacing="0" cellpadding="15" width="100%" border="0" style="border:none; mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse:collapse;">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#ffffff; line-height:18px; padding-bottom:0px;">
                                                                    <p style="font-size: 17px; line-height: 15px; text-transform: uppercase; margin-top: 0px; margin-bottom: 10px;"><b>Christoph Jaeker</b></p>
                                                                    <p>Christoph is a biochemist with extensive experience in life science management.</p>
                                                                    <p>
                                                                    Prior to the Fellowship, Christoph worked in Business Development for biotech company Nimbus Discovery, which uses computational chemistry to develop therapies against difficult-to-drug targets. He was an integral team member for signing two major research collaborations with Shire and Monsanto, and served as the commercial lead for one of Nimbus&rsquo; preclinical programs. 
                                                                    	<br/><a name="Full biography_1" xt="SPCLICK" href="http://www.hbs.edu/news/releases/Pages/blavatnik-fellows-2014.aspx" target="_blank">Full biography<br/></a>
                                                                    </p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="10" style="line-height:10px;"></td>
                                        </tr>
                                        <!-- end profile row 2 -->                           <!-- begin profile row 3 -->
                                        <tr>
                                            <td>
                                            <table cellspacing="0" cellpadding="0" width="540" border="0" style="border:none; mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse:collapse;">
                                                <tbody>
                                                    <tr>
                                                        <td width="110" valign="top" style="border-right:1px solid #ffffff;">
                                                        	<img width="110" height="165" spname="Levy-head-shot-(2).gif" xt="SPIMAGE" contentid="6e8a732f-14914eca879-c2a8af96738cc20073a7254c07aedd78" 
                                                        		style="display:block;font-size:8px;" alt="Louis Levy" name="Levy-head-shot-(2).gif" src="Levy-head-shot-(2).gif" title="Louis Levy" />
                                                        </td>
                                                        <td width="429" valign="top" height="126" bgcolor="#0d667f">
                                                        <table cellspacing="0" cellpadding="15" width="100%" border="0" style="border:none; mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse:collapse;">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#ffffff; line-height:18px; padding-bottom:0px;">
                                                                    <p style="font-size: 17px; line-height: 15px; text-transform: uppercase; margin-top: 0px; margin-bottom: 10px;"><b>louis levy</b></p>
                                                                    <p>Louis is a passionate entrepreneur with a consulting background in biotech.</p>
                                                                    <p>Louis began his career with Cepton, a strategy consulting boutique for French mid-size biotechs, where he worked on pharmaceutical development, European commercial launches, and R&amp;D organizations. As an entrepreneur, he co-founded and launched a web service to book a table, order, and pre-pay for meals at restaurants in France. 
                                                                    	<br/><a style="color:#48c4b7" name="Full biography_1" xt="SPCLICK" href="http://www.hbs.edu/news/releases/Pages/blavatnik-fellows-2014.aspx" target="_blank">Full biography<br/></a>
                                                                    </p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="10" style="line-height:10px;"></td>
                                        </tr>
                                        <!-- end profile row 3 -->      <!-- begin profile row 4 -->
                                        <tr>
                                            <td>
                                            <table cellspacing="0" cellpadding="0" width="540" border="0" style="border:none; mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse:collapse;">
                                                <tbody>
                                                    <tr>
                                                        <td width="110" valign="top" style="border-right:1px solid #ffffff;"><img width="110" height="165" spname="Misra.gif" xt="SPIMAGE" contentid="6e8a732f-14914eca869-c2a8af96738cc20073a7254c07aedd78" style="display:block;font-size:8px" alt="Sid Misra" name="Misra.gif" src="Misra.gif" title="Sid Misra" /></td>
                                                        <td width="429" valign="top" height="126" bgcolor="#0d667f">
                                                        <table cellspacing="0" cellpadding="15" width="100%" border="0" style="border:none; mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse:collapse;">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#ffffff; line-height:18px; padding-bottom:0px;">
                                                                    <p style="font-size: 17px; line-height: 15px; text-transform: uppercase; margin-top: 0px; margin-bottom: 10px;"><b>sid misra</b></p>
                                                                    <p>Sid is an electrical engineer dedicated to commercializing science.</p>
                                                                    <p>Before joining the Fellowship, Sid co-founded two companies in the telecommunications and internet sectors and worked as a venture investor at Khosla Ventures and at Applied Ventures, the corporate venture capital arm of Applied Materials. After completing his MBA, Sid wrote six HBS cases on venture-backed companies with Professors Joe Lassiter and Bill Sahlman.
                                                                    	<br/><a style="color:#48c4b7" name="Full biography_1" xt="SPCLICK" href="http://www.hbs.edu/news/releases/Pages/blavatnik-fellows-2014.aspx" target="_blank">Full biography<br/></a>
                                                                    </p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="10" style="line-height:10px;"></td>
                                        </tr>
                                        <!-- end profile row 4 -->                                         <!-- begin profile row 5 -->
                                        <tr>
                                            <td>
                                            <table cellspacing="0" cellpadding="0" width="540" border="0" style="border:none; mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse:collapse;">
                                                <tbody>
                                                    <tr>
                                                        <td width="110" valign="top" style="border-right:1px solid #ffffff;"><img width="110" height="165" spname="Unger.gif" xt="SPIMAGE" contentid="6e8a732f-14914eca84f-c2a8af96738cc20073a7254c07aedd78" style="display:block;font-size:8px;" alt="Meridith Unger" name="Unger.gif" src="Unger.gif" title="Meridith Unger" /></td>
                                                        <td width="429" valign="top" height="126" bgcolor="#0d667f">
                                                        <table cellspacing="0" cellpadding="15" width="100%" border="0" style="border:none; mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse:collapse;">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#ffffff; line-height:18px; padding-bottom:0px;">
                                                                    <p style="font-size: 17px; line-height: 15px; text-transform: uppercase; margin-top: 0px; margin-bottom: 10px;"><b>meridith unger</b></p>
                                                                    <p>Meridith is a health care entrepreneur with venture capital expertise.</p>
                                                                    <p>Meridith has worked closely with VC and scientific founders in the creation of several NewCos, including FlexLite Corporation, Lumos Catheter Systems, Sage Therapeutics, AviTx Inc, Kala Pharmaceuticals, and Asterand Inc. In 2010, she was the co-founder and CEO of Aukera Therapeutics, which won the Life Sciences Track and the Audience Choice Award in the MIT $100K Competition, and was a finalist for MassChallenge. 
                                                                    	<br /><a style="color:#48c4b7" name="Full biography_1" xt="SPCLICK" href="http://www.hbs.edu/news/releases/Pages/blavatnik-fellows-2014.aspx" target="_blank">Full biography<br/></a>
                                                                    </p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="line-height:10px;"></td>
                                        </tr>
                                        <!-- end profile row 5 -->
                                        <tr>
                                            <td style="line-height:10px;">
                                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                <tbody>
                                                    <tr>
                                                        <td valign="top" align="left" style="padding:15px;font-family:Arial, Helvetica, sans-serif; color:#000000; font-size:14px; line-height:18px;">
                                                        <h3 style="font-size:17px; color:#000000; line-height:19px; margin-top:0px; margin-bottom:0px;"><strong>Welcome Reception for the new Blavatnik Fellows</strong></h3>
                                                        <p>On Thursday, October 2, 2014, members of the Blavatnik Key Advisory Board, Entrepreneurship Network, Office of Technology Development, and Harvard Business School welcomed the new class of Blavatnik Fellows. We look forward to working with them this year.</p>
                                                        </td>
                                                        <td width="225" valign="top"><img width="220" height="146" src="reception2.gif" xt="SPIMAGE" contentid="6e8a732f-14914ee0834-c2a8af96738cc20073a7254c07aedd78" name="reception2.gif" spname="reception2.gif" alt="Blavatnik Fellows" /></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="line-height:10px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="540" align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:20px; color:#a41034;"><strong>Featured Event</strong></td>
                                        </tr>
                                        <tr>
                                            <td height="10" style="line-height:10px;"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                            <table cellspacing="0" cellpadding="0" width="100%" border="0" style="border:none; mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse:collapse;">
                                                <tbody>
                                                    <tr>
                                                        <td width="276" valign="top"><img width="275" height="183" border="0" spname="presentation-event3.gif" xt="SPIMAGE" contentid="6e8a732f-1491511ad4c-c2a8af96738cc20073a7254c07aedd78" style="display:block;" alt="All Fellows" name="presentation-event3.gif" src="presentation-event3.gif" title="All Fellows" /></td>
                                                        <td valign="top" bgcolor="#FFFFFF">
                                                        <table cellspacing="0" cellpadding="15" width="100%" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="left" style="font-family:Arial, Helvetica, sans-serif; color:#000000; font-size:14px; line-height:18px;">
                                                                    <h3 style="font-size:17px; color:#000000; line-height:19px; margin-top:0px; margin-bottom:0px;">Blavatnik Fellows: Achievements &amp; Lessons in Life Science Entrepreneurship</h3>
                                                                    <p>May 21, 2014</p>
                                                                    <p>On May 21st, The Blavatnik Fellowship hosted an event in which the inaugural cohort presented the progress they made with their projects. Hear from the Fellows about their goals in joining the Fellowship,&nbsp;project selection paths,&nbsp;and insights from the year. To view a short video of the event, click below.</p>
                                                                    <p style="margin-bottom:0px;"><a name="Bookmark__3_1" style="color:#48c4b7;" xt="SPCLICK" href="http://youtu.be/eo65AW9hkD4" target="_blank">View video here &raquo;</a></p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                            <table cellspacing="0" width="540" border="0" style="mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse:collapse;">
                                                <tbody>
                                                    <tr>
                                                        <td width="540" bgcolor="#A41034" align="left">
                                                        <table cellspacing="0" cellpadding="15" width="100%" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="121" valign="top" align="center" style="font-size:20px; font-family:Arial, Helvetica, sans-serif; color:#ffffff; line-height:24px;"><img width="45" height="45" border="0" spname="images_HBS_Blavatnik_calicon.png" xt="SPIMAGE" contentid="2cb83a72-148f68b0b27-ce240116a00f91fcaa71fbe5df141a8a" id="images_HBS_Blavatnik_calicon.png" name="images_HBS_Blavatnik_calicon.png" alt="" src="images_HBS_Blavatnik_calicon.png" /><strong><br />
                                                                    </strong><strong>EVENTS<br />
                                                                    FOR FELLOWS</strong></td>
                                                                    <td width="357" valign="middle" align="left" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:13px; line-height:14px;">
                                                                    <p><strong>10/15/14 - All Day: Speaking Backwards with Bink Garrison</strong></p>
                                                                    <p><strong>10/29/14</strong><strong>&nbsp;- 6:00PM: Meet and Greet with the Health Care Alumni Association</strong><strong>&nbsp;</strong></p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="540" align="left" style="font-family:Arial, Helvetica, sans-serif; color:#000000; font-size:14px; line-height:18px;">
                                            <h3 style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 20px; line-height: 24px; margin-top: 0px; margin-bottom: 0px;">Inaugural Year: A Look Back</h3>
                                            <p style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 18px;"><em>by Prof. Vicki Sato</em></p>
                                            <p style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 18px;"><img width="150" height="156" border="0" align="left" spname="vicki-headshot.gif" xt="SPIMAGE" contentid="6e8a732f-149150ed18b-c2a8af96738cc20073a7254c07aedd78" style="display:block;font-size:8px;" alt="Prof. Vicki Sato" name="vicki-headshot.gif" src="vicki-headshot.gif" title="Daniel Oliver" /></p>
                                            <p>The inaugural year of the Blavatnik Fellowship in Life Science Entrepreneurship exceeded expectations and I am still buzzing from the energy and accomplishments of the first class of BFs.</p>
                                            <p>Throughout the year, the Fellows partnered with Harvard inventors to find the best commercialization paths for a varied set of scientific inventions and discoveries. By the end of the Fellowship year, four companies had been formed and two additional projects had secured funding that enabled continued technology development.</p>
                                            <p>Ross Leimberg (MBA 2012) finished his work with Professors Cliff Woolf and Bruce Bean by orchestrating a pivot of the technology and securing non-dilutive funding. The team is pursuing development of novel proprietary therapies for several indications in pain and inflammation. Ross joined AbbVie as a member of the Ventures and Early Stage Collaborations team, where he is identifying and doing deals around projects in the research and early development phase.</p>
                                            <p>Dan Oliver (MBA 2013) helped found Voxel8 with Professor Jennifer Lewis and 3 scientists from the Lewis lab. Voxel8 focuses on broad applications of 3-D printing of highly conductive circuitry and 3D lithium ion batteries for use in various life science applications. As an early member of the team, Dan is using his diverse skills to manage the engineering programs while also identifying new markets and early adopters for the product lines. Voxel8 was recently announced as a MassChallenge Winner. &nbsp;</p>
                                            <p>Steve Porter (MBA 2011), our MD/MBA, managed to juggle numerous balls: working with Ben Bergo, CEO of nPlex, a diagnostic startup using technology from George Whitesides&rsquo; laboratory, he helped shape the product development strategy for this young company. In addition, he contributed to the formation of Macrolide Pharmaceuticals, a company using innovative synthetic chemistry from Professor Andy Myers' laboratory to create new and powerful antibiotics. Together with serial entrepreneur Larry Miller, Steve helped shape the strategy for Series A fundraising before leaving for Cleveland to do his residency in obstetrics and gynecology, but hopes to keep his hand in the business of science.</p>
                                            <p>John Strenkowski (MBA 2009) has been partnering with Professor Kevin Kit Parker from the Wyss Institute on several game changing technologies to create new materials. While the details of their startup financing are still confidential, John has identified partners for a new artificial heart valve technology and for nanofiber-based textiles created by a new industrial process. John continues to work with Kit in a rather cool science-business partnership that aspires to spin out new enterprises on a steady basis.</p>
                                            <p>Ridhi Tariyal (MBA 2009) recently incorporated Next Gen Jane and is securing funding for this enterprise that will enable young women interested to manage many aspects of their health and fertility. She also worked with Professor Bob Datta of the Neurobiology Department at Harvard Medical School to secure $1.05MM in corporate financing for further prototype development of a technology that could greatly facilitate the testing accuracy of experimental drugs treating neurocognitive diseases.</p>
                                            <p>Tracy Saxton Perry, our multitalented Program Manager, and I are now engulfed in the excitement of the new group of Fellows who promise to make Year 2 as exciting as the first. More from us later, but come meet the Fellows at the i-lab and hear their evolving stories.</p>
                                            <p>Cheers,<br />
                                            Vicki</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="10" style="line-height:10px;"></td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="border-top:1px solid #a41034;">
                                            <table cellspacing="0" cellpadding="15" width="90%" border="0" align="center" style="border:none; mso-table-lspace:0pt; mso-table-rspace:0pt; border-collapse:collapse;">
                                                <tbody>
                                                    <tr>
                                                        <td width="540" align="center" style="font-size:12px; font-family:Arial, Helvetica, sans-serif; color:#a41034; line-height:16px;"><strong>The Blavatnik Fellowship in Life Science Entrepreneurship is a one-year fellowship for HBS alumni who have earned their MBA in the last seven years. Blavatnik Fellows are hired to foster entrepreneurship and commercialization of biomedical innovation from laboratories throughout Harvard University. </strong></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left"><img width="215" height="59" spname="images_HBS_Blavatnik_HBSlogo.png" xt="SPIMAGE" contentid="2cb83a72-148f68b0a37-ce240116a00f91fcaa71fbe5df141a8a" alt="Harvard Business School" name="images_HBS_Blavatnik_HBSlogo.png" src="images_HBS_Blavatnik_HBSlogo.png" /></td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000; line-height:16px;">Copyright &copy; 2014 President &amp; Fellows of Harvard College<br />
                                <a name="Bookmark__4_1" style="color:#000000;" xt="SPONECLICKOPTOUT" href="#SPONECLICKOPTOUT" target="_blank">Unsubscribe</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a name="contact_us" xt="SPBOOKMARK" style="color:#000000;" href="mailto:blavatnikfellowship@hbs.edu">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a name="Bookmark__6" xt="SPBOOKMARK" style="color:#000000;" href="http://www.hbs.edu/about/Pages/privacy.aspx">Privacy Policy</a></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
<?php // LINKS FILLED IN BY MARKDOWN ?>
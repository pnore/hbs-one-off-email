# Dear %%alumni_first_name%%, 

Thank you for volunteering to participate in the HBS Healthcare Student-Alumni Mentor Program! 
We are thrilled with your willingness to engage with HBS students.

We are writing to let you know that students have selected their mentors.  
Because there were fewer students than alumni who opted into the program, 
it was not possible to match all the alumni who signed up.  As a result, 
we were unable to match you with a student this year.

As a gesture of our gratitude, we would like to send you a gift at the shipping address below. 
If you have any corrections, please [contact us] by December 3. 

# Address:  
%%alumni_first_name%% %%alumni_last_name%%  
%%address_line_1%%  
%%address_line_2%%  
%%address_line_3%%  
%%address_line_4%%    

On behalf of the Healthcare Initiative, we want to thank you once again for volunteering. 
We hope that you will be a part of the HBS Healthcare Student-Alumni Mentor Program next year. 

## Thank you,  
Richard G. Hamermesh  
Professor of Management Practice  
Harvard Business School  

Rob Huckman  
Professor of Business Administration  
Harvard Business School  

Cara Sterling  
Director, Healthcare Initiative  
Harvard Business School 

[your HBS Alumni Profile Page]: %%hbs_alumni_profile_url%%
[update your profile]: https://www.alumni.hbs.edu/community/Pages/edit-profile.aspx
[contact us]: mailto:healthcare_initiative@hbs.edu?subject=Update%20Alumni%20Address%20for%20Mentor%20Program
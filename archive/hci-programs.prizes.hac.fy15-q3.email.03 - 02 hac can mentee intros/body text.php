<?php 
// MAILING INFO
$title = "Instructions to CAN Advisees";
$campaignName = "hci-programs.prizes.hac.fy15-q3.email.03 - 02 hac can mentee intros";
$campaignUrl = "https://na15.salesforce.com/701i0000001Qybe";
$from = $caraFrom;
$sig = $caraSig;
$subject = "[HBS Health Care] Challenge Advisory Network Instructions";
$containsFirstName = true;
$baseLink = '';
$baseLinkName = "";
$relationalTableQuery = "/Shared/hac advisee query 6";


/**************  BEGIN EMAIL ////////////////

# <?php echo $title ?>  
**Campaign:** <?php echo $campaignName ?>       
**Campaign Link:** <?php echo $campaignUrl ?>  
**From:** <?php echo $from; ?>
**Subject:** <?php echo $subject; ?> 
*/
?>

## You have been matched with several advisors for the Health Acceleration Challenge!

A few weeks ago you identified key topics on which you'd like to receive advice.  We sent your request to the Challenge Advisory Network and the response was overwhelming.  

The following people have volunteered to advise your organization:  

<?php 
beginTable( "$relationalTableQuery", "12", "leftright", "%%Mentor_First_Name%%");

// note, somehow this gets translated from & to &amp; and it messes the merge up. you have to manually fix it!
row( "**%%Mentor_First_Name%% %%Mentor_Last_Name%%** (<a href=\"mailto:%%Mentor_Email%%?subject=Challenge%20Advisory%20Network\">%%Mentor_Email%%</a>)  
%%Mentor_Title%%  
*%%Mentor_Company%%*");

endTable();
?>

Please reach out to them by **Tuesday, March 31st** to make an initial contact.  

Please click [here] for a great article from *Harvard Business Review* entitled "[The Art of Giving and Receiving Advice]." 
I hope it will be useful to you as you determine your role in advising the organization.  

For the latest information about the Challenge, please visit  [www.HealthAccelerationChallenge.com].  

Thank you so much for participating.  I look forward to seeing you in April.  

<?php echo $sig; ?>



<?php // LINKS FILLED IN BY MARKDOWN ?>
[Health Acceleration Challenge]: http://forumonhealthcareinnovation.org/
[www.HealthAccelerationChallenge.com]: http://forumonhealthcareinnovation.org/
[The Art of Giving and Receiving Advice]: https://cb.hbsp.harvard.edu/cbmp/pl/35115216/35115218/6a9dcede3c5b8bf2eae6f9d048920861
[here]: https://cb.hbsp.harvard.edu/cbmp/pl/35115216/35115218/6a9dcede3c5b8bf2eae6f9d048920861
<?php

?>  

The Harvard Business School Health Care Initiative
Cordially Invites You (and Your Partner)  


to an  


Informal Reception for  


Harvard Business School and  

Harvard School of Public Health Faculty and Staff at the home of  
John and Joyce Quelch  
57 Baker Bridge Road  
Lincoln, MA 01773  
Tel: 781-259-0594 on  
Sunday, June 8, 4:00 – 6:00 pm  

![flowers][]

<?php
beginTable();
left("RSVP  
Elaine Shaffer eshaffer@hbs.edu  
617-495-1683   
");
right("");
endTable();
?>


[flowers]: http://www.hbs.edu/healthcare/images/photos/flowers.png
[word doc]: http://www.hbs.edu/healthcare/images/photos/microsoft-word-2010-logo-for-downloadable-file-template.png
[wildside]: http://www.hbs.edu/healthcare/images/photos/wildside.png
[first printer asw]: http://www.hbs.edu/healthcare/images/photos/first-printer.png

[Have a Health Care Mentor? Win an HBS Health Care Fleece or T-Shirt!]: https://secure.hbs.edu/poll/open/pollTakerOpen.jsp?poll=135058
[2014-15 EC health care courses]: http://j.mp/fy14-hc-ec-course-prereg
[Health Care EC Send-off with Wildside]: https://docs.google.com/forms/d/14EwhKbk7nVxn5wkw3KIdTCmh3IUPcaBz5QiFQaigYRA/viewform
[Health Care Student Mixer at First Printer]: http://thefirstprinter.com/

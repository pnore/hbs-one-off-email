.topheader {
    background-color: <?php get_color($topheader_color); ?>;
}

.main-background {
    background-color: <?php echo get_color($background_color); ?>;
}

.main-body {
    background-color: <?php get_color($body_background_color); ?>;
    line-height:25px;
}

h1 {
    color: rgb(144, 0, 42); text-decoration: underline;
    font-family: Arial, Helvetica, sans-serif;
    font-weight: normal;
    font-size: 24px;
    line-height:20px;
}

h2 {
 /*    font-size:9.0pt;*/
    font-size:1em;
    font-family:'Arial','sans-serif';
    color:#515151;
    line-height: normal;
    font-weight:bold;
}

p {
    font-size:9.0pt;
    font-family:'Arial','sans-serif';
    color:#515151;
    padding:0px;
    margin:0px;
}

li {font-size:9.0pt;
    font-family:'Arial','sans-serif';
    color:#515151;
    padding:0px;
    margin:0px;
}

#content a {
    color: <?php echo $possible_colors[$highlight_text_color] ?>; 
    font-weight:normal;
}
    

#content h2 a {
    font-weight:bold;
}
}

<?php section("Health Care Happenings"); ?>

## HEALTH ACCELERATION CHALLENGE 
<?php 
beginTable();
left("
Harvard Business School and Harvard Medical School proudly announce the finalists of the Health Acceleration Challenge:  [Bloodbuy](http://openforum.hbs.org/challenge/hbs-hms-health-acceleration-challenge/winners/bloodbuy-is-a-cloud-based-platform-that-connects-hospitals-and-blood-centers-nationwide-to-ensure-the-efficient-flow-of-lifesaving-blood-products-to-patients-in-need-we-are-redefining-the-vein-to-vein-value-chain), [I-Pass](http://openforum.hbs.org/challenge/hbs-hms-health-acceleration-challenge/winners/the-i-pass-handoff-process-high-reliability-communication-for-better-patient-handoffs-and-safer-care), [Medalogix](http://openforum.hbs.org/challenge/hbs-hms-health-acceleration-challenge/winners/improving-a-patient-s-final-days-by-leveraging-data-insights) and [Twine](http://openforum.hbs.org/challenge/hbs-hms-health-acceleration-challenge/winners/twine-collaborative-care-platform-empowering-patients-through-apprenticeship)! Our announcement video can be seen [here](http://forumonhealthcareinnovation.org/?utm_campaign=hac_announce&utm_source=alumni&utm_medium=email&utm_content=video#video).  

The four finalists will share the $150,000 Cox Prize, have an HBS case study written about their innovation and have the 
opportunity to present at the Forum on Health Care Innovation conference to 150 senior health care executives.  
These finalists were selected out of an impressive pool of 478 applications from all over the world.  ", "280px", "normal");

right("[![Health Acceleration Challenge Poster][]](http://healthaccelerationchallenge.com/?utm_campaign=hac_launch&utm_source=healthcareinitiativelist&utm_medium=email)
	", "292px", "normal", "middle");
endTable();
?>

In addition to the applications, the Health Acceleration Challenge attracted 20,000 visitors from 127 countries. Nearly 3000 participants including numerous alumni posted over 2,000 comments on our open application platform. 

If you'd like to join the Challenge Advisory Network and support these organizations---or any of the applicants---through mentoring and advising, please email Cara Sterling at [csterling@hbs.edu](mailto:csterling@hbs.edu). We know they would appreciate your support!

## Alumni Reception at JP Morgan Conference 


<?php
beginTable();

left("Join more than 100 fellow HBS alumni in San Francisco on January 11, 2015, from 7:00 p.m.--9:30 p.m. at the Bluestem Brasserie at One Yerba Buena Lane.
Kick off the start of the 33rd annual JP Morgan Health Care Conference with cocktails and networking.  Price is $30. 
[RSVP](http://www.hbshealthalumni.org/store.html?event_id=913) is required.  
" . button('RSVP', 'http://www.hbshealthalumni.org/store.html?event_id=913'), "290px", "normal");
right('[![Bluestem Brasserie Image][]](http://www.hbshealthalumni.org/store.html?event_id=913)', "282px");

endTable();
?>  


## Alumni Mentor Program
<?php 
beginTable();

left('![speech bubbles][]');
right("Thanks to your support, over 200 alumni-student pairs were matched as part of the Health Care Mentor Program! Alumni mentor volunteers, 
<a name='update_preferred_address_1' xt='LPWEBFORMOPTIN' href='#LPWEBFORMOPTIN' target='_blank' xtwebform='6694622' style='color: rgb(229, 102, 93); background-color: rgb(255, 255, 255);'>click here to update your address</a>
 by Tue 12/16 and keep an eye out for a special gift that is on its way to you! Schedule some time to connect with your mentee during the upcoming holiday season. If your mentee has not contacted you yet and you have not let us know yet, 
 please [email us](mailto:healthcare_initiative@hbs.edu?subject=HBS%20Health%20Care%20Mentor%20Program).  ");

endTable();
?>


## Exec Ed Offerings in Health Care
<?php 
beginTable();
left('![exec ed][]');
right("Executive Education is offering the following health care programs over the next six months: 
");
endTable();
?>
**[Value
Measurement for Health Care](http://www.exed.hbs.edu/programs/vmhc/Pages/default.aspx)** examines how to
enable systematic improvement in the care delivery process by moving toward
better outcome and cost measurement.   This program will take place
April 6-8, 2015.

**[Business
Innovations in Global Health Care](http://www.exed.hbs.edu/programs/big/Pages/default.aspx)** is a leadership
development program that compares and contrasts innovative global health care
ventures as they consider various expansion decisions.  This program is
scheduled for June 24-27, 2015.  

To hear the story of how Boston Children's Hospital is creating their future one leader at a time, 
[click here](http://www.exed.hbs.edu/assets/testimonials/Pages/pld-mandell-fenwick.aspx).

## Seeking Alumni Applicants: Blavatnik Fellowship
<?php beginTable();
left("[![blavatnik logo][]](http://www.biomedicalaccelerator.org/fellowship/)","171px", "normal", "middle");
right(
"The Blavatnik Fellowship in Life Science Entrepreneurship is seeking entrepreneurial HBS alumni who have graduated
within the last seven years to foster the commercialization of biomedical
innovation from laboratories throughout Harvard University.", "465px", "normal");
endTable(); ?>
Fellows work for one year in the vibrant
Harvard Innovation Lab on the HBS campus and receive a $95K stipend for 12
months with extra resources for due diligence, market research, and licensing
options.  To [apply to the program](http://otd.harvard.edu/accelerators/blavatnik-biomedical-accelerator/blavatnik-fellowship/), submit a completed
application to [blavatnikfellowship@hbs.edu](mailto:blavatnikfellowship@hbs.edu) by March 5, 2015.

Read exciting updates from the inaugural cohort of Blavatnik Fellows below:

### Ross Leimberg, MBA '12
<?php 
beginTable();
left('![Ross Leimberg][]', "75px");
right(
"**Ross Leimberg**, MBA '12, a Senior
Manager at AbbVie Ventures and Early Stage Collaborations, said 'The Fellowship
provided invaluable first-hand experience of the agony and ecstasy in
entrepreneurship, which has been the best training I could ask for in
preparation for my current role as an investor in the startup life science
community.'");
endTable();
?>

### Daniel Oliver, MBA '13
<?php 
beginTable();
left('![Daniel Oliver][]', "75px");
right(
"**Daniel Oliver**, MBA '13, who has
launched Voxel8 with Dr. Jennifer Lewis, says 'without the Blavatnik
Fellowship there is no chance I would have met Prof. Lewis and become involved
with Voxel8.  It was truly
a transformational year.  I am
so thankful for the opportunity to commercialize such ground breaking
technology with the support of the amazing advisers and Fellows the Fellowship
introduced me to.'");
endTable();
?>

### Steve Porter, MBA '11
<?php 
beginTable();
left('![Steve Porter][]', "75px");
right(
"**Steve Porter**, MD/MBA '11, says 'As
a physician, I encounter clinical challenges every day that demand more robust
technological solutions.  The Fellowship has equipped me with the skills
and network to commercialize diagnostics, therapeutics, and medical devices to
better support reproductive and women's health, positioning me for an exciting
career as a clinician-entrepreneur.'");
endTable();
?>

### John Strenkowski, MBA '09
<?php 
beginTable();
left('![John Strenkowski][]', "75px");
right(
"**John Strenkowski**,
MBA '09, who is preparing to launch a company with Dr. Kit Parker, says
'the Blavatnik Fellowship not only helped me to develop relationships
within Harvard that were necessary to form our startup, it also provided me
with a built-in network of advisors and Fellows who have provided incredibly
valuable advice and support.'");
endTable();
?>

### Ridhi Tariyal, MBA '09
<?php 
beginTable();
left('![Ridhi Tariyal][]', "75px");
right(
"**Ridhi Tariyal**, MBA '09, co-founder
of [NextGen Jane](http://nextgenjane.com/) says 'the Blavatnik Fellowship allowed me to take an idea, test
it on the bench, build intellectual property around it, and then shop it around
to a network of smart, subject matter experts to determine whether it would
fail or fly.'
");
endTable();
?>

<?php section("Health Care Events"); ?>

Below is a listing of upcoming events including HBS happenings, alumni events, upcoming opportunity deadlines and conferences at which the Health Care Initiative is considering hosting a reception.  

<?php 
beginTable();

left("Tue Dec 16   
 <span class='time'>12:00 pm -- 1:00 pm</span>   
 <span class='place'>Virtual</span>");
right("[Hacking Health Care, Luc Sirois]  
Hacking Health is an initiative that brings together health professionals and technologists to work together to prototype and problem-solve new ways to deliver health care. Luc Sirois, MBA 1997, will discuss the genesis of Hacking Healthcare's bottom-up approach to inspiring innovation by creating spaces for collaborative, interdisciplinary experimentation. He will provide insights on this business model and why it works. Advance registration required. Hosted by the Health Care Alumni Association. *Open to members and non-members.*");

left("Fri Jan 09   
 <span class='place'>Virtual</span>");
right("[InciteHealth Fellowship Application Deadline]  
This is a one-year fellowship for professionals from a wide variety of disciplines to converge their expertise and develop new solutions for pressing issues in national health care. The fellowship is part-time, enabling professionals to maintain their careers simultaneously. Sponsored by  the Harvard Medical School Center for Primary Care. *Open to the public.*");

left("Sun Jan 11   
 <span class='time'>7:00 pm -- 9:30 pm</span>   
 <span class='place'>Bluestem Brasserie, San Francisco, CA</span>");
right("[Reception at JP Morgan 33rd Annual Conference]  
Join fellow HBS health care Alumni for a cocktail reception to kick-off the 33rd Annual JP Morgan Health Care Conference. 
Enjoy conversation and networking with friends old and new the evening prior to the event. 
Hosted by the Health Care Alumni Association. *Open to HBSHAA members.*");

left("Tue Jan 20   
 <span class='time'>12:00 pm -- 1:00 pm</span>   
 <span class='place'>Virtual</span>");
right("[Is That a Doctor in Your Pocket? (Roy Schoenberg)]  
Simply put - Telehealth is the use of technology to deliver care. Much more than a video chat, it promises to change the way health care is acquired and delivered, its availability in our homes and workplaces and even how we care for our elderly and chronic patients. In the last year alone, Telehealth morphed from an anecdote to a must in any healthcare CEO To-do plan. From an alternative to traditional care, to a key part of it. From a pilot study to a foundational technology embraced by insurers, providers, delivery systems, regulators, employers and consumers. Find out why and how quickly this technology is changing the business of health care delivery. Registration required Hosted by the Health Care Alumni Association. *Open to HBSHAA members and non-members.*");

left("Tue Feb 10   
 <span class='time'>12:00 pm -- 1:00 pm</span>   
 <span class='place'>Virtual</span>");
right("[Ebola: What Can Be Done? (Marcus Lovell Smith)]  
Ebola has killed over 5,000 people and induced panic all over the world. Controlling its spread will require appropriate diagnostic tests. Diagnostics For All's mission is to save lives and improve health in the developing world through pioneering technological innovation. What combination of strategies will bring a halt to this deadly outbreak? Advance registration required. Marcus Lovell Smith is the CEO of Diagnostics for All. Free for members; $25 non-members. Hosted by  the Health Care Alumni Association. *Open to members and non-members.*");

left("Thu Mar 05   
 <span class='place'>Virtual</span>");
right("[Seeking Alumni Applicants: Blavatnik Fellowship]  
The Blavatnik Fellowship in Life Science Entrepreneurship is seeking entrepreneurial HBS alumni who have graduated within the last seven years to foster the commercialization of biomedical innovation from laboratories throughout Harvard University. Fellows work for one year in the vibrant Harvard Innovation Lab on the HBS campus and receive a $95K stipend for 12 months with extra resources for due diligence, market research, and licensing options. To apply to the program, submit a completed application to [blavatnikfellowship@hbs.edu](mailto:blavatnikfellowship@hbs.edu) by March 5, 2015. Sponsored by  the Blavatnik Fellowship in Life Science Entrepreneurship. *Open to HBS alumni that graduated between 2008 and 2014.*");

left("Sun Apr 12   
 <span class='place'>Chicago, IL</span>");
right("*Save the date:* [Reception at HIMSS 2015]  
The HBS Health Care Initiative and the HBS Health Care Alumni Association (HAA) invite you to a reception for HBS alumni at the 2015 Health Care Information and Management Systems Society (HIMSS) conference. Check the HAA website for more information about this event as the date grows nearer. Hosted by  the Health Care Alumni Association and the HBS Health Care Initiative. *Open to members and non-members.*");

left("Mon Jun 15   
 <span class='place'>Philadelphia, PA</span>");
right("*Save the date:* [Reception at BIO International Convention]  
The HBS Health Care Initiative and the HBS Health Care Alumni Association (HAA) invite you to a reception for HBS alumni at the 2015 Biotechnology Industry Organization (BIO) conference. Check the HAA website for more information about this event as the date grows nearer. Hosted by  the Health Care Alumni Association and the HBS Health Care Initiative. *Open to members and non-members.*");




endTable();
?>

## HBR--NEJM Insight Center --- A Great Resource
The [*Harvard Business Review* -- *New England Journal of Medicine* Insight Center](https://hbr.org/insight-center/innovating-for-value-in-health-care) has posts by health care thought leaders about innovating for value in health care. Perhaps the holiday season will present you with an opportunity to peruse this great resource and contemplate what's ahead for health care in the New Year.
 
<?php 
beginTable();

left("[![medical case image](http://www.hbs.edu/healthcare/images/photos/medical_case.png)](https://hbr.org/insight-center/innovating-for-value-in-health-care)");

right("### Bridging Health Care's Innovation-Education Gap
Be sure to check out the article \"[Bridging Health Care's Innovation-Education Gap](https://hbr.org/2014/11/bridging-health-cares-innovation-education-gap)\" by HBS Professor Regina Herzlinger along with Vasant Kumar Ramaswamy and Kevin A. Schulman. Also, see the [accompanying videos here](https://www.youtube.com/channel/UCEHiAeuRb5J2sCIfunvUdLQ/videos). 

### Health Care Needs Less Innovation and More Imitation
Don't miss \"[Health Care Needs Less Innovation and More Imitation](https://hbr.org/2014/11/health-care-needs-less-innovation-and-more-imitation)\" by Anna M. Roth and Thomas H. Lee.");
endTable();
?>

<?php section("Recent Faculty Publications"); ?>


<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48227' target='_blank'>How Not to Cut Health Care Costs</a>
<br><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6487'>Robert Kaplan</a>, Derek A Haas
<br>Harvard Business Review 92, no. 11 (November 2014): 116-122<br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=43717' target='_blank'>Reframing Hierarchical Interactions as Negotiations to Promote Change in Health Care Systems</a>
<br>Patricia Satterstrom, <a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6627'>Jeffrey Polzer</a>, Robert Wei
<br>Chap. 18 in Handbook of Conflict Management Research, 291-307. Edward Elgar Publishing, 2014<br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=47997' target='_blank'>FormPrint Ortho500</a>
<br><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=126057'>Frank Cespedes</a>, Alisa Zalosh
<br>Harvard Business School Brief Case 915-535, September 2014<br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=47831' target='_blank'>Fresno's Social Impact Bond for Asthma</a>
<br><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6684'>John A Quelch</a>, Margaret L. Rodriguez
<br>Harvard Business School Case 515-028, September 2014<br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=47807' target='_blank'>La Ribera Health Department</a>
<br><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6476'>Regina Herzlinger</a>
<br>Harvard Business School Case 315-006, September 2014<br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=47946' target='_blank'>Mayo Clinic: The 2020 Initiative</a>
<br><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6476'>Regina Herzlinger</a>, <a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=10653'>Robert Huckman</a>
<br>Harvard Business School Case 615-027, September 2014<br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=47910' target='_blank'>Pfizer's Centers for Therapeutic Innovation (CTI)</a>
<br><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6530'>Gary Pisano</a>, James Weber, Kaitlyn Szydlowski
<br>Harvard Business School Case 615-024, September 2014<br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=47802' target='_blank'>Pfizer and AstraZeneca: Marketing an Acquisition (A)</a>
<br><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6684'>John A Quelch</a>, James Weber
<br>Harvard Business School Case 515-007, September 2014<br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=47956' target='_blank'>Hospital for Special Surgery (A)</a>
<br><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6476'>Regina Herzlinger</a>, Stacy Schwartz
<br>Harvard Business School Case 315-012, August 2014.<br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=47835' target='_blank'>Don't Take 'No' for an Answer: An Experiment with Actual Organ Donor Registrations</a>
<br>Judd B Kessler, <a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6594'>Alvin Roth</a>
<br>NBER Working Paper Series, No. 20378, August 2014<br><br>




<?php section("HBS Health Care In the News"); ?>

"[Boston Medical Center, Tufts in Merger Talks][]"  
Re: Robert Huckman  
*Boston Globe*  
Dec 11, 2014

"[In the Market for Blood? Here's How to Keep From Getting Squeezed][]"  
Re: Health Acceleration Challenge  
*Dallas Business Journal*  
Nov 21, 2014

"[Finalists Announced in Harvard Business School/Harvard Medical School Health Acceleration Challenge][]"  
Re: Richard Hamermesh, Robert Huckman  
*Harvard Business School*  
Nov 20, 2014

"[Boston Health Tech Groups Are Finalists in Harvard's Health Acceleration Challenge][]"  
Re: Health Acceleration Challenge  
*BetaBoston*  
Nov 20, 2014

"[Medalogix Chosen as Finalist in Harvard Challenge, Wins $37.5K][]"  
Re: Health Acceleration Challenge  
*Nashville Post*  
Nov 20, 2014

"[Bridging Health Care's Innovation-Education Gap][]"  
By: Regina Herzlinger  
*HBR Blogs*  
Nov 12, 2014

"[Health Acceleration Challenge Contestants Aim to Improve U.S. Health Care][]"  
Re: Health Acceleration Challenge  
*Harvard Business School*  
Nov 12, 2014

"[A Recap of the U.S. News Hospital of Tomorrow Forum 2014][]"  
Re: Michael Porter  
*US News & World Report*  
Nov 3, 2014

"[The Businessman Disrupting Organ Transplantation][]"  
Re: Regina Herzlinger  
*Atlantic*  
Oct 29, 2014

"[Ebola as a Wake-Up Call][]"  
By: Gautam Mukunda  
*Harvard Business School*  
Oct 24, 2014

"[Harvard Challenge Testing Healthcare Innovation Ideas That Are Ready to Scale][]"  
Re: Health Acceleration Challenge
*MedCity News*  
Oct 16, 2014

"[Economic Costs of Ebola Rising as People Shun Human Contact][]"  
Re: Eric Werker  
*Economy Watch*  
Oct 15, 2014

"[The Unanticipated Risks of Maximizing Shareholder Value][]"  
Re: Rosabeth Moss Kanter, Michael Porter & Jan Rivkin  
*Forbes*  
Oct 14, 2014

"[Science Innovation Contest to Begin Mid-Oct.][]"  
Re: Nitin Nohria  
*Harvard Crimson*  
Oct 7, 2014

"[Michael Porter: Disrupt Health Care to Save it][]"  
Re: Michael Porter  
*US News & World Report*  
Oct 7, 2014

"[The Rise of the M.D./M.B.a. Degree][]"  
Re: David Hy Gellis (MBA 2010) & Vinod Easwaran Nambudiri (MBA 2010)  
*Atlantic*  
Sep 30, 2014

"[WATCH: Live From the 2014 Hospital of Tomorrow Conference][]"  
Re: Michael Porter  
*US News and World Report*  
Sep 30, 2014

"[The Shortcomings of the Corporate Wellness Program][]"  
Re: Nancy Koehn  
*American Public Media: Marketplace*  
Sep 18, 2014

"[Nanobiosym Global Summit Convenes World Experts in Cambridge to Democratize Health Care][]"  
Re: Tarun Khanna  
*MarketWired*  
Sep 17, 2014

"[If Your Kids Get Free Health Care, You'Re More Likely to Start a Company][]"  
Re: Gareth Olds  
*HBR Blogs*  
Sep 9, 2014







*[FAS]: Harvard Faculty of Arts and Sciences
*[HLS]: Harvard Law School
*[HKS]: Harvard Kennedy School
*[HBR]: Harvard Business Review
*[NEJM]: New England Journal of Medicine
*[BAHM]: Business School Alliance for Health Management 
*[HBSHAA]: Harvard Business School Healthcare Alumni Association
*[FHI]: Forum on Healthcare Innovation
*[HMS]: Harvard Medical School

<?php // reusable images ?>
[speech bubbles]: http://www.hbs.edu/healthcare/images/photos/speechbubbles.png
[pill image]: http://www.hbs.edu/healthcare/images/photos/pill_100.png
[steth image]: http://www.hbs.edu/healthcare/images/photos/steth_100.png
[heart image]: http://www.hbs.edu/healthcare/images/photos/heart_100.png
[up_arrow image]: http://www.hbs.edu/healthcare/images/photos/up_arrow_100.png
[dna image]: http://www.hbs.edu/healthcare/images/photos/dna_100.png
[clip image]: http://www.hbs.edu/healthcare/images/photos/clip_100.png

<?php // logos ?>
[blavatnik logo]: http://www.hbs.edu/healthcare/images/photos/blav-in-life-science-entrepreneurship.png
[exec ed]: http://www.hbshealthalumni.org/images/article_images/436.jpg

<?php // one-time images ?>
[Health Acceleration Challenge Poster]: http://www.hbs.edu/healthcare/images/photos/hac-img-290x278.png
[Ross Leimberg]: http://www.hbs.edu/news/PublishingImages/photos/leimberg.jpg
[Daniel Oliver]: http://www.hbs.edu/news/PublishingImages/photos/oliver.jpg
[Steve Porter]: http://www.hbs.edu/news/PublishingImages/photos/porter.jpg
[John Strenkowski]: http://www.hbs.edu/news/PublishingImages/photos/strenkowski.jpg
[Ridhi Tariyal]: http://www.hbs.edu/news/PublishingImages/photos/tariyal.jpg
[Bluestem Brasserie Image]: http://www.hbs.edu/healthcare/images/photos/bluestem_282.png

<?php // one-time event links ?>
[Hacking Healthcare, Luc Sirois]: http://www.hbshealthalumni.org/article.html?aid=907
[Networking Event: HBS Healthcare Alumni]: http://www.hbshealthalumni.org/article.html?aid=921
[InciteHealth Fellowship Application Deadline]: http://www.hbshealthalumni.org/article.html?aid=909
[Reception at JP Morgan 33rd Annual Conference]: http://www.hbshealthalumni.org/article.html?aid=913
[J.P. Morgana 33rd Annual Healthcare Conference]: http://sternir.com/ai1ec_event/j-p-morgan-33rd-annual-healthcare-conference/
[Is That a Doctor in Your Pocket? (Roy Schoenberg)]: http://www.hbshealthalumni.org/article.html?aid=916
[Drug Development Dynamics: Discovery, Delivery & Demand]: http://www.hbshealthalumni.org/article.html?aid=912
[Ebola: What Can Be Done? (Marcus Lovell Smith)]: http://www.hbshealthalumni.org/article.html?aid=922
[Seeking Alumni Applicants: Blavatnik Fellowship]: http://otd.harvard.edu/accelerators/blavatnik-biomedical-accelerator/blavatnik-fellowship/
[Reception at HIMSS 2015]: http://www.hbshealthalumni.org/events.html
[Reception at BIO International Convention]: http://www.hbshealthalumni.org/events.html

<?php // one-time links for in the news ?>
[Boston Medical Center, Tufts in Merger Talks]: https://www.bostonglobe.com/business/2014/12/11/tufts/yaRVe0uBCrfE4FZlFqPFOJ/story.html
[In the Market for Blood? Here's How to Keep From Getting Squeezed]: http://www.bizjournals.com/dallas/blog/2014/11/in-the-market-for-blood-here-s-how-to-keep-from.html
[Finalists Announced in Harvard Business School/Harvard Medical School Health Acceleration Challenge]: http://www.hbs.edu/news/releases/Pages/hac-finalists.aspx
[Boston Health Tech Groups Are Finalists in Harvard's Health Acceleration Challenge]: http://betaboston.com/news/2014/11/20/boston-health-tech-groups-are-finalists-in-harvards-health-acceleration-challenge/
[Medalogix Chosen as Finalist in Harvard Challenge, Wins $37.5K]: https://www.nashvillepost.com/blogs/postbusiness/2014/11/20/medalogix_chosen_as_finalist_in_harvard_challenge_wins_375k
[Bridging Health Care's Innovation-Education Gap]: https://hbr.org/2014/11/bridging-health-cares-innovation-education-gap
[Health Acceleration Challenge Contestants Aim to Improve U.S. Health Care]: http://www.hbs.edu/news/releases/Pages/hac-applicants-eager.aspx
[A Recap of the U.S. News Hospital of Tomorrow Forum 2014]: http://health.usnews.com/health-news/hospital-of-tomorrow/articles/2014/11/03/a-recap-of-the-us-news-hospital-of-tomorrow-forum-2014
[The Businessman Disrupting Organ Transplantation]: http://www.theatlantic.com/health/archive/2014/10/the-businessman-disrupting-organ-transplantation/381843/?single_page=true&print=
[Ebola as a Wake-Up Call]: http://www.hbs.edu/news/articles/Pages/ebola-as-a-wake-up-call.aspx
[Harvard Challenge Testing Healthcare Innovation Ideas That Are Ready to Scale]: http://medcitynews.com/2014/10/can-harvard-help-make-primary-care-better-patients/
[Economic Costs of Ebola Rising as People Shun Human Contact]: http://www.economywatch.com/features/Economic-costs-of-Ebola-rising-as-people-shun-human-contact.10-15-14.html
[The Unanticipated Risks of Maximizing Shareholder Value]: http://www.forbes.com/sites/stevedenning/2014/10/14/the-unanticipated-risks-of-maximizing-shareholder-value/
[Science Innovation Contest to Begin Mid-Oct.]: http://www.thecrimson.com/article/2014/10/7/ilab-challenge-health-sciences/
[Michael Porter: Disrupt Health Care to Save it]: http://health.usnews.com/health-news/hospital-of-tomorrow/articles/2014/10/07/michael-porter-disrupt-health-care-to-save-it
[The Rise of the M.D./M.B.a. Degree]: http://www.theatlantic.com/education/archive/2014/09/the-rise-of-the-mdmba-degree/380683/
[WATCH: Live From the 2014 Hospital of Tomorrow Conference]: http://health.usnews.com/health-news/hospital-of-tomorrow/articles/2014/09/30/watch-live-from-the-2014-hospital-of-tomorrow-conference
[The Shortcomings of the Corporate Wellness Program]: http://www.marketplace.org/topics/health-care/shortcomings-corporate-wellness-program
[Nanobiosym Global Summit Convenes World Experts in Cambridge to Democratize Health Care]: http://www.marketwired.com/press-release/nanobiosym-global-summit-convenes-world-experts-cambridge-democratize-healthcare-1948278.htm
[If Your Kids Get Free Health Care, You'Re More Likely to Start a Company]: http://blogs.hbr.org/2014/09/if-your-kids-get-free-health-care-youre-more-likely-to-start-a-company/

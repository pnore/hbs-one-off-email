<?php section("Health Care Happenings at HBS"); ?>

## WHAT IS YOUR HEALTH CARE INNOVATION? 
<?php 
beginTable();
left("
HBS and HMS have launched the Health Acceleration Challenge seeking proven innovations for US health care delivery. 
This \"scale-up\" competition seeks to shorten the time frame for innovation dissemination. 
Challenge finalists share $150,000, meet with senior health care leaders at the invite-only conference in April 2015 
and have HBS case studies written about them. All applicants will have the benefit of critical community feedback. 
Visit [HealthAccelerationChallenge.com](http://healthaccelerationchallenge.com/?utm_campaign=hac_launch&utm_source=healthcareinitiativelist&utm_medium=email) to find out more and apply by September 29."  
. button('Apply Now >>', 'http://healthaccelerationchallenge.com/?utm_campaign=hac_launch&utm_source=healthcareinitiativelist&utm_medium=email'), "280px", "normal");
right("[![Health Acceleration Challenge Poster][]](http://healthaccelerationchallenge.com/?utm_campaign=hac_launch&utm_source=healthcareinitiativelist&utm_medium=email)
	", "292px", "normal", "middle");
endTable();
?>

## ALUMNI MENTORS NEEDED: Please share your health care expertise
<?php 
beginTable();

left('![speech bubbles][]');
right("HBS is seeking alumni working in all sectors of the health care
industry to mentor current MBA students. The mentor program requires a minimal
time commitment on your part --- just a few emails or phone conversations over
the course of the year. Please <a href='https://getfeedback.com/r/DwRayaYq?ContactID=%%SFDC Id18%%'>click here</a> to volunteer.");

endTable();
?>

## FALL HEALTH CARE CONFERENCE SURVEY 
<?php beginTable();
left("The Health Care Initiative would like to know which health-related conferences and events you are attending this fall. We plan to host networking receptions for the HBS alumni during selected events. [Click here for a brief survey to let us know where you're going](https://getfeedback.com/r/G6ikYRAp?ContactID=%25%25SFDC%20Id18%25%25).", "470px", "normal"
	);
right(button("Survey >>", "https://getfeedback.com/r/G6ikYRAp?ContactID=%25%25SFDC%20Id18%25%25"), "166px", "normal", "middle");
endTable(); ?>

### Scenes from the Chicago ASCO Alumni Reception -- May, 2014  
The Health Care Initiative looks forward to hosting more of these events for our alumni!
<?php 
beginTable();
left("![ASCO photo of Heather McCullough Ptty Chiang David Darst](http://www.hbs.edu/healthcare/images/photos/mccullough_chiang_darst.png)", "50%", "normal");
right("
**Heather McCullough**, MBA 2011   
**Patty Chiang**,       MBA 2010   
**David Darst**,        MBA 2007", "50%", "normal");

left("![ASCO photo of Dallin Anderson and Michael Smith](http://www.hbs.edu/healthcare/images/photos/anderson_smith.png)", "50%", "normal");
right("
**Dallin Anderson**,    MBA 2002   
**Michael Smith**,      MBA 2010   
", "50%", "normal");

left("![ASCO photo of Kareem Reda Kouki Harasaki Will Aiken](http://www.hbs.edu/healthcare/images/photos/reda_harasaki_aiken.png)", "50%", "normal");
right("
**Kareem Reda**,        MBA 2011  
**Kouki Harasaki**,     MBA 2011  
**Will Aiken**,         MBA 2010   
", "50%", "normal");
endTable();
?>

## New Health Care MBA Elective
<?php 
beginTable();

left("![John Quelch][]  
John A. Quelch  
<span class='place'>Charles Edward Wilson Professor of Business Administration</span>
");
right(" 
In the 2014-15 academic year there will be a record number of 9 health care courses offered to EC students.  Many of the courses, like Regina Herzlinger's *Innovating in Health Care* or Vicki Sato's *Commercializing Science* are  popular and well known to alums. This year a new course will be offered by John Quelch, the first HBS professor to hold a joint appointment at HSPH, titled *Consumers, Corporations and Public Health*. The course includes many new cases on e-cigarettes, 23andMe, Vaxess, Takeda, and Royal Caribbean and is organized into six modules: Corporate Strategy and Public Health; Employee Safety, Wellness and Productivity; Prevention and Adherence; Access and Affordability; Consumerism and Paternalism; and Innovation and Consumer Behavior. [Click here to read more about the course](http://www.hbs.edu/faculty/Profile%20Files/JAQuelch_CCPH_Syllabus__07.08.14_d4d99b65-4784-4d4d-bb78-a9a93b46d8f1.pdf). 
    ");
    
endTable();
?>

## HBS NAMES BLAVATNIK FELLOWS IN LIFE SCIENCE ENTREPRENEURSHIP
<?php beginTable();
left(
"HBS has named its 2014-2015 Blavatnik Fellows in Life Science Entrepreneurship. These five outstanding HBS alumni graduated within the past seven years and will work with inventors from Harvard University's research laboratories to promote the commercialization of innovative life science-oriented technologies. [Click here for more information](http://www.hbs.edu/news/releases/Pages/blavatnik-fellows-2014.aspx).", "465px", "normal"
	);
right("[![blavatnik logo][]](http://www.biomedicalaccelerator.org/fellowship/)","171px", "normal", "middle");
endTable(); ?>  

### Christoph Jaeker, MBA 2011
<?php 
beginTable();
left('![Christoph Jaeker][]', "75px");
right(
    "A biochemist with extensive experience in life sciences management, Christoph most recently worked in business development for the biopharmaceutical company, Nimbus Discovery, which uses computational chemistry to develop therapies against difficult-to-drug targets. [Read More](http://www.hbs.edu/news/releases/Pages/blavatnik-fellows-2014.aspx).");
endTable();
?>

### Alexandra Dickson, MBA 2013
<?php 
beginTable();
left('![Alexandra Dickson][]', "75px");
right(
    "Alexandra worked in Biogen Idec's business insights and analytics group to build the analytic infrastructure to support and monitor the launch of the company's hemophilia franchise. Dickson holds a bachelor's degree in human evolutionary biology from Harvard College. [Read More](http://www.hbs.edu/news/releases/Pages/blavatnik-fellows-2014.aspx).");
endTable();
?>

### Meridith Unger, MBA 2013
<?php 
beginTable();
left('![Meridith Unger][]', "75px");
right(
    "Meridith previously worked with healthcare and technology startups, crafting and executing strategic plans to carry high-potential technologies from pre-inception through a venture's first round of funding. She has worked closely with and for several venture capital firms and the Technology Development Fund, an academic venture fund at Boston Children's Hospital. [Read More](http://www.hbs.edu/news/releases/Pages/blavatnik-fellows-2014.aspx).");
endTable();
?>

### Sid Misra, MBA 2013
<?php 
beginTable();
left('![Sid Misra][]', "75px");
right(
    "Sid cofounded two companies in the telecommunications and internet sectors where he led technology development, marketing, and strategy efforts. He also worked as a venture investor at Khosla Ventures and at Applied Ventures, the corporate venture capital arm of Applied Materials. [Read More](http://www.hbs.edu/news/releases/Pages/blavatnik-fellows-2014.aspx).");
endTable();
?>

### Louis Levy, MBA 2014
<?php 
beginTable();
left('![Louis Levy][]', "75px");
right(
    "Louis began his career with Cepton, a strategy consulting boutique for French mid-size biotechs, where he worked on pharmaceutical development, European commercial launches, and Research and Development organizations. He was involved in operationalizing notable licensing transactions in allergy immunotherapy and hemophilia. [Read More](http://www.hbs.edu/news/releases/Pages/blavatnik-fellows-2014.aspx).");
endTable();
?>

<?php section("Health Care Events"); ?>

Below is a listing of upcoming fall events including HBS happenings, alumni events and conferences at which the Health Care Initiative is considering hosting a reception.  

<?php 
beginTable();

// removed in revised edition from SP
// left("Thu Sep 04   
//  <span class='time'>8:30 am -- 5:15 pm</span>   
//  <span class='place'>HMS, Joseph B. Martin Conference Center</span>");
// right("[9th Teikyo-HSPS Symposium: Training the Next Generation of Public Health Leaders]  
// Speakers include: Eric Mazur, Harvard School of Engineering and Applied Sciences; Julio Frenk, Dean, HSPH; and Sanjay E. Sarma, MIT. Hosted by  HSPH. *Open to the public.*");

left("Wed Sep 10      
 <span class='place'>Washington, DC and San Francisco, CA</span>");
right("[TEDMED2014: Unlocking Imagination]  
TEDMED is a global community of leading doers and thinkers from every walk of life. Our goal is to seed the innovations in health and medicine of today, making the breakthroughs of tomorrow possible. The conference will bring together Delegates in two host cities from across the globe for this year's TEDMED gathering. These Delegates will simultaneously participate in a unified, digitally-linked program with equal numbers of live Speakers, Delegates, and innovative Hive startups. Hosted by  TEDMED. *Open to the public.*");

left("Tue Sep 16   
 <span class='time'>12:00 pm -- 1:00 pm</span>   
 <span class='place'>Virtual</span>");
right("[Roundtable: How is Digital Health Changing the Industry?]  
While healthcare represents a considerable portion of the US economy, it lags behind most industries in terms of its technological savvy. However, venture capital investments in digital health far outpace funding in other healthcare sectors. Where is this market headed? Has it reached the height of its growth? Halle Tecco, MBA 2011, Founder & Managing Director (NYC), and Malay Gandhi, Managing Director (SF), of Rock Health, will discuss the recent movements in this arena and provide insights on funding trends. Hosted by the HBS Healthcare Alumni Association. *Open to the HBS community.*");

left("Wed Sep 17      
 <span class='place'>Seaport Boston Hotel</span>");
right("[*The Economist*'s Health Care Forum]  
Join the HBS community at *The Economist*'s Health Care Forum with our special **$400 discount: EMPMPHAR** to gain new insights into the disruptive forces affecting the health-care sector. Costs are rising, governments are legislating new models and non-traditional health-care companies are entering the global market. Discover how your company can prepare for and capitalize on the potential business opportunities associated with this new era. Speakers include leading health-care experts from GE Healthcare, The Leapfrog Group, athenahealth, Bain Capital and more. Students can receive a 50% discount by sending an email to the following address: [event-tickets@economist.com](mailto:event-tickets@economist.com) and mentioning the Health Care Initiative. [Register here](https://www.eventbrite.com/e/the-economists-health-care-forum-tickets-10819993893?discount=EMPMPHAR). *Open to the public.*");

left("Thu Sep 18      
 <span class='place'>HBS Campus</span>");
right("[HBS Reunion for 1969, 1979, 1984, 1974, 1989, and AMP & ISMP]  
Come reconnect with friends, faculty, and the campus! Spouses, partners, and guests are welcome to attend reunion weekend. Reunion fees are per person, and cover the Thursday Reception, Friday Section Parties, Saturday Gala, and the Sunday Breakfast. All on campus activities are paid for by HBS. Children of all ages are welcome to attend reunion weekend. Childcare options and a range of activities are being planned. Hosted by HBS. *Open to HBS alumni.*");

// removed in revised edition from SP
// left("Thu Sep 18   
//  <span class='time'>7:30 am -- 5:30 pm</span>   
//  <span class='place'>HLS, Wasserstein Hall</span>");
// right("[Post-Trial Responsibilities: Ethics and Implementation]  
// This conference will focus on (1) Continued access to study intervention(s) and/or other care for people who were enrolled in the clinical trial and were benefitting (whether between the end of the trial and product approval or indefinitely), (2) Provision of the study intervention(s) and/or other care to people who were enrolled in the clinical trial but did not get the intervention and would like to try it (whether between the end of the trial and product approval or indefinitely), and (3) Provision of the study intervention, other care, or other resources to the community in which the trial was conducted. Hosted by  The Petrie-Flom Center for Health Law Policy, Biotechnology, and Bioethics at HLS. *Open to the public.*");

// removed in revision from SP
// left("Thu Sep 18   
//  <span class='time'>4:00 pm -- 5:00 pm</span>   
//  <span class='place'>HSPH, Longwood Campus</span>");
// right("[Myrto Lefkopoulou Distinguished Lecture]  
// Discovery Research with Electric Medical Records. Speaker: Tianxi Cai, ScD, Professor of Biostatistics, HSPH. Hosted by  HSPH. *Open to the public.*");

// added from revision from SP
left("Sun Sep 21      
 <span class='place'>Santa Clara, CA</span>");
right("[Health 2.0]  
Health 2.0 promotes, showcases and catalyzes new technologies in health care. They do this through a worldwide series of conferences, code-a-thons, prize challenges, and more. They also have the leading market intelligence on new health technology companies. *Open to the public.*");

left("Mon Oct 06      
 <span class='place'>Chicago, IL</span>");
// updated shorter description from SP
right("[AdvaMed 2014 MedTech Conference]  
AdvaMed 2014 is the leading MedTech Conference in North America, bringing more than 1,000 companies together in a uniquely multifaceted environment for business development, capital formation, innovative technology showcasing, world-class educational opportunities and networking. Hosted by AdvaMed. *Open to the public.*");

left("Mon Oct 06      
 <span class='place'>Suffolk University Law School, Boston, MA</span>");
right("[2014 Health Care Cost Trends Hearing]  
The annual health care cost trends hearing is a public examination into the drivers of health care costs as well as the engagement of experts and witnesses to identify particular challenges and opportunities within the Commonwealth's health care system. Chapter 224 transferred the responsibility to conduct the hearing to the Commission, in coordination with the Office of the Attorney General (AGO) and Center for Health Information and Analysis (CHIA). The will occur on October 6 and 7, 2014 at the Suffolk University Law School. Hosted by Commonwealth of Massachusetts. *Open to the public.*");

left("Tue Oct 14      
 <span class='place'>San Mateo, CA</span>");
right("[HealthTech Conference]  
Growing Your Business in the Changing Health Care Ecosystem. The HealthTech conference has emerged, after the last two years, as the leading conference on how to build successful HealthTech companies. We focus on practical and in depth discussions on how emerging and established companies can grow their innovative products that enable existing health care players to adapt to a fast changing health care delivery system. Hosted by Stanford Hospital and HealthTech Capital. *Open to the public.*");

// removed in revision from SP
// left("Mon Oct 20   
//  <span class='time'>8:00 am -- 5:00 pm</span>   
//  <span class='place'>Washington, DC</span>");
// right("[Emerging Issues and New Frontiers for FDA Regulation]  
// The Petrie-Flom Center for Health Law Policy, Biotechnology, and Bioethics at Harvard Law School and the Food and Drug Law Institute are pleased to announce this collaborative academic symposium. Hosted by  Petrie-Flom Center for Health Law Policy, Biotechnology, and Bioethics at Harvard Law School and the Food and Drug Law Institute. *Open to the public.*");

left("Tue Oct 21   
 <span class='time'>12:00 pm -- 1:00 pm</span>   
 <span class='place'>Virtual</span>");
right("[Roundtable: Where 2014 Is At & 2015 Is Headed]  
Has healthcare fallen victim to the adage \"the more things change, the more they stay the same?\" Barry Staube, Director at The Marwood Group and Former Chief Medical Officer of the Centers for Medicare and Medicaid Services (CMS), will provide his views on where things are at so far this year and where things are headed in the year to come. Hosted by  HBS Healthcare Alumni Assciation. *Open to the HBS community.*");

left("Wed Oct 22      
 <span class='place'>Boston</span>");
right("[Global Networking Night]  
On a single night, in locations around the world, HBS alumni will gather for an evening of networking and socializing. Welcome recent HBS graduates, meet other HBS alum in your area, make personal connections. More details to come! Hosted by  HBS Alumni. *Open to HBS alumni.*");

// removed in revision from SP
// left("Thu Oct 23      
//  <span class='place'>Boston, MA</span>");
// right("[Connected Health Symposium 2014]  
// Collaborating with industry visionaries, clinical experts, patient advocates and researchers around the world, Partners' Center for Connected Health is moving healthcare beyond the hospital and clinic into the day-to-day lives of patients. From mobile devices and social media to sensors and home robots, we are putting technology to work, establishing new ways to improve care. Network with 1,200 health technology leaders world-wide: hospital execs community-based MDs, health plan leaders, Fortune 500 employers, government policymakers, researchers and developers, leading investors, patients and advocates. Explore the intersection of health and technology with exceptional keynotes, debates, probing interviews, demos of new and game-changing technologies, exhibits and networking events. Symposium 2014 will showcase the latest global developments and innovations in healthcare and policy. Hosted by  Partners HealthCare. *Open to the public.*");

// removed in revision from SP
// left("Thu Oct 30      
//  <span class='place'>Seaport World Trade Center, Boston, MA</span>");
// right("[2014 Global Pediatric Innovation Summit + Awards]  
// Taking a worldwide perspective, the Boston Children's Hospital 2014 Global Pediatric Innovation Summit + Awards is designed to address unmet needs, solve problems and seize opportunities in pediatric health care. Join a vibrant mix of thought leaders including clinicians, health care leaders, venture capitalists, policy makers, payers and other stakeholders for a productive exchange about the most pressing pain points in pediatric medicine. Hosted by  Boston Children's Hospital. *Open to the public.*");

// removed in revision from SP
// left("Wed Nov 05   
//  <span class='time'>5:00 pm -- 7:00 pm</span>   
//  <span class='place'>HLS</span>");
// right("[Health, Law, and Human Rights in Surrogacy and Egg Donation]  
// More information coming soon. Hosted by  The Petrie-Flom Center for Health Law Policy, Biotechnology, and Bioethics at HLS. *Open to the Harvard community.*");

left("Thu Nov 06      
 <span class='place'>The Charles Hotel</span>");
right("[HBS Healthcare Alumni Annual Conference]  
Health care is a dynamic industry, where new technologies, providers, regulations, and trends continually join the marketplace. One thing remains constant - the need to be connected to people, information, organizations, and education. This year's conference will bring together individuals who are not only industry leaders but are changing the very nature of the business of healthcare. Hosted by HBS Health Care Alumni Annual Conference. *Open to the HBS community*.");

left("Mon Nov 10      
 <span class='place'>Genzyme Center, Cambridge</span>");
right("[Live & Breathe: Building a Patient-Centered Biotech]  
Join us for the 1st ever MassBio Patient Advocacy Summit. We'll bring industry leaders together with patient advocates and other stakeholders to examine ways in which life sciences companies can more fully incorporate the patient voice into the work they do - not just approaching regulatory applications or at commercialization, but throughout the drug development cycle. The day-long event will include four panel discussions, four case study presentations (spotlighting industry/patient partnerships), a keynote address, as well as a networking breakfast, lunch and cocktail reception. Hosted by MassBio. *Open to the public.*");

left("Wed Nov 12      
 <span class='place'>HMS, Joseph B. Martin Conference Center</span>");
right("[Personalized Medicine Conference]  
The Personalized Medicine Conference is an annual two-day event co-hosted and presented by Partners HealthCare Personalized Medicine, Harvard Business School, and Harvard Medical School in association with the American Association for Cancer Research and Personalized Medicine Coalition. Widely considered the most prestigious event in the field, this conference attracts hundreds of national and international thought leaders across multiple disciplines as speakers, panelists, and attendees. Hosted by Partners HealthCare Personalized Medicine, Harvard Business School, and Harvard Medical School. *Open to the public.*");

left("Wed Nov 19      
 <span class='place'>Harvard Club, Boston, MA</span>");
right("[Drug Development Boot Camp]  
Dr. Lorna Speid, founder, is a global regulatory affairs and drug development expert. After watching many of her client companies make expensive and avoidable mistakes, she decided to help as many as possible avoid making the same mistakes over and over again. This is a unique opportunity to be totally immersed in drug development for two days and to mix with a Faculty that has proven themselves by bringing drugs to market, or that has contributed to this success. Hosted by Harvard University, Office of Technology Development. *Open to the public.*");

left("Sat Dec 06      
 <span class='place'>San Francisco, CA</span>");
right("[56th American Society of Hematology Annual Meeting and Exposition]  
As the premier hematology event, this meeting will provide attendees with an invaluable educational experience and the opportunity to (1) review more than 3,000 scientific abstracts highlighting the updates in the hottest topics in hematology; (2) interact with the global community of more than 20,000 hematology professionals from every subspecialty; (3) attend the hallmark Education and Scientific Program sessions; and (4) network with top minds in the field. Hosted by American Society of Hematology. *Open to the public.*");

// removed in revision from SP
// left("Sun Dec 07      
//  <span class='place'>Orlando, FL</span>");
// right("[26th Annual National Forum on Quality Improvement in Health Care]  
// This conference is more than a chance to network with nearly 5,000 health care professionals and gain actionable ideas for your organization. It's also an opportunity to play a part in effecting real change in health care quality and safety. Hosted by  Institute for Healthcare Improvement. *Open to the public.*");

left("Thu Dec 11      
 <span class='place'>Washington, DC</span>");
right("[FDA/CMS Summit for Payers]  
The healthcare industry is in a state of flux, new disruptions have altered the landscape, changed the status quo and are providing fresh opportunities and challenges for all industry stakeholders to collaborate and work together like never before. Attend the FDA/CMS Summit for Payers to initiate the collaboration, with top government and key regulatory bodies working closely with healthcare leadership to join forces and build an open culture of harmonization to provide efficient and affordable healthcare to all patients. Hosted by  Institute for International Research. *Open to the public.*");

left("Thu Dec 11      
 <span class='place'>Washington, DC</span>");
right("[FDA/CMS Summit for Biopharma Executives]  
 Hosted by  Institute for International Research. *Open to the public.*");

endTable();
?>

<?php section("Recent Faculty Publications"); ?>

<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=46042' target='_blank'>Awards Unbundled: Evidence from a Natural Field Experiment</a><br><span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=337273'>Nava Ashraf</a>, Oriana Bandiera, Scott Lee</span><br><span class='pub cite'>Journal of Economic Behavior &amp; Organization (forthcoming)</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=46481' target='_blank'>Informal Peer Interaction and Practice Type as Predictors of Physician Performance on Maintenance of Certification Examinations</a><br><span class='pub author'>Melissa A Valentine, S Barsade, <a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6451'>Amy Edmondson</a>, A Gal, R Rhodes</span><br><span class='pub cite'>JAMA Surgery (forthcoming)</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=46190' target='_blank'>The Effectiveness of Management-By-Walking-Around: A Randomized Field Study</a><br><span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=10668'>Anita Tucker</a>, Sara J Singer</span><br><span class='pub cite'>Production and Operations Management (forthcoming)</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=47634' target='_blank'>Better Accounting Transforms Health Care Delivery</a><br><span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6487'>Robert Kaplan</a>, Mary L Witkowski</span><br><span class='pub cite'>Accounting Horizons 28, no. 2 (June 2014): 365--383</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=47638' target='_blank'>Improving Value with TDABC</a><br><span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6487'>Robert Kaplan</a></span><br><span class='pub cite'>hfm (Healthcare Financial Management) 68, no. 6 (June 2014): 76--83</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=47639' target='_blank'>Using TDABC to Deliver Better Patient Outcomes at Lower Cost</a><br><span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6487'>Robert Kaplan</a></span><br><span class='pub cite'>hfm (Healthcare Financial Management) 68, no.6 (June 2014). (Web Extra</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=47311' target='_blank'>Merrimack Pharmaceuticals, Inc. (A)</a><br><span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6451'>Amy Edmondson</a>, Bethany S Gerstein, Melissa A Valentine</span><br><span class='pub cite'>Harvard Business School Case 614--063, April 2014</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=47214' target='_blank'>The Organizational and Geographic Drivers of Absorptive Capacity: An Empirical Analysis of Pharmaceutical R&D Laboratories</a><br><span class='pub author'>Francesca Lazzeri, <a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6530'>Gary Pisano</a></span><br><span class='pub cite'>Harvard Business School Working Paper, No. 14--098, April 2014</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=47651' target='_blank'>Attracting Long-Term Investors Through Integrated Thinking and Reporting: A Clinical Study of a Biopharmaceutical Company</a><br><span class='pub author'>Andrew Knauer, George Serafeim</span><br><span class='pub cite'>Journal of Applied Corporate Finance 26, no. 2 (Spring 2014): 57--64</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=46412' target='_blank'>Who Donates Their Bodies to Science? The Combined Role of Gender and Migration Status Among California Whole-body Donors</a><br><span class='pub author'>Asad L Asad, <a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=340838'>Michel Anteby</a>, Filiz Garip</span><br><span class='pub cite'>Social Science &amp; Medicine 106 (April 2014): 53--58</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=46332' target='_blank'>Martini Klinik: Prostate Cancer Care</a><br><span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6532'>Michael Porter</a>, <a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=555155'>Jens Deerberg-Wittram</a>, Clifford Marks</span><br><span class='pub cite'>Harvard Business School Case 714--471, March 2014</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=47229' target='_blank'>The Novartis Malaria Initiative</a><br><span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=261321'>Michael Chu</a>, Vincent Dessain, Emilie Billaud</span><br><span class='pub cite'>Harvard Business School Case 314--103, March 2014</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=46721' target='_blank'>Bio-Piracy or Prospering Together? Fuzzy Set and Qualitative Analysis of Herbal Patenting by Firms</a><br><span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=327154'>Prithwiraj Choudhury</a>, <a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6491'>Tarun Khanna</a></span><br><span class='pub cite'>Harvard Business School Working Paper, No. 14--081, February 2014</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=46704' target='_blank'>Codifying Prior Art and Patenting: Natural Experiment of Herbal Patent Prior Art Adoption at the EPO and USPTO</a><br><span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=327154'>Prithwiraj Choudhury</a>, <a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6491'>Tarun Khanna</a></span><br><span class='pub cite'>Harvard Business School Working Paper, No. 14--079, February 2014</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=46697' target='_blank'>Do Leaders Matter? Natural Experiment and Quantitative Case Study of Indian State Owned Laboratories</a><br><span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=327154'>Prithwiraj Choudhury</a>, <a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6491'>Tarun Khanna</a></span><br><span class='pub cite'>Harvard Business School Working Paper, No. 14--077, February 2014</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=46651' target='_blank'>Toward Resource Independence--Why State-Owned Entities Become Multinationals: An Empirical Study of India's Public R&D Laboratories</a><br><span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=327154'>Prithwiraj Choudhury</a>, <a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6491'>Tarun Khanna</a></span><br><span class='pub cite'>Harvard Business School Working Paper, No. 14--076, February 2014</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=46386' target='_blank'>Hospital Clinic de Barcelona</a><br><span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6424'>Richard Bohmer</a>, Daniela Beyersdorfer, Jaume Ribera</span><br><span class='pub cite'>Harvard Business School Case 614--058, February 2014</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=46080' target='_blank'>Royal Caribbean Cruises Ltd.: Safety, Environment and Health</a><br><span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6684'>John A Quelch</a>, Margaret Rodriguez</span><br><span class='pub cite'>Harvard Business School Case 514--069, February 2014</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=45826' target='_blank'>Iora Health</a><br><span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6544'>William Sahlman</a>, Vineeta Vijayaraghavan</span><br><span class='pub cite'>Harvard Business School Case 814--030, February 2014</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=46192' target='_blank'>Designed for Workarounds: A Qualitative Study of the Causes of Operational Failures in Hospitals</a><br><span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=10668'>Anita Tucker</a>, W Scott Heisler, Laura D Janisse</span><br><span class='pub cite'>Permanente Journal (forthcoming)</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=46142' target='_blank'>Mylan Laboratories' Proposed Merger with King Pharmaceutical</a><br><span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=140857'>Lucy White</a>, Matt Kozlowski</span><br><span class='pub cite'>Harvard Business School Case 214--078, February 2014</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=46086' target='_blank'>Cancer Screening in Japan: Market Research and Segmentation</a><br><span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6684'>John A Quelch</a>, Margaret L Rodriguez</span><br><span class='pub cite'>Harvard Business School Case 514--057, January 2014</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=46081' target='_blank'>23andMe: Genetic Testing for Consumers (A)</a><br><span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6684'>John A Quelch</a>, Margaret Rodriguez</span><br><span class='pub cite'>Harvard Business School Case 514--086, January 2014</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=46069' target='_blank'>Evaluating the Effects of Large Scale Health Interventions in Developing Countries: The Zambian Malaria Initiative</a><br><span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=337273'>Nava Ashraf</a>, Gunther Fink, David N Weil</span><br><span class='pub cite'>In NBER Volume on African Economic Successes, edited by S. Edwards, S. Johnson, and D. Weil. University of Chicago Press, forthcoming</span><br><br>
<a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=46036' target='_blank'>Blame Me</a><br><span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=681583'>Kevin Sharer</a></span><br><span class='pub cite'>Harvard Business Review 92, nos. 1--2 (January--February 2014): 36</span><br><br>

<?php section("HBS Health Care In the News"); ?>

<p><a target='_blank' href='http://www.bizjournals.com/philadelphia/print-edition/2014/08/22/a-q-a-with-carolyn-jackson-ceo-of-st-christophers.html'>A Q&amp;A with Carolyn Jackson, CEO of St. Christopher's Hospital for Children</a><br/>Re: Carolyn Jackson (MBA 2002)<br/><i>Philadelphia Business Journal</i><br/>22 AUG 2014</p>
<p><a target='_blank' href='http://www.forbes.com/sites/hbsworkingknowledge/2014/08/18/harvard-wants-your-proven-ideas-to-improve-health-care/'>Harvard Wants Your Proven Ideas to Improve Health Care</a><br/>Re: Richard Hamermesh<br/><i>Forbes.com</i><br/>18 Aug 2014</p>
<p><a target='_blank' href='http://www.chinadaily.com.cn/world/2014-08/14/content_18310587.htm'>US-China teamwork could mean a win-win-win for Africa</a><br/>Re: John Quelch<br/><i>China Daily USA</i><br/>14 Aug 2014</p>
<p><a target='_blank' href='http://www.realclearmarkets.com/articles/2014/08/05/unfunded_retired_healthcare_benefits_are_the_elephant_in_the_room_101200.html'>Filling the Retiree Healthcare Benefits Gap</a><br/>By: Robert Pozen<br/><i>Real Clear Markets</i><br/>5 Aug 2014</p>
<p><a target='_blank' href='http://radioboston.wbur.org/2014/08/04/cowboy-doctors-cost'>'Cowboy Doctors' May Contribute to High Health Care Costs</a><br/>Re: Ariel Dora Stern<br/><i>WBUR: Radio Boston</i><br/>4 Aug 2014</p>
<p><a target='_blank' href='http://www.hbs.edu/news/releases/Pages/kaplan-life-science-fellows-2014.aspx'>Kaplan Life Sciences Fellows Announced</a><br/><i>Harvard Business School</i><br/>1 Aug 2014</p>
<p><a target='_blank' href='http://news.harvard.edu/gazette/story/newsplus/to-sell-obamacare-officials-should-learn-from-state-success-stories/'>To Sell Obamacare, Officials Should Learn From State Success Stories</a><br/>Re: John Quelch<br/><i>Harvard Gazette</i><br/>29 Jul 2014</p>
<p><a target='_blank' href='http://blogs.hbr.org/2014/07/to-fix-health-care-leaders-need-to-let-go-of-the-status-quo/'>To Fix Health Care, Leaders Need to Let Go of the Status Quo</a><br/>By: Jonathan Bush (MBA 1997)<br/><i>HBR Blogs</i><br/>22 Jul 2014</p>
<p><a target='_blank' href='http://www.forbes.com/sites/benkerschberg/2014/07/09/how-harvard-medical-school-uses-open-innovation-to-solve-world-class-scientific-problems/'>How Harvard Medical School Uses Open Innovation to Solve World-Class Scientific Problems</a><br/>Re: Karim Lakhani<br/><i>Forbes</i><br/>10 Jul 2014</p>
<p><a target='_blank' href='http://norwalk.dailyvoice.com/news/harvard-business-school-club-honors-new-canaan-founder-mmrf'>Harvard Business School Club Honors Norwalk's Multiple Myeloma Foundation</a><br/>Re: Jeff Immelt (MBA 1982) & Kathy Giusti (MBA 1985)<br/><i>Norwalk Daily Voice</i><br/>8 Jul 2014</p>
<p><a target='_blank' href='http://www.kaiserhealthnews.org/Stories/2014/June/30/Retooling-Hospitals-One-Data-Point-At-A-Time.aspx'>Utah Hospitals Try the Unthinkable: Get a Grip on Costs</a><br/>Re: Robert Kaplan<br/><i>Kaiser Health News</i><br/>28 Jun 2014</p>
<p><a target='_blank' href='http://www.nejm.org/doi/full/10.1056/NEJMp1314391'>Shifting Toward Defined Contributions — Predicting the Effects</a><br/>By: Regina Herzlinger<br/><i>New England Journal of Medicine</i><br/>26 Jun 2014</p>
<p><a target='_blank' href='http://wgbhnews.org/post/bpr-cash-incentives-health-callie-crossley-larry-hott-emily-rooney-friday-news-quiz'>Cash Incentives for Health</a><br/>Re: Leslie John<br/><i>Boston Public Radio</i><br/>23 Jun 2014</p>
<p><a target='_blank' href='http://www.irishtimes.com/news/health/health-cuts-wrong-way-to-reform-system-harvard-expert-says-1.1797768'>Health Cuts 'Wrong Way' to Reform System, Harvard Expert Says</a><br/>Re: Robert Kaplan<br/><i>Irish Times</i><br/>16 May 2014</p>
<p><a target='_blank' href='http://www.bostonglobe.com/magazine/2014/05/16/athenahealth-ceo-jonathan-bush-prescription-for-fixing-hospitals/Hz8sfMTdnKDCyXH4NObsZK/story.html'>Athenahealth Ceo Jonathan Bush: a Prescription for Fixing Hospitals</a><br/>Re: Jonathan Bush (MBA 1997)<br/><i>Boston Globe</i><br/>16 May 2014</p>




*[FAS]: Harvard Faculty of Arts and Sciences
*[HLS]: Harvard Law School
*[HKS]: Harvard Kennedy School
*[HBR]: Harvard Business Review
*[NEJM]: New England Journal of Medicine
*[BAHM]: Business School Alliance for Health Management 
*[HBSHAA]: Harvard Business School Healthcare Alumni Association
*[FHI]: Forum on Healthcare Innovation
*[HMS]: Harvard Medical School

[Summer Social in the City: Celebrate the Class of 2014 & Student Interns]: http://www.hbshealthalumni.org/article.html?aid=875
[9th Teikyo-HSPS Symposium: Training the Next Generation of Public Health Leaders]: http://www.hsph.harvard.edu/social-and-behavioral-sciences/wp-content/uploads/sites/66/2014/08/TeikyoHSPH-symposium-Sep-42014.pdf
[TEDMED2014: Unlocking Imagination]: http://www.tedmed.com/event/abouttheevent
[Roundtable: How is Digital Health Changing the Industry?]: http://www.hbshealthalumni.org/article.html?aid=874
[*The Economist*'s Health Care Forum]: http://www.hbshealthalumni.org/article.html?aid=849
[The Health Care Forum]: http://www.hbshealthalumni.org/article.html?aid=849
[HBS Reunion for 1969, 1979, 1984, 1974, 1989, and AMP & ISMP]: https://www.alumni.hbs.edu/events/reunions/Pages/default.aspx
[Health 2.0]: http://www.health2con.com/events/conferences/health-2-0-fall-conference-2014/
[Post-Trial Responsibilities: Ethics and Implementation]: http://petrieflom.law.harvard.edu/events/details/save-the-date-post-trial-access-to-medicines
[Myrto Lefkopoulou Distinguished Lecture]: http://ems.sph.harvard.edu/MasterCalendar/EventDetails.aspx?data=hHr80o3M7J6ZAGfYCbDm3KqXkS5C0o61Cpef15wsB5j2CiBFATFEnnb7nPPYO6r%2b
[BioPharm America]: http://www.massbio.org/events/calendar/2699-biopharm_america/event_detail
[AdvaMed 2014 MedTech Conference]: http://advamed2014.com/
[2014 Health Care Cost Trends Hearing]: http://www.mass.gov/anf/budget-taxes-and-procurement/oversight-agencies/health-policy-commission/annual-cost-trends-hearing/
[HealthTech Conference]: http://www.healthtechconference.com/
[Emerging Issues and New Frontiers for FDA Regulation]: http://petrieflom.law.harvard.edu/events/details/emerging-issues-and-new-frontiers-for-fda-regulation
[Roundtable: Where 2014 Is At & 2015 Is Headed]: http://www.hbshealthalumni.org/article.html?aid=871
[Global Networking Night]: http://www.hbshealthalumni.org/article.html?aid=876
[Connected Health Symposium 2014]: http://symposium.connected-health.org/?gclid=CMOhuai_i8ACFYhcMgodE18AGw
[2014 Global Pediatric Innovation Summit + Awards]: http://bostonchildrensinnovationsummit.org/
[Health, Law, and Human Rights in Surrogacy and Egg Donation]: http://petrieflom.law.harvard.edu/events/details/global-reproduction
[HBS Healthcare Alumni Annual Conference]: http://www.hbshealthalumni.org/article.html?aid=714#
[Live & Breathe: Building a Patient-Centered Biotech]: http://www.massbio.org/events/calendar/2801-live_breathe_building_a_patient-centered/event_detail
[Personalized Medicine Conference]: http://personalizedmedicine.partners.org/Education/Personalized-Medicine-Conference/Default.aspx
[Drug Development Boot Camp]: http://www.drugstomarket.com/harvarddrugbootcamp/
[56th American Society of Hematology Annual Meeting and Exposition]: http://www.hematology.org/Annual-Meeting/
[26th Annual National Forum on Quality Improvement in Health Care]: http://www.ihi.org/education/Conferences/Forum2014/Pages/Overview.aspx
[FDA/CMS Summit for Payers]: http://www.iirusa.com/fdacmspayers/home.xml
[FDA/CMS Summit for Biopharma Executives]: http://www.iirusa.com/FDACMS/home.xml

<?php // reusable images ?>
[speech bubbles]: http://www.hbs.edu/healthcare/images/photos/speechbubbles.png
[pill image]: http://www.hbs.edu/healthcare/images/photos/pill_100.png
[steth image]: http://www.hbs.edu/healthcare/images/photos/steth_100.png
[heart image]: http://www.hbs.edu/healthcare/images/photos/heart_100.png
[up_arrow image]: http://www.hbs.edu/healthcare/images/photos/up_arrow_100.png
[dna image]: http://www.hbs.edu/healthcare/images/photos/dna_100.png
[clip image]: http://www.hbs.edu/healthcare/images/photos/clip_100.png

<?php // logos ?>
[blavatnik logo]: http://www.hbs.edu/healthcare/images/photos/blavatnik_logo_171.png 


<?php // one-time images ?>
[Health Acceleration Challenge Poster]: http://www.hbs.edu/healthcare/images/photos/hac-img-290x278.png
[john quelch]: http://sands.hbs.edu/photos/facstaff/Ent6684.jpg
[Alexandra Dickson]: http://www.hbs.edu/healthcare/images/photos/Alexandra_Dickson.jpg
[Sid Misra]: http://www.hbs.edu/healthcare/images/photos/Sid_Misra.jpg
[Meridith Unger]: http://www.hbs.edu/healthcare/images/photos/Meridith_Unger.jpg
[Christoph Jaeker]: http://www.hbs.edu/healthcare/images/photos/Christoph_Jaeker.jpg
[Louis Levy]:  http://www.hbs.edu/healthcare/images/photos/Louis_Levy.jpg
<?php section('Upcoming Deadline: Independent Projects'); ?>  
EC students still looking for the perfect class for Winter term should check out our [Winter 2014 EC Health Care Opportunities] document. In particular, 
you may wish to consider choosing a project from a company listed on the Company Opportunity Posting page [http://j.mp/hbs-health-care-ips]. For more information, see the Independent Project FAQ [http://j.mp/hbs-ip-faq]. Registration for winter term IP [must be finalized] by the morning of the second Y day of January classes, which this year is [Thursday, January 30]. 

<?php section('Connect with your Alumni Mentor'); ?>  
As we approach the Winter term, we encourage those of you involved with the alumni mentor program to take your mentorship to the next level. 

* Identify 2-3 goals or skills that are most important to ask about  
* Refresh your communication pipeline; commit to a schedule and a medium  
* Be proactive and interested in their story. Ask them about how they found thier mentors and coaches.


*[FAS]: Harvard Faculty of Arts and Sciences
*[HLS]: Harvard Law School
*[HKS]: Harvard Kennedy School
*[HBR]: Harvard Business Review
*[NEJM]: New England Journal of Medicine
*[BAHM]: Business School Alliance for Health Management 
*[HBSHAA]: Harvard Business School Healthcare Alumni Association
*[FHI]: Forum on Healthcare Innovation
*[HMS]: Harvard Medical School

<?php // links for new issue down here ... 
?>
[Bluestem Brasserie Image]: http://www.hbs.edu/healthcare/images/photos/bluestem_282.png

<?php // in the news links ...
?>
[The Numbers Stack up for Better, Cheaper Treatment]: http://www.ft.com/intl/cms/s/0/84a5c49a-5771-11e3-b615-00144feabdc0.html?siteedition=intl#axzz2nB7M9U00
[Your Own Medicine]: https://www.alumni.hbs.edu/stories/Pages/story-bulletin.aspx?num=3032
[Prescription: Free the Data!]: https://www.alumni.hbs.edu/stories/Pages/story-bulletin.aspx?num=3031#prescription1
[Prescription: Build a Killer App]: https://www.alumni.hbs.edu/stories/Pages/story-bulletin.aspx?num=3031#prescription2
[Prescription: Measure Health Care's Real Costs]: https://www.alumni.hbs.edu/stories/Pages/story-bulletin.aspx?num=3031#prescription3
[Prescription: Make Medicine Personal]: https://www.alumni.hbs.edu/stories/Pages/story-bulletin.aspx?num=3031#prescription4
[Prescription: Leverage Human Nature]: https://www.alumni.hbs.edu/stories/Pages/story-bulletin.aspx?num=3031#prescription5
[Prescription: Integrate Preventive Care and Payment]: https://www.alumni.hbs.edu/stories/Pages/story-bulletin.aspx?num=3031#prescription6
[Encourage Breakthrough Health Care by Competing on Products Rather Than Patents]: http://hbswk.hbs.edu/item/7391.html
[The Mistakes Behind Healthcare.gov Are Probably Lurking in Your Company, Too]: http://blogs.hbr.org/2013/12/the-mistakes-behind-healthcare-gov-are-probably-lurking-in-your-company-too/
[A Mission with Precision: Kathy Giusti's Ambitious Quest to Cure Multiple Myeloma]: http://www.forbes.com/sites/vannale/2013/12/03/a-mission-with-precision-kathy-giustis-ambitious-quest-to-cure-multiple-myeloma/
[HBS Panel Discusses Health Care]: http://www.thecrimson.com/article/2013/11/7/hbs-panel-healthcare-kickoff/
[What Obamacare Ignores: Cutting Health Care Costs]: http://www.thefiscaltimes.com/Columns/2013/10/30/What-Obamacare-Ignores-Cutting-Health-Care-Costs

[hbr-nejm]: http://www.hbs.edu/healthcare/images/photos/hbr_nejm.png

<?php // publication authors ... 
?>
[Nava Ashraf]: http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=337273 {.pub .author}
[Robert Huckman]: http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=10653 {.pub .author}
[Richard Bohmer]: http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6424 {.pub .author}
[Robert Huckman]: http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=10653 {.pub .author}
[Elie Ofek]: http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=13407 {.pub .author}
[Anita Tucker]: http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=10668 {.pub .author}
[Amy Edmondson]: http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6451 {.pub .author}
[Michael E. Porter]: http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6532 {.pub .author}
[Leslie John]: http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=589473 {.pub .author}
<?php // publication titles 
?>
[Household Bargaining and Excess Fertility: An Experimental Study in Zambia]: http://www.hbs.edu/faculty/Pages/item.aspx?num=45931
[The Hidden Benefits of Keeping Teams Intact]: http://www.hbs.edu/faculty/Pages/item.aspx?num=45934
[Lessons from England's Health Care Workforce Redesign: No Quick Fixes]: http://www.hbs.edu/faculty/Pages/item.aspx?num=45847
[Public Reporting, Consumerism, and Patient Empowerment]: http://www.hbs.edu/faculty/Pages/item.aspx?num=45889
[Cialis Lifecycle Management: Lilly's BPH Dilemma]: http://www.hbs.edu/faculty/Pages/item.aspx?num=43156
[Increased Speed Equals Increased Wait: The Impact of a Reduction in Emergency Department Ultrasound Order Processing Time]: http://www.hbs.edu/faculty/Pages/item.aspx?num=45790
[Team Scaffolds: How Minimal Team Structures Enable Role-Based Coordination]: http://www.hbs.edu/faculty/Pages/item.aspx?num=42173
[The Strategy That Will Fix Health Care]: http://www.hbs.edu/faculty/Pages/item.aspx?num=45614
[Converging to the Lowest Denominator in Physical Health]: http://www.hbs.edu/faculty/Pages/item.aspx?num=45468

[speech bubbles]: http://www.hbs.edu/healthcare/images/photos/speechbubbles.png
[pill image]: http://www.hbs.edu/healthcare/images/photos/pill_100.png
[steth image]: http://www.hbs.edu/healthcare/images/photos/steth_100.png
[heart image]: http://www.hbs.edu/healthcare/images/photos/heart_100.png
[up_arrow image]: http://www.hbs.edu/healthcare/images/photos/up_arrow_100.png
[dna image]: http://www.hbs.edu/healthcare/images/photos/dna_100.png
[clip image]: http://www.hbs.edu/healthcare/images/photos/clip_100.png

[Steve Porter]: http://www.hbs.edu/healthcare/images/photos/blav_porter_112.png
[Daniel Oliver]: http://www.hbs.edu/healthcare/images/photos/blav_oliver_112.png

[Innovation in Health Care Dinner: Investor &amp; Entrepreneur Perspectives]: http://www.hbshealthalumni.org/article.html?aid=779
[JP Morgan Health Care Conference Reception]: http://www.hbshealthalumni.org/article.html?aid=797
[Crowdfunding Health Care: Secrets from Industry Insiders]: http://www.hbshealthalumni.org/article.html?aid=777
[San Francisco Networking Event for HBS Health Care Alumni]: http://www.hbshealthalumni.org/article.html?aid=801
[New York Networking Event: HBS--Wharton--Columbia]: http://www.hbshealthalumni.org/article.html?aid=798
[HBS Health Care Club Conference 2014]: http://www.hbshealthcareconference.org/health2014/?page_id=89

[December Alumni Bulletin]: https://www.alumni.hbs.edu/bulletin/Pages/table-of-contents.aspx?year=2013&month=12
<!-- [blavatnik logo]: http://www.hbs.edu/healthcare/images/photos/blavatnik_logo.gif -->
[blavatnik logo]: http://www.hbs.edu/healthcare/images/photos/blavatnik_logo_171.png 
[alum newsletter see more blavatnik]:   
http://healthcareinitiative.mkt1960.com/alumni_newsletter_2013-12_blavatnik_see_more

[Lauren Pflepsen]: mailto:lpflepsen@mba2012.hbs.edu
[Elle Pishny]: mailto:epishny@mba2012.hbs.edu

[A set of documents have been produced from the content of the panels]: http://www.hbs.edu/healthcare/pdf/2013%20Health%20Care%20Career%20Days%20Documents.pdf 
[let us know what you think]: mailto:csterling@hbs.edu
[2014 FIELD and IXP social media guidelines]: https://inside.hbs.edu/sites/deptshare/Healthcare/Shared%20Documents/MBA_public/2014%20FIELD%20and%20IXP%20social%20media%20guidelines%20-%20FINAL.docx
[Winter 2014 EC Health Care Opportunities]: http://www.hbs.edu/healthcare/pdf/Winter%202014%20Healthcare%20Courses%20For%20Second%20Year%20Students.pdf
[Rob Kaplan Video Image]: http://www.hbs.edu/healthcare/images/photos/rob-kaplan-mentorship-as-dialog.png
[Rob Kaplan Video]: 
http://www.youtube.com/watch?v=Gwhjr1LR4ck

[http://j.mp/hbs-ip-faq]: http://j.mp/hbs-ip-faq
[http://j.mp/hbs-health-care-ips]: http://j.mp/hbs-health-care-ips
[Thursday, January 30]: https://inside.hbs.edu/Departments/mba/academics/Documents/ec13-14.pdf
[must be finalized]: https://inside.hbs.edu/Departments/mba/ec_prereg/deadlines/Pages/default.aspx#winter

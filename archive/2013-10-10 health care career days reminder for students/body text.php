# Health Care Career Days
<?php beginTable();

left("[![steth image][]](http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=73289&filter=month&viewDay=15&viewMonth=9&viewYear=2013)");
right(
"## [Care Delivery Panel] -- October 15  
<span class='time'>5:45--6:45 p.m.</span>, <span class='place'>McCollum 202</span>  
Hear from Executive Education participants working in care delivery around the globe.
After the panel, stay for a reception to meet and network with the speakers and Exec Ed participants. 
Panel will be 5:45-6:45 PM and Reception will be 6:45- 7:15 PM. Please [RSVP] for this event.");

left("[![pill image][]](http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=70952&filter=month&viewDay=16&viewMonth=9&viewYear=2013)");
right(
"## [Life Sciences Panel] -- October 16  
<span class='time'>2--3 p.m.</span>, <span class='place'>Aldrich 108</span>  
Talk with HBS alumni about their careers in biotech and pharma to determine 
if these sectors are of interest to you.");

left("[![up_arrow image][]](http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=70957&filter=month&viewDay=16&viewMonth=9&viewYear=2013)");
right(
"## [Health Care Finance Panel] -- October 16  
<span class='time'>3--4 p.m.</span>, <span class='place'>Aldrich 108</span>  
On this CPD Day, alumni working in health care VC, PE, Biz Dev, and 
Investment Management discuss how to get your foot in the door.");

left("[![heart image][]](http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=73290)");
right(
"## [Devices and Diagnostics Panel] -- October 21  
<span class='time'>4:30--5:30 p.m.</span>, <span class='place'>Cumnock 230</span>  
Join HBS alums as they speak about their career paths in the devices and diagnostics health care sectors.");
               
endTable();
?>

*Please stay tuned for a digital document recap of a virtual Q&A with alums in the Digital Health sector.*

[pill image]: http://www.hbs.edu/healthcare/images/photos/pill_100.png
[steth image]: http://www.hbs.edu/healthcare/images/photos/steth_100.png
[heart image]: http://www.hbs.edu/healthcare/images/photos/heart_100.png
[up_arrow image]: http://www.hbs.edu/healthcare/images/photos/up_arrow_100.png
[dna image]: http://www.hbs.edu/healthcare/images/photos/dna_100.png
[clip image]: http://www.hbs.edu/healthcare/images/photos/clip_100.png

[Care Delivery Panel]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=73289&filter=month&viewDay=15&viewMonth=9&viewYear=2013
[RSVP]: https://secure.hbs.edu/poll/taker/pollTaker.jsp?poll=134521
[Life Sciences Panel]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=70952&filter=month&viewDay=16&viewMonth=9&viewYear=2013
[Health Care Finance Panel]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=70957&filter=month&viewDay=16&viewMonth=9&viewYear=2013
[Devices and Diagnostics Panel]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=73290

[![![up_arrow image][]]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=73289&filter=month&viewDay=15&viewMonth=9&viewYear=2013

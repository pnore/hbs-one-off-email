<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Economist Health Care Forum</title>
    </head>
    <body>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <p>
        <title>Economist Health Care Forum</title>
        <style type="text/css">
body, td, th {
	color: #F00;
}
</style> <custom name="opencounter" type="tracking">       </custom></p>
        <table bgcolor="#ffffff" border="0" cellpadding="10" cellspacing="0" width="100%">
            <tbody>
                <tr>
                    <td>
                    <table width="620" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tbody>
                            <tr>
                                <td style="line-height: 0pt; font-size: 0pt;"><img width="1" hspace="0" height="5" border="0" align="left" name="Cont_0" alt="" src="http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                            </tr>
                            <tr>
                                <td>
                                <table cellspacing="0" cellpadding="0" border="0" align="left">
                                    <tbody>
                                        <tr>
                                            <!-- this section can be commented in to make the HIAA-specific newsletter: delete next line -->
                                            <td><span style="color: rgb(128, 130, 133); font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 15px;">The Economist has offered a discount to Harvard Business School (HBS) Alumni and Students. You are receiving this update because you have expressed an interest in health care activities at HBS.</span></td>
                                            <!-- this section can be commented in to make the HIAA-specific newsletter: delete previous line -->
                                            <td width="18" style="line-height: 0pt; font-size: 0pt;"><img width="18" hspace="0" height="9" align="left" name="Cont_0" alt="" src="http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                                            <td style="line-height: 15px; font-size: 11px;"><a xt="SPCLICK" style="color: rgb(128, 130, 133); text-decoration: none;" href="http://recp.rm04.net/servlet/MailView?m=%%MAILING_ID%%&r=%%RECIPIENT_ID%%&j=%%JOB_ID_CODE%%&mt=1" name="recp_rm04_net_servlet_MailView"><font size="1" face="Arial, Helvetica, sans-serif" color="#808285" style="line-height: 15px; font-size: 11px;">View in browser</font></a></td>
                                            <td width="18" style="line-height: 0pt; font-size: 0pt;"><img width="18" hspace="0" height="9" align="left" name="Cont_0" alt="" src="http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                                        </tr>
                                    </tbody>
                                </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="line-height: 0pt; font-size: 0pt;"><img width="1" hspace="0" height="5" align="left" name="Cont_0" alt="" src="http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                            </tr>
                        </tbody>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                    <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" height="1629" width="534">
                        <tbody>
                            <tr>
                                <td align="center"><!-- Header --><table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tbody>
                          <tr>
                            <td class="img" style="font-size:0pt;
                              line-height:0pt; text-align:left" valign="top"><a href="http://www.economist.com/events-conferences/americas/healthcare-2014?fsrc=events|EMPMPHAR#ec-event-overview" target="_blank"><img src="http://www.economist.com/sites/default/files/marketingimages/Innovation-2014/logo.png" alt="The Economist
                                  Events" border="0" height="50" width="204"></a></td>
                            <td align="right" valign="top">
                              <table border="0" cellpadding="0" cellspacing="0">
                                <tbody>
                                  <tr>
                                    <td class="top" style="color:#6c6d6d;
                                      font-family:Arial; font-size:11px;
                                      line-height:15px; text-align:left" valign="bottom">Share</td>
                                    <td class="img" style="font-size:0pt;
                                      line-height:0pt; text-align:left" width="10"><br>
                                    </td>
                                    <td class="img" style="font-size:0pt;
                                      line-height:0pt; text-align:left"><a href="http://www.linkedin.com/company/the-economist?trk=company_logo" target="_blank"><img src="http://www.economist.com/sites/default/files/marketingimages/HealthCare2014HTML/linkedin.jpg" alt="LinkedIn" border="0" height="20" width="20"></a></td>
                                    <td class="img" style="font-size:0pt;
                                      line-height:0pt; text-align:left" width="10"><br>
                                    </td>
                                    <td class="img" style="font-size:0pt;
                                      line-height:0pt; text-align:left"><a href="https://twitter.com/TheEconomist" target="_blank"><img src="http://www.economist.com/sites/default/files/marketingimages/HealthCare2014HTML/twitter.jpg" alt="Twitter" border="0" height="20" width="20"></a></td>
                                    <td class="img" style="font-size:0pt;
                                      line-height:0pt; text-align:left" width="10"><br>
                                    </td>
                                    <td class="img" style="font-size:0pt;
                                      line-height:0pt; text-align:left"><a href="https://www.facebook.com/TheEconomist" target="_blank"><img src="http://www.economist.com/sites/default/files/marketingimages/HealthCare2014HTML/facebook.jpg" alt="Facebook" border="0" height="20" width="20"></a></td>
                                  </tr>
                                </tbody>
                              </table>
                              <div style="font-size:0pt;
                                line-height:0pt; height:15px"><img src="empty.gif" style="height:15px" alt="" height="15" width="1"></div>
                              <table border="0" cellpadding="0" cellspacing="0">
                                <tbody>
                                  <tr>
                                    <td class="top" style="color:#6c6d6d;
                                      font-family:Arial; font-size:11px;
                                      line-height:15px; text-align:left"><a href="http://www.economist.com/events-conferences/americas/healthcare-2014?fsrc=events|EMPMPHAR#ec-event-overview" target="_blank" class="link-top" style="color:#0085cf;
                                        text-decoration:none"><span class="link-top" style="color:#dc291e;
                                          text-decoration:none"><b>Learn more</b></span></a></td>
                                    <td class="img-right" style="font-size:0pt;
                                      line-height:0pt; text-align:right" width="11"><a href="http://www.eventbrite.com/e/the-economists-health-care-forum-tickets-10819993893?aff=EPMEMFDA" target="_blank"><img src="http://www.economist.com/sites/default/files/marketingimages/HealthCare2014HTML/bullet1.jpg" alt="" border="0" height="10" width="6"></a></td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <!-- END Header -->
                      <div style="font-size:0pt; line-height:0pt;
                        height:10px"><img src="empty.gif" style="height:10px" alt="" height="10" width="1"></div>
                      <!-- Hero -->
                      <div class="img" style="font-size:0pt;
                        line-height:0pt; text-align:left"><a href="http://www.economist.com/events-conferences/americas/healthcare-2014?fsrc=events|EMPMPHAR#ec-event-overview" target="_blank"><img src="http://www.economist.com/sites/default/files/marketingimages/HealthCare2014HTML/550x283_html-art.jpg" alt="Health Care
                            Forum 2014" border="0" height="283" width="550"></a></div>
                      <!-- END Hero -->
                      <div style="font-size:0pt; line-height:0pt;
                        height:10px"><img src="empty.gif" style="height:10px" alt="" height="10" width="1"></div>
                                <!-- Content -->
                                <table style=" background:
                                    -moz-linear-gradient(top, rgba(212,221,221,0.5)
                                    0%, rgba(212,221,221,0) 100%); /* FF3.6+ */
                                    background: -webkit-gradient(linear, left top,
                                    left bottom,
                                    color-stop(0%,rgba(212,221,221,0.5)),
                                    color-stop(100%,rgba(212,221,221,0))); /*
                                    Chrome,Safari4+ */ background:
                                    -webkit-linear-gradient(top,
                                    rgba(212,221,221,0.5) 0%,rgba(212,221,221,0)
                                    100%); /* Chrome10+,Safari5.1+ */ background:
                                    -o-linear-gradient(top, rgba(212,221,221,0.5)
                                    0%,rgba(212,221,221,0) 100%); /* Opera 11.10+ */
                                    background: -ms-linear-gradient(top,
                                    rgba(212,221,221,0.5) 0%,rgba(212,221,221,0)
                                    100%); /* IE10+ */ background:
                                    linear-gradient(to bottom, rgba(212,221,221,0.5)
                                    0%,rgba(212,221,221,0) 100%); /* W3C */ filter:
                                    progid:DXImageTransform.Microsoft.gradient(
                                    startColorstr='#80d4dddd',
                                    endColorstr='#00d4dddd',GradientType=0 ); /*
                                    IE6-9 */ " border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td class="img" style="font-size:0pt;
                                            line-height:0pt; text-align:left" width="15"></td>
                                            <td width="521" class="text" style="text-align: center;">
                                            <div style="color: rgb(77, 77, 79); font-family: Arial; font-size: 0pt; line-height: 0pt; height: 15px;">
                                            <div align="left"><img name="Cont_5" src="empty.gif" style="height:15px" alt="" width="1" /></div>
                                            </div>
                                            <div class="h2" style="color: rgb(220, 41, 30); font-family: Arial; font-size: 22px; line-height: 26px; text-align: left; font-weight: lighter;">
                                            <div align="left">Join senior health-care influencers Wednesday</div>
                                            </div>
                                            <div style="color: rgb(77, 77, 79); font-family: Arial; font-size: 0pt; line-height: 0pt; height: 10px;">
                                            <div align="left"><span style="text-align: left"><img name="Cont_5" src="empty.gif" style="height:10px" alt="" width="1" /> </span></div>
                                            </div>
                                            <div align="left"><span style="text-align: left;"><br />
                                            <font color="#4d4d4f" face="Arial"><span style="font-size: 14px; line-height: 18px;">                               The health care industry is at a turning point around the world. As technologies offer the promise of a solution, the pace of change in the health care industry has hit fast forward, and payors, providers and patients remain unclear about the impact of these changes for the future. </span></font><br />
                                            <br />
                                            <i style="color: rgb(77, 77, 79); font-family: Arial; font-size: 14px; line-height: 18px;">The Economist</i><font color="#4d4d4f" face="Arial"><span style="font-size: 14px; line-height: 18px;">'s </span></font><a name="www_economist_com_events_" xt="SPCLICK" href="http://www.economist.com/events-conferences/americas/healthcare-2014?fsrc=events|EMPMPHAR#ec-event-overview" target="_blank" style="color: rgb(77, 77, 79); font-family: Arial; font-size: 14px; line-height: 18px;">Health Care Forum</a><font color="#4d4d4f" face="Arial"><span style="font-size: 14px; line-height: 18px;"> on September 17th will examine health care on a global scale. E</span></font><font color="#4d4d4f" face="Arial"><span style="font-size: 14px; line-height: 18px;">xplore how systems around the world are changing and what executives in the US can learn from foreign models and innovation in care delivery both here and abroad.</span></font><br />
                                            <br />
                                            <font color="#4d4d4f" face="Arial"><span style="font-size: 14px; line-height: 18px;">Students can receive a 50% discount by sending an email to the following address: <a href="mailto:event-tickets@economist.com?subject=HBS%20Student%20Sign%20Up%20-%20Economist" target="_blank" name="email for economist student sign up" xt="SPEMAIL">event-tickets@economist.com</a> and mentioning the Health Care Initiative.&nbsp;</span></font><br />
                                            <br />
                                            </span></div>
                                            <p align="left" style="color: rgb(220, 41, 30); font-family: Arial; font-size: 16px; line-height: 18px; text-align: left;">Speakers                                                                                                                                  include:</p>
                                            <div style="color: rgb(77, 77, 79); font-family: Arial; font-size: 0pt; line-height: 0pt; height: 10px;">
                                            <div align="left"><img name="Cont_5" src="empty.gif" style="height:10px" alt="" width="1" /></div>
                                            </div>
                                            <div align="left" style="color: rgb(77, 77, 79); font-family: Arial; font-size: 13px; line-height: 18px;">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td class="text2-right" style="color:#4d4d4f;
                                                        font-family:Arial; font-size:12px;
                                                        line-height:16px;
                                                        text-align:right" valign="top">
                                                        <div style="font-size:0pt;
                                                        line-height:0pt; height:15px"><img name="Cont_5" src="empty.gif" style="height:15px" alt="" width="1" /></div>
                                                        <strong><a xt="SPCLICK" name="www_economist_com_events__1" href="http://www.economist.com/events-conferences/americas/healthcare-2014?fsrc=events|EMPMPHAR#ec-event-speakers" target="_blank" class="link" style="color:#14526e;
                                                        text-decoration:none"><span class="link" style="color:#14526e;
                                                        text-decoration:none">Jim Greiner</span></a></strong><br />
                                                        President<br />
                                                        <strong>iTriage   <br />
                                                        </strong></td>
                                                        <td class="img-right" style="font-size:0pt;
                                                        line-height:0pt; text-align:right" valign="top" width="240"><a xt="SPCLICK" name="www_economist_com_events__1" href="http://www.economist.com/events-conferences/americas/healthcare-2014?fsrc=events|EMPMPHAR#ec-event-speakers" target="_blank"><img name="Cont_8" src="http://www.economist.com/sites/default/files/marketingimages/Jim%20Greiner_html.jpg" alt="Jim Greiner" border="0" height="100" width="218" /></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td class="img" style="font-size:0pt;
                                                        line-height:0pt; text-align:left" valign="top" width="240"><a xt="SPCLICK" name="www_economist_com_events__2" href="http://www.economist.com/events-conferences/americas/healthcare-2014?fsrc=events|EMPMPHAR#ec-event-speakers%20" target="_blank"><img name="Cont_9" src="http://www.economist.com/sites/default/files/marketingimages/HealthCare2014HTML/Anders---GEHC-copy.jpg" alt="Anders Wold" border="0" height="100" width="218" /></a></td>
                                                        <td class="text2" style="color:#4d4d4f;
                                                        font-family:Arial; font-size:12px;
                                                        line-height:16px; text-align:left" valign="top">
                                                        <div style="font-size:0pt;
                                                        line-height:0pt; height:15px"><img name="Cont_5" src="empty.gif" style="height:15px" alt="" width="1" /></div>
                                                        <strong><a xt="SPCLICK" name="www_economist_com_events__1" href="http://www.economist.com/events-conferences/americas/healthcare-2014?fsrc=events|EMPMPHAR#ec-event-speakers" target="_blank" class="link" style="color:#14526e;
                                                        text-decoration:none"><span class="link" style="color:#14526e;
                                                        text-decoration:none">Anders Wold</span></a></strong><br />
                                                        President and chief executive, GE                                         Ultrasound<br />
                                                        <strong>GE Healthcare<br />
                                                        </strong></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td class="text2-right" style="color:#4d4d4f;
                                                        font-family:Arial; font-size:12px;
                                                        line-height:16px;
                                                        text-align:right" valign="top">
                                                        <div style="font-size:0pt;
                                                        line-height:0pt; height:15px"><img name="Cont_5" src="empty.gif" style="height:15px" alt="" width="1" /></div>
                                                        <strong><a xt="SPCLICK" name="www_economist_com_events__1" href="http://www.economist.com/events-conferences/americas/healthcare-2014?fsrc=events|EMPMPHAR#ec-event-speakers" target="_blank" class="link" style="color:#14526e;
                                                        text-decoration:none"><span class="link" style="color:#14526e;
                                                        text-decoration:none">Sandra Fenwick</span></a></strong><br />
                                                        Chief executive<br />
                                                        <strong>Boston Children&rsquo;s Hospital</strong></td>
                                                        <td class="img-right" style="font-size:0pt;
                                                        line-height:0pt; text-align:right" valign="top" width="240"><a xt="SPCLICK" name="www_economist_com_events__2" href="http://www.economist.com/events-conferences/americas/healthcare-2014?fsrc=events|EMPMPHAR#ec-event-speakers%20" target="_blank"><img name="Cont_10" src="http://www.economist.com/sites/default/files/marketingimages/Sandra_Fenwick_html.jpg" alt="Sandra Fenwick" border="0" height="100" width="218" /></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td class="img" style="font-size:0pt;
                                                        line-height:0pt; text-align:left" valign="top" width="240"><a xt="SPCLICK" name="www_economist_com_events__2" href="http://www.economist.com/events-conferences/americas/healthcare-2014?fsrc=events|EMPMPHAR#ec-event-speakers%20" target="_blank"><img name="Cont_11" src="http://www.economist.com/sites/default/files/marketingimages/HealthCare2014HTML/Jon-Bush.jpg" alt="Jonathan Bush" border="0" height="100" width="218" /></a></td>
                                                        <td class="text2" style="color:#4d4d4f;
                                                        font-family:Arial; font-size:12px;
                                                        line-height:16px; text-align:left" valign="top">
                                                        <div style="font-size:0pt;
                                                        line-height:0pt; height:15px"><img name="Cont_5" src="empty.gif" style="height:15px" alt="" width="1" /></div>
                                                        <strong><a xt="SPCLICK" name="www_economist_com_events__1" href="http://www.economist.com/events-conferences/americas/healthcare-2014?fsrc=events|EMPMPHAR#ec-event-speakers" target="_blank" class="link" style="color:#14526e;
                                                        text-decoration:none"><span class="link" style="color:#14526e;
                                                        text-decoration:none">Jonathan Bush</span></a></strong><br />
                                                        Chief executive<br />
                                                        <strong>athenahealth<br />
                                                        </strong></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            </div>
                                            <div style="color: rgb(77, 77, 79); font-family: Arial; font-size: 0pt; line-height: 0pt; height: 35px;"></div>
                                            <p style="color: rgb(220, 41, 30); font-family: Arial; font-size: 15px; line-height: 18px;" align="center"><strong>To obtain the 50% off student discount, please <a xt="SPEMAIL" name="event_tickets_economist_com" href="mailto:event-tickets@economist.com" target="_blank">email <em>The Economist</em></a>.</strong><span style="color:#4d4d4f"><br />
                                            <br />
                                            </span></p>
                                            <div class="img-center" style="color: rgb(77, 77, 79); font-family: Arial; font-size: 0pt; line-height: 0pt; text-align: center;">
                                            <div align="center"><a name="healthcareforum2014_event" xt="SPCLICK" href="http://healthcareforum2014.eventbrite.com/?aff=EMPMPHAR" target="_blank">Join HBS</a></div>
                                            </div>
                                            <div style="color: rgb(77, 77, 79); font-family: Arial; font-size: 0pt; line-height: 0pt; height: 25px;">
                                            <div align="center"><img name="Cont_5" src="empty.gif" style="height:25px" alt="" width="1" /> ss dd</div>
                                            </div>
                                            <p align="center" style="color: rgb(77, 77, 79); font-family: Arial; font-size: 13px; line-height: 18px;"><a name="www_economist_com_events_" xt="SPCLICK" href="http://www.economist.com/events-conferences/americas/healthcare-2014?fsrc=events|EMPMPHAR#ec-event-overview" target="_blank">Health Care Forum</a></p>
                                            <p align="left" style="color: rgb(77, 77, 79); font-family: Arial; font-size: 13px; line-height: 18px;">&nbsp;</p>
                                            <div style="color: rgb(77, 77, 79); font-family: Arial; font-size: 13px; line-height: 18px;">
                                            <div align="left">
                                            <table width="479" border="0" align="center" cellpadding="0" cellspacing="0" style="width: 442px; height: 137px;">
                                                <tbody>
                                                    <tr>
                                                        <td valign="top" width="550">
                                                        <div align="center">
                                                        <div align="center"></div>
                                                        <table width="349" height="115" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="217">
                                                                    <table width="117" border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#d5ded8">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="font-size: 0pt; line-height: 0pt; text-align: left;" width="18">
                                                                                <div style="font-size: 0pt; line-height: 0pt; height: 20px;"><img name="Cont_13" src="file:///C:/Users/Lynne/Downloads/empty.gif" style="height: 20px;" alt="" width="1" /></div>
                                                                                </td>
                                                                                <td style="color: rgb(77, 77, 79); font-family: Arial; font-size: 10px; line-height: 14px; text-align: center; font-weight: bold;" width="89">Platinum sponsor</td>
                                                                                <td style="font-size: 0pt; line-height: 0pt; text-align: left;" width="10"></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <div align="center">
                                                                    <div align="center">
                                                                    <p align="left"></p>
                                                                    </div>
                                                                    </div>
                                                                    </td>
                                                                    <td width="116">
                                                                    <table width="117" border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#d5ded8">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="font-size: 0pt; line-height: 0pt; text-align: left;" width="18">
                                                                                <div style="font-size: 0pt; line-height: 0pt; height: 20px;"><img name="Cont_13" src="file:///C:/Users/Lynne/Downloads/empty.gif" style="height: 20px;" alt="" width="1" /></div>
                                                                                </td>
                                                                                <td style="color: rgb(77, 77, 79); font-family: Arial; font-size: 10px; line-height: 14px; text-align: center; font-weight: bold;" width="89">Silver  sponsor</td>
                                                                                <td style="font-size: 0pt; line-height: 0pt; text-align: left;" width="10"></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <div align="center">
                                                                    <div align="center">
                                                                    <p align="left"></p>
                                                                    </div>
                                                                    </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><a xt="SPCLICK" name="www_lilly_com_" href="http://www.lilly.com/" target="_blank"><img name="Cont_14" src="http://www.economist.com/sites/default/files/marketingimages/Lily_html_22.jpg" alt="Lilly" align="left" style="border: 0px solid ; width: 120px; height: 75px;" sr="sr" www.economist.com="" sites="" default="" files="" marketingimages="" lily_html_22.png="" /></a></td>
                                                                    <td><a xt="SPCLICK" name="www_caremark_com_wps_portal" href="https://www.caremark.com/wps/portal" target="_blank"><img name="Cont_15" src="http://www.economist.com/sites/default/files/marketingimages/CVS_120x120.jpg" alt="Lilly" align="left" style="border: 0px solid ; width: 100px; height: 100px;" sr="sr" www.economist.com="" sites="" default="" files="" marketingimages="" lily_html_22.png="" /></a></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        </div>
                                                        <div style="color:#4d4d4f;
                                                        font-family:Arial; font-size:16px;
                                                        line-height:19px; text-align:center"></div>
                                                        <div><img name="Cont_5" src="empty.gif" style="height:30px" alt="" width="1" /><!-- Footer -->
                                                        <table bgcolor="#ebebeb" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="font-size:0pt;
                                                                    line-height:0pt;
                                                                    text-align:left" width="60"></td>
                                                                    <td style="color:#828a8f;
                                                                    font-family:Arial;
                                                                    font-size:11px;
                                                                    line-height:15px;
                                                                    text-align:left">
                                                                    <div style="font-size:0pt;
                                                                    line-height:0pt; height:25px"><img name="Cont_5" src="empty.gif" style="height:25px" alt="" width="1" /></div>
                                                                    <p><a xt="SPCLICK" name="www_economistgroup_com_results" href="http://www.economistgroup.com/results_and_governance/governance/privacy/?elqTrack=true" target="_blank" class="link2" style="color:#008cd2;
                                                                    text-decoration:none">Privacy                                                                                                                                                                                                                                                                                   policy </a>| <a xt="SPCLICK" name="www_economist_com_contact_info" href="http://www.economist.com/contact-info?elqTrack=true" target="_blank" class="link2" style="color:#008cd2;
                                                                    text-decoration:none">Contact                                                                                                                                                                                                                                    us </a>| <a xt="SPCLICK" name="www_economist_com_legal_terms_" href="http://www.economist.com/legal/terms-of-use?elqTrack=true" target="_blank" class="link2" style="color:#008cd2;
                                                                    text-decoration:none">Terms                                                                                                                                                                                                                                      and conditions</a></p>
                                                                    <p><a xt="SPCLICK" name="response_economistevents_com_E" href="http://response.economistevents.com/EM2180-unsubscribe/?elqTrack=true" target="_blank">Click                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               here</a> if you do not wish                                                       to receive any further e-mails                                                       about The Health Care Forum.<br />
                                                                    <br />
                                                                    <a xt="SPCLICK" name="response_economistevents_com_T" href="http://response.economistevents.com/TLE-US-Unsubscribe?elqTrack=true" target="_blank">Click                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               here</a> if you do not wish                                                       to receive any further e-mails                                                       from us about new features,                                                       events, special offers and for                                                       market research purposes.</p>
                                                                    <p>An                                                                                                                                                                                                                                                                                   Economist Group business                                                       2014. All rights Reserved<br />
                                                                    The Economist Group 750 3rd                                                   Avenue, New York, NY 10017</p>
                                                                    <div style="font-size:0pt;
                                                                    line-height:0pt; height:50px"><img name="Cont_5" src="empty.gif" style="height:50px" alt="" width="1" /></div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">&nbsp;</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            </div>
                                            </div>
                                            </td>
                                            <td width="15" class="text" style="color:#4d4d4f; font-family:Arial; font-size:13px; line-height:18px; text-align:center">&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <p></p>
    </body>
</html>
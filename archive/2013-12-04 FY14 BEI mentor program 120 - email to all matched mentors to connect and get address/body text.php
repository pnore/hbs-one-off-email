# Dear %%alumni_salutation%%, 

Thanks again for being a mentor this year. We would like to remind you to email your mentee to arrange a time to meet by phone or in person before or during break.  

As we approach the holiday season we would we would like to send you a gift as a gesture of gratitude. 
If you have not updated [your alumni profile] in a while, please 
[click here to update your address] by December 9.   

On behalf of the Business and Environment Initiative, we want to thank you once again for volunteering. 

## Thank you,  
Melissa Paschall  
Director, Business & Environment Initiative

Libby Hamer  
Coordinator, Business & Environment Initiative 

[your alumni profile]: https://www.alumni.hbs.edu/community/Pages/edit-profile.aspx
[click here to update your address]: https://www.alumni.hbs.edu/community/Pages/edit-profile.aspx

[your HBS Alumni Profile Page]: %%hbs_alumni_profile_url%%
[update your profile]: https://www.alumni.hbs.edu/community/Pages/edit-profile.aspx
[contact us]: mailto:healthcare_initiative@hbs.edu?subject=Update%20Alumni%20Address%20for%20Mentor%20Program
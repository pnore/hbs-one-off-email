<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?php echo $email_subject; ?></title>
</head>

<body marginwidth="0" marginheight="0" bgcolor="<?php get_color($topheader_color); ?>"
topmargin="0" leftmargin="0">
  <?php // fill in the email pre-header. See goo.gl/IJaxHy and goo.gl/hf4h6P ?><span style="display: none !important;"><?php
                          if (isset($email_preheader) && $email_preheader) {
                                  // not supported at this time
                                  //echo $email_preheader;
                          } else {
                                  //echo $email_subject;
                          }
                  ?></span>

  <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor=
  "<?php get_color($topheader_color); ?>" align="center">
    <tbody>
      <tr>
        <td bgcolor="<?php get_color($topheader_color); ?>">
          <table width="620" cellspacing="0" cellpadding="0" border="0" align="center">
            <tbody>
              <tr>
                <td style="line-height: 0pt; font-size: 0pt;"><img width="1" hspace="0"
                height="5" border="0" align="left" name="Cont_0" alt="" src=
                "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
              </tr>

              <tr>
                <td>
                  <table cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody>
                      <tr>
                        <td><font size="1" face="Arial, Helvetica, sans-serif" color=
                        "#808285" style=
                        "line-height: 15px; font-size: 11px;"><?php get_department_info('topmessage'); ?></font></td>

                        <td width="18" style="line-height: 0pt; font-size: 0pt;">
                        <img width="18" hspace="0" height="9" align="left" alt="" name=
                        "Cont_0" src=
                        "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>

                        <td style="line-height: 15px; font-size: 11px;"><a xt="SPCLICK"
                        style="color: rgb(128, 130, 133); text-decoration: none;" href=
                        "http://recp.rm04.net/servlet/MailView?m=%%MAILING_ID%%&amp;r=%%RECIPIENT_ID%%&amp;j=%%JOB_ID_CODE%%&amp;mt=1"
                        name="recp_rm04_net_servlet_MailView"><font size="1" face=
                        "Arial, Helvetica, sans-serif" color="#808285" style=
                        "line-height: 15px; font-size: 11px;">View in
                        browser</font></a></td>

                        <td width="18" style="line-height: 0pt; font-size: 0pt;">
                        <img width="18" hspace="0" height="9" align="left" name="Cont_0"
                        alt="" src=
                        "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>

              <tr>
                <td style="line-height: 0pt; font-size: 0pt;"><img width="1" hspace="0"
                height="5" align="left" name="Cont_0" alt="" src=
                "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>

      <tr>
        <td valign="top" bgcolor="#333333">
          <table width="620" cellspacing="0" cellpadding="0" border="0" align="center">
            <tbody>
              <tr>
                <td width="470" style="line-height: 0pt; font-size: 0pt;"><a href=
                "<?php get_department_info('url'); ?>" name="HBS_Healthcare" xt=
                "SPCLICK" target="_blank"><img width="470" hspace="0" height="76" border=
                "0" align="left" src=
                "<?php get_department_info('image'); ?>" /></a></td>

                <td width="20" style="line-height: 0pt; font-size: 0pt;"><img width="20"
                hspace="0" height="1" align="left" name="Cont_0" alt="" src=
                "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>

      <tr>
        <td height="500" bgcolor="<?php get_color($background_color); ?>">
          <table width="620" cellspacing="0" cellpadding="0" border="0" align="center">
            <tbody>
              <tr>
                <td valign="top" align="left">
                  <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor=
                  "<?php get_color($background_color); ?>">
                    <tbody>
                      <tr>
                        <?php if(!(isset($show_header_image) && $show_header_image )): ?>

                        <td style="line-height: 0pt; font-size: 0pt;"><img width="470"
                        hspace="0" height="75" border="0" align="left" name="Cont_0" alt=
                        "" src=
                        "http://www.hbs.edu/shared/images/email/pixel.gif" /></td><?php else: ?>

                        <td width="470"><img width="472" height="102" src=
                        "<?php get_header_image(); ?>" /></td><?php endif ?>

                        <td valign="bottom" align="right">
                          <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td width="130" valign="bottom" align="right" style=
                                "text-align: right;"><font size="4" face=
                                "Arial, Helvetica, sans-serif" color=
                                "<?php get_color($header_date_color); ?>" style=
                                "font-size: 20px;"><strong><?php echo $header_date_phrase ?></strong></font></td>

                                <td width="16" style="line-height: 0pt; font-size: 0pt;">
                                <img width="16" hspace="0" height="10" align="left" name=
                                "Cont_0" alt="" src=
                                "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                              </tr>

                              <tr>
                                <td valign="top" height="15" align="left" colspan="2">
                                <img width="20" hspace="0" height="15" name="Cont_0" alt=
                                "" src=
                                "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>

              <tr>
                <td width="620" valign="top" align="left"></td>
              </tr>

              <tr>
                <td valign="top" align="left">
                  <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor=
                  "<?php get_color($default_text_color); ?>">
                    <tbody>
                      <tr>
                        <td width="580" valign="top" align="left" bgcolor=
                        "<?php get_color($background_color); ?>">
                          <?php if( isset($show_body_topdescription_row) && $show_body_topdescription_row ): ?>

                          <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                              <tr>
                                <td style=
                                "PADDING-BOTTOM: 0px; PADDING-LEFT: 15px; PADDING-RIGHT: 15px; PADDING-TOP: 25px"
                                bgcolor="<?php get_color($body_background_color); ?>">
                                  <div align="center">
                                    <table border="0" cellspacing="0" cellpadding="0"
                                    width="500">
                                      <tbody>
                                        <tr>
                                          <td align="center" bgcolor=
                                          "<?php get_color($background_color); ?>" style=
                                          " width: 228px; height: 114px;"><img style=
                                          "border-style:none;DISPLAY: block; FONT-FAMILY: Arial; COLOR: #333; FONT-SIZE: 12px; FONT-WEIGHT: bold"
                                          border="0" alt=
                                          "making a difference in healthcare" src=
                                          "http://www.hbs.edu/healthcare/images/photos/making-a-difference-in-healthcare-228x146.png"
                                          width="228" height="146" /></td>

                                          <td>
                                            <table border="0" cellspacing="0"
                                            cellpadding="0" width="100%">
                                              <tbody>
                                                <tr>
                                                  <td style=
                                                  "PADDING-BOTTOM: 5px; PADDING-LEFT: 25px; LETTER-SPACING: -1px; PADDING-RIGHT: 0px; FONT-FAMILY: Helvetica, Arial, sans-serif; COLOR: #eadfc8; FONT-SIZE: 23px; FONT-WEIGHT: bold; PADDING-TOP: 0px;"
                                                  align="left">
                                                  <?php echo $body_topdescription_title; ?></td>
                                                </tr>

                                                <tr>
                                                  <td style=
                                                  "PADDING-BOTTOM: 10px; LINE-HEIGHT: 20px; PADDING-LEFT: 25px; PADDING-RIGHT: 0px; FONT-FAMILY: Helvetica, Arial, sans-serif; COLOR: &lt;?php get_color($default_text_color); ?&gt;; FONT-SIZE: 16px; TEXT-DECORATION: none; PADDING-TOP: 0px">
                                                  <?php echo $body_topdescription_date; ?><br />

                                                  <?php echo $body_topdescription_time; ?><br />

                                                  <?php echo $body_topdescription_location; ?></td>
                                                </tr>

                                                <tr>
                                                  <td style=
                                                  "PADDING-BOTTOM: 0px; PADDING-LEFT: 25px; PADDING-RIGHT: 0px; PADDING-TOP: 0px">
                                                  <?php if(isset($show_body_topdescription_download_calendar_event_button)&& $show_body_topdescription_download_calendar_event_button):?>

                                                    <table border="0" cellspacing="0"
                                                    cellpadding="0">
                                                      <tbody>
                                                        <tr>
                                                          <td style=
                                                          "PADDING-BOTTOM: 4px; PADDING-LEFT: 6px; PADDING-RIGHT: 8px; PADDING-TOP: 5px"
                                                          bgcolor="#C1B394"><a target=
                                                          "_blank" name="Download" style=
                                                          "FONT-FAMILY: Helvetica, Arial, sans-serif; COLOR: #524b3b; FONT-SIZE: 18px; FONT-WEIGHT: bold; TEXT-DECORATION: none"
                                                          href=
                                                          "http://beech.hbs.edu/eventCalendar/user/downloadEvents.do?downloadEvents=67808&amp;action=Download+selected+events"
                                                          xt="SPCLICK">Download Calendar
                                                          Event</a></td>
                                                        </tr>
                                                      </tbody>
                                                    </table><?php endif; ?>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table><?php endif; // \$show_body_topdescription_row ?>

                          <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                              <tr>
                                <td style=
                                "PADDING-BOTTOM: 10px; PADDING-LEFT: 15px; PADDING-RIGHT: 25px; PADDING-TOP: 0px"
                                bgcolor="<?php get_color($body_background_color); ?>"
                                align="center">
                                  <table border="0" cellspacing="0" cellpadding="0"
                                  width="500">
                                    <tbody>
                                      <tr>
                                        <td>
                                          <table border="0" cellspacing="0" cellpadding=
                                          "0" width="100%" height="100">
                                            <tbody>
                                              <tr>
                                                <td valign="middle" align="left">
                                                  <table border="0" cellspacing="0"
                                                  cellpadding="0" width="100%">
                                                    <tbody>
                                                      <tr>
                                                        <td style=
                                                        "PADDING-TOP: 10PX; PADDING-BOTTOM: 10px; LINE-HEIGHT: 25px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Helvetica, Arial, sans-serif; COLOR: &lt;?php get_color($default_text_color); ?&gt;; FONT-SIZE: 16px; PADDING-TOP: 15px">
                                                        <?php if(isset($sidebar_image) && strlen($sidebar_image) > 0) :
                                                                                                                                                                                                                                                                        if(isset($sidebar_image_link_url) && strlen($sidebar_image_link_url) > 0 ) : ?>
                                                        <a target="_blank" style=
                                                        "text-decoration: none;" href=
                                                        "<?php echo $sidebar_image_link_url; ?>">
                                                        <?php endif; ?> <img style=
                                                        "float:left; margin-right: 10px !important;"
                                                        src=
                                                        "<?php echo $sidebar_image; ?>" />
                                                         <?php if(isset($sidebar_image_link_url) && strlen($sidebar_image_link_url) > 0) :?></a>
                                                        <?php endif; ?> <?php endif; ?>
                                                        <?php email_body_html(); ?></td>
                                                      </tr>

                                                      <tr>
                                                        <td>
                                                          <?php if(isset($show_bottom_button) && $show_bottom_button ): ?>

                                                          <table border="0" cellspacing=
                                                          "0" cellpadding="0">
                                                            <tbody>
                                                              <tr>
                                                                <td style=
                                                                "PADDING-BOTTOM: 4px; PADDING-LEFT: 6px; PADDING-RIGHT: 8px; PADDING-TOP: 5px"
                                                                bgcolor="#2A3947">
                                                                <a style=
                                                                "FONT-FAMILY: Helvetica, Arial, sans-serif; COLOR: &lt;?php get_color($button_text_color); ?&gt;; FONT-SIZE: 22px; FONT-WEIGHT: bold; TEXT-DECORATION: none"
                                                                href=
                                                                "<?php echo $bottom_button_url?>">
                                                                <?php echo $bottom_button_text; ?></a></td>
                                                              </tr>
                                                            </tbody>
                                                          </table><?php endif ?>
                                                        </td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table><!--Main Content Ends-->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>

              <tr>
                <!--<td bgcolor='#000000' style='line-height: 0pt; font-size: 0pt;'><img width='1' hspace='0' height='7' align='left' name='Cont_0' alt='' src='http://www.hbs.edu/shared/images/email/pixel.gif' /></td>-->

                <td style="line-height: 0pt; font-size: 0pt;" bgcolor="#000000">
                <img name="Cont_0" src="http://www.hbs.edu/shared/images/email/pixel.gif"
                alt="" height="7" hspace="0" width="1" align="left" /></td>
              </tr>

              <tr>
                <td height="60" style="line-height: 0pt; font-size: 0pt;"><img width="1"
                hspace="0" height="60" align="left" name="Cont_0" alt="" src=
                "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>

      <tr>
        <td bgcolor="<?php get_color($footer_background_color); ?>">
          <table width="620" cellspacing="0" cellpadding="0" border="0" align="center">
            <tbody>
              <tr>
                <td style="line-height: 0pt; font-size: 0pt;"><img width="1" hspace="0"
                height="15" align="left" name="Cont_0" alt="" src=
                "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
              </tr>

              <tr>
                <td>
                  <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td width="20" valign="top" align="left" style=
                        "line-height: 0pt; font-size: 0pt;"><img width="20" hspace="0"
                        height="20" align="left" name="Cont_0" alt="" src=
                        "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>

                        <td width="48" valign="top" style=
                        "line-height: 0pt; font-size: 0pt;"><img width="48" hspace="0"
                        height="57" align="left" name="Cont_10" alt="" src=
                        "http://www.hbs.edu/shared/images/email/logo2.gif" /></td>

                        <td width="18" style="line-height: 0pt; font-size: 0pt;">
                        <img width="18" hspace="0" height="1" align="left" name="Cont_0"
                        alt="" src=
                        "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>

                        <td valign="top" align="left" style=
                        "line-height: 16px; font-size: 14px;"><a name=
                        "www_hbs_edu_healthcare" style=
                        "color: rgb(1, 0, 0); text-decoration: none;" xt="SPCLICK" href=
                        "<?php get_department_info('url')?>" target=
                        "_blank"><font size="2" face="Arial, Helvetica, sans-serif"
                        color="#010000" style=
                        "line-height: 16px; font-size: 14px;"><strong><?php get_department_info('url')?></strong></font></a></td>

                        <td width="290" valign="top" align="right" style=
                        "text-align: right; line-height: 16px; font-size: 12px;"><a xt=
                        "SPBOOKMARK" style=
                        "color: rgb(144, 0, 42); text-decoration: underline;" href=
                        "<?php get_department_info('email')?>" name=
                        "__1"><font size="1" face="Arial, Helvetica, sans-serif" color=
                        "#90002A" style="line-height: 16px; font-size: 12px;">Contact
                        Us</font></a> &nbsp; <a xt="SPBOOKMARK" style=
                        "color: rgb(144, 0, 42); text-decoration: underline;" href=
                        "http://www.hbs.edu/about/privacy.html" name="__2"><font size="1"
                        face="Arial, Helvetica, sans-serif" color="#90002A" style=
                        "line-height: 16px; font-size: 12px;">Privacy Policy</font></a>
                        &nbsp; <a target="_blank" href="#SPONECLICKOPTOUT" name="_optout"
                        style="color: rgb(144, 0, 42); text-decoration: underline;" xt=
                        "SPONECLICKOPTOUT"><font size="1" face=
                        "Arial, Helvetica, sans-serif" color="#90002A" style=
                        "line-height: 16px; font-size: 12px;">Unsubscribe</font></a></td>

                        <td width="20" valign="top" align="left" style=
                        "line-height: 0pt; font-size: 0pt;"><img width="20" hspace="0"
                        height="1" align="left" name="Cont_0" alt="" src=
                        "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>

              <tr>
                <td style="line-height: 0pt; font-size: 0pt;"><img width="50" hspace="0"
                height="75" align="left" name="Cont_0" alt="" src=
                "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
              </tr>

              <tr>
                <td>
                  <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td width="20" style="line-height: 0pt; font-size: 0pt;">
                        <img width="20" hspace="0" height="1" align="left" name="Cont_0"
                        alt="" src=
                        "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>

                        <td style="line-height: 14px; font-size: 10px;"><font size="1"
                        face="Arial, Helvetica, sans-serif" color="#8B8B8B" style=
                        "line-height: 14px; font-size: 10px;">Copyright &#169; 2013
                        President &amp; Fellows of Harvard College.</font></td>

                        <td width="20" style="line-height: 0pt; font-size: 0pt;">
                        <img width="20" hspace="0" height="1" align="left" name="Cont_0"
                        alt="" src=
                        "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>

              <tr>
                <td style="line-height: 0pt; font-size: 0pt;"><img width="1" hspace="0"
                height="30" align="left" name="Cont_0" alt="" src=
                "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</body>
</html>
p {
	line-height: 18px; 
	color: <?php echo $possible_colors[$default_text_color]; ?>; 
	font-size: 13px;
	font-family:Arial;
}

p.small {
	line-height: 10px; 
	font-size: 8px;
}

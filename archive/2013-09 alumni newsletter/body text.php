<?php section('Making a difference in health care'); ?>
    
## New health care video
<?php 
	beginTable();
	left('[![Watch this inspiring video!][]](http://www.youtube.com/watch?v=OnBBv0Bm-yY)');
	
	right( "You'll laugh, you'll cry, and you'll be inspired by how HBS alumni are
	making a difference in health care.  This video profiles stories from
	alumni---Michael Schrader, MBA '12; Nabihah Sachedina, MBA/MPP '13; Fola
	Laoye, MBA '99; and Stephane Bancel, MBA '00---and faculty Richard G.
	Hamermesh and Robert S. Huckman, who are making a difference in health care.
	Produced by the HBS Health Care Initiative.");

	endTable();
?>

## Health care sweeps Harvard innovation awards 

* **Vaxess is the [Grand Prize winner of Harvard University's 2012 President's
Challenge][] for social entrepreneurship ---** Vaxess Technologies was launched by
HBS alum, Michael Schrader (MBA '12) along with counterparts from the FAS, HLS, and HKS.

* **Deans' Health and Life Sciences Challenge runners-up from HBS ---**
Sponsored by deans from across the University and hosted at the iLab, the
challenge invited  Harvard students to develop entrepreneurial solutions to
for affordable health care. The first runner-up was
[CareSolver](https://www.caresolver.com/), a web-based application  that helps
families better coordinate the care of their elders, founded by HBS students
Shana Hoffman and Arick Morton (MBA '14) with Grant Hamm.

* **HBS [Alumni New Venture Competition][] won by Vaxess and Yaso Biotech ---** Vaxess
Technologies (Michael Schrader, MBA '12) won the Business Track for  heat-stable 
vaccines and Yaso Biotechnology, Inc. (Mary Weitzel, MBA '88) won the
Social Enterprise Track of the 2013 Alumni New Venture Contest at HBS for
reproductive  health products. Both teams took away $50K each.

* **FIELD 3 New Business Challenge won by CareWrite ---** In May,
the [CareWrite][] team won the FIELD 3 course startup competition beating out
150 teams composed of 900 students.  Team members include: Caitlin Cohen, Mike
Czapar, Luke Marker, Eva Luo, Radha Patel, and Linda Wang (all HBS '14).
CareWrite is a mobile tool to coordinate care for loved ones with cancer or a
chronic disease.

* **Three health care start-ups win Rock Accelerator awards ---** The Rock
Accelerator helps students develop a minimum viable product for a startup. The
2012--13 [winners](http://www.hbs.edu/news/releases/Pages/mvp-2013.aspx)
include three health care teams: **EngagedHealth** (Andrew Kaplan, Justin
Oppenheimer, and Daniel Stein, MD, all HBS '13); **CareSolver** (Shana Hoffman and
Arick Morton, both HBS '14, with Grant Hamm); and **myProxy** (Amy Flaster,
Azalea Kim, and Margaret Terry, all HBS '13).

## Blavatnik Fellows announced

New at HBS this year is the [Blavatnik Fellowship in Life Science
Entrepreneurship][], which is designed to foster entrepreneurship in the life
sciences and to enhance the commercialization of biomedical innovation
originating from Harvard University. Five Blavatnik Fellows have been selected
for this competitive position from a large appliant pool:

<?php beginTable();

left('![Ross Leimberg][]');
right(
	"**Ross Leimberg**  
	Ross received a BS magna cum laude in Economics from the Wharton School, a B.S. in
	Bioengineering from the University of Pennsylvania (2006), and an MBA from
	Harvard Business School (2012).");

left('![Daniel Oliver][]');
right(
	"**Daniel Oliver**  
	Daniel received a BS in Mechanical Engineering from the  California
	Institute of Technology (2007) and an MBA from  Harvard Business School
	(2013).");

left('![Steve Porter][]');
right("**Steve Porter**  
	Steve received a BA from Princeton University (2004), an MD with honors
	from Harvard Medical School (2011), and an MBA from Harvard Business
	School (2011).");

left('![John Strenkowski][]');
right("**John Strenkowski**  
	John received a BS in Business Administration from the University of
	North Carolina at Chapel Hill (2004) and an MBA from Harvard Business
	School (2009).");

left('![Ridhi Tariyal][]');
right("**Ridhi Tariyal**  
	Ridhi received a BS in Industrial Engineering from the  Georgia Institute
	of Technology (2002), an MBA from Harvard Business School (2009), and a
	Master in Biomedical Enterprise from MIT (2010).");

endTable();
?>

## New HBR-NEJM online forum launched  
The editors of *Harvard Business Review* and *The New England Journal of
Medicine* have just launched an eight week forum called [Leading Healthcare
Innovation][]. The site features daily posts, peer-reviewed reports, webinars,
video- and audiocasts, and articles from the archives of *Harvard Business
Review* and *The New England Journal of Medicine*.

## Global health competition winners

As a founding member of the Business School Alliance for Health 
Management ([BAHM](http://bahm-alliance.org/index.php)), HBS was 
chosen to host the 2013 competition *Entrepreneurship in Global 
Health* on Saturday, April 20.  On the morning of April 19, Boston 
awoke to a manhunt for the Marathon Day bombing suspects.  With 
students and faculty from across the country set to arrive that day, 
HBS faculty and staff quickly made the tough call to cancel the in-person 
competition.  Acknowledging the students' hard work and not 
wanting their efforts to be in vain, an online competition was devised 
whereby students videotaped their presentations and answered a set of 
questions.  After the judges viewed, scored and deliberated, the 
winners were:

**1st Place:** *Haas School of Business, University of California, Berkeley* for an
infectious diarrhea treatment  
**2nd Place:** *Carlson School of Management, University of Minnesota* for a
cardiovascular device that can identify blockages  
**3rd Place (tie):** *the Wharton School, University of Pennsylvania* for a vaccine
refrigeration system powered by cell phone towers;  *Owen Graduate School of
Management, Vanderbilt University* for nutritional solutions for
malnourishment


<?php section("To--dos"); ?>


## Are you willing to help a student by sharing your health care industry expertise?
<?php 
beginTable();

left(button('Mentor a student', 'https://secure.hbs.edu/poll/open/pollTakerOpen.jsp?poll=134358'));

right("HBS is seeking alumni working in all sectors of the health care
industry to mentor current MBA students. The mentor program requires a minimal
time commitment on your part --- just a few emails or phone conversations over
the course of the year. Please click the button at left to volunteer.");

endTable();
?>

## The Forum on Healthcare Innovation: continue the conversation
<?php 
beginTable();

left(button('Subscribe', 'http://healthcareinitiative.mkt1960.com/fhi/'));

right("The Forum on Healthcare Innovation (FHI), a collaboration between the
HBS Health Care Initiative and HMS, is happy to announce a new blog post from
Dr. Barbara J. McNeil. The FHI would like to hear your thoughts on *[What
process improvements have moved the needle on healthcare costs, quality or
access?][]* To receive the latest updates from the *Forum on Healthcare
Innovation*, please click button at left.");

endTable();
?>

## Upcoming health care events

<?php 
beginTable();

left("Tue, Sep 24  
	<span class='time'>12:00pm - 1:00pm</span>  
	<span class='place'>Online</span>
");
right( "[The strategy that will fix health care (webinar)][]  
	On September 24, 2013, in an interactive *Harvard Business Review* webinar,
	Harvard Business School professor Michael Porter and Press Ganey's CMO,
	Thomas H. Lee, will share insights from their recent Harvard Business
	Review article, *The Strategy That Will Fix Health Care.* *Hosted by HBR-NEJM*.");

left("Tue, Oct 1  
	<span class='time'>9:00am - 7:30pm</span>  
	<span class='place'>The Westin Boston Waterfront  
	Boston, MA</span>");
right("[CareForward 2013 (conference)][]  
	CareForward is a brand new, regional cross-industry event bringing the
	Massachusetts  health care community together on October 1, 2013 at the
	Westin Waterfront Hotel  for a day filled with keynotes, panels, and TEDx-style 
	talks with some of the biggest  names in the industry. *Hosted by
	CareForward.*");

left("Wed, Oct 30  
	<span class='time'>8:30am - 7:00pm</span>  
	<span class='place'>Computer History Museum  
	Mountain View, CA</span>");
right( "[HealthTech conference in Silicon Valley][]  
	The HBS community is invited to this year's HealthTech Conference 2013 -
	at registration, **use the 15% discount code: HBSHTC13**. CEOs and other
	HealthTech leaders will provide first-hand experience on how the latest
	technology innovations are transforming hospitals and patient care---and
	will share their wisdom on how to build game changing healthtech
	initiatives. *Hosted by the Healthtech Conference.*");

left("Thu & Fri, Nov 7--8  
	<span class='place'>The Charles Hotel  
	Cambridge, MA</span>
");
right( "[HBS Healthcare Alumni Association conference][]  
	**Early bird registration through 9/23/2013!** This year's agenda brings together individuals who not only are industry
	leaders, but are also changing the very nature of the business of healthcare.
	Confirmed speakers include Don Berwick, MD, Candidate,
	Governor of Massachusetts, and Former Administrator, Centers for Medicare &
	Medicaid Services; Nitin Nohria, PhD, Dean, Harvard Business School; Kent
	Thiry, MBA '83, Co-Chairman & CEO, Davita Healthcare Partners Inc.; and Scott
	Huennekens, MBA '91, President & CEO, Volcano Corporation.
	*Hosted by the HBSHAA*.");

left("Tue, Nov 19  
	<span class='time'>12:00pm - 1:00pm</span>  
	<span class='place'>Online</span>");
right( "[Building a business around a need (webinar)][]  
	Join Deborah Stewart, CEO of Finity, and Lauren Korettzki, Director of
	Wellness Strategy & Innovation at Harvard Pilgrim Health Care, as they
	discuss how organizations can help individuals make healthier
	decisions. *Hosted by the HBSHAA.*");

endTable();
?>

<?php section('HBS health care publications'); ?>

## HBS faculty publications
<a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=10668'>Anita Tucker</a> and Sara J Singer.  <a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=42673' target='_blank'>The Effectiveness of Management-By-Walking-Around: A Randomized Field Study.</a>  <span class='pub cite'>Harvard Business School Working Paper, No. 12-113, June 2012.</span>  <br><br>
<span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=10668'>Anita Tucker</a>, W Scott Heisler, and Laura D Janisse.</span>  <a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=45539' target='_blank'>Organizational Factors That Contribute to Operational Failures in Hospitals.</a>  <span class='pub cite'>Harvard Business School Working Paper, No. 14-023, September 2013.</span>  <br><br>
<span class='pub author'>George Loewenstein, Joelle Y Friedman, Barbara McGill, Sarah Ahmad, Suzanne Linck, Stacey Sinkula, and <a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=8943'>John Beshears</a>.</span>  <a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=45349' target='_blank'>Consumers' Misunderstanding of Health Insurance.</a>  <span class='pub cite'>Journal of Health Economics 32, no. 5 (2013): 850-862.</span>  <br><br>
<span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=10653'>Robert Huckman</a>.</span>  <a class='pub title' href='http://projects.iq.harvard.edu/forum-on-healthcare-innovation/what-is-it-about-healthcare-costs' target='_blank'>What Is It About Healthcare Costs?</a>  <span class='pub cite'>Forum on Healthcare Innovation Blog, August 2013</span>  <br><br>
<span class='pub author'><a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=658461'>Pian Shu</a>.</span>  <a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=45155' target='_blank'>Asset Accumulation and Labor Force Participation of Disability Insurance Applicants.</a>  <span class='pub cite'>Harvard Business School Working Paper, No. 14-008, July 2013.</span>  <br><br>
<span class='pub author'>Kevin J. Boudreau and <a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=240491'>Karim Lakhani</a>.</span>  <a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=45073' target='_blank'>Cumulative Innovation & Open Disclosure of Intermediate Results: Evidence from a Policy Experiment in Bioinformatics.</a>  <span class='pub cite'>Harvard Business School Working Paper, No. 14-002, July 2013.</span>  <br><br>
<span class='pub author'>James I. Merlino and <a class='pub author' href='http://drfd.hbs.edu/fit/public/facultyInfo.do?facInfo=pub&facId=6534'>Ananth Raman</a>.</span>  <a class='pub title' href='http://www.hbs.edu/faculty/Pages/item.aspx?num=44886' target='_blank'>Health Care's Service Fanatics.</a>  <span class='pub cite'>Harvard Business Review 91, no. 5 (May 2013).</span>  <br><br>

## HBS health care in the news
[The 10 Most Influential Women in Biotech][]  
Faculty Vicki Sato  
*Boston Globe*  
Sep 18, 2013


[MBAs Help to Bring Vaccines Out of the Cold][]  
Alumnus Michael Schrader, MBA '12  
*Financial Times*
Sep 8, 2013

[Management Lessons are Just What the Doctor Ordered][]  
Faculty Regina E. Herzlinger  
*Financial Times*  
Sep 8, 2013


[With Change Coming, Aetna Targets Employers][]  
Faculty Robert S. Huckman  
*New York Times*  
Sep 2, 2013


[Integrating Specialty Care: Panelists Discuss Population Health, IT Data Risks and Challenges, Cutting Costs][]  
Faculty Robert S. Huckman and Richard G. Hamermesh  
*Modern Healthcare*  
Jul 27, 2013


[The Vaccine Challenge][]  
Alumni MBA '12: Michael Schrader  
*WGBH News*  
Jul 19, 2013


[Making Lives Better][]  
Alumni MBA '93: Alison Tepper Singer  
*HBS Alumni Bulletin*  
Jul 17, 2013


[Is This the End of Health Insurers?][]  
Alumni MBA '07: Seth Blackley  
*The Washington Post*  
Jul 5, 2013


[Obamacare: Too Much, Too Fast?][]  
Faculty Bill George   
*CNBC: Squawk on the Street*  
Jul 5, 2013


[Hospital Operators and Obamacare: Prescription for Change][]  
Faculty Robert S. Kaplan  
*Economist.com*  
Jun 29, 2013


[US Accountable Care Organizations: Factory-Style Focus Calls for Change in Behaviour][]  
Faculty Regina E. Herzlinger  
*Financial Times*  
Jun 19, 2013


[China's Healthcare Choice: Innovation or Access?][]  
Faculty Arthur Daemmrich
*Forbes India*  
Jun 5, 2013


[What the U.S. Can Learn From Healthcare Delivery Overseas: Q and A With Harvard Business School's Regina E. Herzlinger][]  
Faculty Regina E. Herzlinger  
*Becker's Hospital Review*  
May 28, 2013


[Aimed at Increasing Vaccine Access, Vaxess Raises the First Part of a $3.75M Series A][]  
Alumni MBA '12: Michael Schrader  
*BostInno*  
May 23, 2013


[Will Health-Care Law Beget Entrepreneurs?][]  
Faculty Shikhar Ghosh  
*Wall Street Journal*  
May 8, 2013


[What's the Common Ingredient for Team Success in Surgery, Banking, Software, Airlines, and Basketball?][]  
Faculty Robert S. Huckman and Gary P. Pisano  
*Huffington Post*  
May 6, 2013

*[FAS]: Harvard Faculty of Arts and Sciences
*[HLS]: Harvard Law School
*[HKS]: Harvard Kennedy School
*[HBR]: Harvard Business Review
*[NEJM]: New England Journal of Medicine
*[BAHM]: Business School Alliance for Health Management 
*[HBSHAA]: Harvard Business School Healthcare Alumni Association
*[FHI]: Forum on Healthcare Innovation
*[HMS]: Harvard Medical School

[CareWrite]: http://www.hbs.edu/healthcare/pdf/Carewrite%20About%20Us.pdf
[What process improvements have moved the needle on healthcare costs, quality or access?]: http://projects.iq.harvard.edu/forum-on-healthcare-innovation/blog/improving-quality-care-united-states
[Alumni New Venture Competition]: http://www.hbs.edu/entrepreneurship/new-venture-competition/
[Blavatnik Fellowship in Life Science Entrepreneurship]: http://www.biomedicalaccelerator.org/fellowship/
[Ross Leimberg]: http://www.hbs.edu/healthcare/images/photos/blav_leimberg_112w.png
[Ridhi Tariyal]: http://www.hbs.edu/healthcare/images/photos/blav_tariyal_112.png
[Steve Porter]: http://www.hbs.edu/healthcare/images/photos/blav_porter_112.png
[Daniel Oliver]: http://www.hbs.edu/healthcare/images/photos/blav_oliver_112.png
[John Strenkowski]: http://www.hbs.edu/healthcare/images/photos/blav_strenkowski_112.png
[Leading Healthcare Innovation]: http://hbr.org/healthcare
[Grand Prize winner of Harvard University's 2012 President's Challenge]: http://harvardmagazine.com/2012/06/vaccine-delivery-system-wins-presidents-challenge-grand-prize

[The Strategy that Will Fix Health Care (Webinar)]: http://online.krm.com/iebms/coe/coe_p2_details.aspx?oc=10&cc=0011408&eventid=20601
[CareForward 2013 (Conference)]: http://carefwd2013.eventfizz.com
[HealthTech conference in Silicon Valley]: http://www.healthtechconference.com/
[HBS Healthcare Alumni Association conference]: http://www.hbshealthalumni.org/article.html?aid=714
[Building a business around a need (webinar)]: http://www.hbshealthalumni.org/article.html?aid=763
[MBAs Help to Bring Vaccines Out of the Cold]: http://www.ft.com/intl/cms/s/2/c62edaf6-f613-11e2-a55d-00144feabdc0.html

[Management Lessons are Just What the Doctor Ordered]: http://www.ft.com/intl/cms/s/2/0dc7188e-cddc-11e2-a13e-00144feab7de.html

[Watch this inspiring video!]: http://www.hbs.edu/healthcare/images/thumbnail-making-a-difference-in-healthcare.jpg
[The 10 most influential women in biotech]: http://www.bostonglobe.com/business/2013/09/14/the-most-influential-women-biotech/gba2RTZ1SuJLt5a9wYwasO/story.html
[With Change Coming, Aetna Targets Employers]: http://www.nytimes.com/2013/09/03/business/media/with-change-coming-aetna-targets-employers.html
[Integrating specialty care: Panelists discuss population health, IT data risks and challenges, cutting costs]: http://www.modernhealthcare.com/article/20130727/MAGAZINE/307279979
[The Vaccine Challenge]: http://wgbhnews.org/post/vaccine-challenge
[Making Lives Better]: http://www.alumni.hbs.edu/bulletin/alumni-news/story-singer.html
[Is this the end of health insurers?]: http://www.washingtonpost.com/blogs/wonkblog/wp/2013/07/05/want-to-kill-health-insurers-this-start-up-is-teaching-hospitals-how/?goback=%2Egde_2181454_member_255963134  
[Obamacare: Too Much, Too Fast?]: http://video.cnbc.com/gallery/?video=3000180479#eyJ2aWQiOiIzMDAwMTgwNDc5IiwiZW5jVmlkIjoiTTd4MTZLZW93YlUramVONlBKcGtjZz09IiwidlRhYiI6ImluZm8iLCJ2UGFnZSI6MSwiZ05hdiI6WyLCoExhdGVzdCBWaWRlbyJdLCJnU2VjdCI6IkFMTCIsImdQYWdlIjoiMSIsInN5bSI6IiIsInNlYXJjaCI6IiJ9
[Hospital operators and Obamacare: Prescription for change]: http://www.economist.com/news/business/21580181-americas-hospital-industry-prepares-upheaval-prescription-change
[US Accountable Care Organizations: Factory-Style Focus Calls for Change in Behaviour]: http://www.ft.com/cms/s/0/ee121f74-c954-11e2-bb56-00144feab7de.html#axzz2WrdltGF4
[China's Healthcare Choice: Innovation or Access?]: http://forbesindia.com/article/ceibs/chinas-healthcare-choice-innovation-or-access/35231/0
[What the U.S. Can Learn From Healthcare Delivery Overseas: Q and A With Harvard Business School's Regina E. Herzlinger]: http://www.beckershospitalreview.com/hospital-management-administration/what-the-us-can-learn-from-healthcare-delivery-overseas-qaa-with-harvard-business-schools-regina-e-herzlinger.html
[Four Harvard Business School Students Win Dean's Award for Service to the School and Society]: http://www.hbs.edu/news/releases/Pages/deans-award-2013.aspx  
[Aimed at Increasing Vaccine Access, Vaxess Raises the First Part of a $3.75M Series A]: http://bostinno.streetwise.co/2013/05/23/aimed-at-increasing-vaccine-access-vaxess-raises-the-first-part-of-a-3-75m-series-a/
[Will Health-Care Law Beget Entrepreneurs?]: http://online.wsj.com/article/SB10001424127887324059704578471122746420826.html
[What's the Common Ingredient for Team Success in Surgery, Banking, Software, Airlines, and Basketball?]: http://www.huffingtonpost.com/adam-grant/team-success_b_3224506.html
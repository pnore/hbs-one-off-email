#Dear HBS alumni and students,

Dr. Krishna Yeshwant (MD/MBA class of 2009) and Ben Robbins (MD/MBA class of 2016), on behalf of the many students and alumni who have or are pursuing MDs, would like to establish a network of Harvard Business School physician alumni.

You all are a subset of alumni and students mentioned by your peers as someone they'd like to help develop this network.  We'd be pleased if you could join us at an initial event to discuss the best strategy to foster connections within this impressive group of physicians-leaders.

# Event Details
|               |                                     |
|---------------|-------------------------------------|
| **Date:**     | Monday, December 16, 2013           |
| **Time:**     | 8:30 pm                             |
| **Place:**    | Meadhall, 4 Cambridge Center, 02142 |
| **Cost:**     | There is no cost to attend          |

Please use the following link to RSVP and/or confirm that you want this group to include you in future communication: [http://tinyurl.com/o3m6c98]

Sincerely,

Krishna, Ben, and the HBS Health Care Initiative

[http://tinyurl.com/o3m6c98]: http://tinyurl.com/o3m6c98
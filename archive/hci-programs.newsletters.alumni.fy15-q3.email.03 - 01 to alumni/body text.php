<?php 
// MAILING INFO
$title = "FY15-q3 alumni newsletter";
$campaignName = "hci-programs.newsletters.alumni.fy15-q3.email.03 - 01 to alumni";
$campaignUrl = "https://na15.salesforce.com/701i0000001R26WAAS";
$templateUrl = "https://engage1.silverpop.com/composeMailing7.do?action=displayLoadTemplate&mailingid=11713740&templateType=0";
$from = null;
$sig = null;
$subject = "";
$containsFirstName = false;
$relationalTableQuery = "/Shared/mp080 mentor reminder all students";



/**************  BEGIN EMAIL HEADER ******************/
?>

<!--
# <?php echo $title ?>  
**Campaign:** <?php echo $campaignName ?>       
**Campaign Link:** <?php echo $campaignUrl ?>  
**From:** <?php echo $from; ?>
**Subject:** <?php echo $subject; ?> 
-->

<?php
/**************  BEGIN EMAIL BODY ******************/
?>

<?php section("Coming Up Soon!"); ?>

## Special Guest Speaker on the HBS Campus --- U.S. Secretary of Veterans Affairs
<?php 
beginTable();
left("
At at 5:30 pm this Wednesday, March 25, Robert A. McDonald, U.S. Secretary of Veterans Affairs and former CEO of Procter & Gamble will be 
speaking in Spangler auditorium. Mr. McDonald will be speaking on the topic of \"The Leadership Journey from Corporate CEO to the President's Cabinet: Leading with Purpose\"
with an introduction by Professor Rosabeth Moss Kanter.
*This event is open to the Harvard community. Please arrive early to ensure you get a great seat.*
", "280px", "normal");
right('<img src="http://www.hbs.edu/healthcare/PublishingImages/thumbnails/robert_mcdonald_us_secretary_of_veterans_affairs_282.jpg" alt="Robert McDonald">', "292px", "normal", "middle");
endTable();
?>

## HIMSS Reception in Chicago for HBS Alumni
<?php
beginTable();
left("Sun Apr 12   
<span class='time'>6:00 pm -- 9:00 pm</span>   
<span class='place'>Chicago, IL -- [Zed451](http://www.zed451.com/)</span>");


right("<img src='http://www.hbs.edu/healthcare/PublishingImages/thumbnails/hims-convention.png' alt='HIMSS logo'/>  
	Network with Chicago alumni as well as those traveling to The Windy City to attend the annual HIMSS Conference. We've heard that quite a few people will be participating in the pre-event workshop at McCormick place or flying into Chicago Sunday afternoon and we wanted to provide the opportunity to mix and mingle. There is no cost to attend. Membership to HBSHAA is **NOT** required to attend." 
	. button("RSVP -- HIMSS", "http://www.hbshealthalumni.org/store.html?event_id=949"));

endTable();
?>  

## Alumni-Student Mixer
<?php
beginTable();

left("Thu Apr 16   
<span class='time'>6:00 pm -- 9:00 pm</span>   
<span class='place'>Harvard Square: Park Restaurant and Bar     
59 JFK St,   
Cambridge, MA 02138  
</span>");


right("You are invited to attend the annual health care mixer organized by HBS Health Care Club students, the HBS Health Care Initiative and the Healthcare Alumni Association. We had a great turn out last year and we are anticipating an lively evening this year!  There is no cost to attend, but please [RSVP](http://www.hbshealthalumni.org/store.html?show_item=1913) so we can plan accordingly. We look forward to seeing you there! There is no cost to attend." 
	. button("RSVP -- Mixer", "http://www.hbshealthalumni.org/store.html?show_item=1913"));

endTable();
?>  

<?php section("Health Care Happenings"); ?>

## Time to Reconnect through the Student-Alumni Mentor Program
Schedule some time to connect with your mentee during the upcoming spring interview season. We highly recommend reading [The Art of Giving and Receiving Advice](https://hbr.org/2015/01/the-art-of-giving-and-receiving-advice) by David A. Garvin and Joshua D. Margolis from the January 2015 issue of HBR for tips on how to help your mentee reach their potential. If you have any questions about mentoring, please email [healthcare_initiative@hbs.edu](mailto:healthcare_initiative@hbs.edu?subject=Health%20Care%20Mentor%20Program). 

<?php 
beginTable( "$relationalTableQuery", "4", "leftright");

left();
right("### Mentee %%mentor match num%%");

left("**Name:**");
right("%%student_first_name%% %%student_last_name%%");

left("**Email:**");
right("%%student_email%%");

left("**Year:**");
right("%%student_year%%");

endTable();
?>

## Sign up for the Innovating in Health Care MOOC with Regina Herzlinger 
Problems with health care quality, access, and costs bedevil all countries. [Innovating in Health Care](https://www.edx.org/course/innovating-health-care-harvardx-bus5-1#.VRBdFPnF_dg) is an online course that explores how to create successful global business innovations in health care that can better meet consumer and social needs. Upon conclusion, you will learn to evaluate opportunities and gain an understanding of the elements necessary for viable health care business models.  Through case study analysis and application of the "Innovating in Health Care Framework," this course focuses on evaluating and crafting health care startup business models that focus on the six factors that critically shape new health care ventures.  The course begins May 5, 2015 and runs for 9 weeks.

## Health Care Entrepreneur in Residence and Mentor Help Needed
Interested in being a mentor/entrepreneur in residence to help bridge health care's innovation education gap?  If so, please send your resume with your specific interest, location, and availability to Prof. Regina Herzlinger, [rherzlinger@hbs.edu](mailto:rherzlinger@hbs.edu?subject=Health%20Care%20Entrepreneur%20in%20Residence). You can read more about this call-to-action in the HBR article, [Bridging Health Care's Innovation-Education Gap](https://hbr.org/2014/11/bridging-health-cares-innovation-education-gap) and the [Health Affairs blog](http://healthaffairs.org/blog/2015/01/29/innovation-in-health-care-education-a-call-to-action/).  

Prof. Herzlinger will post it on the web site of her HarvardX MOOC, [Innovating in Health Care](https://www.edx.org/course/innovating-health-care-harvardx-bus5-1x#.VN4jXvnF98E), which attracted 20,000 participants in its first year of airing, and [GENiE](http://www.thegeniegroup.org/), the organization she formed as a Global Educators Network for Innovating Health Care Education.  In its two years of existence, GENiE has helped spur the adoption of an Innovating in Health Care degree program or course of study in dozens of schools and online education sites. 

The MOOC's thousands of participants, who are urged to create a business plan for an innovative venture, and GENiE's hundreds of member academics urgently need your help as mentors/entrepreneurs in residence to help advance this important movement.

## Blavatnik Fellowship Updates

<?php 
beginTable();

left("
### Subscribe to New Newsletter
To learn more about the Fellowship, **[view a sample of the Blavatnik Quarterly Newsletter](http://healthcareinitiative.mkt1960.com/blavatnik-newsletter)** pictured here
and [email Tracy Perry to sign up](mailto:tperry@hbs.edu?subject=Subscribe%20to%20Blavatnik%20Quarterly%20Newsletter) to receive future editions.  

### Blavatnik in the News  
[NextGen Jane Launches Pre-Sale After Three-City Road Show](http://www.nextgenjane.com/agency)  
Re: Ridhi Tariyal, Blavatnik Fellow '13  
*nextgenjane.com*  
Mar 24, 2015  

[Twenty Team Finalists Named in Deans' Challenges]  
Re: Alexandra Dickson and Meridith Unger, Blavatnik Fellows '14  
*Harvard Gazette*  
Mar 9, 2015  

[In-Q-Tel Invests in 3D Printing Innovator Voxel8]  
Re: Dan Oliver, Blavatnik Fellow '13  
*Business Wire*  
Mar 5, 2015  

[Macrolide Pharmaceuticals, Inc. Launches with $22 Million Series A Financing to Develop Novel Macrolide Antibiotics]  
Re: Steve Porter, Blavatnik Fellow '13  
*FierceBiotech*  
Mar 4, 2015  
", "450px", "normal");
right("[![blav-newsletter-preview][]](http://healthcareinitiative.mkt1960.com/blavatnik-newsletter)", "130px", "normal");

endTable();
?>




<?php section("Health Care Events"); ?>

Below is a listing of upcoming events including HBS happenings, alumni events, upcoming opportunity deadlines and conferences at which the Health Care Initiative is hosting a reception.

<?php 
beginTable();

left("Wed Mar 25   
<span class='time'>5:30 pm -- 7:00 pm</span>   
 <span class='place'>HBS, Spangler</span>");
right("[US Secretary of Veterans Affairs Robert A. McDonald]  
US Secretary of Veterans Affairs Robert A. McDonald will be speaking on The Leadership Journey from Corporate CEO to the President's Cabinet: Leading with Purpose and Values. The VA runs one of the largest health care systems in the world and is looking to Secretary McDonald for a turnaround. Secretary McDonald was formerly the CEO of Procter and Gamble. The topic will be his leadership journey and his translation of values, principles, and change tactics from private sector to the VA and its hospital network. *Hosted by Harvard Business School.*  *Open to the HBS community.*");

left("Tue Mar 31   
<span class='time'>3:15 pm -- 4:00 pm</span>   
 <span class='place'>HBS, Aldrich 7</span>");
right("[The Future of Innovation in Pharma and Biotech]  
Please join Pfizer's Jose Carlos Gutierrez-Ramos - SVP BioTherapeutics Research & Development and Belen Carrillo-Rivas D.Phil - Head R&D Innovation Projects, Biotherapeutics R&D for a talk on the future of Innovation in Pharma and Biotech: Reinventing R&D for the future of Healthcare. *Hosted by Harvard Business School.*  *Open to the HBS community.*");

left("Wed Apr 01   
<span class='time'>2:00 pm -- 5:00 pm</span>   
 <span class='place'>Rotunda, Joseph Martin Conference Center</span>");
right("[2nd Annual State of Global Health Symposium]  
Surviving and Thriving: A new era in global child health and development.  *Hosted by Harvard School of Public Health.*  *Open to the public.*");

left("Thu Apr 09   
<span class='time'>5:30 pm -- 8:30 pm</span>   
 <span class='place'>HBS, Hawes 101</span>");
right("[The Business of the Brain, Part II]  
This is part II of a very special program co-hosted with The Harvard Stem Cell Institute (HSCI), Harvard NeuroDiscovery Center and the HBSAB Healthcare Shared Interest Group (SIG). The Harvard community and its affiliates are doing tremendous work uncovering the mysteries of the brain.  This year's Business of the Brain 2.0 highlights that work, and builds on last year's program with another wonderful panel to provide unique perspectives on the future of brain research, with a specific focus on approaches to accelerate treatments and cures for brain related conditions and diseases. The panel will be moderated by HBS Professor William Sahlman. *Hosted by Harvard Business School Association of Boston.*  *Open to HBS Alumni.*");

left("Sun Apr 12   
<span class='place'>Chicago, IL</span>");
right("[Reception at HIMSS 2015]  
The HBS Health Care Initiative and the HBS Health Care Alumni Association (HAA) invite you to a reception for HBS alumni at the 2015 Health Care Information and Management Systems Society (HIMSS) conference. Check the HAA website for more information about this event as the date grows nearer. 
*Hosted by the Health Care Alumni Association and the HBS Health Care Initiative.*  *Open to the HBS community.*");

left("Tue Apr 14   
<span class='time'>12:00 pm -- 1:00 pm</span>   
 <span class='place'>Virtual</span>");
right("[Consumerism in Healthcare]  
Professor Robert Huckman, the faculty co-chair of the Harvard Business School Healthcare Initiative, will discuss what it takes for consumer health to succeed in the current healthcare environment. He will draw on his research on the topic to focus on the structural changes occurring in the healthcare system that are driving increased consumerism and will help explain the opportunities and challenges facing the rise of consumerism in health.  *Hosted by HBS Health Alumni Association.*  *Open to the HBS community.*");

left("Tue Apr 14   
<span class='time'>12:00 pm -- 1:00 pm</span>   
 <span class='place'>HSPH, Griswold Hall, Room 110</span>");
right("[FDA's Impact on Pharmaceutical Innovation: Neil Flanzraich]  
Speaker: Neil Flanzraich, Executive Chairman, Kirax Corporation and the Executive Chairman, ParinGenix. He was also appointed by Dean Martha Minow as an Expert in Residence at the Harvard Innovation Lab. *Hosted by Harvard Law School.*  *Open to the public.*");

left("Wed May 06--07   
<span class='time'>9:00 am -- 5:00 pm</span>   
 <span class='place'>Joseph B. Martin Conference Center  
 Boston, MA 02115</span>");
right("[The Lancet Commission: Global Surgery 2030](http://www.eventbrite.com/e/global-surgery-2030-a-movement-for-sustainable-resilient-health-systems-tickets-16039689142)  
The Lancet Commission on Global Surgery will be hosting its North American launch in Boston on May 6, 2015. Speakers will include Dr. Paul Farmer, co-founder of Partners in Health and a video address by the current president of the World Bank, Dr. Jim Kim. *Hosted by the Lancet Commission on Global Surgery.*  *Open to the public.*");

left("Sun Jun 14   
<span class='time'>6:30 pm -- 8:30 pm</span>   
 <span class='place'>Drexel University, Philadelphia, PA</span>");
right("[Reception at BIO International Convention]  
Join fellow HBS Healthcare Alumni for a cocktail reception to kick-off the 2015 BIO International Convention. Enjoy conversation and networking with friends old and new the evening prior to the event. Tickets are $20. *Hosted by the HBS Health Alumni Association and the HBS Health Care Initiative.*  *Open to HBS Alumni. Membership to HBSHAA is **NOT** required to attend.*");



endTable();
?>

<?php section("Recent HBS Health Care Publications"); ?>

'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=47483' target='_blank'>Access Health CT: Marketing Affordable Care</a>'<br>John Quelch and Michael  Norris<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=36929' target='_blank'>Cleveland Clinic: Growth Strategy 2014</a>'<br>Michael Porter<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=47990' target='_blank'>Compass Group: The Ascension Health Decision</a>'<br>Ryan Buell<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48451' target='_blank'>CVS Health: Promoting Drug Adherence</a>'<br>Robert S. Huckman, John A. Quelch and Leslie K. John<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48260' target='_blank'>DaVita HealthCare Partners and the Denver Public Schools: Creating Connections</a>'<br>John Kim and Christine  An<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48555' target='_blank'>Decisions about Medication Use and Cancer Screening Across Age Groups in the US</a>'<br>Mary McNaughton-Collins, Heidi Wierman, Vickie Stringfellow, Carrie A. Levin, Bethany S. Gerstein and Kathleen M. Fairfield<br><em>Patient Education and Counseling</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48352' target='_blank'>Discovery Limited</a>'<br>Michael Porter and Aldo  Sesia<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=37222' target='_blank'>Dr. Benjamin Hooks and Children's Health Forum</a>'<br>Rosabeth Kanter and Aldo  Sesia<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=46020' target='_blank'>GenapSys: Business Models for the Genome</a>'<br>Richard Hamermesh, Joseph  Fuller, and Matthew  Preble<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48380' target='_blank'>Hoag Orthopedic Institute</a>'<br>Robert Kaplan and Jonathan  Warsh<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48246' target='_blank'>Improving Melanoma Screening: MELA Sciences</a>'<br>Regina Herzlinger and Kevin  Schulman<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48486' target='_blank'>Innovation in Health Care Education: A Call to Action</a>'<br>Karen Staman, Kevin A. Schulman, Vasant Kumar and Regina E. Herzlinger<br><em>Health Affairs Blog</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=47807' target='_blank'>La Ribera Health Department</a>'<br>Regina Herzlinger, Emer  Moloney, and Daniela  Beyersdorfer<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=47986' target='_blank'>MedCath Corporation (C)</a>'<br>F. Fallon Upke, Kevin A. Schulman and Regina E. Herzlinger<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=47595' target='_blank'>No Margin, No Mission? A Field Experiment on Incentives for Public Services Deli</a>'<br>B. Kelsey Jack, Oriana Bandiera and Nava Ashraf<br><em>Journal of Public Economics</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=45917' target='_blank'>Obamacare</a>'<br>Katrina Flanagan and Matthew C. Weinzierl<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=45651' target='_blank'>PepsiCo, Profits, and Food: The Belt Tightens</a>'<br>Matthew Preble and Joseph L. Badaracco<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=41579' target='_blank'>Schön Klinik: Measuring Cost and Value</a>'<br>Robert Kaplan<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48341' target='_blank'>Social Business at Novartis: Arogya Parivar</a>'<br>Michael Porter<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=28366' target='_blank'>SonoSite: A View Inside</a>'<br>Clayton Christensen<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48544' target='_blank'>The Affordable Care Act (A): Legislative Strategy in the House of Representative</a>'<br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48545' target='_blank'>The Affordable Care Act (B): Industry Negotiations</a>'<br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48546' target='_blank'>The Affordable Care Act (C): Legislative Strategy in the Senate</a>'<br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48547' target='_blank'>The Affordable Care Act (D): Making a Decision on the Employer-Sponsored Health</a>'<br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48548' target='_blank'>The Affordable Care Act (E): The August 2009 Recess</a>'<br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48551' target='_blank'>The Affordable Care Act (G): The Final Votes</a>'<br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48552' target='_blank'>The Affordable Care Act (H): Implementation Begins</a>'<br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48553' target='_blank'>The Affordable Care Act (I): The Supreme Court</a>'<br>Michael Norris and Joseph L. Bower<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=48450' target='_blank'>Vanderbilt: Transforming a Health Care Delivery System</a>'<br>Derek A. Haas, Zachary C. Landman and Michael E. Porter<br><em>Harvard Business School Case</em><br><br>
'<a href='http://www.hbs.edu/faculty/Pages/item.aspx?num=46887' target='_blank'>Vision 2020: Takeda and the Vaccine Business</a>'<br>John Quelch and Margaret  Rodriguez<br><em>Harvard Business School Case</em><br><br>


<?php section("HBS Health Care In the News"); ?>

[Twenty Team Finalists Named in Deans' Challenges][]  
Re: Nitin Nohria  
*Harvard Gazette*  
Mar 9, 2015

[Doximity: MD/MBA Launches Healthcare Revolution From Harvard Business School][]  
Re: Nathan Gross (MBA 2011)  
*Poets & Quants*  
Mar 6, 2015

[Halting Hospital Mergers Not the Same as Boosting Competition][]  
Re: Regina Herzlinger  
*Modern Healthcare*  
Mar 6, 2015

[Market-Based Solutions to Antitrust Threats -- the Rejection of the Partners Settlement][]  
By: Regina Herzlinger  
*New England Journal of Medicine*  
Mar 4, 2015

[Milton Hospital Grows as Part of Beth Israel Deaconess Network][]  
Re: Robert Huckman  
*Boston Globe*  
Mar 3, 2015

[Harvard Health Acceleration Challenge: Meet the Finalists][]  
Re: HBS & HMS Health Acceleration Challenge  
*MedTech Boston*  
Feb 20, 2015

[Improving Patient Care][]  
Re: Michael Porter  
*HealthFeed*  
Feb 18, 2015

[An Afternoon With Michael Porter][]  
Re: Michael Porter  
*Notes*  
Feb 17, 2015

[Business Strategist as Coach: Michael Porter on Building Health Care Champions][]  
Re: Michael Porter  
*Algorithms for Innovation*  
Feb 13, 2015

[Athenahealth Plans Growth in 2015 After Successful 2014][]  
Re: Jonathan Bush (MBA 1997)  
*Boston Business Journal*  
Feb 9, 2015

[Should Harvard Business School Hit Refresh?][]  
Re: Regina Herzlinger, Nitin Nohria, Karim Lakhani, Marco Iansiti & Tom Eisenmann  
*Wall Street Journal*  
Feb 5, 2015

[Founder of Oscar Health: 'A Lot of People in This Industry Are Just Evil'][]  
Re: Josh Kushner (MBA 2011)  
*Forbes*  
Feb 2, 2015

[Innovation In Health Care Education: a Call to Action][]  
By: Regina Herzlinger  
*Health Affairs*  
Jan 29, 2015

[Boost the Right Kind of Innovation][]  
Re: Clayton Christensen  
*Modern Healthcare*  
Jan 24, 2015

[Soledad O'Brien, Paula Johnson, MD, and Amy Cuddy, Phd, Join together to Inspire Women to Lead Healthcare and Academic Medicine][]  
Re: Amy Cuddy  
*Press Release Rocket*  
Jan 20, 2015

[Merck Exec Moves to Caremore][]  
Re: Sachin H. Jain (MBA 2007)  
*Modern Healthcare*  
Jan 16, 2015

[Cms Administrator Marilyn Tavenner Is Stepping Down][]  
Re: Andrew Slavitt (MBA 1993)  
*Washington Post*  
Jan 16, 2015

[Andy Slavitt's Journey: From Contractor for Healthcare.Gov to Head of Cms][]  
Re: Andrew Slavitt (MBA 1993)  
*Washington Business Journal*  
Jan 16, 2015

[Cambridge-Based Company Aims to Bridge Doctor-Patient Gap][]  
Re: HBS & HMS Health Acceleration Challenge (Twine Health)  
*WBUR: Radio Boston*  
Jan 14, 2015

[Merck's Ex-Innovation Chief Jain Heads to Anthem Subsidiary Caremore][]  
Re: Sachin H. Jain (MBA 2007)  
*Mobihealthnews*  
Jan 14, 2015

[Bidding for Blood, the Priceline Way][]  
Re: HBS & HMS Health Acceleration Challenge (Bloodbuy)  
*American Public Media: Marketplace*  
Jan 13, 2015

[Report: Don'T Let Integration Cause Physician Micromanagement][]  
Re: Derek Haas  
*Healthcare Finance News*  
Jan 9, 2015

[Healthcare Strategy 2015 -- Back to the Basics: 12 Key Thoughts][]  
Re: Michael Porter  
*Becker's Hospital Review*  
Jan 8, 2015

[Better Path to Health Care Reform][]  
Re: Jonathan Bush (MBA 1997)  
*Rutland Herald*  
Jan 8, 2015

[Wanted: the It-Enabled Health Professional][]  
Re: Regina Herzlinger  
*LinkedIn*  
Jan 6, 2015

[Delivering Higher Value Care Means Spending More Time With Patients][]  
By: Robert Kaplan  
*Harvard Business Review*  
Dec 26, 2014

[The Future of Green China][]  
Re: William Kirby  
*Harvard Business School*  
Dec 16, 2014

[Curbing the Cost of Cancer Care][]  
Re: Robert Kaplan & Michael Porter  
*Dallas Morning News*  
Dec 15, 2014



---

### Update Your Alumni Profile
<a name='update_alumni_info' href='https://www.alumni.hbs.edu/community/Pages/edit-profile.aspx' target='_blank' style='color: rgb(229, 102, 93); background-color: rgb(255, 255, 255);'>Update your career profile in the alumni directory today</a> today so you can be found by HBS alumni, faculty and students that share your interest in health care. 

<?php 
/**************  END EMAIL BODY ******************/

/**************  BEGIN LINKS ******************/
?>


*[FAS]: Harvard Faculty of Arts and Sciences
*[HLS]: Harvard Law School
*[HKS]: Harvard Kennedy School
*[HBR]: Harvard Business Review
*[NEJM]: New England Journal of Medicine
*[BAHM]: Business School Alliance for Health Management 
*[HBSHAA]: Harvard Business School Healthcare Alumni Association
*[FHI]: Forum on Healthcare Innovation
*[HMS]: Harvard Medical School

<?php // reusable images ?>
[speech bubbles]: http://www.hbs.edu/healthcare/images/photos/speechbubbles.png
[pill image]: http://www.hbs.edu/healthcare/images/photos/pill_100.png
[steth image]: http://www.hbs.edu/healthcare/images/photos/steth_100.png
[heart image]: http://www.hbs.edu/healthcare/images/photos/heart_100.png
[up_arrow image]: http://www.hbs.edu/healthcare/images/photos/up_arrow_100.png
[dna image]: http://www.hbs.edu/healthcare/images/photos/dna_100.png
[clip image]: http://www.hbs.edu/healthcare/images/photos/clip_100.png

<?php // logos ?>
[blavatnik logo]: http://www.hbs.edu/healthcare/images/photos/blav-in-life-science-entrepreneurship.png
[blavatnik logo square]: http://www.hbs.edu/healthcare/PublishingImages/thumbnails/blav_sm_75x75.png
[exec ed]: http://www.hbshealthalumni.org/images/article_images/436.jpg

<?php // one-time images ?>


<?php // one time links for blavatnik fellowship ?>
[In-Q-Tel Invests in 3D Printing Innovator Voxel8]: http://www.businesswire.com/news/home/20150305005064/en/In-Q-Tel-Invests-3D-Printing-Innovator-Voxel8#.VQHXwvnF9fi
[Macrolide Pharmaceuticals, Inc. Launches with $22 Million Series A Financing to Develop Novel Macrolide Antibiotics]: http://www.fiercebiotech.com/press-releases/macrolide-pharmaceuticals-inc-launches-22-million-series-financing-develop
[pre-sale]: http://www.nextgenjane.com/agency?utm_source=Website+Subscribers&amp;utm_campaign=4c832633e0-March_2015&amp;utm_medium=email&amp;utm_term=0_a7226bcf7a-4c832633e0-216897193&amp;ct=t(Feb_20151_31_2015)
[Alexandra Dickson and Meridith Unger Named Finalists]: http://news.harvard.edu/gazette/story/2015/03/twenty-team-finalists-named-in-deans-challenges/?utm_source=Harvard+i-lab&amp;utm_campaign=ce759c8cd4-3%2F11%2F15&amp;utm_medium=email&amp;utm_term=0_f2b82a2fec-ce759c8cd4-300591361
[blav-newsletter-preview]: http://www.hbs.edu/healthcare/PublishingImages/thumbnails/blav-newsletter-130.png

<?php // one-time event links ?>
[ReSourcing Big Data: A Symposium & Collaboration Opportunity]: http://catalyst.harvard.edu/programs/reactor/
[US Secretary of Veterans Affairs Robert A. McDonald]: http://www.hbs.edu/healthcare/download-event.aspx?title=The+Leadership+Journey+From+Corporate+CEO+to+the+President%27s+Cabinet&dtstart=20150325T173000&dtend=20150325T190000&location=Spangler+Auditorium&description=%3cdiv+class%3d%22ExternalClass389B1DD15BC04CD6832229ACC5503205%22%3e%3cp%3eThe+HBS+Community+is+invited+to+hear+a+talk+by%c2%a0Robert+A.+McDonald%2c+U.S.+Secretary+of+Veterans+Affairs+and+former+CEO+of+Procter+%26amp%3b+Gamble%2c+with+an+introduction+by+Professor+Rosabeth+Moss+Kanter.%c2%a0He%c2%a0will+discuss%c2%a0his+journey+from+the+business+world+to+the+White+House.+The+event+will+take+place+on+March+25%2c%c2%a05%3a30+-+7%3a00+p.m.%2c+in+Spangler+Auditorium.%3c%2fp%3e%3c%2fdiv%3e&status=TENTATIVE&allday=false
[TOM Seminar: Critical Care in Hospitals: When to Introduce a Step Down Unit?]: http://www.hbs.edu/faculty/units/tom/Pages/events.aspx
[The Future of Innovation in Pharma and Biotech]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=92308&filter=month&viewDay=1&viewMonth=2&viewYear=2015
[2nd Annual State of Global Health Symposium]: http://www.hsph.harvard.edu/global-health-and-population/events/2nd-annual-state-of-global-health-symposium/
[The Business of the Brain, Part II]: http://www.hbsab.org/article.html?aid=950
[Reception at HIMSS 2015]: http://www.hbshealthalumni.org/events.html
[Consumerism in Healthcare]: http://www.hbshealthalumni.org/article.html?aid=946
[FDA's Impact on Pharmaceutical Innovation: Neil Flanzraich]: http://petrieflom.law.harvard.edu/events/details/fdas-impact-on-pharmaceutical-innovation
[Reception at BIO International Convention]: http://www.hbshealthalumni.org/article.html?aid=947


<?php // one-time links for in the news ?>
[Twenty Team Finalists Named in Deans' Challenges]: http://news.harvard.edu/gazette/story/2015/03/twenty-team-finalists-named-in-deans-challenges/?utm_source=Harvard+i-lab&utm_campaign=ce759c8cd4-3%2F11%2F15&utm_medium=email&utm_term=0_f2b82a2fec-ce759c8cd4-281914273
[Doximity: MD/MBA Launches Healthcare Revolution From Harvard Business School]: http://poetsandquants.com/2015/03/02/doximity-mdmba-launches-healthcare-revolution-from-harvard/
[Halting Hospital Mergers Not the Same as Boosting Competition]: http://www.modernhealthcare.com/article/20150306/blog/150309916
[Market-Based Solutions to Antitrust Threats -- the Rejection of the Partners Settlement]: http://www.nejm.org/doi/full/10.1056/NEJMp1501782?query=featured_home&
[Milton Hospital Grows as Part of Beth Israel Deaconess Network]: http://www.bostonglobe.com/business/2015/03/03/once-struggling-milton-hospital-grows-part-beth-israel-deaconess-network/H3nHMyKmtggHLmZCQ6J5FJ/story.html?event=event12
[Harvard Health Acceleration Challenge: Meet the Finalists]: https://medtechboston.medstro.com/harvard-health-acceleration-challenge-meet-the-finalists/
[Improving Patient Care]: http://healthcare.utah.edu/healthfeed/postings/2015/02/021815_nyt.php
[An Afternoon With Michael Porter]: http://healthsciences.utah.edu/notes/postings/2015/02/021815_porter.lee.php#.VQiJ547F98H
[Business Strategist as Coach: Michael Porter on Building Health Care Champions]: http://healthsciences.utah.edu/innovation/blog/2014/12/michael-porter-15.php
[Athenahealth Plans Growth in 2015 After Successful 2014]: http://www.bizjournals.com/boston/blog/health-care/2015/02/athenahealth-plans-growth-in-2015-after-successful.html
[Should Harvard Business School Hit Refresh?]: http://www.wsj.com/articles/should-harvard-business-school-hit-refresh-1423100330
[Founder of Oscar Health: 'A Lot of People in This Industry Are Just Evil']: http://www.forbes.com/sites/danmunro/2015/02/02/founder-of-oscar-health-a-lot-of-people-in-this-industry-are-just-evil/?utm_source=followingweekly&utm_medium=email&utm_campaign=20150202
[Innovation In Health Care Education: a Call to Action]: http://healthaffairs.org/blog/2015/01/29/innovation-in-health-care-education-a-call-to-action/
[Boost the Right Kind of Innovation]: http://www.modernhealthcare.com/article/20150124/MAGAZINE/301249984/boost-the-right-kind-of-innovation
[Soledad O'Brien, Paula Johnson, MD, and Amy Cuddy, Phd, Join together to Inspire Women to Lead Healthcare and Academic Medicine]: http://www.pressreleaserocket.net/soledad-obrien-paula-johnson-md-and-amy-cuddy-phd-join-together-to-inspire-women-to-lead-healthcare-and-academic-medicine/43900/
[Merck Exec Moves to Caremore]: http://www.modernhealthcare.com/article/20150116/NEWS/301169935
[Cms Administrator Marilyn Tavenner Is Stepping Down]: http://www.washingtonpost.com/blogs/wonkblog/wp/2015/01/16/cms-administrator-marilyn-tavenner-is-stepping-down/
[Andy Slavitt's Journey: From Contractor for Healthcare.Gov to Head of Cms]: http://www.bizjournals.com/bizjournals/washingtonbureau/2015/01/andy-slavitts-journey-from-contractor-to-cms-head.html
[Cambridge-Based Company Aims to Bridge Doctor-Patient Gap]: http://radioboston.wbur.org/2015/01/14/twine-health
[Merck's Ex-Innovation Chief Jain Heads to Anthem Subsidiary Caremore]: http://mobihealthnews.com/39704/mercks-ex-innovation-chief-jain-heads-to-anthem-subsidiary-caremore/
[Bidding for Blood, the Priceline Way]: http://www.marketplace.org/topics/business/bidding-blood
[Report: Don'T Let Integration Cause Physician Micromanagement]: http://www.healthcarefinancenews.com/news/report-dont-let-integration-cause-physician-micromanagement
[Healthcare Strategy 2015 -- Back to the Basics: 12 Key Thoughts]: http://www.beckershospitalreview.com/hospital-management-administration/healthcare-strategy-2015-back-to-the-basics-12-key-thoughts.html
[Better Path to Health Care Reform]: http://www.rutlandherald.com/article/20150108/OPINION04/701089906
[Wanted: the It-Enabled Health Professional]: https://www.linkedin.com/pulse/wanted-it-enabled-health-professional-sarah-murray
[Delivering Higher Value Care Means Spending More Time With Patients]: https://hbr.org/2014/12/delivering-higher-value-care-means-spending-more-time-with-patients
[The Future of Green China]: http://www.hbs.edu/news/articles/Pages/future-of-green-china.aspx
[Curbing the Cost of Cancer Care]: http://www.dallasnews.com/business/columnists/jim-landers/20141215-curbing-the-cost-of-cancer-care.ece


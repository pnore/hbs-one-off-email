For a long time, the science behind breakthrough therapeutics has been locked behind patents held by universities. Examining the case of Myriad Genetics, Harvard Business School's Richard Hamermesh implores the market to compete on products rather than intellectual property rights.  

**To read more, go to [Forbes.com]**. 

[Forbes.com]: http://www.forbes.com/sites/hbsworkingknowledge/2013/12/05/health-care-companies-should-compete-on-products-not-patents/ 
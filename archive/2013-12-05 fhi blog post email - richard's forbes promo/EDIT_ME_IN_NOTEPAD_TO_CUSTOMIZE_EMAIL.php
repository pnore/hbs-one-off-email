<?php
///////////////////////// google analytics: (* = required) 
///////////////////////// for fhi only //////////////////////
$use_google_analytics = false;

// an instance of this string in a url will be replaced by the google analytics code
$ga_replace = '%%GOOGLE_ANALYTICS%%';

//  Campaign Source* (referrer: google, yahoo, newsletter_0602)
$ga_source = 'blogpost-2013-12';

// Campaign Medium* (marketing medium: cpc, banner, email)
$ga_medium = 'email';

// Campaign Name* (product, promo code, or slogan)
$ga_name = "Let's open the licenses and compete on solutions";

// Campaign Term (keywords)
$ga_term = '';

// Campaign Content* (use to differentiate ads)
$ga_content = '';
///////////////////////////////////////////////////////////////

$main_link_url = "http://www.forbes.com/sites/hbsworkingknowledge/2013/12/05/health-care-companies-should-compete-on-products-not-patents/";

/**
 * Every link in the links array has a name on the left and a URL on the right. 
 * Every occurance of the phrase on the left will be made into a link to the right. 
 * Between link lines there should be a comma. For mailto links, use http://goo.gl/0LduFn
 */
$links = array(
	"What should be in the commons, what should remain private? Where can we set the line to yield the most breakthrough drugs?" => "$main_link_url#discuss"
);

/**
 * Edit the values below to customize the email. Each value has a variable
 * name that begins with a dollar sign on the left, followed by an equals sign, 
 * followed by the value in quotation marks. Every assignment statement should end with a semicolon. 
 */
 
$email_subject = "[HBS-HMS Healthcare Forum] Health Care Companies Should Compete On Products, Not Patents";

/**
 * This is the text that should appear in the email preview pane after the subject. 
 * See also: goo.gl/IJaxHy and goo.gl/hf4h6P
 */
$email_preheader = "";


/**
 * Show sidebar
 */
$sidebar_image = "";
$sidebar_image_link_url = "";

/**
 * If true, the header image will show. If false, it will not. 
 */
$show_header_image = true;

$possible_departments = array(
	"hci" => array(
		"url" => 'http://www.hbs.edu/healthcare/',
		"email" => 'mailto:healthcare_initiative@hbs.edu',
		"image" => "http://www.hbs.edu/healthcare/images/photos/health-care-initiative-logo-x000-470x76.png",
		"topmessage" => "You are receiving this update because you have expressed an interest in health care activities at HBS."
		// "topmessage" => "This email recently went out to alumni from the Health Care Initiative. You are receiving this as a staff member interested in alumni communications from the initiatives."
	), 
	"bei" => array(
		"url" => 'http://www.hbs.edu/environment/',
		"email" => 'bei@hbs.edu',
		"image" => 'http://www.hbs.edu/healthcare/images/photos/business-and-environment-logo-470x76.png',
		// You are receiving this update because you have participated in the BEI mentor program in the past.
		// You are receiving this update because you are a member of the Green Business Alumni Association, which has partnered with HBS on this alumni mentor opportunity.
		"topmessage" => "%%top_phrase%%"
	),
	'fhi' => array(
		'url' => 'http://projects.iq.harvard.edu/forum-on-healthcare-innovation',
		'image' => 'http://www.hbs.edu/healthcare/images/photos/HBS_Forum-email-banner.png'
	)
);

$department = "fhi";

$possible_authors = array(
	"huckman" => array(
		"name" => 'Robert S. Huckman',
		"url" => 'http://projects.iq.harvard.edu/forum-on-healthcare-innovation/people/robert-s-huckman?%%GOOGLE_ANALYTICS%%',
		"image" => "http://projects.iq.harvard.edu/files/styles/profile_thumbnail/public/forum-on-healthcare-innovation/files/robert_huckman.jpg",
	),
	"mcneil" => array(
		"name" => 'Barbara J. McNeil, MD',
		"url" => 'http://projects.iq.harvard.edu/forum-on-healthcare-innovation/people/barbara-j-mcneil-md?%%GOOGLE_ANALYTICS%%',
		"image" => "http://projects.iq.harvard.edu/files/styles/profile_thumbnail/public/forum-on-healthcare-innovation/files/barbara_mcneil.jpg",
	),
	"hamermesh" => array(
		"name" => 'Richard G. Hamermesh',
		"url" => 'http://projects.iq.harvard.edu/forum-on-healthcare-innovation/people/richard-g-hamermesh?%%GOOGLE_ANALYTICS%%',
		"image" => "http://projects.iq.harvard.edu/files/styles/profile_thumbnail/public/forum-on-healthcare-innovation/files/richard_hamermesh.jpg",
	),
);

$post_author_type = "a faculty co-chair";

$author = 'hamermesh';

$post_url =  $main_link_url;

$post_type = 'Forbes.com Blog Post';

$post_title = "Health Care Companies Should Compete On Products, Not Patents";

$post_summary = 'Can we agree to compete on therapeutic solutions, not on the underlying science we need to develop them? What should be in the commons, what should remain private? Where can we set the line to yield the most breakthrough drugs?';

// url will be appended
$post_tweet = "#healthcare #pharma should compete on solutions - HBS's Richard Hamermesh, @ForumHealthInno, @HarvardHBS, @harvardmed";

/**
 * This is the date phrase that will appear at the top right of the email.
 */
$header_date_phrase = "FRI DEC 6";

$possible_colors = array(
	"mint" => "#c4e4dd",
	"white" => "#ffffff",
	"black" => "#000000",
	"darkblue" => "#0D667F",
	"peach" => "#fcd4a1",
	"ltpeach" => "#eadfc8",
	"crimson" => "#a41034",
	"orange" => "#faae53",
	"dkorange" => "#ed6a47",
	"med-orange" => "#ce614a",
	"ash" => "#333",
	"medgrey" => "#808285",
	"coolgrey" => "#b6b6b6",
	"light yellow" => '#eee29f',
	'ltblue' => '#afe6f1',
	'cherry' => '#e5665d',
	'medblue' => '#368D89'
);

$heading_text_color = "darkblue";
$button_text_color = "white";
$background_color = "darkblue";
$body_background_color = "white";
$default_text_color = "black";
$highlight_text_color = "crimson";
$header_date_color = "white";
$footer_background_color = "white";
$button_text_color = "white";
$topheader_color = "white";

$template_name = "fhi-blog";

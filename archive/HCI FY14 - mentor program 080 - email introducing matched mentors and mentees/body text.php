# %%student_first_name%% meet %%alumni_first_name%%. %%alumni_first_name%% meet %%student_first_name%%.

Congratulations! You two have been matched as a mentor and mentee through the 
HBS Student-Alumni Health Care Mentor Program. As a next step, 
we recommend that Mentees get in contact with Mentors to set up an initial meeting, 
either by phone or in person, to get acquainted.

We have provided some background information below as well some materials 
on mentoring that may help you define your relationship and goals. Mentors 
may wish to read [How To Be An Effective Mentor], and students may wish to 
read [Developing A Mentoring Relationship]. 

# Mentor Information
|               |                                                          |
|---------------|----------------------------------------------------------|
| **Name:**     | %%alumni_first_name%% %%alumni_last_name%% (%%alum_id%%) |
| **Email:**    | %%alumni_email%%                                         |
| **Company:**  | %%alumni_company%%                                       |
| **Title:**    | %%alumni_title%%                                         |
| **Location:** | %%alumni_location%%                                      |


# Student Information 
|            |                                              |
|------------|----------------------------------------------|
| **Name:**  | %%student_first_name%% %%student_last_name%% |
| **Email:** | %%student_email%%                            |
| **Year:**  | %%student_year%%                             |

If you have any questions or concerns, please feel free to contact us at [healthcare_initiative@hbs.edu]. We also encourage you to provide any feedback that will be useful in improving this program.

Sincerely,  

Richard G. Hamermesh  
MBA Class of 1961 Professor of Management Practice

Robert S. Huckman  
Albert J. Weatherhead III Professor of Business Administration 

Cara Sterling  
Director, Health Care Initiative

[bei@hbs.edu]: mailto:bei@hbs.edu?Subject=Mentor%20Program
[healthcare_initiative@hbs.edu]: mailto:healthcare_initiative@hbs.edu?Subject=Mentor%20Program
[Developing A Mentoring Relationship]: http://www.hbs.edu/healthcare/pdf/Mentoringrelationship.pdf
[How To Be An Effective Mentor]: http://www.hbs.edu/healthcare/pdf/Effectivementor.pdf
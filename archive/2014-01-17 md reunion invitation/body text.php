#Dear Harvard MD/MBAs,

Dr. Krishna Yeshwant (MD/MBA 2009) and I (MD/MBA 2016), on behalf of the many students and alumni who have or are pursuing MDs, would like to establish a network of Harvard-affiliated physician alumni.  

We'd be pleased if you could join us at an event to foster connections within this impressive group of physicians-leaders.


# Event Details
|               |                                      |
|---------------|--------------------------------------|
| **Date:**     | Tuesday, February 4, 2014            |
| **Time:**     | 8:30 pm                              |
| **Place:**    | John Harvard's, 33 Dunster St, 02138 |
| **Cost:**     | There is no cost to attend           |

Please use the following link to RSVP and/or confirm that you want this group to include you in future communication: [http://tinyurl.com/lkx64z7].

Sincerely,

Ben and Krishna

[http://tinyurl.com/lkx64z7]: http://tinyurl.com/lkx64z7
<?php
///////////////////////// google analytics: (* = required) 
///////////////////////// for fhi only //////////////////////
$use_google_analytics = true;

// an instance of this string in a url will be replaced by the google analytics code
$ga_replace = '%%GOOGLE_ANALYTICS%%';

//  Campaign Source* (referrer: google, yahoo, newsletter_0602)
$ga_source = 'blogpost-2014-02';

// Campaign Medium* (marketing medium: cpc, banner, email)
$ga_medium = 'email';

// Campaign Name* (product, promo code, or slogan)
$ga_name = "How Should We Structure Healthcare Cost Sharing?";

// Campaign Term (keywords)
$ga_term = '';

// Campaign Content* (use to differentiate ads)
$ga_content = '';
///////////////////////////////////////////////////////////////

$main_link_url = "http://projects.iq.harvard.edu/forum-on-healthcare-innovation/blog/how-should-we-structure-healthcare-cost-sharing";

/**
 * Every link in the links array has a name on the left and a URL on the right. 
 * Every occurance of the phrase on the left will be made into a link to the right. 
 * Between link lines there should be a comma. For mailto links, use http://goo.gl/0LduFn
 */
$links = array(
	"What is your view on VBID?" => "$main_link_url#discuss"
);

/**
 * Edit the values below to customize the email. Each value has a variable
 * name that begins with a dollar sign on the left, followed by an equals sign, 
 * followed by the value in quotation marks. Every assignment statement should end with a semicolon. 
 */
 
$email_subject = "[HBS-HMS Healthcare Forum] How Should We Structure Healthcare Cost Sharing?";

/**
 * This is the text that should appear in the email preview pane after the subject. 
 * See also: goo.gl/IJaxHy and goo.gl/hf4h6P
 */
$email_preheader = "";


/**
 * Show sidebar
 */
$sidebar_image = "http://www.hbs.edu/healthcare/images/photos/ad_insight_NEJM_125.gif";
$sidebar_image_link_url = "http://hbr.org/healthcare";

/**
 * If true, the header image will show. If false, it will not. 
 */
$show_header_image = true;

$possible_departments = array(
	"hci" => array(
		"url" => 'http://www.hbs.edu/healthcare/',
		"email" => 'mailto:healthcare_initiative@hbs.edu',
		"image" => "http://www.hbs.edu/healthcare/images/photos/health-care-initiative-logo-x000-470x76.png",
		"topmessage" => "You are receiving this update because you have expressed an interest in healthcare activities at HBS."
		// "topmessage" => "This email recently went out to alumni from the Healthcare Initiative. You are receiving this as a staff member interested in alumni communications from the initiatives."
	), 
	"bei" => array(
		"url" => 'http://www.hbs.edu/environment/',
		"email" => 'bei@hbs.edu',
		"image" => 'http://www.hbs.edu/healthcare/images/photos/business-and-environment-logo-470x76.png',
		// You are receiving this update because you have participated in the BEI mentor program in the past.
		// You are receiving this update because you are a member of the Green Business Alumni Association, which has partnered with HBS on this alumni mentor opportunity.
		"topmessage" => "%%top_phrase%%"
	),
	'fhi' => array(
		'url' => 'http://projects.iq.harvard.edu/forum-on-healthcare-innovation',
		'image' => 'http://www.hbs.edu/healthcare/images/photos/HBS_Forum-email-banner.png'
	)
);

$department = "fhi";

$possible_authors = array(
	"huckman" => array(
		"name" => 'Robert S. Huckman',
		"url" => 'http://projects.iq.harvard.edu/forum-on-healthcare-innovation/people/robert-s-huckman?%%GOOGLE_ANALYTICS%%',
		"image" => "http://projects.iq.harvard.edu/files/styles/profile_thumbnail/public/forum-on-healthcare-innovation/files/robert_huckman.jpg",
	),
	"mcneil" => array(
		"name" => 'Barbara J. McNeil, MD',
		"url" => 'http://projects.iq.harvard.edu/forum-on-healthcare-innovation/people/barbara-j-mcneil-md?%%GOOGLE_ANALYTICS%%',
		"image" => "http://projects.iq.harvard.edu/files/styles/profile_thumbnail/public/forum-on-healthcare-innovation/files/barbara_mcneil.jpg",
	),
	"hamermesh" => array(
		"name" => 'Richard G. Hamermesh',
		"url" => 'http://projects.iq.harvard.edu/forum-on-healthcare-innovation/people/richard-g-hamermesh?%%GOOGLE_ANALYTICS%%',
		"image" => "http://projects.iq.harvard.edu/files/styles/profile_thumbnail/public/forum-on-healthcare-innovation/files/richard_hamermesh.jpg",
	),
	"newhouse" => array(
        "name" => 'Joseph P. Newhouse',
        'url' => 'http://projects.iq.harvard.edu/forum-on-healthcare-innovation/people/joseph-p-newhouse',
        'image' => 'http://projects.iq.harvard.edu/files/styles/profile_thumbnail/public/forum-on-healthcare-innovation/files/joseph_newhouse.jpg?itok=tZ5Z70zK'
    )
);

$author = 'newhouse';
$post_author_type='a Faculty Co-chair';

$post_url =  $main_link_url;

$post_type = 'Blog Post';

$post_title = "How Should We Structure Healthcare Cost Sharing?";

// $post_summary = 'The key to success is ... failure, or at least what developers do when they fail. But failed trials become a source of regret since they are neither well documented nor discussed. Can we learn faster by failing smarter?';
// needs to be less than 256 characters
$post_summary = 'How Should We Structure Healthcare Cost Sharing? Although attractive in theory, Value-Based Insurance Design (VBID) so far has had limited impact for several reasons.';

// url will be appended
// $post_tweet = "#healthcare #pharma should fail smarter - HBS's Richard Hamermesh, @ForumHealthInno, @HarvardHBS, @harvardmed";
// needs to be less than 117 characters
$post_tweet = "How should we structure #healthcare Cost Sharing? - HMS's Joe Newhouse @ForumHealthInno, @HarvardHBS, @harvardmed";

/**
 * This is the date phrase that will appear at the top right of the email.
 */
$header_date_phrase = "THU FEB 20";

$possible_colors = array(
	"mint" => "#c4e4dd",
	"white" => "#ffffff",
	"black" => "#000000",
	"darkblue" => "#0D667F",
	"peach" => "#fcd4a1",
	"ltpeach" => "#eadfc8",
	"crimson" => "#a41034",
	"orange" => "#faae53",
	"dkorange" => "#ed6a47",
	"med-orange" => "#ce614a",
	"ash" => "#333",
	"medgrey" => "#808285",
	"coolgrey" => "#b6b6b6",
	"light yellow" => '#eee29f',
	'ltblue' => '#afe6f1',
	'cherry' => '#e5665d',
	'medblue' => '#368D89'
);

$heading_text_color = "darkblue";
$button_text_color = "white";
$background_color = "darkblue";
$body_background_color = "white";
$default_text_color = "black";
$highlight_text_color = "crimson";
$header_date_color = "white";
$footer_background_color = "white";
$button_text_color = "white";
$topheader_color = "white";

$template_name = "fhi-blog";

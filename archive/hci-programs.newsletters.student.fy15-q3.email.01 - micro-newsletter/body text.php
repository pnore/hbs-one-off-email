<?php 
// MAILING INFO
$title = "";
$campaignName = "hci-programs.newsletters.student.fy15-q3.email.01 - micro-newsletter";
$campaignUrl = "https://na15.salesforce.com/701i0000001Qoj6AAC";
$templateUrl = "";
$from = null;
$sig = null;
$subject = "";
$containsFirstName = false;


/**************  BEGIN EMAIL HEADER ******************/

/*# <?php echo $title ?>  
**Campaign:** <?php echo $campaignName ?>       
**Campaign Link:** <?php echo $campaignUrl ?>  
**From:** <?php echo $from; ?>
**Subject:** <?php echo $subject; ?> 
*/

/**************  BEGIN EMAIL BODY ******************/
?>
# Upcoming Deadline: Independent Projects
EC students still looking for the perfect class for Winter term should check out our [Winter 2015 EC Health Care Opportunities] document. In particular, 
you may wish to consider choosing a project from a company listed on the [company opportunity posting page]. For more information, see the [Independent Project FAQ]. 
Registration for winter term IP [must be finalized] by [8:30 AM ET on Thursday, January 29].  

# Seeking Alumni Applicants: Blavatnik Fellowship
<?php beginTable();
left("[![blavatnik logo][]](http://www.biomedicalaccelerator.org/fellowship/)","171px", "normal", "middle");
right(
"The Blavatnik Fellowship in Life Science Entrepreneurship is seeking entrepreneurial HBS alumni who have graduated
within the last seven years to foster the commercialization of biomedical
innovation from laboratories throughout Harvard University.", "465px", "normal");
endTable(); ?>
Fellows work for one year in the vibrant
Harvard Innovation Lab on the HBS campus and receive a $95K stipend for 12
months with extra resources for due diligence, market research, and licensing
options.  To [apply to the program](http://otd.harvard.edu/accelerators/blavatnik-biomedical-accelerator/blavatnik-fellowship/), submit a completed
application to [blavatnikfellowship@hbs.edu](mailto:blavatnikfellowship@hbs.edu) by March 5, 2015.


<?php 
/**************  END EMAIL BODY ******************/

/**************  BEGIN LINKS FILLED IN BY MARKDOWN  ******************/
?>
[blavatnik logo]: http://www.hbs.edu/healthcare/images/photos/blav-in-life-science-entrepreneurship.png
[Independent Project FAQ]: http://j.mp/hbs-ip-faq
[company opportunity posting page]: http://j.mp/hbs-ips
[8:30 AM ET on Thursday, January 29]: https://inside.hbs.edu/Departments/mba/ec_planning/Pages/default.aspx
[must be finalized]: https://inside.hbs.edu/Departments/mba/ec_prereg/deadlines/Pages/default.aspx#winter
[Winter 2015 EC Health Care Opportunities]: http://www.hbs.edu/healthcare/Documents/Winter%202015%20Health%20Care%20Courses%20for%20Second%20Year%20Students.docx
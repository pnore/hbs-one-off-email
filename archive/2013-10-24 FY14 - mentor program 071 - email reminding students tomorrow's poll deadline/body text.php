## [Alumni Mentor Preferences Due] -- SUN OCT 27
Sign up to be matched with a senior alum from the health care sector that interests you.

## [Tell Us What You Think About Health Care Events] -- MON OCT 28
<span class='time'>3:30--4:30 p.m.</span>, <span class='place'>Cotting Lounge</span> - *refreshments served*  
Have you recently attended health care events? We want to hear from you.  
Email [pnore@hbs.edu] to RSVP for our health care events focus group. 

[speech bubbles]: http://www.hbs.edu/healthcare/images/photos/speechbubbles.png
[pill image]: http://www.hbs.edu/healthcare/images/photos/pill_100.png
[steth image]: http://www.hbs.edu/healthcare/images/photos/steth_100.png
[heart image]: http://www.hbs.edu/healthcare/images/photos/heart_100.png
[up_arrow image]: http://www.hbs.edu/healthcare/images/photos/up_arrow_100.png
[dna image]: http://www.hbs.edu/healthcare/images/photos/dna_100.png
[clip image]: http://www.hbs.edu/healthcare/images/photos/clip_100.png

[Deans' Health and Life Sciences Challenge Kickoff]: http://ilab.harvard.edu/calendar-event/content/2014-deans-health-life-sciences-challenge-kick
[pnore@hbs.edu]: mailto:pnore@hbs.edu?Subject=Student%20Focus%20Group
[aly@harvard.edu]: mailto:aly@harvard.edu
[Tell Us What You Think About Health Care Events]: mailto:pnore@hbs.edu?Subject=Student%20Focus%20Group
[Alumni Mentor Preferences Due]: https://secure.hbs.edu/poll/taker/pollTaker.jsp?poll=1,134531
[j.mp/2014-hbs-hc-mentor]: https://secure.hbs.edu/poll/taker/pollTaker.jsp?poll=1,134531
[Care Delivery Panel]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=73289&filter=month&viewDay=15&viewMonth=9&viewYear=2013
[RSVP]: https://secure.hbs.edu/poll/taker/pollTaker.jsp?poll=134521
[Life Sciences Panel]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=70952&filter=month&viewDay=16&viewMonth=9&viewYear=2013
[Health Care Finance Panel]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=70957&filter=month&viewDay=16&viewMonth=9&viewYear=2013
[Career Days: Devices and Diagnostics Panel]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=73290

[![![up_arrow image][]]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=73289&filter=month&viewDay=15&viewMonth=9&viewYear=2013

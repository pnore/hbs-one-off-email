<?php 

// see http://stationinthemetro.com/storage/dev/Markdown_Cheat_Sheet_v1-1.pdf
// body text is in markdown format

define('TEMPLATES', 'templates/');
include('EDIT_ME_IN_NOTEPAD_TO_CUSTOMIZE_EMAIL.php');
include(TEMPLATES . 'components-template.php');
include('libs' . DIRECTORY_SEPARATOR . 'SmartyPants' . DIRECTORY_SEPARATOR . 'smartypants.php');

// todo: make it so this doesn't override the existing colors in the config
$possible_colors = array(
	"mint" => "#c4e4dd",
	"white" => "#ffffff",
	"black" => "#000000",
	"darkblue" => "#0D667F",
	"peach" => "#fcd4a1",
	"ltpeach" => "#eadfc8",
	"crimson" => "#a41034",
	"orange" => "#faae53",
	"dkorange" => "#ed6a47",
	"med-orange" => "#ce614a",
	"ash" => "#333",
	"medgrey" => "#808285",
	"coolgrey" => "#b6b6b6",
	"light yellow" => '#eee29f',
	'ltblue' => '#afe6f1',
	'cherry' => '#e5665d',
	'medblue' => '#368D89'
);

// do not edit below this line -------------------------------------

# Install PSR-0-compatible class autoloader
spl_autoload_register(function($class){
	require 'libs' . DIRECTORY_SEPARATOR . preg_replace('{\\\\|_(?!.*\\\\)}', DIRECTORY_SEPARATOR, ltrim($class, '\\')).'.php';
});

	# Get Markdown class
	use \Michelf\MarkdownExtra;
	
	# Get CSS style inliner class
	use \TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;
	
	// TODO: add scss compiler http://leafo.net/scssphp/docs/
	
$template_name = (isset($template_name) && strlen($template_name)) ? $template_name : 'one-off' ;
$use_google_analytics = (isset($use_google_analytics) && strlen($use_google_analytics)) ? $use_google_analytics : false ;

$css_filename = TEMPLATES . $template_name . '-style.php';

$template_filename = TEMPLATES . $template_name . '-template.php';
$template = (file_exists($template_filename)!==FALSE) ? file_get_contents($template_filename) : '';
//$css = (file_exists($css_filename)!==FALSE) ? file_get_contents($css_filename) : '';
$css = '';
if(file_exists($css_filename)!==FALSE) {
	ob_start();
	include($css_filename);
	$css = ob_get_clean();	
} 

$components_css = '';
if(file_exists(TEMPLATES . 'components-style.php')!==FALSE) {
	ob_start();
	include(TEMPLATES . 'components-style.php');
	$components_css = ob_get_clean();	
} 

$body_text_filename = 'body text.php';
$body_text = '';
if(file_exists($body_text_filename)!==FALSE) {
	ob_start();
	include($body_text_filename);
	$body_text = ob_get_clean();	
} else if(file_exists($body_text_filename)!==FALSE) {
	ob_start();
	include('body text.txt');
	$body_text = ob_get_clean();
}

function load($name = null) {
	global $template_name;
	if($name == null ) $name = $template_name;
	$template_filename = TEMPLATES . $name . '-template.php';
	$template = (file_exists($template_filename)!==FALSE) ? file_get_contents($template_filename) : '';
	ob_start();
	include($template_filename);
	return ob_get_clean();
}


//$google_analytics_url_suffix = '?utm_source=blogpost-2013-08&utm_medium=email&utm_campaign=Forum+on+Healthcare+Innovation';

$ga = array();
$ga[] = isset($ga_source) && strlen($ga_source) ? 'utm_source=' . urlencode($ga_source) : '';
$ga[] = isset($ga_medium) && strlen($ga_medium) ? '&utm_medium=' . urlencode($ga_medium) : '';
$ga[] = isset($ga_campaign) && strlen($ga_campaign) ? '&utm_campaign=' . urlencode($ga_campaign) : '';
$ga[] = isset($ga_name) && strlen($ga_name) ? '&utm_name=' . urlencode($ga_name) : '';
$ga[] = isset($ga_content) && strlen($ga_content) ? '&utm_content=' . urlencode($ga_content) : '';

$google_analytics_url_suffix = implode($ga);
$google_analytics_url_suffix = $use_google_analytics ? $google_analytics_url_suffix : '';
$main_link_url = (stripos($main_link_url, $ga_replace)!==FALSE) ? str_replace($ga_replace, $google_analytics_url_suffix, $main_link_url) : $main_link_url ;

function get_color($color_name) {
	global $possible_colors;
	$color_name = ($color_name === null) ? "white" : $color_name ;
	if( isset($possible_colors[$color_name]) && strlen($possible_colors[$color_name]) > 0) {
		echo $possible_colors[$color_name];
	} // TODO: handle case where color name isn't found
}

function email_body_html() {
	global $body_text; 
	global $links;
	global $google_analytics_url_suffix;
	global $ga_replace;
	global $css;
	//$body_text = file_get_contents("body text.txt");

	# Read file and pass content through the Markdown praser
	//$body_text = file_get_contents('body text.txt');
	
	$body_text = MarkdownExtra::defaultTransform($body_text);

	
			
	// todo - only replace first instance of text http://stackoverflow.com/questions/1252693/php-str-replace-that-only-acts-on-the-first-match
	// $suffix = (isset($google_analytics_url_suffix) && strlen($google_analytics_url_suffix) > 0 ) ? $google_analytics_url_suffix : '';
	
	foreach ($links as $linktext => $url) {
		if(isset($google_analytics_url_suffix) && strlen($google_analytics_url_suffix) > 0 && (strpos($url, $ga_replace)!== false)) { 
			$url = str_replace($ga_replace, $google_analytics_url_suffix, $url);
		}
		$body_text = str_replace($linktext, get_link($url , $linktext),$body_text);
	}
	$contains_paras = stripos($body_text, '<p>') === false;
	$body_text = (! $contains_paras ) ? $body_text :  str_replace("\n", "</p>\n<p>", '<p>'.$body_text.'</p>');
	$body_text = style($body_text);
	echo $body_text;
	//echo nl2br(style($body_text));
}

function get_link($href, $text) {
	return "<a href='$href'>$text</a>";
}

function style($bodytext){
    global $google_analytics_url_suffix;
	$searchReplaceArray = array();
	
	$searchReplaceArray['<hr />'] 
		= "<table width='100%' cellspacing='0' cellpadding='0' border='0'><tr><td class='c1'><img width='580' height='40' border='0' src='http://www.hbs.edu/healthcare/images/photos/double-line.jpg' name='http://www.hbs.edu/healthcare/images/photos/double-line.jpg' alt=''/></td></tr></table>";
    $searchReplaceArray['%%GOOGLE_ANALYTICS%%']
        = "$google_analytics_url_suffix";
	return str_replace(array_keys($searchReplaceArray), array_values($searchReplaceArray), $bodytext);
}

function get_header_image($image_name = null) {
	global $possible_header_images;
	global $header_image_name;
	$image_name = ($image_name === null) ? $header_image_name : $image_name ;
	if( isset($possible_header_images[$image_name]) && strlen($possible_header_images[$image_name]) > 0) {
		echo $possible_header_images[$image_name];
	} // TODO: handle case where image name isn't found
}

// TODO: make single 'get image' function
function get_department_info($info) {
	global $possible_departments;
	global $department;
	global $google_analytics_url_suffix;
	global $ga_replace;
	$info = ($info === null) ? $department : $info ;
	if( isset($possible_departments[$department][$info]) && strlen($possible_departments[$department][$info]) > 0) {
			$url = $possible_departments[$department][$info];	
		if(($info === 'url') && isset($google_analytics_url_suffix) && (strpos($url, $ga_replace)!== false)) {
			 
			echo str_replace($ga_replace, $google_analytics_url_suffix, $url);
		} else {
			echo $possible_departments[$department][$info];
		}
	} // TODO: handle case where image name isn't found
}

function get_author_info($info) {
	global $possible_authors;
	global $author;
	global $google_analytics_url_suffix;
	global $ga_replace;
	$suffix = (isset($google_analytics_url_suffix) && strlen($google_analytics_url_suffix) > 0 ) ? $google_analytics_url_suffix : '';
	
	$info = ($info === null) ? $author : $info ;
	if( isset($possible_authors[$author][$info]) && strlen($possible_authors[$author][$info]) > 0) {
		$url = $possible_authors[$author][$info];	
		if(($info === 'url') && isset($google_analytics_url_suffix) && (strpos($url, $ga_replace)!== false)) {
			 
			echo str_replace($ga_replace, $google_analytics_url_suffix, $url);
		} else {
		echo $possible_authors[$author][$info];
			}
	} // TODO: handle case where image name isn't found
}

function get_linkedin_share() {
	global	$google_analytics_url_suffix;
	global $post_url;
	global $post_title;
	global $post_summary;
	$ptitle = urlencode($post_title);
	$psummary = urlencode($post_summary);
	$purl = urlencode($post_url . $google_analytics_url_suffix);
	$url = 'http://www.linkedin.com/shareArticle?mini=true&';
	$url .= 'url=' . $purl . '&';
	$url .= 'title=' . $ptitle . '&';
	$url .= 'summary=' . $psummary;

?>
<a 	target="_blank" xt="SPCLICK"
href="<?php echo $url; ?>"
name="linkedin_img"> <span style="font-family: Arial"><img
id="social_network_image" name="Cont_0" alt=""
style="border-bottom: medium none; border-left: medium none; padding-bottom: 3px; padding-left: 0px; padding-right: 0px; vertical-align: middle; border-top: medium none; border-right: medium none; padding-top: 0px"
src="http://projects.iq.harvard.edu/files/styles/os_files_small/public/forum-on-healthcare-innovation/files/sn_linkedin.gif" /></span></a>

&nbsp;

<a 	target="_blank" xt="SPCLICK"
style="font-size: 12px"
href="<?php echo $url; ?>"
name="linkedin_1"> <span style="font-size: 12px"><span style="font-family: Arial">LinkedIn</span></span> </a><!-- /linkedin text button --><?php
}

function get_twitter_share() {
global $google_analytics_url_suffix;
global $post_url;
global $post_title;
global $post_tweet;
$psummary = urlencode($post_tweet . ' ' . $post_url . '?' . $google_analytics_url_suffix);
$url = 'https://twitter.com/intent/tweet?';
$url .= 'text=' . $psummary;
?>
<a 	target="_blank" xt="SPCLICK"
href="<?php echo $url ?>"
name="twitter_img"> <span style="font-family: Arial"><img 	id="social_network_image"
name="Cont_1"
alt="" style="border-bottom: medium none; border-left: medium none; padding-bottom: 3px; padding-left: 0px; padding-right: 0px; vertical-align: middle; border-top: medium none; border-right: medium none; padding-top: 0px"
src="http://projects.iq.harvard.edu/files/styles/os_files_small/public/forum-on-healthcare-innovation/files/sn_twittert.gif" /></span></a><!-- /twitter image button -->
&nbsp;
<!-- twitter text button -->
<a 	target="_blank" xt="SPCLICK" style="font-size: 12px"
href="<?php echo $url ?>"
name="twitter_1"> <span style="font-size: 12px"><span style="font-family: Arial">Twitter</span></span> </a><!-- /twitter text button --><?php
}

function get_comment_bar() { global $main_link_url;
?>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
<td style="padding: 0 0 0 0;" align="center">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#b1233b" style="padding: 12px 18px 12px 18px; -webkit-border-radius:3px; border-radius:3px"><a href="<?php echo $main_link_url . '#disqus_thread'; ?>" target="_blank" style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none;"><b>Comment</b></a></td>
</tr>
</table>
</td>
<td style="text-align: center;padding-top:.7em;">

<?php get_linkedin_share(); ?>

</td><!-- /linkedin buttons -->

<!-- twitter buttons -->
<td style="text-align: center;padding-top:.7em;">
<?php get_twitter_share(); ?>
</td><!-- /twitter buttons -->
</tr>
</table><?php
}


ob_start();
include($template_filename);
$html = ob_get_clean();
$cssToInlineTransformer = new CssToInlineStyles($html, $components_css);
$html = $cssToInlineTransformer->convert();
$cssToInlineTransformer = new CssToInlineStyles($html, $css);
$html = $cssToInlineTransformer->convert();

// do smartypants transformation last
$html = SmartyPants($html);

echo $html;

<?php 
// MAILING INFO
$title = "";
$campaignName = "hci-programs.fellowships.blav.fy14-q3.email.02 - promo to ecs 2";
$campaignUrl = "https://na15.salesforce.com/701i0000001QuftAAC";
$templateUrl = "";
$from = null;
$sig = null;
$subject = "";
$containsFirstName = false;


/**************  BEGIN EMAIL HEADER ******************/

/*# <?php echo $title ?>  
**Campaign:** <?php echo $campaignName ?>       
**Campaign Link:** <?php echo $campaignUrl ?>  
**From:** <?php echo $from; ?>
**Subject:** <?php echo $subject; ?> 
*/

/**************  BEGIN EMAIL BODY ******************/
?>
# Seeking Alumni Applicants: Blavatnik Fellowship in Life Science Entrepreneurship


<?php beginTable();
left(
"The Blavatnik Fellowship in Life Science Entrepreneurship is seeking entrepreneurial HBS alumni who have graduated within the last seven years to foster the commercialization of biomedical innovation from laboratories throughout Harvard University.  Fellows work for one year in the vibrant Harvard Innovation Lab on the HBS campus and receive a $95K stipend for 12 months with extra resources for due diligence, market research, and licensing options. To [apply to the program](http://otd.harvard.edu/accelerators/blavatnik-biomedical-accelerator/blavatnik-fellowship/), submit a completed application to [blavatnikfellowship@hbs.edu](mailto:blavatnikfellowship@hbs.edu) by March 5, 2015. Read exciting updates from the inaugural cohort of Blavatnik Fellows below:", "465px", "normal");
right("[![blav app logo prod][]](http://www.biomedicalaccelerator.org/fellowship/)","171px", "normal", "middle");

endTable(); ?>

<?php beginTable();

left('![Ross Leimberg][]');
right(
	"**Ross Leimberg**, a Senior Manager at AbbVie Ventures and Early Stage Collaborations, said \"The Fellowship provided invaluable first-hand experience of the agony and
ecstasy in entrepreneurship, which has been the best training I could ask for in preparation for my current role as an investor in the startup life science
community.\"");

left('![Daniel Oliver][]');
right(
	"**Daniel Oliver**, who has launched Voxel8 with Dr. Jennifer Lewis, says \"without the Blavatnik Fellowship there is no chance I would have met Prof. Lewis and become
involved with Voxel8. It was truly a transformational year. I am so thankful for the opportunity to commercialize such ground breaking technology with the
support of the amazing advisers and Fellows the Fellowship introduced me to.\"");

left('![Steve Porter][]');
right("**Steve Porter**, MD/MBA, says \"As a physician, I encounter clinical challenges every day that demand more robust technological solutions. The Fellowship
has equipped me with the skills and network to commercialize diagnostics, therapeutics, and medical devices to better support reproductive and women's
health, positioning me for an exciting career as a clinician-entrepreneur.\"");

left('![John Strenkowski][]');
right("**John Strenkowski**, who is preparing to launch a company with Dr. Kit Parker, says \"the Blavatnik Fellowship not only helped me to develop relationships within Harvard that
were necessary to form our startup, it also provided me with a built-in network of advisors and Fellows who have provided incredibly valuable advice and
support.\"");

left('![Ridhi Tariyal][]');
right("**Ridhi Tariyal**, co-founder of [NextGen Jane](http://nextgenjane.com/) says \"the Blavatnik Fellowship allowed me to take an idea, test it on the bench, build
intellectual property around it, and then shop it around to a network of smart, subject matter experts to determine whether it would fail or fly.\"");

endTable(); ?>

<?php 
/**************  END EMAIL BODY ******************/

/**************  BEGIN LINKS FILLED IN BY MARKDOWN  ******************/
?>
[Ross Leimberg]: http://www.hbs.edu/healthcare/images/photos/blav_leimberg_112w.png
[Ridhi Tariyal]: http://www.hbs.edu/healthcare/images/photos/blav_tariyal_112.png
[Steve Porter]: http://www.hbs.edu/healthcare/images/photos/blav_porter_112.png
[Daniel Oliver]: http://www.hbs.edu/healthcare/images/photos/blav_oliver_112.png
[John Strenkowski]: http://www.hbs.edu/healthcare/images/photos/blav_strenkowski_112.png
[blavatnik logo]: http://www.hbs.edu/healthcare/images/photos/blav-in-life-science-entrepreneurship.png
[blav app logo prod]: http://www.hbs.edu/healthcare/PublishingImages/thumbnails/blav_application_170x170.png

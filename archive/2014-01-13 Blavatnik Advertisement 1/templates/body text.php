# Seeking Alumni Applicants: Blavatnik Fellowship in Life Science Entrepreneurship
[![blavatnik logo][]](http://www.biomedicalaccelerator.org/fellowship/)  

The Blavatnik Fellowship in Life Science Entrepreneurship is a one-year fellowship for HBS alumni who have earned their  MBA in the last seven years. Blavatnik Fellows are hired to foster entrepreneurship and commercialization of biomedical innovation from laboratories throughout Harvard University. Fellows work in the vibrant Harvard Innovation Lab community on the HBS campus and receive a $95,000 yearly stipend with extra resources for due diligence, market research, and licensing options. To [apply to the program](http://www.biomedicalaccelerator.org/fellowship/), submit a completed application to [blavatnikfellowship@hbs.edu] by February 14, 2014.


[blavatnik logo]: http://www.hbs.edu/healthcare/images/photos/blavatnik_logo_171.png 
[blavatnikfellowship@hbs.edu]: mailto:blavatnikfellowship@hbs.edu

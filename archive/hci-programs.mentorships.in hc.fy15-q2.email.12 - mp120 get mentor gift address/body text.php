<?php 
// MAILING INFO
$title = "";
$campaignName = "hci-programs.mentorships.in hc.fy15-q2.email.10 - mp090 unmatched mentors";
$campaignUrl = "https://na15.salesforce.com/701i0000000t0Lb";
$templateUrl = "https://engage1.silverpop.com/composeMailing7.do?action=displayLoadTemplate&mailingid=11455373&templateType=0";
$from = null;
$sig = null;
$subject = "";
$containsFirstName = false;


/**************  BEGIN EMAIL HEADER ******************/

/*# <?php echo $title ?>  
**Campaign:** <?php echo $campaignName ?>       
**Campaign Link:** <?php echo $campaignUrl ?>  
**From:** <?php echo $from; ?>
**Subject:** <?php echo $subject; ?> 
*/

/**************  BEGIN EMAIL BODY ******************/
?>
## Dear %%ALUMNI_FIRST_NAME%%,

Thank you for volunteering to participate in the HBS Healthcare Student-Alumni Mentor Program! We are thrilled with your willingness to engage with HBS students.

We are writing to let you know that students have selected their mentors.  
Because there were fewer students than alumni who opted into the program, it was not possible to match all the alumni who signed up. As a result, we were unable to match you with a student this year.

As a gesture of our gratitude, we would like to send you a gift at the shipping address below. If you have any corrections, please [contact us](mailto:healthcare_initiative@hbs.edu?subject=Update%20Alumni%20Address%20for%20Mentor%20Program) by December 5.

## Address:

%%ALUMNI_FIRST_NAME%% %%ALUMNI_LAST_NAME%%  
%%ADDRESS_LINE_1%%  
%%ADDRESS_LINE_2%%  

On behalf of the Healthcare Initiative, we want to thank you once again for volunteering. We hope that you will be a part of the HBS Healthcare Student-Alumni Mentor Program next year.

### Thank you,

Richard G. Hamermesh  
Professor of Management Practice  
Harvard Business School

Rob Huckman  
Professor of Business Administration  
Harvard Business School

Cara Sterling  
Director, Healthcare Initiative  
Harvard Business School

<?php 
/**************  END EMAIL BODY ******************/

/**************  BEGIN LINKS FILLED IN BY MARKDOWN  ******************/
?>
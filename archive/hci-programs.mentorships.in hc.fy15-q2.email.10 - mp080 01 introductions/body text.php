<?php 
// MAILING INFO
$title = "Mentor Program 080 - match 1 introductions of mentors and students";
$campaignName = "hci-programs.mentorships.in hc.fy15-q2.email.10 - mp080 01 introductions";
$campaignUrl = "https://na15.salesforce.com/701i0000000t0LM";
$templateUrl = "https://engage1.silverpop.com/composeMailing7.do?action=displayLoadTemplate&mailingid=11370046&templateType=0";
$sig = $richardRobCaraSig;
$subject = "[HBS Health Care] Mentor Program - you've been matched! ";
$containsFirstName = false;
$relationalTableQuery = "/Shared/mp080 mentor introduction 1 - 1";


/**************  BEGIN EMAIL HEADER ******************/

/*# <?php echo $title ?>  
**Campaign:** <?php echo $campaignName ?>       
**Campaign Link:** <?php echo $campaignUrl ?>  
**From:** <?php echo $from; ?>
**Subject:** <?php echo $subject; ?> 
*/

/**************  BEGIN EMAIL BODY ******************/
?>

<?php 
beginTable( "$relationalTableQuery", "1", "leftright");
row("
## %%student_first_name%% meet %%alumni_first_name%%.  
## %%alumni_first_name%% meet %%student_first_name%%.  

Congratulations! You two have been matched as a mentor and mentee through the 
HBS Student-Alumni Health Care Mentor Program. As a next step, we recommend that 
Mentees get in contact with Mentors to set up an initial meeting, either by phone 
or in person, to get acquainted. 

**As a reminder, the [Healthcare Alumni Association Conference] is next week.  
If you are planning on attending, that might be a great time to meet in person.**

We have provided some background information below as well some materials on mentoring that 
may help you define your relationship and goals. Mentors may wish to read
[How To Be An Effective Mentor], and students may wish to read
[Developing A Mentoring Relationship].");

left();
right("## Mentor Information");

left("**Name:**");
right("%%alumni_first_name%% %%alumni_last_name%%");
left("**Email:**");
right("%%alumni_email%%");
left("**Company:**");
right("%%alumni_company%%");
left("**Title:**");
right("%%alumni_title%%");
left("**Location:**");
right("%%alumni_location%%");

left();
right("## Student Information");

left("**Name:**");
right("%%student_first_name%% %%student_last_name%%");
left("**Email:**");
right("%%student_email%%");
left("**Year:**");
right("%%student_year%%");

endTable();
?>

If you have any questions or concerns, please feel free to contact us at 
[healthcare_initiative@hbs.edu](mailto:healthcare_initiative@hbs.edu?Subject=Mentor%20Program). 
We also encourage you to provide any feedback that will be useful in improving this program.

Thank you, 

<?php echo $sig; ?>

<?php 
/**************  END EMAIL BODY ******************/

/**************  BEGIN LINKS FILLED IN BY MARKDOWN  ******************/
?>
[Developing A Mentoring Relationship]: http://www.hbs.edu/healthcare/pdf/Mentoringrelationship.pdf
[How To Be An Effective Mentor]: http://www.hbs.edu/healthcare/pdf/Effectivementor.pdf
[Healthcare Alumni Association Conference]: http://www.hbshealthalumni.org/article.html?aid=714
<?php 

// NUMBERS
$number_of_applications = "74";
$number_participators = "511";
$days_left = "12";

$unsubscribe = "<a name='Unsubscribe_1' xt='SPONECLICKOPTOUT' href='#SPONECLICKOPTOUT' target='_blank'>Unsubscribe</a>";

// RE-USED PARAGRAPHS AND SENTENCES
$please_help_us_spread_the_word = "Please help us spread the word about this Challenge before our application deadline on September 29 (see attached flyer) or even better, email me your nominations of two teams or start-ups that must apply to the Challenge!  
";

$as_you_may_recall_seeking_innovations = "As you may recall, we are seeking innovations that have demonstrated value for US health care delivery and are ready to scale rapidly. We are not a startup but a “scale up” competition.
";

$finalists_will_share_150k = "Finalists will share $150,000 in prize money, present to senior health care leaders at the Forum's conference next April and have an HBS case written about them. Even if not selected, all applicants benefit from crowd-sourced interactions on our application platform. Anyone can participate.
";

$closing_thank_you_let_know_questions = "Thank you and please let me know if you have any questions.
";

// LINKS
$link_to_all_applications = "http://openforum.hbs.org/challenge/hbs-hms-health-acceleration-challenge/innovations";



// SIGNATURES
$barbaraFrom = "McNeil, Barbara J. (mcneil@hcp.med.harvard.edu)  
";
$barbaraSig = "
Barbara  

Barbara J. McNeil, M.D.  
Founding Head, Department of Health Policy and Professor of Radiology  
Faculty Co-Chair, Forum on Health Care Innovation  
Harvard Medical School  
180 Longwood Avenue  
Boston, MA 02115  
Phone: 617.432.1909  

$unsubscribe
";

$caraFrom = "Sterling, Cara (csterling@hbs.edu)  
";
$caraSig = "
Cara

Cara Sterling  
Director, Health Care Initiative  
Harvard Business School  
Phone: 617-495-6126  
Email: [csterling@hbs.edu](mailto:csterling@hbs.edu)  

$unsubscribe
";

$joeFrom = "Newhouse, Joseph Paul (newhouse@hcp.med.harvard.edu)  
";
$joeSig = "
Joe  

Joseph Newhouse, PhD  
John D. MacArthur Professor of Health Policy and Management  
Faculty Co-Chair, Forum on Health Care Innovation  
Harvard Medical School  
180 Longwood Ave.  
Boston, MA 02115  
Phone: 617.432.1325  

$unsubscribe
";


$richardFrom = "Hamermesh, Richard (rhamermesh@hbs.edu)  
";
$richardSig = "
Richard  

Richard G. Hamermesh  
MBA Class of 1961 Professor of Management Practice  
Faculty Co-Chair, Forum on Health Care Innovation  
Harvard Business School  
Rock Center forEntrepreneurship  
Boston, MA 02163  
Phone: 617.495.4179  

$unsubscribe
";

$robFrom = "Huckman, Rob (rhuckman@hbs.edu)  
";
$robSig = "
Rob

Robert S. Huckman  
Albert J. Weatherhead III Professor of Business Administration  
Faculty Co-Chair, Forum on Health Care Innovation  
Harvard Business School  
435 Morgan Hall  
Boston, MA 02163  
Phone: 617.495.6649  

$unsubscribe
";

?>


# Challenge Update (With First Name) – Barbara
**Note:** going to a. FHI Invitees, b. Top 100 Executives, c. HC Investor List, d. Healthcare Executives AAMC/ IOM (with names)  
**Campaign:** hci-programs.prizes.hac.fhi14-q1.email.09 - 4 5 7 8 hac blast fnames 2 - barbara  
**Campaign Link:** https://na15.salesforce.com/701i0000000sqSG  
**From:** <?php echo $barbaraFrom; ?>
**Subject:** Health Acceleration Challenge – <?php echo $days_left; ?> days left!  


Dear %%Preferred Name%%,

We have <?php echo $number_of_applications; ?> applications so far!

In my last email to you, we announced the launch of the [Health Acceleration Challenge].  I am happy to report that we have applications from numerous entrepreneurs as well as from venerable institutions such as the Mayo Clinic, MD Anderson and Kaiser Permanente. You can check out all the applications [here](<?php echo $link_to_all_applications;?>).

<?php echo $please_help_us_spread_the_word; ?>

<?php echo $as_you_may_recall_seeking_innovations; ?>

<?php echo $finalists_will_share_150k; ?>

To find out more and apply, go to [www.HealthAccelerationChallenge.com].

<?php echo $closing_thank_you_let_know_questions; ?>

<?php echo $barbaraSig; ?>

[Flyer Attachment]




# Challenge Update (Without First Name) – Barbara
**Campaign:** hci-programs.prizes.hac.fhi14-q1.email.09 - 08-hc execs - barbara - no fname  
**Campaign Link:** https://na15.salesforce.com/701i0000000sqS1  
**From:** <?php echo $barbaraFrom; ?>
**Subject:** Health Acceleration Challenge – <?php echo $days_left; ?> days left!  


Good afternoon,

We have <?php echo $number_of_applications; ?> applications so far!

In my last email to you, we announced the launch of the [Health Acceleration Challenge].  I am happy to report that we have applications from numerous entrepreneurs as well as from venerable institutions such as the Mayo Clinic, MD Anderson and Kaiser Permanente. You can check out all the applications [here](<?php echo $link_to_all_applications;?>).

<?php echo $please_help_us_spread_the_word; ?>

<?php echo $as_you_may_recall_seeking_innovations; ?>

<?php echo $finalists_will_share_150k; ?>

To find out more and apply, go to [www.HealthAccelerationChallenge.com].

<?php echo $closing_thank_you_let_know_questions; ?>

<?php echo $barbaraSig; ?>

[Flyer Attachment]



# Entrepreneur Update (With First Name) – 
**Campaign:** hci-programs.prizes.hac.fhi14-q1.email.09 - 06-hc enterpreneurs  
**Campaign Link:** https://na15.salesforce.com/701i0000000sqRw  
**From:** <?php echo $richardFrom; ?>
**Subject:** Health Acceleration Challenge – <?php echo $days_left; ?> days left!  


Dear %%Preferred Name%%,

<?php echo $number_of_applications; ?> of your fellow entrepreneurs have already applied to the Health Acceleration Challenge and <?php echo $number_participators; ?> are registered to participate!

In my last email, we announced the launch of the [Health Acceleration Challenge].  I am happy to report that we have applications from numerous entrepreneurs as well as from venerable institutions such as the Mayo Clinic, MD Anderson and Kaiser Permanente. You can check out all the applications [here](<?php echo $link_to_all_applications;?>). We also have received extensive press including [Forbes] and the [Boston Magazine].

Our application deadline is September 29 and I encourage you to find out more information and apply at [www.HealthAccelerationChallenge.com].

As you may recall, we are seeking innovations that have demonstrated value for US health care delivery and are ready to scale rapidly. We are not a startup but a "scale up" competition.

Finalists will share $150,000 in prize money, present to senior health care leaders at the Forum's conference next April and have an HBS case written about them. Even if not selected, all applicants benefit from crowd-sourced interactions on our application platform. Anyone can participate.

<?php echo $closing_thank_you_let_know_questions; ?>

<?php echo $richardSig; ?>

[Flyer Attachment]



# Judges – 
**Campaign:** hci-programs.prizes.hac.fhi14-q1.email.09 - 13-judges  
**Campaign Link:** https://na15.salesforce.com/701i0000000sqSB  
**From:** <?php echo $caraFrom; ?>  
**Subject:** Health Acceleration Challenge Judges  


Dear %%Preferred Name%%,

Thank you for agreeing to be a judge for the Health Acceleration Challenge!

We have <?php echo $number_of_applications; ?> applications so far from numerous entrepreneurs as well as from venerable institutions such as the Mayo Clinic, MD Anderson and Kaiser Permanente. You can check out all the applications [here](<?php echo $link_to_all_applications; ?>).

Please help us spread the word about this Challenge before our application deadline on September 29 (see attached flyer) or even better, email me your nominations of two teams or start-ups that must apply to the Challenge!

As you may recall, we are seeking innovations that have demonstrated value for US health care delivery and are ready to scale rapidly. We are not a startup but a "scale up" competition.

Finalists will share $150,000 in prize money, present to senior health care leaders at the Forum's conference next April and have an HBS case written about them. Even if not selected, all applicants benefit from crowd-sourced interactions on our application platform. Anyone can participate.

To find out more and apply, go to [www.HealthAccelerationChallenge.com].

Thank you and I will be in touch shortly with more information about the judging process.

<?php echo $caraSig; ?>

[Flyer Attachment]



# Press - 
**Campaign:** hci-programs.prizes.hac.fhi14-q1.email.09 - 10-press  
**Campaign Link:** https://na15.salesforce.com/701i0000000sqS6  
**From:** <?php echo $robFrom; ?>
**Subject:** Health Acceleration Challenge  


Dear %%Preferred Name%%,

"Innovation" has become a buzzword in health care with venture capitalists striving to identify the next big idea that will meaningfully impact health care delivery. 

Yet in a world full of innovations we already struggle to absorb, perhaps what we really need is not another radical breakthrough but better ways of distributing smaller ideas more effectively.

Every day across our nation our physicians, nurses and health care executives pioneer new ideas and tools to make health care delivery faster, safer and less costly and we need to get better at dissemination incremental ideas for process improvement that are proven, transferrable, adaptable, and simple to initiate.

That is why Harvard Business School and Harvard Medical School launched the [Health Acceleration Challenge].  I am happy to report that we already have <?php echo $number_of_applications; ?> applications from numerous entrepreneurs as well as from venerable institutions such as the Mayo Clinic, MD Anderson and Kaiser Permanente. You can check out all the applications [here](<?php echo $link_to_all_applications;?>).

As you may recall, we are seeking innovations that have demonstrated value for US health care delivery and are ready to scale rapidly. Finalists will share $150,000 in prize money, present to senior health care leaders at our conference next April and have an HBS case written about them. 

To find out more, visit [www.HealthAccelerationChallenge.com].

<?php echo $closing_thank_you_let_know_questions; ?>

Best regards, 
<?php echo $robSig; ?>  

[Flyer Attachment]


<?php // LINKS FILLED IN BY MARKDOWN ?>
[Boston Magazine]: http://www.bostonmagazine.com/health/blog/2014/08/25/harvard-health-acceleration-challenge/  
[Forbes]: http://www.forbes.com/sites/hbsworkingknowledge/2014/08/18/harvard-wants-your-proven-ideas-to-improve-health-care/  
[Health Acceleration Challenge]: http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%  
[www.HealthAccelerationChallenge.com]: http://www.healthaccelerationchallenge.com/?%%SP_CRM_BLOCK%%  
Dear alumni, students, staff and faculty interested in health care,

The HBS Health Care Initiative has created a new private LinkedIn group for HBS faculty, alumni, students and staff interested in health care. We invite you to log into LinkedIn and join the group at [http://www.example.com/tbd]. This community will feature:

*   faculty research, news, profiles
*   alumni and student stories and experiences
*   opportunities, Q&A sessions, opinion polls
*   community-submitted health care news and discussion

We hope this new channel will make it easier for you to connect and discuss health care issues with each other.

Sincerely,  

Rob Huckman and Richard Hamermesh  
Faculty Co-Chairs, HBS Health Care Initiative

[http://www.example.com/tbd]: http://www.example.com/tbd
*[FAS]: Harvard Faculty of Arts and Sciences
*[HLS]: Harvard Law School
*[HKS]: Harvard Kennedy School
*[HBR]: Harvard Business Review
*[NEJM]: New England Journal of Medicine
*[BAHM]: Business School Alliance for Health Management 
*[HBSHAA]: Harvard Business School Healthcare Alumni Association
*[FHI]: Forum on Healthcare Innovation
*[HMS]: Harvard Medical School

[speech bubbles]: http://www.hbs.edu/healthcare/images/photos/speechbubbles.png
[pill image]: http://www.hbs.edu/healthcare/images/photos/pill_100.png
[steth image]: http://www.hbs.edu/healthcare/images/photos/steth_100.png
[heart image]: http://www.hbs.edu/healthcare/images/photos/heart_100.png
[up_arrow image]: http://www.hbs.edu/healthcare/images/photos/up_arrow_100.png
[dna image]: http://www.hbs.edu/healthcare/images/photos/dna_100.png
[clip image]: http://www.hbs.edu/healthcare/images/photos/clip_100.png

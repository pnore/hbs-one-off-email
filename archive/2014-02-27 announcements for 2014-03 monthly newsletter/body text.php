<?php
beginTable();
left('[![escape fire screening logo][]](http://campusgroups.us4.list-manage.com/track/click?u=f13c93cf7f3135092ad648239&id=28ad5553f2&e=1a7eeacfea)');
right("Come watch <i>Escape Fire: The Fight to Rescue American Healthcare</i>, an investigative documentary on the American health care system. Award-winning filmmakers Matthew Heineman and Susan Froemke follow dramatic human stories as well as leaders fighting to transform health care at the highest levels of medicine, industry, government, and even the US military. A discussion with Matthew Heineman will follow the screening.");

endTable();
?>  

[escape fire screening logo]: http://www.hbs.edu/healthcare/images/photos/escape_fire_feature_img_sm_100w.png

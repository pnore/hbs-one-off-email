![blavatnik][]

[blavatnik]: http://www.hbs.edu/healthcare/images/photos/blavatnik_logo.gif

The Blavatnik Key Advisory Board invite you to the Blavatnik Fellows Showcase featuring a presentation by each of the Blavatnik Fellows on the progress of their year-long work.  The Fellows will give background on their projects, highlight the milestones achieved to date, and look for feedback from the life science community.  

Date:  
Time:  
Location:  

RSVP to Tracy Saxton at tsaxton@hbs.edu  

Appetizers and refreshments will be served   

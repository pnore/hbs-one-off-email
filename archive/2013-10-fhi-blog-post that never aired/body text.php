Like many people interested in the tangled connections between healthcare progress and 
intellectual property rights, I avidly followed the [Myriad Genetics case], decided by the 
U.S. Supreme June 13, 2013. In sum, Myriad sought "the exclusive right to isolate an individual's BRCA1 
and BRCA2 genes," but the court, wisely I believe, made thoughtful distinctions about what is 
and is not patentable, deciding that, "A naturally occurring DNA segment is a product of nature 
and not patent eligible because it has been isolated... ."

Of course, had the University of Utah not given Myriad an exclusive license to a discovery that the 
university had made in its own labs, there may have been no need for a conflict whatsoever. But 
among other things, the conflict resonates deeply with a call to action made by [Dr. Eric S. Lander], 
President and Founding Director of the Eli and Edythe L. Broad Institute of Harvard and MIT, at 
last November's [*Healing Ourselves* Health Care Forum event]. There, Dr. Lander made a potent case for 
creating a multi-corporate consortium that would openly share validated targets. Instead of 
stalling progress by withholding information on targets, he argued, they should open target data 
and compete on solutions, such as compounds and molecules addressing the targets.

I agree. If we're going to get the breakthrough products we want at a development cost we can 
afford, we have to figure out the best place to set the line between public and private interests, 
between the "intellectual commons" that benefits us all, and the intellectual property protection 
that encourages innovation and investment. What should be opened to the commons? What 
should be held by private interests?

Given the stakes, these are more than academic questions. But I believe that the academy can assert 
leadership on these issues. Let's face it: The majority of significant discoveries come from 
university labs and hospitals, and their ownership of intellectual property gives them enormous 
influence over the distribution of knowledge. To date, they have been far too concerned with the 
size of their royalties, at the expense of public utility.  

The fate of discoveries we all rely upon, and the intellectual property that results from them, rests 
in the hands of university technology licensing offices. Here's my proposal: I ask that the 
[Association of University Technology Managers] establish a set of standards, balancing public 
interests and private incentives, that would encourage the development of new chemical entities 
for reducing human suffering. As Dr. Lander has suggested, anything having to do with genes, 
molecular pathways, and targets should be made openly available, or at least not subject to 
exclusive licensing. Instead, we should encourage competition around the development of 
molecules that interfere with or enhance activity in these pathways, rewarding the creators with 
the usual private incentives.

Can we agree to compete on therapeutic solutions, not on the underlying science we need to 
develop them? I'd like to hear your thoughts on the matter: What should be in the commons, 
and what should remain private? Where can we set the line to yield the most breakthrough drugs? 

[Myriad Genetics case]: http://www.autm.net/AM/Template.cfm?Section=Public_Policy&Template=/CM/ContentDisplay.cfm&ContentID=11126
[Dr. Eric S. Lander]: http://projects.iq.harvard.edu/forum-on-healthcare-innovation/people/eric-s-lander%%GOOGLE_ANALYTICS%%
[*Healing Ourselves* Health Care Forum event]: http://projects.iq.harvard.edu/forum-on-healthcare-innovation/book/key-insights%%GOOGLE_ANALYTICS%%
[Association of University Technology Managers]: http://www.autm.net/Home.htm

<?php
beginTable();

left("[![fleece][]](https://secure.hbs.edu/poll/open/pollTakerOpen.jsp?poll=135058)");
right("[Have a Health Care Mentor? Win an HBS Health Care Fleece or T-Shirt!]    
**Tue Apr 15** <span class='time'>11:59 p.m.,</span> <span class='place'>Online</span>  
[Give the HCI feedback](https://secure.hbs.edu/poll/open/pollTakerOpen.jsp?poll=135058) on your mentor experience by 4/15 and you could be selected to win an HBS Health Care Initiative fleece or t-shirt!");

left("[![regi][]](http://beech.hbs.edu/eventCalendar/user/downloadEvents.do?downloadEvents=80368&action=Download+selected+events)");
right("[2014-15 EC Health Care Course Presentation Session]  
**Tue Apr 15** <span class='time'>4:30 p.m.  -- 5:30 p.m.,</span> <span class='place'>Aldrich 211</span>  
Love health care? Interested in finding out all there is to know about the EC courses offered next year? The Health Care Initiative will be hosting faculty from each health care course that is being offered next year to talk about their course and what to expect for next year!");

left("[![wildside][]](https://docs.google.com/forms/d/14EwhKbk7nVxn5wkw3KIdTCmh3IUPcaBz5QiFQaigYRA/viewform)");
right("[Health Care EC Send-off with Wildside]  
**Wed Apr 16** <span class='time'>8:00 p.m.  -- 10:00 p.m.,</span> <span class='place'>Lizard Lounge, Cambridge, MA</span>  
This year in honor of our departing 2014 Health Care students the HCI will be hosting WildSide at the Lizard Lounge from 8-10pm on April 16. [RSVP is required](https://docs.google.com/forms/d/14EwhKbk7nVxn5wkw3KIdTCmh3IUPcaBz5QiFQaigYRA/viewform). More [INFO HERE](http://www.hbs.edu/healthcare/images/photos/EC-Send-Off-16.png). *Hosted by the Health Care Initiative*. Open to MBA Students. 
");

left("[![first printer asw][]](http://thefirstprinter.com/)");
right("[Health Care Student Mixer at First Printer]  
**Thu Apr 17** <span class='time'>9:00 p.m.  -- 11:00 p.m,</span>, <span class='place'>Boston</span>  
The Health Care Club is sponsoring a mixer at First Printer (15 Dunster Street, Cambridge) from 9:00 p.m. to 11:00 p.m. on Thursday, April 17th. You are invited to come meet current MBA students interested in health care and enjoy drinks and light appetizers.  
");



endTable();
?>  
[fleece]: http://www.hbs.edu/healthcare/images/photos/fleece.PNG
[regi]: http://www.hbs.edu/healthcare/images/photos/regi.png
[wildside]: http://www.hbs.edu/healthcare/images/photos/wildside.png
[first printer asw]: http://www.hbs.edu/healthcare/images/photos/first-printer.png

[Have a Health Care Mentor? Win an HBS Health Care Fleece or T-Shirt!]: https://secure.hbs.edu/poll/open/pollTakerOpen.jsp?poll=135058
[2014-15 EC Health Care Course Presentation Session]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=80368
[Health Care EC Send-off with Wildside]: https://docs.google.com/forms/d/14EwhKbk7nVxn5wkw3KIdTCmh3IUPcaBz5QiFQaigYRA/viewform
[Health Care Student Mixer at First Printer]: http://thefirstprinter.com/

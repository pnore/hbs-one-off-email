<?php error_reporting(E_ALL);
$background_color = "darkblue";
$body_background_color = "white";
$default_text_color = "black";
$highlight_text_color = "crimson";
$header_date_color = "white";
$footer_background_color = "white";
$button_text_color = "white";
$topheader_color = "white";
$use_excel = false;
$header_date_phrase = "";
$show_header_image = false;
$department = "ihc";

ini_set("display_errors", 1);

define('GLOBAL_SCRIPTS', getcwd() . "/libs/");
include (GLOBAL_SCRIPTS . "ExcelReader" . DIRECTORY_SEPARATOR . "excel_reader2.php");
define('INPUT_LOC', getcwd() . "/");
define('OUTPUT_LOC', getcwd() . "/body text.txt");
define('ISSUE_DATA', INPUT_LOC . "alum_newsletter.xls");
if ($use_excel && !file_exists(ISSUE_DATA)) {
        echo "<h1>" . ISSUE_DATA . " does not exist!<br/>Make sure that it has world-readable file permissions.</h1>";
        die();
}
if ($use_excel) {
        // ### FUNCTIONS ###
        function debug($something) {
                $str = "\n<pre>\n";
                $str .= str_replace('<', '&lt;', str_replace('>', '&gt;', print_r($something, true)));
                $str .= "\n</pre>\n";
                return $str;
        }

        function pushAllColumnsOfRow(&$dest_arr, &$excel, $row) {
                $temp = array();
                for ($i = 1; $i <= $excel -> colcount(); $i++) {
                        $temp[$excel -> val(1, $i)] = $excel -> val($row, $i);
                }
                $dest_arr[] = $temp;
        }

        function tablify($arr) {
                $str = "<table><tr>";
                $keys = array_keys($arr[0]);
                for ($i = 0; $i < count($keys); $i++) {
                        $str .= "<th style='border: 1px solid black;font-weight: bold;'>" . $keys[$i] . "</th>";
                }
                $str .= "</tr>";
                for ($i = 0; $i < count($arr); $i++) {
                        $str .= "<tr>";
                        for ($j = 0; $j < count($keys); $j++) {
                                $str .= "<td style='border: 1px solid black;'>" . $arr[$i][$keys[$j]] . "</td>";
                        }
                        $str .= "</tr>";
                }
                return $str . "</table>";
        }

        function sanitize($str)//escapes,strips and trims all members of array
        {
                $badwordchars = array("\xe2\x80\x98", "\xe2\x80\x99", "\xe2\x80\x9c", "\xe2\x80\x9d", "\xe2\x80\x93", "\xe2\x80\x94", "\xe2\x80\xa6");
                $fixedwordchars = array("'", "'", '"', '"', '-', '--', '...');
                return str_replace($badwordchars, $fixedwordchars, $str);
        }

        function writeFile($str, $myFile = OUTPUT_LOC) {
                //echo( "trying to open " . $myFile );
                $fh = fopen($myFile, 'w') or die("can't open file");
                $stringData = $str;
                fwrite($fh, $stringData);
                fclose($fh);

        }

        function openBrowser($myFile = OUTPUT_LOC) {
                // open output loc in web browser (windows only
                shell_exec("start \"\" \"" . $myFile . "\"");
        }

        // ############### PREVIEW CODE #######################
        $preview = "";
        // ### ISSUE DATA ###
        $issue = new Spreadsheet_Excel_Reader(ISSUE_DATA, false);
        $numRows = intval($issue -> rowcount());
        // we could add some indirection here if necessary to look for the first column with a certain label
        $year = $issue -> val(2, 1);
        $month = $issue -> val(2, 2);
        $date = mktime(0, 0, 0, $month, 1, $year);
        $issueDatePhrase = strtoupper(date("M Y", $date));
        $sectNumNames = array(0 => "zero", 1 => "one", 2 => "two", 3 => "three", 4 => "four", 5 => "five", 6 => "six", 7 => "seven", 8 => "eight");
        $sections = array();
        // ### ISSUE SPECIFIC FUNCTIONS ###
        //$level stands for h level, h1, h2, etc
        //$content is the content for the section
        //$link is the link (if applicable) for the section
        //call this function and then call makeTOC
        function addSect($level, $content, $link) {
                global $sectNumNames, $issue, $sections;
                $out = "";
                $hasLink = isset($link) && strlen($link) > 0;
                if ($level == 1) {
                        $out .= sectionBorder();
                        $sections[] = $content;
                        $out .= "<h1><a id='";
                        $out .= $sectNumNames[count($sections)];
                        $out .= "'></a>";
                        $out .= $content;
                        $out .= "</h1>";
                        return $out;
                }
                if ($level == 2) {
                        $out = '<h2>';
                        if ($hasLink) {
                                //<span style="color: rgb(144, 0, 42);" >
                                $out .= '<a href="';
                                $out .= $link;
                                $out .= '">' . $content . '</a></h2>';
                                return $out;
                        }
                        $out .= $content;
                        $out .= '</h2>';
                        return $out;
                }
                if ($level == 3) {
                        //$out = "<br />";
                        $out .= '<p>';
                        $out .= $content;
                        $out .= '</p>';
                        return $out;
                }
        }

        function mission_statement() {
                return "
        <tr>
                                       <td valign='top' align='left' style='line-height: 0pt; font-size: 0pt;'><img width='20' hspace='0' height='20' align='left' src='http://www.hbs.edu/shared/images/email/pixel.gif' alt='' name='Cont_0' /></td>
                                       <td valign='top' style='line-height: 30px; font-family: Arial,Helvetica,sans-serif; color: rgb(189, 189, 189); font-size: 16px;'>The Healthcare Initiative at Harvard Business School is a multidisciplinary program dedicated to innovative thinking in the healthcare industry. The Initiative brings together the extensive research, thought leadership, and interest in the business and management of healthcare that exist at HBS.</td>
                                       <td valign='top' align='left' style='line-height: 0pt; font-size: 0pt;'><img width='20' hspace='0' height='20' align='left' src='http://www.hbs.edu/shared/images/email/pixel.gif' alt='' name='Cont_0' /></td>
                                    </tr>
        ";
        }/*

         function tableOfContents() {
         global $sectNumNames, $issue, $sections;

         //<a style='color: rgb(144, 0, 42); text-decoration: underline;' href='#one' name='_one' xt='SPBOOKMARK'><font size='2' face='Arial, Helvetica, sans-serif' color='#90002a' style='font-size: 14px; line-height: 20px;'>Kudos and Congrats</font></a><br />

         $out = "

         <tr>
         <td valign='top' bgcolor='#ffffff' align='left' style='line-height: 0pt; font-size: 0pt;' colspan='3'><img width='80' hspace='0' height='24' align='left' src='http://www.hbs.edu/shared/images/email/pixel.gif' alt='' name='Cont_0' /></td>
         </tr>
         <tr>
         <td width='20' bgcolor='#ffffff' style='line-height: 0pt; font-size: 0pt;'><img width='20' hspace='0' height='1' align='left' src='http://www.hbs.edu/shared/images/email/pixel.gif' alt='' name='Cont_0' /></td>
         <td width='580' valign='top' height='25' bgcolor='#ffffff'><img border='0' alt='' src='http://www.hbs.edu/shared/images/email/InThisIssue_onWhite.gif' name='Cont_14' /></td>
         <td width='20' bgcolor='#ffffff' style='line-height: 0pt; font-size: 0pt;'><img width='20' hspace='0' height='10' align='left' src='http://www.hbs.edu/shared/images/email/pixel.gif' alt='' name='Cont_0' /></td>
         </tr>
         <tr>
         <td width='20' bgcolor='#ffffff' style='line-height: 0pt; font-size: 0pt;'><img width='20' hspace='0' height='1' align='left' src='http://www.hbs.edu/shared/images/email/pixel.gif' alt='' name='Cont_0' /></td>
         <td width='580' valign='top' bgcolor='#ffffff' style='line-height: 20px;'>

         ";
         $out .= "<p>";
         for( $i = 0; $i < count($sections); $i++ ) {

         $out .= '<a href="#';
         $out .= $sectNumNames[$i+1];
         $out .= '" class="c13">';
         $out .= $sections[$i];
         $out .= '</a>
         <br />';
         }
         $out .= "</p>";
         $out .= "

         </td>
         <td width='20' bgcolor='#ffffff' style='line-height: 0pt; font-size: 0pt;'><img width='20' hspace='0' height='1' align='left' src='http://www.hbs.edu/shared/images/email/pixel.gif' alt='' name='Cont_0' /></td>
         </tr>
         <tr>
         <td valign='middle' bgcolor='#ffffff' align='center' style='line-height: 0pt; font-size: 0pt;' colspan='3'><img width='20' hspace='0' height='20' align='left' src='http://www.hbs.edu/shared/images/email/pixel.gif' alt='' name='Cont_0' /></td>
         </tr>
         ";
         return $out;
         }*/
        function emailTop() {
                global $sectNumNames, $issue, $sections, $issueDatePhrase;
                $out = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
    <head>
        <title>On Healthcare</title>
    </head>
    <body marginwidth="0" marginheight="0" bgcolor="#ffffff" topmargin="0" leftmargin="0">
        <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center">
            <tbody>
                <tr>
                    <td bgcolor="#ffffff">
                  <table width="620" cellspacing="0" cellpadding="0" border="0" align="center">
                     <tbody>
                        <tr>
                           <td style="line-height: 0pt; font-size: 0pt;"><img width="1" hspace="0" height="5" border="0" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                        </tr>
                        <tr>
                           <td>
                           <table cellspacing="0" cellpadding="0" border="0" align="right">
                              <tbody>
                                                                <tr>
                                                                                
                                                                        <td><font size="1" face="Arial, Helvetica, sans-serif" color="#808285" style="line-height: 15px; font-size: 11px;">You are receiving this update because you are a Harvard Business School alumnus that has expressed an interest in healthcare.</font></td>
                                                                                
                                                                                
                                                                        <td width="18" style="line-height: 0pt; font-size: 0pt;"><img width="18" hspace="0" height="9" align="left" name="Cont_0" alt="" src="http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                                    <td style="line-height: 15px; font-size: 11px;"><a name="recp_rm04_net_servlet_MailView" href="http://recp.rm04.net/servlet/MailView?m=%%MAILING_ID%%&r=%%RECIPIENT_ID%%&j=%%JOB_ID_CODE%%&mt=1" style="color: rgb(128, 130, 133); text-decoration: none;" xt="SPCLICK"><font size="1" face="Arial, Helvetica, sans-serif" color="#808285" style="line-height: 15px; font-size: 11px;">View in browser</font></a></td>
                                 </tr>
                              </tbody>
                           </table>
                           </td>
                        </tr>
                        <tr>
                           <td style="line-height: 0pt; font-size: 0pt;"><img width="1" hspace="0" height="5" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                        </tr>
                     </tbody>
                  </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" bgcolor="#000000">
                  <table width="620" cellspacing="0" cellpadding="0" border="0" align="center">
                     <tbody>
                        <tr>
                           <td width="470" style="line-height: 0pt; font-size: 0pt;"><a name="HBS_Healthcare" href="http://www.hbs.edu/healthcare/"><img width="470" hspace="0" height="76" border="0" align="left" alt="" src="http://www.hbs.edu/healthcare/images/photos/healthcare_w.gif" name="http://www.hbs.edu/healthcare/images/photos/healthcare_w.gif" /></a></td>
                           <td width="260" valign="top">
                              <table cellspacing="0" cellpadding="0" border="0" align="right">
                                 <tbody>
                                    <tr>
                                       <td style="line-height: 0pt; font-size: 0pt;"><img width="1" hspace="0" height="16" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                                    </tr>
                                    <tr>
                                       <td>
                                          <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                             <tbody>
                                                <tr>
                                                   <td width="7" style="line-height: 0pt; font-size: 0pt;"><img width="7" hspace="0" height="1" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                                                   <td width="16" style="line-height: 0pt; font-size: 0pt;"><a xt="SPBOOKMARK" name="_" href="#"><br /></a></td>
                                                   <td width="7" style="line-height: 0pt; font-size: 0pt;"><img width="7" hspace="0" height="1" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                                                   <td width="16" style="line-height: 0pt; font-size: 0pt;"><a name="_" href="#" xt="SPBOOKMARK"><br /></a></td>
                                                   <td width="7" style="line-height: 0pt; font-size: 0pt;"><img width="7" hspace="0" height="1" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                                                   <td width="16" style="line-height: 0pt; font-size: 0pt;"><a name="_" href="#" xt="SPBOOKMARK"><br /></a></td>
                                                   <td width="7" style="line-height: 0pt; font-size: 0pt;"><img width="7" hspace="0" height="1" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                                                   <td width="16" style="line-height: 0pt; font-size: 0pt;"><a name="_" href="#" xt="SPBOOKMARK"> <br /></a></td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td width="20" style="line-height: 0pt; font-size: 0pt;"><img width="20" hspace="0" height="1" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                        </tr>
                     </tbody>
                  </table>
                    </td>
                </tr>
                <tr>
                    <td  bgcolor="#CED665">
                  <table width="620" cellspacing="0" cellpadding="0" border="0" align="center">
                     <tbody>
                        <tr>
                           <td valign="top" align="left">
                              <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#CED665">
                                 <tbody>
                                    <tr>
                                       <td width="470"><img width="472" height="102" border="0" alt="" name="http://www.hbs.edu/healthcare/images/photos/title_OnHealthcare_CED665.gif" src="http://www.hbs.edu/healthcare/images/photos/title_OnHealthcare_CED665.gif" /></td>
                                       <td valign="bottom" align="right">
                                          <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                             <tbody>
                                                <tr>
                                                   <td width="130" valign="bottom" align="right" style="text-align: right;"><font size="4" face="Arial, Helvetica, sans-serif" color="#ffffff" style="font-size: 20px;"><strong>' . $issueDatePhrase . '</strong></font></td>
                                                   <td width="16" style="line-height: 0pt; font-size: 0pt;"><img width="16" hspace="0" height="10" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                                                </tr>
                                                <tr>
                                                   <td valign="top" height="15" align="left" colspan="2"><img width="20" hspace="0" height="15" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td width="620" valign="top" align="left">
                              <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#000000">
                                 <tbody>
                                    <tr>
                                       <td valign="top" align="left" style="line-height: 0pt; font-size: 0pt;" colspan="3"><img width="90" hspace="0" height="27" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                                    </tr>
                                    <tr>
                                       <td valign="top" align="left" style="line-height: 0pt; font-size: 0pt;"><img width="20" hspace="0" height="20" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                                       <td valign="top" style="line-height: 30px; font-family: Arial,Helvetica,sans-serif; color: rgb(189, 189, 189); font-size: 16px;">The Healthcare Initiative at Harvard Business School is a multidisciplinary program dedicated to innovative thinking in the healthcare industry. The Initiative brings together the extensive research, thought leadership, and interest in the business and management of healthcare that exist at HBS.</td>
                                       <td valign="top" align="left" style="line-height: 0pt; font-size: 0pt;"><img width="20" hspace="0" height="20" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                                    </tr>
                                    <tr>
                                       <td valign="top" align="left" style="line-height: 0pt; font-size: 0pt;" colspan="3"><img width="100" hspace="0" height="38" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                                    </tr>
                                    <tr>
                                       <td valign="top" bgcolor="#ffffff" align="left" style="line-height: 0pt; font-size: 0pt;" colspan="3"><img width="80" hspace="0" height="24" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                                    </tr>
                                    <tr>
                                       <td width="20" bgcolor="#ffffff" style="line-height: 0pt; font-size: 0pt;"><img width="20" hspace="0" height="1" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                                       <td width="580" valign="top" height="25" bgcolor="#ffffff"><img border="0" alt="" src="http://www.hbs.edu/shared/images/email/InThisIssue_onWhite.gif" name="Cont_14" /></td>
                                       <td width="20" bgcolor="#ffffff" style="line-height: 0pt; font-size: 0pt;"><img width="20" hspace="0" height="10" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                                    </tr>
                                    <tr>
                                       <td width="20" bgcolor="#ffffff" style="line-height: 0pt; font-size: 0pt;"><img width="20" hspace="0" height="1" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                                       <td width="580" valign="top" bgcolor="#ffffff" style="line-height: 20px;">
';
                $out .= tableOfContents();
                $out .= '                                       </td>
                                       <td width="20" bgcolor="#ffffff" style="line-height: 0pt; font-size: 0pt;"><img width="20" hspace="0" height="1" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                                    </tr>
                                    <tr>
                                       <td valign="middle" bgcolor="#ffffff" align="center" style="line-height: 0pt; font-size: 0pt;" colspan="3"><img width="20" hspace="0" height="20" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td valign="top" align="left">
                              <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
                                 <tbody>
                                    <tr>
                                       
                                       <td width="20" style="line-height: 0pt; font-size: 0pt;"><img width="20" hspace="0" height="1" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                                       
                                       <td width="580" valign="top" align="left">                                       
';
                return $out;
        }

        function emailBottom() {
                global $sectNumNames, $issue, $sections;
                return '                                       
                                       </td>
                                       
                                       <td width="20" style="line-height: 0pt; font-size: 0pt;"><img width="20" hspace="0" height="1" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
                                    </tr>   
                                    <tr>
                                       
                                       <td colspan="3" style="line-height: 0pt; font-size: 0pt;"><img width="25" hspace="0" height="25" align="left" name="Cont_0" alt="" src="http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td bgcolor="#000000" style="line-height: 0pt; font-size: 0pt;"><img width="1" hspace="0" height="7" align="left" name="Cont_0" alt="" src="http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                        </tr>
                        <tr>
                           <td height="60" style="line-height: 0pt; font-size: 0pt;"><img width="1" hspace="0" height="60" align="left" name="Cont_0" alt="" src="http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                        </tr>
                     </tbody>
                  </table>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#ffffff">
                    <table width="620" cellspacing="0" cellpadding="0" border="0" align="center">
                        <tbody>
                            <tr>
                                <td style="line-height: 0pt; font-size: 0pt;"><img width="1" hspace="0" height="15" align="left" name="Cont_0" alt="" src="http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                            </tr>
                            <tr>
                                <td>
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td width="20" valign="top" align="left" style="line-height: 0pt; font-size: 0pt;"><img width="20" hspace="0" height="20" align="left" name="Cont_0" alt="" src="http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                                            <td width="48" valign="top" style="line-height: 0pt; font-size: 0pt;"><img width="48" hspace="0" height="57" align="left" name="Cont_10" alt="" src="http://www.hbs.edu/shared/images/email/logo2.gif" name="logo2.gif" /></td>
                                            <td width="18" style="line-height: 0pt; font-size: 0pt;"><img width="18" hspace="0" height="1" align="left" name="Cont_0" alt="" src="http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                                            <td valign="top" align="left" style="line-height: 16px; font-size: 14px;"><a name="www_hbs_edu_healthcare" style="color: rgb(1, 0, 0); text-decoration: none;" xt="SPCLICK" href="http://www.hbs.edu/healthcare" target="_blank"><font size="2" face="Arial, Helvetica, sans-serif" color="#010000" style="line-height: 16px; font-size: 14px;"><strong>www.hbs.edu/healthcare</strong></font></a></td>
                                            <td width="290" valign="top" align="right" style="text-align: right; line-height: 16px; font-size: 12px;"><a xt="SPBOOKMARK" style="color: rgb(144, 0, 42); text-decoration: underline;" href="http://www.hbs.edu/contact" name="__1"><font size="1" face="Arial, Helvetica, sans-serif" color="#90002a" style="line-height: 16px; font-size: 12px;">Contact Us</font></a> &nbsp; <a xt="SPBOOKMARK" style="color: rgb(144, 0, 42); text-decoration: underline;" href="http://www.hbs.edu/about/privacy.html" name="__2">                                                         <font size="1" face="Arial, Helvetica, sans-serif" color="#90002a" style="line-height: 16px; font-size: 12px;">Privacy Policy</font></a> &nbsp; <a target="_blank" href="#SPONECLICKOPTOUT" name="_optout" style="color: rgb(144, 0, 42); text-decoration: underline;" xt="SPONECLICKOPTOUT"><font size="1" face="Arial, Helvetica, sans-serif" color="#90002a" style="line-height: 16px; font-size: 12px;">Unsubscribe</font></a></td>
                                            <td width="20" valign="top" align="left" style="line-height: 0pt; font-size: 0pt;"><img width="20" hspace="0" height="1" align="left" name="Cont_0" alt="" src="http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                                        </tr>
                                    </tbody>
                                </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="line-height: 0pt; font-size: 0pt;"><img width="50" hspace="0" height="75" align="left" name="Cont_0" alt="" src="http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                            </tr>
                            <tr>
                                <td>
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td width="20" style="line-height: 0pt; font-size: 0pt;"><img width="20" hspace="0" height="1" align="left" name="Cont_0" alt="" src="http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                                            <td style="line-height: 14px; font-size: 10px;"><font size="1" face="Arial, Helvetica, sans-serif" color="#8b8b8b" style="line-height: 14px; font-size: 10px;">Copyright <?php echo date("Y"); ?> President &amp; Fellows of Harvard College. All Rights Reserved.</font></td>
                                            <td width="20" style="line-height: 0pt; font-size: 0pt;"><img width="20" hspace="0" height="1" align="left" name="Cont_0" alt="" src="http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                                        </tr>
                                    </tbody>
                                </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="line-height: 0pt; font-size: 0pt;"><img width="1" hspace="0" height="30" align="left" name="Cont_0" alt="" src="http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                            </tr>
                        </tbody>
                    </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>';
        }

        function sectionBorder() {
                return '<hr/>';
                return '                    

               <table width="100%" cellspacing="0" cellpadding="0" border="0">
                      <tr>
                        <td class="c1"><img width="580" height="40" border="0" src="http://www.hbs.edu/healthcare/images/photos/double-line.jpg" name="http://www.hbs.edu/healthcare/images/photos/double-line.jpg" alt="" /></td>
                  
                      </tr>
                    </table>
';
        }

        /*
         <td width="20" style="line-height: 0pt; font-size: 0pt;"><img width="20" hspace="0" height="1" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt="" name="Cont_0" /></td>
         */
        // ### COMPILE ISSUE WITH FUNCITONS ###
        $link = "";
        $content = "";
        for ($row = 2; $row <= $numRows; $row++) {
                if (strlen($issue -> val($row, 7)) > 0) {
                        $link = sanitize($issue -> val($row, 7));
                } else {
                        $link = "";
                }
                for ($col = 4; $col < 7; $col++) {
                        if (strlen($issue -> val($row, $col)) > 1) {
                                $sect = sanitize($issue -> val($row, $col));
                                $content .= addSect($col - 3, nl2br($sect), $link);
                        }
                }
        }
        $beginning = emailTop();
        $end = emailBottom();
        $html = $beginning . $content . $end;
        writeFile($content);

        // for tidying source code
        /*
         $config = array(
         'indent'         => true,
         'output-xhtml'   => true,
         'wrap'           => 300);
         */

        /*
         $config = array(
         'indent'         => false,
         'output-xhtml'   => true,
         'wrap'           => 0,
         'break-before-br' => false
         );
         //

         // Tidy
         $tidy = new tidy;
         $tidy->parseString($html, $config, 'utf8');
         $tidy->cleanRepair();
         $html=$tidy;

         $html = str_replace(array(">\r\n<", ">\r<", ">\n<"), "><", $html);
         $html = str_replace(" />","/>",$html);
         //
         //*/

        //writeFileAndOpen( $html );
} // \ USE_EXCEL
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="generator" content=
  "HTML Tidy for Linux/x86 (vers 25 March 2009), see www.w3.org" />

  <title><?php echo $email_subject; ?></title>
</head>

<body marginwidth="0" marginheight="0" bgcolor="<?php get_color($topheader_color); ?>"
topmargin="0" leftmargin="0">
  <?php // fill in the email pre-header. See goo.gl/IJaxHy and goo.gl/hf4h6P ?><span style="display: none !important;"><?php
    if (isset($email_preheader) && $email_preheader) {
            // not supported at this time
            //echo $email_preheader;
    } else {
            //echo $email_subject;
    }
                        ?></span>

  <table class="topheader" width="100%" cellspacing="0" cellpadding="0" border="0" align=
  "center">
    <tbody>
      <tr>
        <td>
          <table width="620" cellspacing="0" cellpadding="0" border="0" align="center">
            <tbody>
              <tr>
                <td style="line-height: 0pt; font-size: 0pt;"><img width="1" hspace="0"
                height="5" border="0" align="left" name="Cont_0" alt="" src=
                "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
              </tr>

              <tr>
                <td>
                  <table cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody>
                      <tr>
                        <td width="500"><?php 
                        if(strlen( return_department_info('topmessage') ) == 0 ) {
                            echo '<img width="500" hspace="0" height="9" align="left" alt="" src="http://www.hbs.edu/shared/images/email/pixel.gif" />';
                        } else {
                            echo '<font size="1" face="Arial, Helvetica, sans-serif" color="#808285" style="line-height: 15px; font-size: 11px;">';
                            echo return_department_info('topmessage') . '</font>';
                        } ?></td>
                        <td width="18" style="line-height: 0pt; font-size: 0pt;">
                        <img width="18" hspace="0" height="9" align="left" alt="" name=
                        "Cont_0" src=
                        "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>

                        <td style="line-height: 15px; font-size: 11px;"><a xt="SPCLICK"
                        style="color: rgb(128, 130, 133); text-decoration: none;" href=
                        "http://recp.rm04.net/servlet/MailView?m=%%MAILING_ID%%&amp;r=%%RECIPIENT_ID%%&amp;j=%%JOB_ID_CODE%%&amp;mt=1"
                        name="recp_rm04_net_servlet_MailView"><font size="1" face=
                        "Arial, Helvetica, sans-serif" color="#808285" style=
                        "line-height: 15px; font-size: 11px;">View in
                        browser</font></a></td>

                        <td width="18" style="line-height: 0pt; font-size: 0pt;">
                        <img width="18" hspace="0" height="9" align="left" name="Cont_0"
                        alt="" src=
                        "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>

              <tr>
                <td style="line-height: 0pt; font-size: 0pt;"><img width="1" hspace="0"
                height="5" align="left" name="Cont_0" alt="" src=
                "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>

      <tr>
        <td valign="top" bgcolor="#000">
          <table width="620" cellspacing="0" cellpadding="0" border="0" align="center">
            <tbody>
              <tr>
                <td width="470" style="line-height: 0pt; font-size: 0pt;"><a href=
                "<?php get_department_info('url'); ?>" name="HBS_Healthcare" xt=
                "SPCLICK" target="_blank"><img width="470" hspace="0" height="76" border=
                "0" align="left" src=
                "<?php get_department_info('image'); ?>" /></a></td>

                <td width="20" style="line-height: 0pt; font-size: 0pt;"><img width="20"
                hspace="0" height="1" align="left" name="Cont_0" alt="" src=
                "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>

      <tr>
        <td height="380" class="main-background">
          <table width="620" cellspacing="0" cellpadding="0" border="0" align="center">
            <tbody>
              <tr>
                <td valign="top" align="left">
                  <table class="main-background" width="100%" cellspacing="0"
                  cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <?php if(!(isset($show_header_image) && $show_header_image )): ?>

                        <td style="line-height: 0pt; font-size: 0pt;"><img width="470"
                        hspace="0" height="15" border="0" align="left" name="Cont_0" alt=
                        "" src=
                        "http://www.hbs.edu/shared/images/email/pixel.gif" /></td><?php else: ?>

                        <td width="470"><img width="472" height="102" src=
                        "<?php get_header_image(); ?>" /></td><?php endif ?>

                        <td valign="bottom" align="right">
                          <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td width="130" valign="bottom" align="right" style=
                                "text-align: right;"><font size="4" face=
                                "Arial, Helvetica, sans-serif" color=
                                "<?php get_color($header_date_color); ?>" style=
                                "font-size: 20px;"><strong class=
                                "header-date"><?php echo $header_date_phrase ?></strong></font></td>

                                <td width="16" style="line-height: 0pt; font-size: 0pt;">
                                <img width="16" hspace="0" height="10" align="left" name=
                                "Cont_0" alt="" src=
                                "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                              </tr>

                              <tr>
                                <td valign="top" height="15" align="left" colspan="2">
                                <img width="20" hspace="0" height="15" name="Cont_0" alt=
                                "" src=
                                "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>

              <tr>
                <td width="580">
                  <table class="toc" width="100%" cellspacing="0" cellpadding="0" border=
                  "0">
                    <?php
                    	if(isset($show_mission) && $show_mission === true) {
                    		//mission();
                    	}
                      	tableOfContents();
                    ?>
                  </table>
                </td>
              </tr>

              <tr>
                <td>
                  <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tbody>
                      <tr>
                        <td class="main-body"><img width="20" hspace="0" height="33" align="left" src=
                                "http://www.hbs.edu/shared/images/email/pixel.gif" alt="" /></td>
                            <td class="main-body"></td>
                            <td class="main-body"><img width="20" hspace="0" height="33" align="left" src=
                        "http://www.hbs.edu/shared/images/email/pixel.gif" alt="" />
                        </td>
                      </tr>
                      <tr>
                        <td class="main-body" width="20" style="line-height: 0pt; font-size: 0pt;">
                        <img width="20" hspace="0" height="1" align="left" src=
                        "http://www.hbs.edu/shared/images/email/pixel.gif" alt="" /></td>

                        <td width="580" id="content" class="one-off" valign="top" align="left">
                          <?php if( isset($show_body_topdescription_row) && $show_body_topdescription_row ): ?>

                          <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                              <tr>
                                <td style=
                                "PADDING-BOTTOM: 0px; "
                                class="main-body">
                                  <div align="center">
                                    <table border="0" cellspacing="0" cellpadding="0"
                                    width="580">
                                      <tbody>
                                        <tr>
                                          <td align="center" class="main-body" style=
                                          " width: 228px; height: 114px;"><img style=
                                          "border-style:none;DISPLAY: block; FONT-FAMILY: Arial; COLOR: #333; FONT-SIZE: 12px; FONT-WEIGHT: bold"
                                          border="0" alt=
                                          "making a difference in healthcare" src=
                                          "http://www.hbs.edu/healthcare/images/photos/making-a-difference-in-healthcare-228x146.png"
                                          width="228" height="146" /></td>

                                          <td>
                                            <table border="0" cellspacing="0"
                                            cellpadding="0" width="100%">
                                              <tbody>
                                                <tr>
                                                  <td style=
                                                  "PADDING-BOTTOM: 5px; PADDING-LEFT: 25px; LETTER-SPACING: -1px; PADDING-RIGHT: 0px; FONT-FAMILY: Helvetica, Arial, sans-serif; COLOR: #eadfc8; FONT-SIZE: 23px; FONT-WEIGHT: bold; PADDING-TOP: 0px;"
                                                  align="left">
                                                  <?php echo $body_topdescription_title; ?></td>
                                                </tr>

                                                <tr>
                                                  <td style=
                                                  "PADDING-BOTTOM: 10px; LINE-HEIGHT: 20px; PADDING-LEFT: 25px; PADDING-RIGHT: 0px; FONT-FAMILY: Helvetica, Arial, sans-serif; COLOR: <?php get_color($default_text_color); ?>; FONT-SIZE: 16px; TEXT-DECORATION: none; PADDING-TOP: 0px">
                                                  <?php echo $body_topdescription_date; ?><br />

                                                  <?php echo $body_topdescription_time; ?><br />

                                                  <?php echo $body_topdescription_location; ?></td>
                                                </tr>

                                                <tr>
                                                  <td style=
                                                  "PADDING-BOTTOM: 0px; PADDING-LEFT: 25px; PADDING-RIGHT: 0px; PADDING-TOP: 0px">
                                                  <?php if(isset($show_body_topdescription_download_calendar_event_button)&& $show_body_topdescription_download_calendar_event_button):?>

                                                    <table border="0" cellspacing="0"
                                                    cellpadding="0">
                                                      <tbody>
                                                        <tr>
                                                          <td style=
                                                          "PADDING-BOTTOM: 4px; PADDING-LEFT: 6px; PADDING-RIGHT: 8px; PADDING-TOP: 5px"
                                                          bgcolor="#C1B394"><a target=
                                                          "_blank" name="Download" style=
                                                          "FONT-FAMILY: Helvetica, Arial, sans-serif; COLOR: #524b3b; FONT-SIZE: 18px; FONT-WEIGHT: bold; TEXT-DECORATION: none"
                                                          href=
                                                          "http://beech.hbs.edu/eventCalendar/user/downloadEvents.do?downloadEvents=67808&amp;action=Download+selected+events"
                                                          xt="SPCLICK">Download Calendar
                                                          Event</a></td>
                                                        </tr>
                                                      </tbody>
                                                    </table><?php endif; ?>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table><?php endif; // \$show_body_topdescription_row ?>

                          <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                              <tr>
                                <td 
                                class="main-body" align="center">
                                  <table border="0" cellspacing="0" cellpadding="0"
                                  width="580">
                                    <tbody>
                                      <tr>
                                        <td height="140">
                                          <table border="0" cellspacing="0" cellpadding=
                                          "0" width="100%" height="100">
                                            <tbody>
                                              <tr>
                                                <td valign="middle" width="188" align=
                                                "left">
                                                  <table border="0" cellspacing="0"
                                                  cellpadding="0" width="100%">
                                                    <tbody>
                                                      <tr>
                                                      	<td id="inner-content">
                                                        <?php /*
                                                        <td style=
                                                        "PADDING-TOP: 10PX; PADDING-BOTTOM: 10px; LINE-HEIGHT: 20px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Helvetica, Arial, sans-serif; COLOR: <?php get_color($default_text_color); ?>; FONT-SIZE: 16px; PADDING-TOP: 15px"> */ 
                                                        ?>
                                                        <?php email_body_html(); ?></td>
                                                      </tr>

                                                      <tr>
                                                        <td><?php
                                                                                                                                                                                                                                if (isset($show_bottom_button) && $show_bottom_button) { button();
                                                                                                                                                                                                                                }
                                                                                                                                                                                                                         ?></td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table><!--Main Content Ends-->
                        </td>

                        <td class="main-body" width="20" style="line-height: 0pt; font-size: 0pt;">
                        <img width="20" hspace="0" height="1" align="left" src=
                        "http://www.hbs.edu/shared/images/email/pixel.gif" alt="" /></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>

              

              <tr>
                <td height="60" style="line-height: 0pt; font-size: 0pt;"><img width="1"
                hspace="0" height="60" align="left" name="Cont_0" alt="" src=
                "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>

      <tr>
        <td class="footer">
          <table width="620" cellspacing="0" cellpadding="0" border="0" align="center">
            <tbody>
              <tr>
                <td style="line-height: 0pt; font-size: 0pt;"><img width="1" hspace="0"
                height="15" align="left" name="Cont_0" alt="" src=
                "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
              </tr>

              <tr>
                <td>
                  <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td width="20" valign="top" align="left" style=
                        "line-height: 0pt; font-size: 0pt;"><img width="20" hspace="0"
                        height="20" align="left" name="Cont_0" alt="" src=
                        "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>

                        <td width="48" valign="top" style=
                        "line-height: 0pt; font-size: 0pt;"><img width="48" hspace="0"
                        height="57" align="left" name="Cont_10" alt="" src=
                        "http://www.hbs.edu/shared/images/email/logo2.gif" /></td>

                        <td width="18" style="line-height: 0pt; font-size: 0pt;">
                        <img width="18" hspace="0" height="1" align="left" name="Cont_0"
                        alt="" src=
                        "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>

                        <td valign="top" align="left" style=
                        "line-height: 16px; font-size: 14px;"><a name=
                        "www_hbs_edu_healthcare" style=
                        "color: rgb(1, 0, 0); text-decoration: none;" xt="SPCLICK" href=
                        "<?php get_department_info('url')?>" target=
                        "_blank"><font size="2" face="Arial, Helvetica, sans-serif"
                        color="#010000" style=
                        "line-height: 16px; font-size: 14px;"><strong><?php get_department_info('url')?></strong></font></a></td>

                        <td width="290" valign="top" align="right" style=
                        "text-align: right; line-height: 16px; font-size: 12px;"><a xt=
                        "SPBOOKMARK" style=
                        "color: rgb(144, 0, 42); text-decoration: underline;" href=
                        "<?php get_department_info('email')?>" name=
                        "__1"><font size="1" face="Arial, Helvetica, sans-serif" color=
                        "#90002A" style="line-height: 16px; font-size: 12px;">Contact
                        Us</font></a> &nbsp; <a xt="SPBOOKMARK" style=
                        "color: rgb(144, 0, 42); text-decoration: underline;" href=
                        "http://www.hbs.edu/about/privacy.html" name="__2"><font size="1"
                        face="Arial, Helvetica, sans-serif" color="#90002A" style=
                        "line-height: 16px; font-size: 12px;">Privacy Policy</font></a>
                        &nbsp; <a target="_blank" href="#SPONECLICKOPTOUT" name="_optout"
                        style="color: rgb(144, 0, 42); text-decoration: underline;" xt=
                        "SPONECLICKOPTOUT"><font size="1" face=
                        "Arial, Helvetica, sans-serif" color="#90002A" style=
                        "line-height: 16px; font-size: 12px;">Unsubscribe</font></a></td>

                        <td width="20" valign="top" align="left" style=
                        "line-height: 0pt; font-size: 0pt;"><img width="20" hspace="0"
                        height="1" align="left" name="Cont_0" alt="" src=
                        "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>

              <tr>
                <td style="line-height: 0pt; font-size: 0pt;"><img width="50" hspace="0"
                height="75" align="left" name="Cont_0" alt="" src=
                "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
              </tr>

              <tr>
                <td>
                  <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                      <tr>
                        <td width="20" style="line-height: 0pt; font-size: 0pt;">
                        <img width="20" hspace="0" height="1" align="left" name="Cont_0"
                        alt="" src=
                        "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>

                        <td style="line-height: 14px; font-size: 10px;"><font size="1"
                        face="Arial, Helvetica, sans-serif" color="#8B8B8B" style=
                        "line-height: 14px; font-size: 10px;">Copyright &#169; <?php echo date("Y"); ?>
                        President &amp; Fellows of Harvard College.</font></td>

                        <td width="20" style="line-height: 0pt; font-size: 0pt;">
                        <img width="20" hspace="0" height="1" align="left" name="Cont_0"
                        alt="" src=
                        "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>

              <tr>
                <td style="line-height: 0pt; font-size: 0pt;"><img width="1" hspace="0"
                height="30" align="left" name="Cont_0" alt="" src=
                "http://www.hbs.edu/shared/images/email/pixel.gif" /></td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</body>
</html>

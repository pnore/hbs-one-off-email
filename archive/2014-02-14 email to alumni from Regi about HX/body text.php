
Health care quality, access, and cost challenges abound, from implementing health care reform legislation in the U.S., to developing promising health information technology and life science innovations, to creating affordable health care delivery and insurance options. 

Innovation provides an answer, but few know how to do it well.

We hope you will join us in two educational programs, grounded by expertise from Harvard Business School, that focus on how to make health care innovation happen: 

1.  **BUSINESS INNOVATIONS IN GLOBAL HEALTH CARE (BIG)**, an executive education program offered at Harvard Business School, June 25-28, 2014

2.  **INNOVATING IN HEALTH CARE (IHC)**, a new online learning experience, offered by HarvardX.  

Please reach out to your colleagues and friends who are eager to create successful global business innovations in health care to enroll in one or both of these courses, described in greater detail below. 


# BUSINESS INNOVATIONS IN GLOBAL HEALTH CARE { #one }

Focused on health care innovations from around the world, the senior HBS faculty members who teach in the program apply a powerful framework for evaluating and implementing global innovations to new case studies. Last year's inaugural session featured cases on Health care for Rural India (eHealthpoint), Making Health care Affordable for All (Gilead Sciences), and the Health Care System in Brazil, Amil, a firm purchased by an American firm for $6 billion, to name just a few. 

Participants not only sharpen their ability to differentiate good ideas from bad ones and develop sustainable business models but also become part of a powerful global network. Last year's session, for example, attracted senior health care entrepreneurs and Ministers of Health from across the globe. Since then, as an example of the results of this network, a California telemedicine provider developed a vital business relationship with the Nigerian health care system. Because the case studies in each session of this program are entirely new, we expect repeat attendees that help to anchor this network. 

The program is chaired by HBS Professors Regina Herzlinger and Michael Chu with their colleagues---Senior Professors Richard Hamermesh, Robert Huckman, John Quelch, and V. Kasturi Rangan. They are complemented with guest speakers---entrepreneurs such as James Heywood, CEO of Patients Like Me, and public policy gurus, such as Dr. Thomas Zeltner, Special Envoy for Financing, WHO.

Enclosed are an application form and a brochure. To learn more about the program or to apply online, please visit: **[http://www.exed.hbs.edu/programs/big/Pages/default.aspx]**.


# INNOVATING IN HEALTH CARE (IHC) { #two }

Offered by HarvardX through the edX platform (edX.org), IHC is entirely open and free.

Innovating in Health Care provides concrete frameworks, case-based examples, and business plan exercises for evaluating entrepreneurial opportunities and building viable business models for different kinds of health care transformations.

Your guides for IHC will be health care business expert Prof. Regina Herzlinger of Harvard Business School and a multi-disciplinary team, including some of the leading global health care entrepreneurs. Their aim is to provide a space to learn, engage, collaborate, and innovate.

IHC will be offered in two formats: an open, online experience for self-paced learners, and a smaller, team-based deep dive experience for likeminded individuals who want to produce business plans.

A few weeks into the course, participants will be able to form groups, aided by an algorithm based on their needs and initial business ideas or interests, and apply for one of the 100 spaces in the private course. Pre-formed teams are also welcome to apply during this time.

Please join with us to innovate health care and help spread the word about this fantastic opportunity to "do well and do good."

Click this link to register: **[http://bit.ly/1g36Ifl]**  

[![hex image][]](https://www.youtube.com/watch?v=f9qL2EPsNGI)
*[click to play video]*

[http://www.exed.hbs.edu/programs/big/Pages/default.aspx]: http://www.exed.hbs.edu/programs/big/Pages/default.aspx
[http://bit.ly/1g36Ifl]: http://bit.ly/1g36Ifl
[BUSINESS INNOVATIONS IN GLOBAL HEALTH CARE (BIG)]: #one
[INNOVATING IN HEALTH CARE (IHC)]: #two
[hex image]: http://contentz.mkt1960.com/ra/2014/2395/02/8228162/hex.jpg
[click to play video]: https://www.youtube.com/watch?v=f9qL2EPsNGI
[spacer]: http://www.hbs.edu/shared/images/email/pixel.gif

# 2013 HBS Health Care Alumni Conference
## Last Day to Register -- WED OCT 30

You are invited to attend the [2013 HBS Health Care Alumni Conference], which will take place THU NOV 07 and FRI NOV 08 in Cambridge, MA.
 
[What's Going On] / [Who Will be Speaking] / [See Who's Attending]

We hope to see you there!

<?php echo button('Register Now', 'http://www.hbshealthalumni.org/article.html?aid=714' ); ?>



[speech bubbles]: http://www.hbs.edu/healthcare/images/photos/speechbubbles.png
[pill image]: http://www.hbs.edu/healthcare/images/photos/pill_100.png
[steth image]: http://www.hbs.edu/healthcare/images/photos/steth_100.png
[heart image]: http://www.hbs.edu/healthcare/images/photos/heart_100.png
[up_arrow image]: http://www.hbs.edu/healthcare/images/photos/up_arrow_100.png
[dna image]: http://www.hbs.edu/healthcare/images/photos/dna_100.png
[clip image]: http://www.hbs.edu/healthcare/images/photos/clip_100.png

[2013 HBS Health Care Alumni Conference]: http://www.hbshealthalumni.org/article.html?aid=714
[What's Going On]: http://www.hbshealthalumni.org/e.html?u=http://www.hbshealthalumni.org/article.html?aid=734&t=133651-1223919
[Who Will be Speaking]: http://www.hbshealthalumni.org/e.html?u=http://www.hbshealthalumni.org/article.html?aid=741&t=133651-1223919
[See Who's Attending]: http://www.hbshealthalumni.org/e.html?u=http://www.hbshealthalumni.org/article.html?aid=736&t=133651-1223919

[Deans' Health and Life Sciences Challenge Kickoff]: http://ilab.harvard.edu/calendar-event/content/2014-deans-health-life-sciences-challenge-kick
[pnore@hbs.edu]: mailto:pnore@hbs.edu?Subject=Student%20Focus%20Group
[aly@harvard.edu]: mailto:aly@harvard.edu
[Tell Us What You Think About Health Care Events]: mailto:pnore@hbs.edu?Subject=Student%20Focus%20Group
[Alumni Mentor Preferences Due]: https://secure.hbs.edu/poll/taker/pollTaker.jsp?poll=1,134531
[j.mp/2014-hbs-hc-mentor]: https://secure.hbs.edu/poll/taker/pollTaker.jsp?poll=1,134531
[Care Delivery Panel]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=73289&filter=month&viewDay=15&viewMonth=9&viewYear=2013
[RSVP]: https://secure.hbs.edu/poll/taker/pollTaker.jsp?poll=134521
[Life Sciences Panel]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=70952&filter=month&viewDay=16&viewMonth=9&viewYear=2013
[Health Care Finance Panel]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=70957&filter=month&viewDay=16&viewMonth=9&viewYear=2013
[Career Days: Devices and Diagnostics Panel]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=73290

[![![up_arrow image][]]: http://beech.hbs.edu/eventCalendar/user/eventDetail.do?masterEventId=73289&filter=month&viewDay=15&viewMonth=9&viewYear=2013

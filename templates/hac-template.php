<html>
	<head>
		<title><?php if(isset($post_title)&&strlen($post_title)>0) { echo $post_title; } ?></title>
	</head>
	<body style="padding-bottom: 0px; margin: 0px; padding-left: 0px; padding-right: 0px; background: #ffffff; padding-top: 0px">
		<p></p>
		<center></center>
		
		<table width="600" cellspacing="0" cellpadding="0" border="0" align="center">
			<tbody>
				<tr>
					<td>
					<table width="600" cellspacing="0" cellpadding="5" border="0">
						<tbody>
							<tr>
								<td style="text-align: center">
								<p>
									<span style="font-family: Arial"><font size="1">If you are having problems viewing this message, please </font></span><a target="_blank" xt="SPCLICKTOVIEW" href="#SPCLICKTOVIEW" name="viewinbrowser_1"><span style="font-family: Arial"><font size="1">click to view in browser</font></span></a><span style="font-family: Arial"><font size="1">.</font></span>
								</p></td>
							</tr>
						</tbody>
					</table></td>
				</tr>
				<tr>
					<td bgcolor="#b1233b"><a target="_blank" xt="SPCLICK" href="<?php if( isset( $main_link_url ) && strlen( $main_link_url ) > 0 ) { echo $main_link_url; } else { echo "http://www.forumonhealthcareinnovation.org"; } ?>" name="forumonhealthcareinnovation"><img width="600" border="0" height="232" class="image-fix" title="Forum on Healthcare Innovation" name="HBS_Forum-email-banner.png" alt="Forum on Healthcare Innovation" style="display: block" src="http://www.hbs.edu/healthcare/images/photos/HCI-FHI-emailbanner.jpg" /></a></td>
				</tr>
				<tr>
					<td>
					<table width="600" cellspacing="0" cellpadding="0" border="0">
						<tbody>
							<tr>
								<td width="160" valign="top">
								<p>
									<table width="150" cellspacing="0" cellpadding="10" border="0">
										<tbody>

											<tr>
												<td><p style="line-height: 18px; color: #808080; font-size: 13px"></p><p style="line-height: 18px; font-family: Arial; color: rgb(128,128,128); font-size: 13px"></p>
												<p style="line-height: 18px; font-family: Arial; color: #808080; font-size: 13px">
														This <?php if(isset($post_type) && strlen($post_type) > 0){echo strtolower($post_type);} else { echo "message"; } ?> comes from the <a  
																href="<?php get_department_info('url')?>">
																Forum on Healthcare Innovation</a>, a collaboration between Harvard Business School and Harvard Medical School to explore innovations to increase value in the health care industry.
												</p>
												</td>
											</tr>
											<?php if(isset($sidebar_image) && strlen($sidebar_image) > 0) : ?>
												<tr>
												<td>
													<?php if(isset($sidebar_image_link_url) && strlen($sidebar_image_link_url) > 0 ) : ?>
													<a target="_blank" style="text-decoration: none;" href="<?php echo $sidebar_image_link_url; ?>">
													<?php endif; ?>
														<img src="<?php echo $sidebar_image; ?>"/>
													<?php if(isset($sidebar_image_link_url) && strlen($sidebar_image_link_url) > 0 ) : ?>
													</a>
													<?php endif; ?>
												</td>
											</tr>
											<?php endif; ?>
										</tbody>
									</table>
								</p></td>
								<td width="1" bgcolor="#b1233b">&nbsp;</td>
								<td>
								<table id="content" width="424" cellspacing="0" cellpadding="20" border="0">
									<tbody>
										<tr>
											<td>
											
											<?php email_body_html(); ?>
											
										</tr>
									</tbody>
								</table></td>
							</tr>
						</tbody>
					</table></td>
				</tr>
				<tr>
					<td bgcolor="#000000" height="1"></td>
				</tr>
				<tr>
					<td>
					<table width="600" cellspacing="0" cellpadding="5" border="0">
						<tbody>
							<tr><td style="padding: 0 0 0 0;" align="center">
								<!-- follow @ForumHealthInno button -->
								<table border="0" cellspacing="0" cellpadding="0">
									<tbody>
										<tr>
											<td valign="middle" bgcolor="#007697" style="vertical-align:middle;padding: 2px 2px 2px 8px; ">
								<a 	style="font-size: 12px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none;" 
									href="https://twitter.com/intent/user?screen_name=ForumHealthInno">
										
								<!--<img height="20" width="168" alt="Follow @ForumHealthInno on Twitter" title="Follow @ForumHealthInno on Twitter" src="http://projects.iq.harvard.edu/files/styles/os_files_small/public/forum-on-healthcare-innovation/files/follow-forumhealthinno-twitter.png"/>-->
								Follow @ForumHealthInno </a>
											</td><?php /*
											<td valign="middle" bgcolor="#007697" style="vertical-align:middle;padding: 2px 2px 2px 2px; ">
												<a 	style="font-size: 12px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none;" 
									href="https://twitter.com/intent/user?screen_name=ForumHealthInno">
												<img alt="Follow @ForumHealthInno on Twiter" height="15" width="15" src="http://www.hbs.edu/healthcare/images/photos/twitter-bird-dark-bgs.png"/>
												</a>
											</td>*/?>
										</tr>
									</tbody>
								</table>
								
								</td>
								<td>
									
								<p>
									
									<span style="color: #808080"><span style="font-size: 10px"><span style="font-family: Arial"><em>Copyright &copy; <?php echo date("Y"); ?> President and Fellows of Harvard College. All rights reserved.</em></span></span><span style="font-family: Arial" />
									
										<br />
									</span>

									<span style="color: #000000">
										<span style="font-size: 9px">
											<span style="font-family: Arial">
												<table width="100%">
													<tr>
                                            <td><a name="unsubscribe_html_1" style="color: #000; font-weight: bold; font-family: Arial; font-size: 9px;" xt="SPONECLICKOPTOUT" href="#SPONECLICKOPTOUT" target="_blank">Unsubscribe</a></td>

													</tr>
												</table>
											</span>
										</span>
									</span>
								</p></td>
							</tr>
						</tbody>
					</table></td>
				</tr>
			</tbody>
		</table>
	</body>
</html>
a.button {
	FONT-FAMILY: Helvetica, Arial, sans-serif; 
	COLOR: <?php get_color($body_background_color); ?>; 
	FONT-SIZE: 18px; 
	FONT-WEIGHT: normal; 
	TEXT-DECORATION: none;
	LINE-HEIGHT: 20px;
}

td.button {
	PADDING-TOP: 5px; 
	PADDING-BOTTOM: 5px; 
	PADDING-LEFT: 5px; 
	PADDING-RIGHT: 5px; 
	text-align:center;
	width:150px;
	background-color: <?php get_color($heading_text_color); ?>; 
}

.toc {
	background-color:#000;
}

.toc a {
	background-color:#fff;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px; 
	line-height: 20px;
	color: <?php get_color($highlight_text_color); ?>;
}



td.right {
	/*width:77%*/
}

table.leftright {
	margin:0px;
	border-collapse: collapse;
}
table.leftright td {
	padding-top: 8px;
	padding-right: 8px;
	padding-bottom:8px;
}
td.left {
	/*width:23%;*/
	font-size:1em; 
	font-family:'Arial','sans-serif';
	color:#515151;
	padding-bottom:17px
	padding-left:0px;
	font-weight: bold;
}
td.left a {
	font-size:1em;
	line-height:18px;
}
td.normal {
    font-size:1em; 
    font-family:'Arial','sans-serif';
    /*color:#515151;*/
    padding-bottom:17px
    padding-left:0px;
    font-weight: normal; 
}
td.center {
    padding-left: 8px;
}

span.time, span.place {
	font-weight:normal;
	font-size:11px;
	line-height:14px;
}

/*#content.one-off span.time, span.place {
    font-size:1em;
    line-height:1em;
    font-weight:bold;
}*/

.goright {
    align:right;
    text-align:right;
}

td.plainWithborders {
	font-size:1em; 
    font-family:'Arial','sans-serif';
    padding:8px;
    font-weight: normal; 
    border-top: 1px solid black;
    border-botton: none;
    border-left: none;
    border-right: none;
}

p {
	line-height: 18px; 
	color: <?php echo $possible_colors[$default_text_color]; ?>; 
	font-size: 13px;
	font-family:Arial;
}

p.small {
	line-height: 10px; 
	font-size: 8px;
}


#content h1 { 
    margin-top:16px;
	margin-bottom:0px;
	font-size:48px;
	font-weight:bold;
	   color: <?php echo $possible_colors[$default_text_color] ?>;
}

#content h2 {
	font-size:16px; 
	margin=0;
	color: <?php echo $possible_colors['medblue'] ?>;
	margin-top:20px;
}

#content img {
    margin-top:20px;
}

table.leftright td h2 {
    margin-top:8px;
}

#content h2 abbr {
	font-size:16px; 
}

#content h2 a {
	font-size:16px; 
}


#content h3 {
	font-size:16px; 
}

#content td {
	color: <?php echo $possible_colors[$default_text_color] ?>;
	/*padding:5px;*/
	FONT-FAMILY: Helvetica, Arial, sans-serif; 
}
#content a.author {
	color: <?php echo $possible_colors['medgrey'] ?>;
}

#content .title {
	font-style:italic 
}

#content li {
	line-height: 18px; 
	color: <?php echo $possible_colors[$default_text_color]; ?>; 
	font-size: 13px;
	font-family:Arial;
}

.topheader {
	background-color: <?php get_color($topheader_color); ?>;
}

.main-background {
	background-color: <?php echo get_color($background_color); ?>;
}

.main-body {
	background-color: <?php get_color($body_background_color); ?>;
	line-height:25px;
}

.footer {
	background-color: <?php get_color($footer_background_color); ?>;
}

td.left {
	width:23%;
	font-size:9.0pt; 
	font-family:'Arial','sans-serif';
	color:#515151;
	padding-bottom:17px
	padding-left:0px;
	font-weight: bold;
}



#content a, body a, p a { 
    color:<?php echo $possible_colors["crimson"]; ?>;    
    font-weight:bold;
}
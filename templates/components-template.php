<?php 

function button($text = null, $link = null) { 
	global $button_text_color;
	global $bottom_button_text;
	global $bottom_button_url;
	if( $text == null ) {
		$text = $bottom_button_text;
	} 
	if( $link == null ) {
		$link = $bottom_button_url;
	}
	
	$out = '';
	$out .= '<table border="0" cellspacing="0" cellpadding="0"><tbody><tr><td class="button">';
	if(strpos($link, '<a href') !== false ) {
		$out .= str_replace('<a href', '<a class="button" href',$link); 
	} else { 
		$out .= '<a target="_blank" class="button" href="' . $link . '">' . $text . '</a>';
	}
	$out .= '</td></tr></tbody></table>';
	return $out;
}

function mission() { ?>
	
					<tr>
						<td valign="top" align="left" style="line-height: 0pt; font-size: 0pt;" colspan="3"><img width="90" hspace="0" height="27" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt=""></td>
					</tr>
					<tr>
						<td valign='top' align='left' style='line-height: 0pt; font-size: 0pt;'><img width='20' hspace='0' height='20' align='left' src='http://www.hbs.edu/shared/images/email/pixel.gif' alt=''  /></td>
            <?php /* commented out on 2014-08-25 and replaced with new mission text
            <td valign='top' style='line-height: 30px; font-family: Arial,Helvetica,sans-serif; color: rgb(189, 189, 189); font-size: 16px;'>The Health Care Initiative at Harvard Business School is a multidisciplinary program dedicated to innovative thinking in the health care industry. The Initiative brings together the extensive research, thought leadership, and interest in the business and management of health care that exist at HBS.</td> 
            */ ?>
            <td valign='top' style='line-height: 30px; font-family: Arial,Helvetica,sans-serif; color: rgb(189, 189, 189); font-size: 16px;'>The Health Care Initiative at Harvard Business School is a multidisciplinary program dedicated to innovative thinking in the health care industry. The Initiative brings together the extensive research, thought leadership, and interest in the business and management of health care that thrive at HBS.</td>
						<td valign='top' align='left' style='line-height: 0pt; font-size: 0pt;'><img width='20' hspace='0' height='20' align='left' src='http://www.hbs.edu/shared/images/email/pixel.gif' alt=''  /></td>
					</tr>
					<tr>
						<td valign="top" align="left" style="line-height: 0pt; font-size: 0pt;" colspan="3"><img width="90" hspace="0" height="27" align="left" src="http://www.hbs.edu/shared/images/email/pixel.gif" alt=""></td>
					</tr>
				
	<?php 
}

$sections = array();

function section($title = null) {
	global $sections;
	$sections[] = $title;
	
	echo "* * *\n\n";
	echo "# " . $title . "\t{#section" . count($sections) . "}" ;
}

function tableOfContents() {
   global $sections;
   
   	$out = "";
	if(count($sections) > 1) {
										$out .= 
										"<tr>
                                       <td valign='top' bgcolor='#ffffff' align='left' style='line-height: 0pt; font-size: 0pt;' colspan='3'><img width='80' hspace='0' height='24' align='left' src='http://www.hbs.edu/shared/images/email/pixel.gif' alt='' name='Cont_0' /></td>
                                    </tr>
                                    <tr>
                                       <td width='20' bgcolor='#ffffff' style='line-height: 0pt; font-size: 0pt;'><img width='20' hspace='0' height='1' align='left' src='http://www.hbs.edu/shared/images/email/pixel.gif' alt='' name='Cont_0' /></td>
                                       <td width='580' valign='top' height='25' bgcolor='#ffffff'><img border='0' alt='' src='http://www.hbs.edu/shared/images/email/InThisIssue_onWhite.gif' name='Cont_14' /></td>
                                       <td width='20' bgcolor='#ffffff' style='line-height: 0pt; font-size: 0pt;'><img width='20' hspace='0' height='10' align='left' src='http://www.hbs.edu/shared/images/email/pixel.gif' alt='' name='Cont_0' /></td>
                                    </tr>";
                                     
                                    $out .= "
                                    <tr>
                                       <td width='20' bgcolor='#ffffff' style='line-height: 0pt; font-size: 0pt;'><img width='20' hspace='0' height='1' align='left' src='http://www.hbs.edu/shared/images/email/pixel.gif' alt='' name='Cont_0' /></td>
                                       <td width='580' valign='top' bgcolor='#ffffff' style='line-height: 20px;'>
                                    ";
   $out .= "<p>";
   for( $i = 0; $i < count($sections); $i++ ) {

	  $out .= '<a href="#section';
      $out .= $i+1;
      $out .= '" class="toc">';
      $out .= $sections[$i];
      $out .= '</a>
                  <br />';
   }
   $out .= "</p>";
   $out .= "
   
                                          </td>
                                       <td width='20' bgcolor='#ffffff' style='line-height: 0pt; font-size: 0pt;'><img width='20' hspace='0' height='1' align='left' src='http://www.hbs.edu/shared/images/email/pixel.gif' alt='' name='Cont_0' /></td>
                                    </tr>
                                    <!--<tr>
                                       <td valign='middle' bgcolor='#ffffff' align='center' style='line-height: 0pt; font-size: 0pt;' colspan='3'><img width='20' hspace='0' height='20' align='left' src='http://www.hbs.edu/shared/images/email/pixel.gif' alt='' name='Cont_0' /></td>
                                    </tr>-->
   ";
   echo $out;
   }
}

$tableInProgress = false;
$isRq = false;
function beginTable($queryName = NULL, $maxRows = NULL, $class="leftright", $orderby="" ) {
    global $tableInProgress;
    global $isRq;
    $rqStr='';
    if(! $tableInProgress ) {
        $tableInProgress = true;
        if( $queryName != null && $maxRows != null ) {
            $isRq=true;
            
            $rqStr .= '<pre>
%%RT_LOOKUP_BEGIN query="' . $queryName . '" order_by="' . $orderby . '" max_rows="' . $maxRows . '" %% %%RT_HEADER_BEGIN%% </pre>' ;
        }
        $rqStr .= "<table class='$class' border='0' cellspacing='0' cellpadding='0'>";
        if( $isRq ) {
          $rqStr .=  '<pre>%%RT_HEADER_END%% %%RT_ROW_BEGIN%%</pre>';
        }
        echo $rqStr;
    }
}

function left($content = "", $width="23%", $class="left", $valign="top") {
    global $tableInProgress;
    if( $tableInProgress ) {
        echo "<tr><td class='$class' markdown='block' valign='$valign' align='left' style='width:$width; vertical-align:$valign'>$content</td>";
    }
}

function cell($content = "", $width="77%", $class="right", $valign="top") {
    global $tableInProgress;
    if( $tableInProgress ) {
        echo "<td class='$class' valign='$valign' markdown='block' style='width:$width; vertical-align:$valign'>" . $content . "</td>";
    }
}

function right($content = "", $width="77%", $class="right", $valign="top") {
    global $tableInProgress;
    if( $tableInProgress ) {
        echo "<td class='$class' valign='$valign' markdown='block' style='width:$width; vertical-align:$valign'>" . $content . "</td></tr>";
    }
}

function row($content = "", $width="100%", $class="right", $valign="top") {
    global $tableInProgress;
    if( $tableInProgress ) {
        echo "<tr><td class='$class' colspan='2' valign='$valign' markdown='block' style='width:$width; vertical-align:$valign'>" . $content . "</td></tr>";
    }
}

function endTable() {
    global $tableInProgress;
    global $isRq;
    $rqStr = '';
    if( $tableInProgress ) {
        if ($isRq) {
            $rqStr .= '<pre>%%RT_ROW_END%% %%RT_FOOTER_BEGIN%%';
        }
        $rqStr .= '</table>';
        if ($isRq) {
            $rqStr .= '%%RT_FOOTER_END%% %%RT_NOT_FOUND_BEGIN%%  %%RT_NOT_FOUND_END%% %%RT_LOOKUP_END%%</pre>';
        }
        echo $rqStr;
    }
    $tableInProgress = false;
    $isRq = false;
}


$queryInProgress = false;
$isRqQuery = false;
function beginQuery($queryName = NULL, $maxRows = NULL, $class="leftright") {
  global $queryInProgress;
  global $isRqQuery;
  $rqStr='';
    if(! $queryInProgress ) {
        $queryInProgress = true;
        if( $queryName != null && $maxRows != null ) {
            $isRqQuery=true;
            
            $rqStr .= "%%RT_LOOKUP_BEGIN query='$queryName' order_by='' max_rows='$maxRows' %% %%RT_HEADER_BEGIN%% %%RT_HEADER_END%% %%RT_ROW_BEGIN%%";
        }
        echo $rqStr;
    }
}

function endQuery() {
  global $queryInProgress;
    global $isRqQuery;
    $rqStr = '';
    if( $queryInProgress ) {
        if ($isRqQuery) {
            $rqStr .= "%%RT_ROW_END%% %%RT_FOOTER_BEGIN%% %%RT_FOOTER_END%% %%RT_NOT_FOUND_BEGIN%%  %%RT_NOT_FOUND_END%% %%RT_LOOKUP_END%%";
        }
        echo $rqStr;
    }
    $tableInProgress = false;
}

function singleQuery($queryName = NULL, $passIn = "") {
  echo "<pre>%%RT_LOOKUP_BEGIN query=\"$queryName\" order_by=\"\" max_rows=\"1\" %% %%RT_HEADER_BEGIN%% %%RT_HEADER_END%% %%RT_ROW_BEGIN%%$passIn%%RT_ROW_END%% %%RT_FOOTER_BEGIN%% %%RT_FOOTER_END%% %%RT_NOT_FOUND_BEGIN%%  %%RT_NOT_FOUND_END%% %%RT_LOOKUP_END%%</pre>";
}
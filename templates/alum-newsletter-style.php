#inner-content {
	padding-bottom:15px;
}
#inner-content * {
	line-height:20px; 
	font-size:14px;
}
#inner-content td {
	line-height:20px;
}


#content p {
	background-color: <?php echo $possible_colors['white'] ?>; 
	font-size: 14px;
}

#content a {
	color: <?php echo $possible_colors[$highlight_text_color] ?>; 
	font-weight:normal;
}

#content strong {
	color: <?php echo $possible_colors[$highlight_text_color] ?>;
	/*font-size:14px; */
	font-weight:normal;
}

strong.header-date {
	color: <?php echo $possible_colors[$header_date_color] ?>;
}

#content h1, h2, h3 { 
	color: <?php echo $possible_colors[$heading_text_color] ?>;
	/*font-size:25px; */
	font-weight:bold;
	margin-bottom:0px;
}



#content h1 { 
	margin-bottom:0px;
	font-weight:bold;
	font-size: 22px;
	line-height: 33px;
}

#content h2 {
	font-size:18px;
	line-height: 27px; 
	font-weight: bold;
	color: <?php echo $possible_colors['medblue'] ?>;
}

#content h3 {
	font-size:16px; 
	line-height: 24px;
}

# content h4 {
	font-size: 14px;
	line-height: 21px;
}

#content td {
	color: <?php echo $possible_colors[$default_text_color] ?>;
	/*padding:5px;*/
	FONT-FAMILY: Helvetica, Arial, sans-serif; 
}
#content a.author {
	color: <?php echo $possible_colors['medgrey'] ?>;
}

#content .title {
	font-style:italic 
}

.topheader {
	background-color: <?php get_color($topheader_color); ?>;
}

.main-background {
	background-color: <?php get_color($background_color); ?>;
}

.main-body {
	background-color: <?php get_color($body_background_color); ?>;
	line-height:25px;
}

.footer {
	background-color: <?php get_color($footer_background_color); ?>;
}

td.left {
	width:23%;
	font-size:9.0pt; 
	font-family:'Arial','sans-serif';
	color:#515151;
	padding-bottom:17px
	padding-left:0px;
	font-weight: bold;
}

a img {
    border: none;
}

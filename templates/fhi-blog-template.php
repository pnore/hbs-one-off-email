<html>
	<head>
		<title><?php if(isset($post_title)&&strlen($post_title)>0) { echo $post_title; } ?></title>
	</head>
	<body style="padding-bottom: 0px; margin: 0px; padding-left: 0px; padding-right: 0px; background: #ffffff; padding-top: 0px">
		<p></p>
		<center></center>
		
		<table width="600" cellspacing="0" cellpadding="0" border="0" align="center">
			<tbody>
				<tr>
					<td>
					<table width="600" cellspacing="0" cellpadding="5" border="0">
						<tbody>
							<tr>
								<td style="text-align: center">%%DC::2012 FHI Attendance Segment::2012 FHI Attendance Segment%%</td>
							</tr>
							<tr>
								<td style="text-align: center">
								<p>
									<span style="font-family: Arial"><font size="1">If you are having problems viewing this message, please </font></span><a target="_blank" xt="SPCLICKTOVIEW" href="#SPCLICKTOVIEW" name="viewinbrowser_1"><span style="font-family: Arial"><font size="1">click to view in browser</font></span></a><span style="font-family: Arial"><font size="1">.</font></span>
								</p></td>
							</tr>
						</tbody>
					</table></td>
				</tr>
				<tr>
					<td bgcolor="#b1233b"><a target="_blank" xt="SPCLICK" href="http://projects.iq.harvard.edu/forum-on-healthcare-innovation?utm_source=blogpost-2013-08&utm_medium=email&utm_campaign=Forum+on+Healthcare+Innovation" name="forumonhealthcareinnovation"><img width="600" border="0" height="232" class="image-fix" title="Forum on Healthcare Innovation" name="HBS_Forum-email-banner.png" alt="Forum on Healthcare Innovation" style="display: block" src="http://www.hbs.edu/healthcare/images/photos/HCI-FHI-emailbanner.jpg" /></a></td>
				</tr>
				<tr>
					<td>
					<table width="600" cellspacing="0" cellpadding="0" border="0">
						<tbody>
							<tr>
								<td width="160" valign="top">
								<p>
									<table width="150" cellspacing="0" cellpadding="10" border="0">
										<tbody>

											<tr>
												<td><p style="line-height: 18px; color: #808080; font-size: 13px"></p><p style="line-height: 18px; font-family: Arial; color: rgb(128,128,128); font-size: 13px"></p>
												<p style="line-height: 18px; font-family: Arial; color: #808080; font-size: 13px">
														This <?php echo strtolower($post_type);?> is by <?php echo strtolower($post_author_type);?> of the <a style="text-decoration: none;color:<?php get_color( $highlight_text_color );?>" 
																href="<?php get_department_info('url')?>">
																Forum on Healthcare Innovation</a>, a collaboration between Harvard Business School and Harvard Medical School to explore innovations to increase value in the healthcare industry.
												</p>
												</td>
											</tr>
											<?php if(isset($sidebar_image) && strlen($sidebar_image) > 0) : ?>
												<tr>
												<td>
													<?php if(isset($sidebar_image_link_url) && strlen($sidebar_image_link_url) > 0 ) : ?>
													<a target="_blank" style="text-decoration: none;" href="<?php echo $sidebar_image_link_url; ?>">
													<?php endif; ?>
														<img src="<?php echo $sidebar_image; ?>"/>
													<?php if(isset($sidebar_image_link_url) && strlen($sidebar_image_link_url) > 0 ) : ?>
													</a>
													<?php endif; ?>
												</td>
											</tr>
											<?php endif; ?>
										</tbody>
									</table>
								</p></td>
								<td width="1" bgcolor="#b1233b">&nbsp;</td>
								<td>
								<table id="content" width="424" cellspacing="0" cellpadding="20" border="0">
									<tbody>
										<tr>
											<td>
												
											<table style="width:100%" border="0" cellspacing="0" cellpadding="0">
												<tbody>
													<tr><h6 style="
													font-family: Helvetica, Arial, Sans-Serif;
													font-size: 1em;
													font-weight: bold;
													line-height: 138.9%;
													font-size: 0.812em;	
													color: <?php get_color( $highlight_text_color );?>;
													margin-top:0em;
													margin-bottom:.8em;"><?php echo $post_type;?></h6></tr>
													<tr>
														<td style="width:75px!important;white-space:nowrap;vertical-align:top"><a href="<?php get_author_info('url');?>" style="text-decoration:underline;color:#2585b2;display:block;margin-right:10px" target="_blank"><img border="0" alt="" src="<?php get_author_info('image')?>" height="75" width="75"></a></td>
														<td style="padding:.3em;"><h2 style="font-family: Arial;margin-top:0px;margin-right:0px;margin-bottom:.3em;margin-left:0em;font-size:1.3em;color:#555;"><a href="<?php echo $main_link_url . '?' . $google_analytics_url_suffix; ?>" style="text-decoration:underline;color:#007697;text-decoration:none!important" target="_blank"><?php echo $post_title;?></a></h2><span style="font-family: Arial;color:#888;margin-left:0em;">by <a href="<?php get_author_info('url'); ?>" style="text-decoration:underline;color:#2585b2;color:#888!important" target="_blank"><?php get_author_info('name');?></a></span></td>
													</tr>
												</tbody>
											</table>
											<?php email_body_html(); get_comment_bar()?>
											
										</tr>
									</tbody>
								</table></td>
							</tr>
						</tbody>
					</table></td>
				</tr>
				<tr>
					<td bgcolor="#000000" height="1"></td>
				</tr>
				<tr>
					<td>
					<table width="600" cellspacing="0" cellpadding="5" border="0">
						<tbody>
							<tr><td style="padding: 0 0 0 0;" align="center">
								<!-- follow @ForumHealthInno button -->
								<table border="0" cellspacing="0" cellpadding="0">
									<tbody>
										<tr>
											<td valign="middle" bgcolor="#007697" style="vertical-align:middle;padding: 2px 2px 2px 8px; ">
								<a 	style="font-size: 12px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none;" 
									href="https://twitter.com/intent/user?screen_name=ForumHealthInno">
										
								<!--<img height="20" width="168" alt="Follow @ForumHealthInno on Twitter" title="Follow @ForumHealthInno on Twitter" src="http://projects.iq.harvard.edu/files/styles/os_files_small/public/forum-on-healthcare-innovation/files/follow-forumhealthinno-twitter.png"/>-->
								Follow @ForumHealthInno </a>
											</td>
											<?php /*<td valign="middle" bgcolor="#007697" style="vertical-align:middle;padding: 2px 2px 2px 2px; ">
												<a 	style="font-size: 12px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none;" 
									href="https://twitter.com/intent/user?screen_name=ForumHealthInno">
												<img alt="Follow @ForumHealthInno on Twiter" height="15" width="15" src="http://www.hbs.edu/healthcare/images/photos/twitter-bird-dark-bgs.png"/>
												</a>
											</td>*/ ?>
										</tr>
									</tbody>
								</table>
								
								</td>
								<td>
									
								<p>
									
									<span style="color: #808080"><span style="font-size: 10px"><span style="font-family: Arial"><em>Copyright &copy; <?php echo date("Y"); ?> President and Fellows of Harvard College. All rights reserved.</em></span></span><span style="font-family: Arial" />
									
										<br />
									</span>

									<span style="color: #000000">
										<span style="font-size: 9px">
											<span style="font-family: Arial">
												<table width="100%">
													<tr>
														<td><a style="font-family:Arial; color: #000; font-size:9px;" target="_blank" xt="LPWEBFORMOPTIN" xtwebform="2260398" 
													href="#LPWEBFORMOPTIN" name="update_contact_info">Update Contact Information</a></td>
														<td><a style="font-family:Arial; color: #000; font-size:9px;" target="_blank" xt="LPWEBFORMOPTIN" xtwebform="2433979" 
													href="#LPWEBFORMOPTIN" name="unsubscribe_html">Unsubscribe</a></td>
														<td><a style="font-family:Arial; color: #000; font-size:9px;" target="_blank" xt="LPWEBFORMOPTIN" xtwebform="2420372" 
													href="#LPWEBFORMOPTIN" name="subscribe">Subscribe</a></td>
													</tr>
													<tr>
														<td><a style="font-family:Arial; color: #000; font-size:9px;" target="_blank" xt="LPWEBFORMF2F" xtwebform="2350828" 
													href="#LPWEBFORMF2F" name="fwdtoafriend_1">Forward This Email</a></td>
														<td><a style="font-family:Arial; color: #000; font-size:9px;" target="_blank" xt="SPCLICK" 
													href="http://www.hbs.edu/about/Pages/privacy.aspx" name="privacy_1">Privacy Policy</a></td>
														<td><a style="font-family:Arial; color: #000; font-size:9px;" target="_blank" xt="SPCLICK" 
													href="http://projects.iq.harvard.edu/forum-on-healthcare-innovation/about-us<?php echo $google_analytics_url_suffix; ?>" name="about_us_1">About</a></td>
													</tr>
												</table>
												<!--
												<a target="_blank" xt="LPWEBFORMOPTIN" xtwebform="2260398" 
													href="#LPWEBFORMOPTIN" name="update_contact_info">Update Contact Information</a>
												&nbsp;&bull;&nbsp;
												<a target="_blank" xt="LPWEBFORMOPTIN" xtwebform="2433979" 
													href="#LPWEBFORMOPTIN" name="unsubscribe_html">Unsubscribe</a>
												&nbsp;&bull;&nbsp;
												<a target="_blank" xt="LPWEBFORMOPTIN" xtwebform="2420372" 
													href="#LPWEBFORMOPTIN" name="subscribe">Subscribe</a>
												<br/>
												<a target="_blank" xt="LPWEBFORMF2F" xtwebform="2350828" 
													href="#LPWEBFORMF2F" name="fwdtoafriend_1">Forward This Email</a>
												&nbsp;&bull;&nbsp;
												<a target="_blank" xt="SPCLICK" 
													href="http://www.hbs.edu/about/Pages/privacy.aspx" name="privacy_1">Privacy Policy</a>
												&nbsp;&bull;&nbsp;
												<a target="_blank" xt="SPCLICK" 
													href="http://projects.iq.harvard.edu/forum-on-healthcare-innovation/about-us<?php echo $google_analytics_url_suffix; ?>" name="about_us_1">About</a>
												-->
											</span>
										</span>
									</span>
								</p></td>
							</tr>
						</tbody>
					</table></td>
				</tr>
			</tbody>
		</table>
	</body>
</html>
<?php /*********  BEGIN OPTIONAL MAIL INFORMATION - FILL IN ********/

$title = "";
$campaignName = "";
$campaignUrl = "";
$templateUrl = "";
$from = null;
$sig = null;
$subject = "";
$containsFirstName = false;

/**************  END OPTIONAL MAIL INFORMATION - FILL IN ***********/
/**************  BEGIN EMAIL HEADER (DON'T TOUCH) ****************/?>

<!--
# <?php echo $title ?>  
**Campaign:** <?php echo $campaignName ?>       
**Campaign Link:** <?php echo $campaignUrl ?>  
**From:** <?php echo $from; ?>
**Subject:** <?php echo $subject; ?> 
-->

<?php /********  END EMAIL HEADER (DON'T TOUCH) ********************/
/**************  BEGIN EMAIL BODY ******************/?>

# Fill in the body text here
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

<?php /*********  END EMAIL BODY ******************/?>